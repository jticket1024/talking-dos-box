TENPIN BOWLING
PRODUCED BY
PERSONAL COMPUTER SYSTEMS
WRITTEN BY
CARL MICKLA
SOUNDS BY
PHILIP VLASAK
VERSION 1.0
COPY WRITE 1996
Tenpin bowling may be played by one to eight players.
A ball is thrown down a lane with the aim of knocking down ten pins that are
positioned in a triangle at the end of the lane.
INTRODUCTION:
This bowling game was written for the blind community.  The game
follows the format of the real tenpin bowling game.
It was written with speech in mind to enable blind people to know exactly what
is going on in the game at all times.  This game also has multi media sounds. 
If you have a sound card in your computer, you will now be able to enjoy the
game even more.  If you do not have a sound card, the game is designed to play
the sounds through the P C speaker.  But the sounds sound best through the
sound card.  So give this game a try and see how you like it!  GETTING STARTED:
To start the game type TENPIN from with in the directory where the game is
located.  For example:  If the TENPIN game was installed on the C drive in a
directory call GAMES type TENPIN at the C:>GAMES prompt.  The game will begin
with the title screen.
The first thing the computer will display at the bottom of the title
screen is what mode the sound driver is in.  Then the computer will ask if you
would like to change the sound driver setting.  Type Y to change the settings. 
When you type Y, you will get the following menu:
A. = SOUND BLASTERS OR COMPATIBLE.
B. = P C SPEAKER.
C. = NO SOUND.
M. = MASTER TOUCH.
ENTER YOUR CHOICE.

If you have a SOUND BLASTER card or compatible try setting A. If not try
setting B and if you do not want any sound choose C.  
Many of today's computers come with a sound card that plays the sound in this
game the way they were designed to be heard. If you don't have a sound card,
you can still hear the sounds through the small speaker built into every
computer by choosing the B setting.
The no sound setting will still play a range of tones to enable the players to
aim the ball.

If you use the Master Touch speech program you should type M from within this
sound menu.  This is important only if your computer plays the tones too fast.

When you switch sound settings the game will test out the new setting by
playing a crowd cheering, then allow you to switch it to another setting.
When you choose B the game allows you to change the speed and volume of the
sounds.  You will get a menu where you can adjust them either manually by
typing in numbers, or by using the up and down arrow keys.  It will then save
the settings so the sounds will always play correctly in the game.

NOTE:  A musical peace will begin to play after leaving the sound choice, you
may hit the escape key at any time during the playing of the song and the music
will stop.
Then the game will ask for the number of players.  The game can be played by
one to eight people.  If only one person is playing you can still play against
the record set at your skill level.
Then you will be prompted for each players name.  The name must contain only
one word.
Then each player will have to pick a skill level for adjusting the speed
of the tones.  You can decide what skill level to choose for each player.  The
range is from one to five, with one being the fastest speed.
The speed of 5 is set to play 21 tones in approximately 30 seconds,
The speed of 1 is set to play 21 tones in approximately 1 second.
Once the last player picks their skill rating you hit the space bar or any
other key and the game will begin.
You will first get a list of Lane Leaders.
The game remembers the highest score at each of the five skill levels and the
name of the person that set that record.  If you get a higher score, your name
will be recorded at that level.
Below is an example of a leaders list:
THE LANE LEADERS.
SKILL 1 SCORE 119 NAME PHIL.
SKILL 2 SCORE 144 NAME CARL.
SKILL 3 SCORE 169 NAME PHIL.
SKILL 4 SCORE 177 NAME CARL.
SKILL 5 SCORE 300 NAME PHIL.

After all the players have had a turn, you will get a list of the players in
the game and their scores, with the leader first.
You can then hit Q if you want to Quit and end the game early.

DESCRIPTION LEVELS:
To change the level of description at any point type D.
You will toggle through three levels of description, full, intermediate, and
expert with expert having the least description.

THE PINS:
The pins are numbered from one to ten.
The one or head pin forms the point of the triangle.
Behind it are the two and three pins in a row with the two on the left of the
three pin.
Behind the two and three pins are the four, five and six pins in a row from
left to right.
The last row of pins are the seven, eight, nine and ten from left to right.

ELECTRONIC AIMING:
The game calculates the point where you should hit the triangle of pins and
converts that point to a musical tone which is called the Target tone.
The game then plays that target tone three times so you can later identify it.

The game then plays a total of 21 tones, then plays them again in reverse order
for a full cycle.  The lowest two tones represent the left gutter, and the
highest two tones represent the right gutter.  The center 17 tones represent
the pins.
Try to imagine a piano keyboard set in a bowling alley, you start at the left
gutter and as you walk across the alley to the right gutter, you are stepping
on the keys.  You can stop and throw the ball at any time by hitting the enter
key or space bar.

For example the twelfth tone is the point on the right side of the one
pin, which is the target for the first ball in a frame.
You can hit the one pin by hitting the enter key or space bar when you hear the
twelfth target tone played within the tone cycle.

Any exposed pins will have five tones to aim for.  The first tone symbolizes
the ball slightly hitting the left side used to skim the pin and push it to the
right.  The second tone is on the left side and will knock the pin right at a
45 degree angle back.  The third tone is at the center point of the pin and is
used to whack the pin straight back.  This is the easiest point to hit on the
pin because you have two tones on either side to miss by.  The fourth tone is
on the right side and will knock the pin left at a 45 degree angle back.  The
fifth tone represents the ball slightly hitting the right side used to skim the
pin and push it to the left.


FOULING IN THE GAME:
The game will play three cycles of tones before calling a foul.  If a foul is
called, you loose your chance to roll the ball. If a foul occurs on the first
ball of a frame, you skip to the second ball.  If a foul occurs on the second
ball of a frame, you only get the points scored on your first ball, and the
game goes to the next player.


OFFICIAL TENPIN BOWLING RULES AND EQUIPMENT:
EQUIPMENT:
The ball weighs less than 16 lbs. and measures 8 and 1/2 inches in diameter,
made of a hard rubber composition or plastic.
The ten pins are 15 inches high by 5 inches in diameter made of maple wood, and
weigh 3 lb 2 oz.  and stand on spots marked within a 3 foot triangle.
The lane's surface is wood, most often pine or maple and is 60 feet long and 3
feet 6 inches wide.
Gutters run on either side to catch badly aimed balls.
There are fifteen feet of approach space before a foul line where the bowler
uses a four-step delivery.  The ball must be delivered underarm, so that it
runs along the surface of the lane.

RULES OF PLAY:
Players take their turn to bowl.  A turn is completed when the player has
bowled a frame.
A game consists of ten frames.  Every player bowls twice in each frame, unless
the player knocks down all ten pins with the first ball which is called a
strike.
The winning player has the highest score at the end of ten frames.  One point
is scored for every pin knocked over, and a bonus is given for a strike or a
spare.
A strike is scored when a player knocks down all ten pins with the first ball
of a frame.  A strike scores ten points, plus the score from the next two balls
bowled.
If a player scores a strike in their final frame, they are allowed an extra two
balls to complete their bonus.
If a strike is achieved in every frame and with both bonus balls, the maximum
score of 300 is achieved.
A spare is scored when a player knocks down all ten pins with both balls in a
frame. (This includes knocking down all ten pins with the second ball of a
frame.)
A spare scores ten points, plus the score from the next ball bowled.
If a player scores a spare in their final frame, they are allowed one extra
ball to complete their bonus.

FOULS:
The player must not touch or cross the foul line, even after having sent the
ball down the lane, or the automatic foul-detecting device will signal an
illegal ball.
Any pins knocked down by that ball do not score.  If the first ball in a frame
is a foul, all the pins must be reset.  Should they then all be knocked down by
a legal second ball, a spare and not a strike is scored.
If the foul ball is the second in a frame, only those pins knocked down with
the first ball are counted.

OTHER GAMES BY PCS:
ANY NIGHT FOOTBALL.  This is a text based football game.  Which is simple to
play, and the teams are historically reflected in the game.
ANY NIGHT FOOTBALL sells for $30.00
MONOPOLY. A very speech friendly Monopoly game with multi media sounds.
MONOPOLY sells for $30.00
MOBIUS MOUNTAIN.  A very speech friendly math adventure game with real sounds. 
It was written with speech in mind to enable blind children to know exactly
what is going on in the game at all times.
MOBIUS sells for $20.00
SOUND CARD.  16 bit sound card current price $59.00 but call prices change
frequently.
COMPUTER PARTS AND MACHINES call for prices.

If you have any suggestions, comments, ideas or problems feel free to contact
me at the address below.

The market base for programs to supply the visually impaired and blind
community is small.  If programmers are going to write fun and educational
programs then your support is very important.  
Giving or receiving unpaid for programs and using them only makes it very hard
for people to produce products for this small base of people.  For example, a
very good leisure game has been on the market for several years, and in all
that time only 250 to 300 games have been sold.  The game is very good and
either there are many unpaid for games out there, or not many people like it. 
Programmers are not looking for any hand outs. Just pay them for what is do
them.  Thank you for your time and support and if you would like to give me
some feedback on this or any other
topic feil free to contact me.

Carl Mickla
551 Compton ave.
Perth Amboy N.J.  08861
Phone (9 0 8) 8 2 6 - 1 9 1 7
