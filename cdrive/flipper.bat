REM    this batch file was written by
REM    Flipper's installation program.
REM
path C:\FLIP50;%path%

cd \FLIP50
beep 256 700 512 700 1024 700
SBTALKER /dBLASTER
F_Blast A�
echo off
flipper 
if errorlevel 1 goto noflip
IF EXIST start.flp goto goodflp
FLIP TALK
ECHO You need to run MAKE_FLP to build your configurations
goto exit
:goodflp
flipload start.flp start.flp
REM            You can change the second configuration
REM            to be some other file, if you want.
flipext start.flp
REM            You can add the filenames of 
REM            additional configurations,
REM            or delete this one to save memory.
REM            (Approximately 3 kilobytes per external 
REM            configuration.)
REM        the next line should read "FLIP AUTOLOAD ON" 
REM                          or "FLIP AUTOLOAD OFF" 
REM
FLIP AUTOLOAD OFF
goto exit
:noflip
beep 256 1000
:exit
cd \
