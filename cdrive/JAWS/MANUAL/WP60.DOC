WP60.DOC
June 1, 1994

Notes on JAWS 2.31 and WordPerfect 6.0 for DOS
Configurations version 1.3.  Requires JAWS version 2.31

** Please read this entire file before using these configurations
**


Numpad + key: Turns on PC cursor, but when pressed twice within one
second it turns on the "WP cursor".  The WP Cursor will be turned
on
automatically when a dialog box appears or the menus are activated. 
The
current item is also spoken automatically.  When the dialog box or
menu
disappears the WP Cursor is turned off and the PC cursor is turned
on, all
automatically.  When the dialog box or menu disappears, the status
line at the
bottom of the screen is spoken.  Turning on the PC cursor (Numpad+)
or
the JAWS cursor (Numpad-) will turn off the WP cursor.


When the WP Cursor is on, the arrow keys (left, right, up, & down)
will
track and speak the current item in the WP menus or the various
dialog
boxes.  Numpad 5 will speak the current item without moving. 
Alt+arrow
keys, and the alt+Numpad5, will also track and speak the current
item,
whether the WP Cursor is on or off.

Insert+T will read the Title of the dialog box or menu.

Insert+D will read the contents of the dialog box.

Insert+S reads the "Specific" item, the current item in the dialog
box or
menus.  this is the same as alt+Numpad5, or Numpad5 when the WP
cursor
is on.

Insert+F "finds" and reads the current item that has a right-arrow
pointing to
it, like the options or choices in the spell checker.  If there is
no arrow
pointing to an item then it reads the current item same as insert+S
or
Numpad5.

Insert+tab reads the "pointed to" current item, same as insert+f. 
If there is
no arrow then it reads the current item like insert+S or Numpad5.

Tab and Back-Tab (shifted tab) will move to the next or previous
choice in
the menu or dialog box and speak it, using the logic described in
Insert+F or
Insert+Tab.  It looks for the arrow first, if there is no arrow
then it lets
Insert+S speak the current item.

Insert+X will read all the "bracketed" items.  If an "X" appears
between the
brackets then the item is turned on or selected.  For example, the
"[X] Case
sensitive search" in the Search dialog box.  If there is no "X"
then the item is
not selected.

Insert+E reads greyed-out or shaded items in the dialog box.  These
are items
that are not "available" or not selectable.

Insert+Q reads the text that is "blocked".



Spell Checker:

Control+F2 starts the spell checker.  The "Speller" dialog box
appears, the
current item is read.  Use the up/down arrow keys to read through
the
choices.  Press Enter to select.

When the misspelled word is displayed on the screen, JAWS speaks
it, spells
it, and  reads the current choice from the menu (such as "skip
once" or
"replace word").  Then it will read the first alternative word if
any is
displayed at that time.  Use Control+pad- to read the misspelled
word again
and place the JAWS cursor on it.  Then you can read it in context
(insert+up
arrow reads the line, shift+Numpad5 reads the sentence).  Tab,
shift+tab, and
insert+tab read through the options when the Wp cursor is on (press
Numpad+ twice within one second).  Up/down arrow and Numpad5 (or
alt+up/down arrow and alt+Numpad5) read through the alternative
word
choices, if any.  

It may take a bit of time for all the alternative words to appear
on the screen. 
WordPerfect 6.0 seems to put them up on the screen as  it thinks of
them. 
This process can take a long time for certain  words.  You will
have the most
obvious alternatives on the screen  the quickest, and the rest will
follow one
at a time.  When there are more than ten or eleven alternative
words they
will not all be visible.  As you use the down-arrow to read through
the list,
the screen will scroll more into view.

Control+Numpad+ will read all the alternative word choices
currently
displayed, and activate a frame around them so you can read them in
detail
with the JAWS cursor.  Remember that some may not be visible or
readable
until the list is scrolled.  Pressing Control+Numpad+ again will
de-activate
the frame, as will pressing almost any other key.  This same key,
Control+Numpad+, will also read the choices in the File Manager
screen and
the Help screens.

Insert+D will read all the choices in the "Word not found" dialog
box. 
Insert+F (Find Arrow) or Insert+Tab will read the current choice in
this
box.  Tab or Shift-tab will move to the next or previous choice.
 
Pressing enter on the desired choice will cause WP to perform the
task and
move on to the next misspelled word.  If you select "edit" then the
PC cursor
is placed at the misspelled word, turn on the PC cursor to edit
accurately. 
Control+pad- will turn on the PC cursor and speak the word.  If you
select
"replace word" the alternative word that is highlighted (read by
Numpad5
with WP cursor on, or alt+Numpad5, or insert+S) will be substituted
for the
misspelled word.  At any time you can press Control+Numpad- to read
and
spell the misspelled word.


File Manager:

function Key 5 invokes the File Manager.  The prompt for
subdirectory is
spoken, use the arrow keys or tab to read through the choices. 
Enter selects
the subdirectory and displays the file manager screen.  The
highlighted file
name is spoken.

Arrow keys read through the file names, and Numpad5 or alt+Numpad5
will
read the highlighted file name without moving.  This "reading" also
positions
the JAWS cursor at the start of the highlighted data.  Then to read
the file
information in detail, like the spelling, file size or date and
time, turn on the
JAWS cursor (Numpad-).  Use the left/right arrows (read by
character) or
Insert+left/right arrows (read by words).  Use the alt+arrow keys
to move to
the next or previous file name, or turn on the WP cursor and use
the arrow
keys by themselves.

Tab reads through some of the choices in the "menu". 
Control+Numpad+
reads all the menu choices and activates a frame, then you can look
at them in
detail with the JAWS cursor.  Your selection can e made by entering
the
number or letter of the item you want.  Insert+F or Insert+Tab
reads the
choice being pointed to with the little arrow, which is the current
menu
choice (independent from the current file name).


Installation hints:

If you are going to be using WordPerfect 5.x and WordPerfect 6.0
the
preferred method of doing so would be to use a batch file to load 
WP 6 and
our configurations.  A sample batch file is included as WP60.BAT. 
The
WP60 config files will be installed on your hard drive whenever you
select
any of the other WP versions, like WP51, in the installation
procedure.  The
WP51 (or WP50 or WP42) will be named "WP.J?F" and will be loaded
automatically, while the WP60 files will be WP60.J?F and will be
loaded by
the WP60.BAT file.

If you are going to be using WordPerfect 6.0 as your primary
version, and
select it during the installation procedure, the  JAWS
configuration files will
be named "WP.J?F" and will be loaded automatically when you run WP. 
The
WP51 config files will also be loaded on your hard drive, just in
case you
want to use them.  The will be named "WP51.J?F".

The WP60 macros require a larger macro buffer than JAWS defaults 
to, so
the new installation program adds a "/B8" parameter to the command
line. 
This expands the macro buffer.  If you do not let the installation
program
update your AUTOEXEC.BAT file then you may need to add this
parameter
manually.  To do this you will need to edit your JAWS.BAT file to
add
"/B8" to your command line, or add it when you run JAWS.  If your
macro
buffer is not big enough you will get an error message that the
"macros are
too large for buffer"  when you try to use these WordPerfect
macros.

WordPerfect 6.0 requires the same /ND that was necessary for WP 5.1 
dated
3/9/92.  When you run WP 6.0 either use the command line  parameter
/ND
each time you load, or add the following line to  your
AUTOEXEC.BAT: 
  SET WP=/ND
This line tells WordPerfect to use the /ND each time it loads WP
6.0, the
/ND parameter stands for No Disable Keyboard, and is used to
prevent a
lockup problem with the newer versions of WordPerfect when used
with
JAWS and products that make extensive use of the keyboard.  If you
still
encounter lockup problems please replace the /ND switch with /NK,
this is a
more aggressive version of the /ND switch.


The Function Keys were labeled with Help Labels according to 
information
obtained from the WordPerfect documentation.  If you find any of
these keys
that are labeled incorrectly, or find some problems  with these
macros, or
have other comments about this set of macros please leave us an
electronic
message via one of the following  E Mail addresses:

Internet: JAWS@mechanic.fidonet.org
Compuserve: 
Fidonet: 1:3603/5297
BBS: (813) 528-8903
FAX: (813) 528-8901

If you are unable to leave electronic mail, please leave us a voice 
message at
813-528-8900, ask for extension 18.

Thank you for your interest in Henter-Joyce products.

Jeff M. Belina

Henter-Joyce, Inc.

