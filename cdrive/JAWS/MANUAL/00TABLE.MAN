INFORMATION
ABOUT THIS MANUAL
 
 
 
CHANGES AND UPDATES
 
    The names of the computer files on the disk version of the
manual
begin with a reference number.  The reference numbers indicate the
PART number and Chapter number for the file.  For example, the
chapter "JAWS INSTALLATION" is contained in the file named
"12INSTAL.MAN".  The "1" is a reference to PART ONE of the manual
and the "2" indicates the Chapter number within that part of the
manual.  If we had supplemental information for the chapter
"12INSTAL.MAN", then it would be contained on a file named
"12INSTAL.NEW".  The files with the extension "NEW" only contain
supplemental or new information.
 
    An important documentation file named "CHANGES.DOC"
contains a summary of program improvements and supplemental
information that is updated each time changes are made to JAWS. 
The
"CHANGES.DOC" file is located in the default subdirectory where
your
JAWS program files are stored (usually "C:\JAWS").
 
SYSTEM OF PAGE NUMBERING
 
    The bottom of each printed page contains a footer that displays
the part number of the manual, the chapter number, the chapter
name,
and the page number.  A typical page number would appear as
"1-4.36". 
The "1" indicates PART ONE of the manual.  The "4" indicates
Chapter
4 in PART ONE.  The "36" is the actual page number.  Pages are
numbered consecutively throughout the manual.
 
 
TABLE OF CONTENTS
 
 
 

** PART ONE:
BEGINNING
 
PART ONE:  Chapter 1
THE POWER OF JAWS
(Also contained in the disk file "11POWER.MAN".)
    1.   POWER AND COMMITMENT . . . . . . . . .1-1.1
    2.   SIMPLE DESIGN. . . . . . . . . . . . .1-1.1
    3.   UNLIMITED ACCESS AND FLEXIBILITY . . .1-1.2
    4.   TWO SEPARATE CURSORS ARE THE KEY . . .1-1.2
    5.   CONCENTRATED CONTROL OF READING. . . .1-1.3
    6.   SINGLE COMMANDS FOR MANY USES. . . . .1-1.3
    7.   MACROS MAKE EVERYTHING EASY. . . . . .1-1.4
    8.   AUTOMATIC MACROS RESPOND TO SCREEN
         CHANGES. . . . . . . . . . . . . . . .1-1.4
    9.   SOFT KEYS ARE NEVER IN CONFLICT. . . .1-1.5
    10.  EXPANSION OF KEYBOARD UTILIZATION. . .1-1.5
    11.  POPULAR MENU SYSTEM DESIGN . . . . . .1-1.6
    12.  UNIVERSAL SEARCH AND FIND. . . . . . .1-1.7
    13.  SMARTER THAN THE AVERAGE SCREEN
         READER . . . . . . . . . . . . . . . .1-1.8
    14.  SAY IT ALL AND STOP. . . . . . . . . .1-1.8
    15.  AUTOMATIC LOADING OF SPECIAL
         CONFIGURATIONS . . . . . . . . . . . .1-1.9
    16.  STRONG COMPUTER COMPATIBILITY. . . . .1-1.9
    17.  COMPREHENSIVE SPEECH SYNTHESIZER
         COMPATIBILITY. . . . . . . . . . . . 1-1.10
    18.  GRAPHIC CHARACTERS AND GRAPHICS
         DISPLAYS . . . . . . . . . . . . . . 1-1.10
    19.  THE NEXT STEP IS INSTALLATION. . . . 1-1.11
 
PART ONE:  Chapter 2
INSTALLATION
(Also contained in the disk file "12INSTAL.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . . 1-2.13
    2.   QUICK INSTALLATION DESCRIPTION . . . 1-2.13
    3.   LOADING AND USING JAWS . . . . . . . 1-2.14
         3.1  Command to Load JAWS. . . . . . 1-2.15
         3.2  Installation Problems . . . . . 1-2.15


    6.   INFORMATION ABOUT THIS MANUAL. . . . 1-2.17
 
PART ONE:  Chapter 3
QUICK START
(Also contained in the disk file "13QUICK.MAN".)
    1.   BARE BONES FACTS FOR GETTING STARTED 1-3.19
    2.   QUICK ADJUSTMENT OF SPEECH RATE. . . 1-3.19
    3.   QUICK ADJUSTMENT OF VOLUME . . . . . 1-3.20
    4.   SAVING SPEECH RATE AND VOLUME
         SETTINGS . . . . . . . . . . . . . . 1-3.21
    5.   SILENCING SPEECH . . . . . . . . . . 1-3.21
    6.   SPECIAL USE OF THE "INSERT" KEY. . . 1-3.22
    7.   EXPLORING THE KEYBOARD WITH HELP
         MODE . . . . . . . . . . . . . . . . 1-3.23
    8.   BASIC READING FUNCTIONS OF THE
         SPEECH PAD . . . . . . . . . . . . . 1-3.23
    9.   SCREEN READING TUTORIAL. . . . . . . 1-3.25
    10.  BYPASSING THE JAWS SCREEN READING
         KEYS . . . . . . . . . . . . . . . . 1-3.27
    11.  MASTERING JAWS . . . . . . . . . . . 1-3.27
 
PART ONE:  Chapter 4
LEARNING STRATEGIES
(Also contained in the disk file "14LEARN.MAN".)
    1.   IN THE BEGINNING . . . . . . . . . . 1-4.29
    2.   SHORT TERM LEARNING STRATEGIES . . . 1-4.30
         2.1  What Should You Learn First?. . 1-4.30
         2.2  How Should You Learn? . . . . . 1-4.32
         2.3  Where Do You Go When You Need Help?1-4.33
    3.   LONG TERM LEARNING STRATEGIES. . . . 1-4.34
         3.1  Which Software Programs Should I Use?1-4.34
         3.2  How Do I Learn To Use Software Programs?1-4.35
         3.3  How Do I Become A Power User? . 1-4.36
    4.   THE NEXT STOP. . . . . . . . . . . . 1-4.37
 

** PART TWO:
USE
 
PART TWO:  Chapter 1
CURSOR MOVEMENT
AND SCREEN READING
(Also contained in the disk file "21CURSOR.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . . 2-1.39
    2.   CREATING THE SPEECH PAD. . . . . . . 2-1.39
         2.1  NUM LOCK Key. . . . . . . . . . 2-1.39
         2.2  Standard JAWS Configuration . . 2-1.40
    3.   LOCATION OF JAWS SCREEN READING KEYS 2-1.40
         3.1  Speech Pad Keys . . . . . . . . 2-1.40
         3.2  Number Row Keys . . . . . . . . 2-1.41
         3.3  Alphabet Keys . . . . . . . . . 2-1.41
         3.4  The Shift Function of the INSERT Key2-1.42
         3.5  Keyboards Without Numeric Keypads2-1.42
    4.   RELATIONSHIP BETWEEN CURSORS . . . . 2-1.42
    5.   SPEECH PAD FUNCTIONS . . . . . . . . 2-1.44
         5.1  Say Character:  Prior, Next, Current2-1.44
         5.2  Say Word:  Prior, Next, Current 2-1.45
         5.3  Say Line:  Prior, Next, Current 2-1.45
         5.4  Say Up To Cursor and Say From Cursor2-1.46
         5.5  Go To:  Beginning, End, Top, Bottom2-1.47
         5.6  Say Top Line and Say Bottom Line2-1.48
         5.7  Say Sentence:  Prior, Next, Current2-1.48
         5.8  Say Paragraph:  Prior, Next, Current2-1.49
         5.9  Reading Screens and Documents . 2-1.50
         5.10 Determining Cursor Position . . 2-1.52
         5.11 Joining Cursors Together. . . . 2-1.54
         5.12 Lost Text on the Screen Display 2-1.55
    6.   USE OF CONTROL AND ALT . . . . . . . 2-1.56
         6.1  Speech in Progress. . . . . . . 2-1.56
         6.2  Speaking Not in Progress. . . . 2-1.57
    7.   PHONETIC PRONUNCIATION AND SPELLING. 2-1.57
    8.   NUMBER ROW FUNCTIONS . . . . . . . . 2-1.59
         8.1  Help Mode . . . . . . . . . . . 2-1.59
         8.2  Pass Key to Application . . . . 2-1.59
         8.3  Say Screen Enhancement. . . . . 2-1.60
         8.4  Say Field Mode. . . . . . . . . 2-1.60
         8.5  Singles Mode. . . . . . . . . . 2-1.61
    9.   READING SPECIFIC LINES . . . . . . . 2-1.62
    10.  MARGIN BELL. . . . . . . . . . . . . 2-1.63
    11.  EXTENDED 101 KEYBOARDS.. . . . . . . 2-1.63
    12.  HEARING KEY STROKES, WORDS, AND
         CAPITALIZATION . . . . . . . . . . . 2-1.64
    13.  SUMMARY OF JAWS KEYS . . . . . . . . 2-1.65
         13.1 Speech Pad Keys . . . . . . . . 2-1.65
         13.2 Numbers Row . . . . . . . . . . 2-1.68
         13.3 Original Numeric Keypad Functions2-1.68
         13.4 Extended Arrow Keys . . . . . . 2-1.69
         13.5 Smart Screen Key Combinations . 2-1.69
         13.6 Other Keys. . . . . . . . . . . 2-1.70
    14.  MODIFYING AND EXPANDING THE POWER
         OF JAWS. . . . . . . . . . . . . . . 2-1.70
 
PART TWO:  Chapter 2
JAWS MENU SYSTEM
(Also contained in the disk file "22MENU.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . . 2-2.73
    2.   ACTIVATING THE JAWS MENU SYSTEM. . . 2-2.73
    3.   MENU SYSTEM DISPLAY. . . . . . . . . 2-2.74
    4.   MOVING THE MENU BAR AND MAKING
         SELECTIONS . . . . . . . . . . . . . 2-2.75
    5.   MAIN MENU SELECTIONS . . . . . . . . 2-2.78
         5.1  Voice . . . . . . . . . . . . . 2-2.78
         5.2  Config Files. . . . . . . . . . 2-2.79
         5.3  Frames. . . . . . . . . . . . . 2-2.79
         5.4  Macro . . . . . . . . . . . . . 2-2.79
         5.5  Enhance . . . . . . . . . . . . 2-2.80
         5.6  Search. . . . . . . . . . . . . 2-2.80
         5.7  Goto. . . . . . . . . . . . . . 2-2.81
         5.8  Place Markers . . . . . . . . . 2-2.81
    6.   THE NEXT STEP. . . . . . . . . . . . 2-2.81
 
PART TWO:  Chapter 3
VOICE CONTROL
(Also contained in the disk file "23VOICE.MAN".)
    1.   INTRODUCTION TO THE VOICE MENU . . . 2-3.83
    2.   GLOBAL . . . . . . . . . . . . . . . 2-3.86
         2.1  Volume. . . . . . . . . . . . . 2-3.87
         2.2  Rate. . . . . . . . . . . . . . 2-3.87
         2.3  Pitch . . . . . . . . . . . . . 2-3.87
         2.4  Tone. . . . . . . . . . . . . . 2-3.88
         2.5  Standby . . . . . . . . . . . . 2-3.89
    3.   KEYBOARD . . . . . . . . . . . . . . 2-3.89
         3.1  Echo. . . . . . . . . . . . . . 2-3.89
         3.2  Volume, Rate, Pitch, Tone . . . 2-3.90
         3.3  Interruptable . . . . . . . . . 2-3.90
         3.4  Key Repeat. . . . . . . . . . . 2-3.91
    4.   SCREEN . . . . . . . . . . . . . . . 2-3.91
         4.1  Echo. . . . . . . . . . . . . . 2-3.92
         4.2  Volume, Rate, Pitch, Tone . . . 2-3.92
    5.   SPEECH PAD . . . . . . . . . . . . . 2-3.92
         5.1  Volume, Rate, Pitch, Tone . . . 2-3.92
         5.2  Interruptable . . . . . . . . . 2-3.93
    6.   JAWS CURSOR. . . . . . . . . . . . . 2-3.93
    7.   UNIVERSAL. . . . . . . . . . . . . . 2-3.94
         7.1  Numbers . . . . . . . . . . . . 2-3.94
         7.2  Symbols . . . . . . . . . . . . 2-3.95
         7.3  Line Pause. . . . . . . . . . . 2-3.97
         7.4  Case. . . . . . . . . . . . . . 2-3.97
         7.5  Filter. . . . . . . . . . . . . 2-3.98
         7.6  Acronyms. . . . . . . . . . . . 2-3.99
         7.7  Dictionary. . . . . . . . . . .2-3.100
    8.   DICTIONARY EDITOR. . . . . . . . . .2-3.100
         8.1  Add a Pronunciation Rule. . . .2-3.101
         8.2  Change a Pronunciation Rule . .2-3.104
         8.3  Delete a Pronunciation Rule . .2-3.105
         8.4  Bytes Available for Rules . . .2-3.105
    9.   EXTRA. . . . . . . . . . . . . . . .2-3.106
    10.  SAVING VOICE AND DICTIONARY
         CONFIGURATIONS . . . . . . . . . . .2-3.106
 
PART TWO:  Chapter 4
FINDING SCREEN LOCATIONS,
TEXT, AND  COLORS
(Also contained in the disk file "24FIND.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . .2-4.107
    2.   GOTO . . . . . . . . . . . . . . . .2-4.107
         2.1  Absolute Screen Location. . . .2-4.108
         2.2  Relative Screen Location. . . .2-4.109
    3.   PLACE MARKERS. . . . . . . . . . . .2-4.111
    4.   STRING SEARCH. . . . . . . . . . . .2-4.112
         4.1  Entering a Search String. . . .2-4.113
         4.2  Entering Search Options . . . .2-4.114
         4.3  Case Sensitive Searches . . . .2-4.117
         4.4  Verify First Letter of Word . .2-4.117
    5.   ENHANCEMENTS AND COLORS. . . . . . .2-4.118
         5.1  Analyzing the Screen Display. .2-4.119
         5.2  Enhancements Menu . . . . . . .2-4.121
         5.3  Different . . . . . . . . . . .2-4.122
         5.4  Find. . . . . . . . . . . . . .2-4.122
         5.5  Repeat. . . . . . . . . . . . .2-4.127
         5.6  Testing for Screen Attributes .2-4.128
    6.   COLOR BAR TRACKING . . . . . . . . .2-4.129
    7.   SUMMARY OF STRING AND ENHANCEMENT
         SEARCHES . . . . . . . . . . . . . .2-4.130
 
PART TWO:  Chapter 5
FRAME MANAGER
(Also contained in the disk file "25FRAMES.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . .2-5.133
    2.   FRAMES MENU. . . . . . . . . . . . .2-5.135
         2.1  Revise. . . . . . . . . . . . .2-5.135
         2.2  Speak . . . . . . . . . . . . .2-5.138
         2.3  Activate. . . . . . . . . . . .2-5.139
         2.4  Quiet . . . . . . . . . . . . .2-5.140
         2.5  Monitor . . . . . . . . . . . .2-5.141
    3.   STRATEGY FOR QUIETING CHATTER. . . .2-5.142
    4.   LARGE SCREEN DISPLAYS. . . . . . . .2-5.143
    5.   PRIORITY OF FRAME DEFINITIONS. . . .2-5.144
    6.   SAVING FRAME DEFINITIONS . . . . . .2-5.145
 
PART TWO:  Chapter 6
SMART SCREEN MANAGER
(Also contained in the disk file "26SMART.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . .2-6.147
    2.   AUTOMATIC READING STRATEGIES . . . .2-6.148
    3.   SMART SCREEN MENU. . . . . . . . . .2-6.149
    4.   ACTIVATING SMART SCREEN. . . . . . .2-6.150
    5.   SMART FOCUS. . . . . . . . . . . . .2-6.151
    6.   CONTROLLING SMART SPEAKING . . . . .2-6.153
         6.1  Smart Level . . . . . . . . . .2-6.154
         6.2  Color Bar . . . . . . . . . . .2-6.156
         6.3  Smart Options . . . . . . . . .2-6.157
    7.   SUMMARY OF SMART SCREEN KEYS . . . .2-6.162
    8.   ABOUT THE SMART SPEAK MACRO. . . . .2-6.162
 
PART TWO:  Chapter 7
MACRO MANAGER
(Also contained in the disk file "27MACRO1.MAN".
Also read disk Appendix 3, "33MACRO2.MAN".)
    1.   INTRODUCTION TO MACROS . . . . . . .2-7.165
    2.   MACRO MANAGER. . . . . . . . . . . .2-7.169
    3.   MACROS MENU. . . . . . . . . . . . .2-7.171
         3.1  Revise. . . . . . . . . . . . .2-7.171
         3.2  Duplicate . . . . . . . . . . .2-7.173
         3.3  Erase . . . . . . . . . . . . .2-7.174
         3.4  Identify. . . . . . . . . . . .2-7.174
         3.5  Auto Macros and Frame Monitoring2-7.175
    4.   USING THE MACRO EDITOR . . . . . . .2-7.177
         4.1  Macro Revise Window . . . . . .2-7.178
         4.2  JAWS Function Window. . . . . .2-7.180
         4.3  Macro Identify Window . . . . .2-7.183
    5.   SAMPLE MACROS. . . . . . . . . . . .2-7.184
         5.1  Say Current Line Macro. . . . .2-7.184
         5.2  Say Position of Cursor Macro. .2-7.186
         5.3  Voice Level Adjustment Macro. .2-7.187
         5.4  WordPerfect Spell Check Macro .2-7.188
    6.   BIOS MACROS. . . . . . . . . . . . .2-7.190
    7.   STRATEGIES FOR DEVELOPING MACROS . .2-7.192
    8.   MACROS THAT DO NOT WORK. . . . . . .2-7.193
         8.1  Macro Cannot Find Color . . . .2-7.194
         8.2  Macro Cannot Find Graphic Character2-7.194
    9.   MACRO FUNCTION SUMMARY . . . . . . .2-7.195
 
PART TWO:  Chapter 8
CONFIGURATION MANAGER
(Also contained in the disk file "28CONFIG.MAN".)
    1.   INTRODUCTION . . . . . . . . . . . .2-8.203
    2.   CONFIGURATION MANAGER. . . . . . . .2-8.205
         2.1  Load. . . . . . . . . . . . . .2-8.206
         2.2  Save. . . . . . . . . . . . . .2-8.207
         2.3  Autoload. . . . . . . . . . . .2-8.208
    3.   COMMAND LINE LOADING OF
         CONFIGURATION FILES. . . . . . . . .2-8.211
 

** PART THREE:
APPENDICES
 
PART THREE:  Appendix 1
SPECIALIZED EXPRESSIONS
(Also contained in disk file "31EXPRES.MAN".)
    1.   DOCUMENTATION CONVENTIONS. . . . . .3-1.213
         1.1  Typographical Conventions . . .3-1.213
         1.2  Keyboard Conventions. . . . . .3-1.214
    2.   PHONETIC EQUIVALENTS FOR LETTERS . .3-1.215
    3.   GLOSSARY . . . . . . . . . . . . . .3-1.216
 
PART THREE:  Appendix 2
SUPPLEMENTAL INFORMATION
FOR SOLVING PROBLEMS
(Also contained in the disk file "32SUPPLE.MAN".)
    1.   PROBLEM SOLVING STARTS HERE. . . . .3-2.227
         1.1  Problems with Software Installation3-2.227
         1.2  Problems with Loading JAWS Software3-2.227
         1.3  Problems Installing an Internal Speech
              Synthesizer . . . . . . . . . .3-2.227
         1.4  Problems Installing an External Speech
              Synthesizer . . . . . . . . . .3-2.228
         1.5  Problems with Toshiba Laptop PC's3-2.228
         1.6  Problems with Application Software
              Compatibility . . . . . . . . .3-2.228
         1.7  Problems with Synthesizer Behavior3-2.229
    2.   PREPARING TO INSTALL JAWS. . . . . .3-2.229
         2.1  Hardware Installation . . . . .3-2.229
         2.2  Protecting the Hardware . . . .3-2.230
         2.3  Starting the PC . . . . . . . .3-2.230
    3.   DETAILED DESCRIPTION OF JAWS
         INSTALLATION . . . . . . . . . . . .3-2.230
    4.   JAWS WILL NOT SPEAK. . . . . . . . .3-2.235
         4.1  JAWS Logo and Copyright Screen Do Not
              Appear. . . . . . . . . . . . .3-2.236
         4.2  JAWS Logo Appears, but the Synthesizer
              Does Not Speak. . . . . . . . .3-2.237
         4.3  JAWS Logo Appears and the Computer
              Locks-Up. . . . . . . . . . . .3-2.238
         4.4  Nothing Seems to Work!. . . . .3-2.238
    5.   PROBLEMS WITH USING DIFFERENT PC's .3-2.239
    6.   PROBLEM DETERMINING SCREEN COLOR . .3-2.240
    7.   PROBLEM WITH SOME TEXT NOT BEING
         SPOKEN . . . . . . . . . . . . . . .3-2.242
 
PART THREE:  Appendix 3
MACRO FUNCTIONS
(Only contained in the disk file "33MACRO2.MAN".
Also read Chapter 7 in PART 2, disk file "27MACRO1.MAN".)
 
PART THREE:  Appendix 4
COMMAND LINE SWITCHES
AND PROGRAM FILES
(Only contained in the disk file "34COMAND.MAN".)
 

** PART FOUR:
APPLICATIONS
 
PART FOUR:  Application 1
WORDPERFECT
(Only contained in the disk file "41WP.MAN".)
 
PART FOUR:  Application 2
LOTUS 1-2-3
(Only contained in the disk file "42LOTUS.MAN".)
 

** PART FIVE:
INSTALLATION REFERENCES
 
PART FIVE:
Installation Reference 1
INTERNAL SYNTHESIZERS
(Only contained in the disk file "51INTERN.MAN".)
 
PART FIVE:
Installation Reference 2
EXTERNAL SYNTHESIZERS
AND HARDWARE CONFLICTS
(Only contained in the disk file "52EXTERN.MAN".)
 
PART FIVE:
Installation Reference 3
SPEECH SYNTHESIZERS
AND RELATED EQUIPMENT
(Only contained in the disk file "53SYNTH.MAN.)
 
PART FIVE:
Installation Reference 4
TOSHIBA COMPUTERS
(Only contained in the disk file "54TOSH.MAN".)
 
PART FIVE:
Installation Reference 5
LOADING JAWS
INTO HIGH MEMORY
(Only contained in the disk file "55HIGH.MAN".)
 

** PART SIX:
SOFTWARE UPDATES
 
PART SIX:  Update 1
ABOUT JAWS VERSION 2.0
(Only contained in the disk file "61JAWS20.MAN".)
 
PART SIX:  Update 2
ABOUT JAWS VERSION 2.2
(Only contained in the disk file "62JAWS22.MAN".)
 
PART SIX:  Update 3
ABOUT JAWS VERSION 2.3
(Only contained in the disk file "63JAWS23.MAN".)
 

** PART SEVEN:
INDEX 
(Also contained in the disk file "71INDEX.MAN".)
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
. . . . . . . . . . . . . . .7-1.245
 

 
