PART FIVE:
Installation Reference 5
LOADING JAWS
INTO HIGH MEMORY
 (Only contained in the disk file "55HIGH.MAN".)
 
 
(Last revision:  1992.)
 
 
 
CONTENTS OF INSTALLATION REFERENCE 5
 
    1.   REQUIREMENTS FOR LOADING JAWS INTO
         HIGH MEMORY
    2.   INSTALLING A MEMORY MANAGER
    3.   LOADING JAWS WITH 386 TO THE MAX
    4.   LOADING JAWS WITH QEMM
 
1.  REQUIREMENTS FOR LOADING JAWS INTO HIGH
    MEMORY
 
    If you have a 386 computer, that is a PC with a 80386 micro
processor, you can load JAWS version 2 into high memory.  This
means
JAWS will take up very little of the 640k of DOS memory.  This
technique can also be used for JAWS version 1 assuming the PC has
enough high memory (expanded memory).  You can even do this on some
286 machines using "QRAM" from QuarterDeck.  Please contact
QuarterDeck   for information about the QRAM software.
 
 
 
2.  INSTALLING A MEMORY MANAGER
 

memory management system.  The information presented here may be
helpful in your case, for further questions or problems consult
your
manual or your computer vendor.
 
    Begin by installing the memory manager as a "device driver" in
your CONFIG.SYS file.  Lets assume you are using 386 To the Max,
and
its files are in the \386MAX subdirectory.  You must add the
following
line to your CONFIG.SYS file:
 
    DEVICE=\386MAX\386MAX.SYS
 
    If you are using QEMM and it is in the \QEMM subdirectory,
then you must add the following line to your CONFIG.SYS file:
 
    DEVICE=\QEMM\QEMM.SYS
 
    The commands described above will install the device driver the
next time you boot your machine.  Before you load JAWS you need to
tell the device driver to load JAWS into high memory and to close
off
high memory afterward.  Lets assume that JAWS is installed on your
hard drive in the \JAWS subdirectory, and that you are using the
Accent PC version of JAWS.
 
 
3.  LOADING JAWS WITH 386 TO THE MAX
 
    If you are using 386MAX create a batch file, or add the
following
lines to your AUTOEXEC.BAT file, or simply type in the following
lines
from the keyboard:
 
    \386MAX\MAXHI
    CD\JAWS
    J_ACNTPC
    \386MAX\MAXLO
 
    The "\386MAX\MAXHI" command loads the MAXHI program
from its subdirectory, this tells the memory manager to load any
following programs into high memory.  The "CD\JAWS" changes to the
JAWS subdirectory.  "J_ACNTPC" is the name of JAWS for the Accent
PC synthesizer. This command loads JAWS.  The memory manager will
put it into high memory.  "\386MAX\MAXLO" tells the memory
manager to close off high memory, and to not place additional
programs
in that area.
 
 
 
4.  LOADING JAWS WITH QEMM
 
    If you are using QEMM then use the following commands:
 
    \QEMM\LOADHI
    CD\JAWS
    J_ACNTPC
    \QEMM\LOADLO
 
    The "QEMM\LOADHI" command  loads the LOADHI program
from its subdirectory, this tells the memory manager to load any
following programs into high memory.  The "CD\JAWS" changes to the
JAWS subdirectory. "J_ACNTPC" is the name of JAWS for the Accent
PC synthesizer, this command loads JAWS.  The memory manager will
put it into high memory.  "QEMM\LOADLO" tells the memory manager
to close off high memory, do not put any more programs up there.
 
    This is an example of how to do it using a specific set of
circumstances.  If your situation varies you must make the
appropriate
substitutions.  If you are not using one of the memory managers we
cover here then you should refer to the manual of the memory
manager
you are using.  If you are not using the Accent PC version of JAWS
then
substitute the correct name for the JAWS version you are using,
i.e.
"J_DECTLK" for the DECtalk or "J_BNS" for the Braille 'n Speak,
etc. 
If you are using JAWS version 1 precede the synthesizer name with
"JAWS" as you normally would.  If you are using different
subdirectories
than this example then make the proper changes.  If you have
specific
questions about your memory manager please read the manual or call
the manufacturer.  We do not make memory managers.  If you do not
understand subdirectories or CONFIG.SYS or AUTOEXEC please read
your DOS manual.
