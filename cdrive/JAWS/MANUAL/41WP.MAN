PART FOUR:  Application 1
WORDPERFECT
(Only contained in the disk file "41WP.MAN".)
 
 
(Last revision:  November 1993.)
 
 
 
CONTENTS FOR APPLICATION 1
 
     1.   SUMMARY OF WORDPERFECT 5.1 MACRO
          KEYS
          1.1  Spell Check Keys
          1.2  Reading Reveal Codes, Menus, and
               Filenames
          1.3  Reading Program Status Information
          1.4  Reading Larger Units of Written
               Language
     2.   USING THE SPELL CHECKER
          2.1  Finding the Misspelled Word
          2.2  Reading the Alternative Spelling Choices
     3.   REVEAL CODES MODE
     4.   LIST FILES DIRECTORY
     5.   MENU CHOICE MACRO
     6.   READ DOCUMENT, PAGE, LINE AND
          POSITION NUMBERS
     7.   READ STATUS LINE
     8.   CURRENT SCREEN ATTRIBUTE
     9.   MOVE TO PRIOR/NEXT PARAGRAPH
     10.  PRIOR/NEXT SCREEN
     11.  FRAME MONITORING IN WORDPERFECT
     12.  OTHER WORDPERFECT TECHNIQUES
1.   SUMMARY OF WORDPERFECT 5.1 MACRO KEYS
 
1.1  Spell Check Keys
 
CONTROL + MINUS 
Finds and speaks the misspelled word.
 
CONTROL + PLUS
Moves to and speaks the first line of alternative spellings for the
misspelled word.
 
1.2  Reading Reveal Codes, Menus, and Filenames
 
     The ALT key is used with the cursor keys to move the cursor and
read information that is displayed with screen highlighting.  These keys
can be used to read information in the Reveal Codes window, in the pull
down menu, or in the List Files directory.  They could read the name of
a WordPerfect function code in the Reveal Codes window, read the
current menu choice, or read the name of the highlighted file in the list
of file names.  
 
ALT + LEFT ARROW, ALT+ RIGHT ARROW,
ALT + UP ARROW, ALT + DOWN ARROW
These keys are used to move the highlighted cursor bar when reading
highlighted information.
 
ALT + 5 (speech pad),
[ALT + END with Toshiba keyboard configuration]
This reads the highlighted information at the cursor, in a manner
similar to Say Field or Say Word.
 
ALT + EQUAL
Causes the pull down menu to be displayed at the top of the screen and
then reads the highlighted choice.
 
1.3  Reading Program Status Information
 
SHIFT + DELETE
This is used to read the status line where the document, page, line, and
position numbers are displayed.  
 
INSERT + PAGE DOWN or ALT + SEMICOLON
This reads the status line, and may read line 24 and 25 as when a menu
is displayed on both lines 24 and 25.  The ALT + SEMICOLON is an
auto-macro for frame 25.
 
INSERT + A
This key speaks the screen attribute that is used to display the position
number on the status line.  This attribute changes as screen attributes
at the cursor change to indicate bolded and underlined text.
 
INSERT + Q
This reads the text that is highlighted when the block function is turned
"on" for defining a block of text prior to moving, copying, deleting, or
appending the text.  This allows the block to be read, for example, before
it is deleted.
 
1.4  Reading Larger Units of Written Language
 
SHIFT + 5 (speech pad), 
Speaks the current sentence.
 
SHIFT + LEFT ARROW
Moves the cursor to the beginning of the current sentence and reads the
sentence.  If the cursor is already at the beginning of the sentence, then
it is moved to the beginning of the preceding sentence, and that
sentence is read.
 
SHIFT + RIGHT ARROW
Moves the cursor to the beginning of the next sentence and reads the
sentence.
 
SHIFT + PLUS
This reads the current paragraph.  It does not move the cursor from its
current location.
 
SHIFT + PAGE UP 
This moves the cursor to the beginning of the current paragraph and
then reads the paragraph.  If the cursor is already at the beginning of
the paragraph, then it moves to the beginning of the preceding
paragraph and reads that paragraph.
 
SHIFT + PAGE DOWN
This moves the cursor to the beginning of the next paragraph and reads
the paragraph.
 
CONTROL + UP ARROW
Moves the cursor up to the prior paragraph and reads the first line of
the paragraph.
 
CONTROL + DOWN ARROW
Moves the cursor to the next paragraph and reads the first line of the
paragraph.
 
ALT + MINUS
Displays the preceding screen of information.
 
ALT + PLUS
Displays the next screen of information.
 
2.   USING THE SPELL CHECKER
 
2.1  Finding the Misspelled Word
 
     The WordPerfect spell checker will highlight the misspelled or
unrecognized word with a distinct screen attribute, (color or
monochrome enhancement).  In order for JAWS to read the misspelled
word, it must first find it on the screen.  With PC's that have a color
video display, JAWS searches for the specific color that WordPerfect
uses to highlight the misspelled word, or searches for a reverse video
enhancement when the PC has a monochrome display.
 
     Whenever the misspelled word appears in the spell check window
it is spoken automatically, because the spell check macro is an auto
macro which is triggered when changes are observed in frame 12.  When
the word is found, the JAWS Cursor is placed at the beginning of the
word, so that it can be examined in detail.  You can read the word
character by character, or you could read it in context with the Say
Line, or Say Sentence functions.  If the word is not misspelled, but it is
one of a pair of double words, or has been identified as having an
irregular capitalization pattern, then the message at the bottom of the
screen will be spoken.
 
     The macro key that finds the misspelled word in WordPerfect
version 5.1 is the CONTROL + MINUS.  In WordPerfect versions 5.0
and 4.2, the spell check key is CONTROL + UP ARROW.  Simply press
the macro key to reread the misspelled word whenever you need to hear
it spoken again.
 
2.2  Reading the Alternative Spelling Choices
 
     When you use the CONTROL + PLUS macro, it will read the first
line of alternative spelling choices that WordPerfect displays on line 15. 
JAWS will not only read the line, but will also place the JAWS Cursor
on the first alternative spelling choice.  In this way, the alternatives can
be reviewed in detail and the alternative spelling choices  that maybe
displayed on other lines can be read.  If you want to select one of the
alternative choices, then press the letter that precedes the choice you
wish to select.  WordPerfect substitutes the new word you select directly
into your document.
 
     The CONTROL + PLUS macro invokes the JAWS Menu System,
performs a goto function to place the JAWS Cursor on row 15 column 6,
then does a Say Line.  The result is the JAWS Cursor is positioned on
the first letter of the first spelling alternative before the whole line is
read.  You can use the Say Word or Spell Word functions to read the
words again, and other JAWS Cursor keys to read any additional words. 
If you want to edit the word, then press "4" to select edit, then turn on
the PC Cursor and edit the text.  Alternatively you can turn on the PC
Cursor and press the Say Next Character key (RIGHT ARROW), which
automatically puts WordPerfect into its spell checking edit mode.
 
 
3.   REVEAL CODES MODE
 
     The WordPerfect Reveal Codes mode (ALT + F3) displays the
hidden control codes that are contained in WordPerfect documents. 
These codes are not visible on the regular WordPerfect screen but can
be seen in Reveal Codes, for example, tab stops, page breaks, hard
carriage returns, margin settings, page number settings, etc.  When ALT
+ F3 is pressed, then the screen display splits into upper and lower
windows.  The upper window shows the text as it normally appears on
the screen and the PC Cursor is positioned in this window.  The bottom
window is the Reveal Codes window and it uses a special WordPerfect
cursor.  These two WordPerfect cursors move together as you move
around the window to examine the codes and as you remove unwanted
codes.  The ALT + 5 (or ALT + END for Toshiba), is used to find the
special cursor in the Reveal Codes window and to speak the word it
points to.  This macro key actually aligns the JAWS Cursor with the
Reveal Codes cursor, so that you can use the regular speech pad keys to
read information in the area of the cursor.  If you hold down the ALT
and press the arrow keys, then you will move the Reveal Codes cursor. 
You can press ALT + 5 (speech pad) to read the word the Reveal Code
cursor points to, and if you press ALT + DELETE, then you will delete
the character or code at the cursor.  Pressing ALT + F3 will exit Reveal
Codes mode.  It is important to remember that when you are reading
the information in the Reveal Codes window, you are reading with the
JAWS Cursor.  This gives you the ability to read freely around the
Reveal Codes window, but be sure to realign the JAWS Cursor with the
Reveal Codes cursor and check the data at that point before editing.
 
     If you are using WP 5.1 or 5.0 and this macro does not work, then
you have the "color" problem mentioned below with the spell checker. 
This macro is identical to the misspelled word macro except that it
searches from the bottom of the screen instead of from the top.  With
WordPerfect version 4.2, this macro searches for a graphic character.
 
 
4.   LIST FILES DIRECTORY
 
     The WordPerfect List Files function is F5, and it displays a list
of all the files in the current subdirectory.  The ALT + 5 is used to read
the highlighted file name in the List Files menu, which is the same
macro used in the Reveal Codes window.  You can use ALT with the
arrow keys to move the menu bar from one file name to another.  On a
Toshiba PC, this macro is usually ALT + END.  You can use the JAWS
Cursor to look at the file information in detail.  Be sure, however, to
realign the JAWS Cursor with the highlighted choice before making
selections.  The List Files options are shown on lines 24 and 25, and can
be read with the INSERT + PAGE DOWN.
 
     If this macro does not work then you have the "color" problem
described below for the spell checker.  Refer to that section for remedies.
 
 
5.   MENU CHOICE MACRO
 
     The pull down menu in WP 5.1 can be read by the JAWS Reveal
Codes macro.  Invoke the WP menu with ALT + EQUAL, then use the
Reveal Codes macro to read the highlighted choice.  Use ALT with the
arrow keys to move the choice, and ALT + 5 (or ALT + END) to read the
choice.
 
 
6.   READ DOCUMENT, PAGE, LINE AND POSITION NUMBERS
 
     This macro will announce the document (1 or 2), page, line, and
the column position of the cursor within your document.  This
information appears on line 25.  The macro simply speaks frame 27. 
The frame coordinates are:  top row 25, bottom row 25, left column 56,
right column 80.  You may want to change the left column number of
the frame to include more or less of the information displayed on line
25.  The macro invokes the JAWS Menu System, and uses the frame
manager to speak frame 27.  This macro key is SHIFT + DELETE for
WP 5.1, and is CONTROL + DELETE for WP 5.0 and WP 4.2.
 
 
7.   READ STATUS LINE
 
     This macro reads either line 25, or both lines 24 and 25 when the
status line occupies both lines.  Both lines 24 and 25 often display menu
selections such as in the List Files directory.  The macro decides if it
should read line 24 and 25 or just lines 25 based on what is being
displayed.  The information that is read depends on the results of a
macro search that looks for "6" in column 1 of line 25.  If it finds the
number "6", then this means there are other menu selections listed on
line 24 and the macro then reads both lines.  If it does not find "6", then
it just reads line 25.  If the JAWS Menu System is in use, then the
macro reads the menu help line.  It is the INSERT + PAGE DOWN key.
 
 
8.   CURRENT SCREEN ATTRIBUTE
 
     This reads the screen color or monochrome enhancement that is
used to highlight the "position number" which is displayed on line 25. 
When you turn on one of the special print enhancements, like bold or
underlined, then WP displays the corresponding screen attribute or color
at this position.  This is how sighted WP users determine the print
enhancement they have selected.  JAWS speaks the screen attribute at
the position number but does not say the name of the corresponding
print attribute being used.  You will need to determine which screen
attributes are used to display the print attributes of bold and underline
for your version of WP.  You can use the WordPerfect setup menu to
find out which colors are being used for print attributes, or simply
highlight a few words with the bold and underline codes and use the
Say Screen Attribute function to read the color.  With a few
modifications to the INSERT + A macro, JAWS could speak the proper
name of the print attribute that is represented by the color.
 
     To modify this macro to speak the name of the print attribute,
you can use the enhancement menu to find specific colors.  If the color
is found, then it can speak the name of the print attribute with a Label
function.  If it does not find the first color, then it can search for another
one, and so forth.  The macro can perform a series of searches until it
finds a color that allows it to speak the print attribute.  The "If then
else" statement and the "Success Flag" make this possible.  We have not
pre-defined this macro, because we cannot be sure what color will
correspond to which print attribute on your PC, since the palette of
colors can be changed.  
 
 
9.   MOVE TO PRIOR/NEXT PARAGRAPH
 
     These macros will move the PC Cursor to the beginning of a
paragraph and read the first line.  This is a BIOS macro that simply
executes a CONTROL + UP ARROW OR DOWN ARROW, which is
WordPerfect's command to move up or down to a paragraph.  The macro
then performs a Say Line function.  Remember, WP looks for a hard
carriage return to signify the end of a paragraph.
 
     Sometimes JAWS speaks the line before WordPerfect gets to the
correct position, because WP is moving slower than JAWS If this
happens, you can add one or more Pause functions to these macros in
front of the Say Line function.  The Pause functions will give WP more
time to move its cursor to the top of the paragraph before JAWS speaks. 

 
     You can also use the SHIFT + PAGE UP and SHIFT + PAGE
DOWN for moving up and down by paragraphs and for reading the
entire paragraphs.  These macro keys allow JAWS to decide what is a
paragraph, so that JAWS can read the paragraph.  SHIFT + PLUS
reads the paragraph without moving the cursor.  You can combine
functions such as the Next Paragraph and Say Sentence, to create a
macro that speaks the first sentence of each paragraph.
 
 
10.  PRIOR/NEXT SCREEN
 
     The ALT + MINUS or the ALT + PLUS macros perform the
standard WP functions performed by the MINUS and PLUS keys on the
number keypad.  These keys move the previous screen or the next
screen full of information on to the visible screen.  A screen is only 24
lines, which is not the same as a document page.  Since JAWS usually
uses the speech pad MINUS and PLUS keys for activating cursors, then
the ALT + MINUS and ALT + PLUS keys are needed to move through
documents one screen at a time.
 
 
11.  FRAME MONITORING IN WORDPERFECT
 
     The WordPerfect configuration includes a few frames which are
monitored for screen changes.  JAWS monitors frame 25, which occupies
line 25, columns 1 through 9.  When the screen changes in the frame
then JAWS performs the ALT + SEMICOLON macro, which looks at the
contents of the frame and decides what to do.  It usually reads line 25,
which is the status line.  Sometimes it reads both lines 24 and 25, like
the INSERT + PAGE DOWN.  If a misspelled word is displayed, or you
are in one of the menus, then it does nothing.  
 
     If this macro function is repeating or failing to read all
information as it should, then the "delay factor" may need to be
increased or a Pause function may need to be added to the macro.  The
delay factor can be increased to stop repeated speaking by resetting the
delay factor to a higher number in the frame "monitor" menu of the
JAWS Menu System.  If some of the words or information seem to be
missing then a Pause function can be added to the beginning of the
macro.  The chapter on the macro manager and the JAWS training
tapes will explain how to do this.
 
 
12.  OTHER WORDPERFECT TECHNIQUES
 
     In describing the special features of the JAWS configuration files
for WordPerfect, we do not wish to leave you with the impression that
you should overlook regular JAWS screen reading features.  The JAWS
Cursor is the tool that is needed in many situations to use WP features. 
WordPerfect often places the PC Cursor on line 25 when it displays
screens of instructions and menu choices.  In these situations, the JAWS
Cursor must be used to read the screen.
 
     You can use the JAWS Cursor keys and the JAWS Cursor
whenever you use the WP help system with the F3 key.  Activate the
JAWS Cursor, use the PAGE UP key to move to the top of the screen,
then use the Say All function to read down the screen or manually read
the screen with the speech pad keys.  You can press alphabet and
function keys to change the help system display when you desire, and
you do not need to activate the PC Cursor.  
 
     Some of the WP menu displays such as for printing, formatting,
and WP set up also display information on the entire screen and require
the use of the JAWS Cursor.  Sometimes the PC Cursor will jump up
from line 25 to a position on the menu when you select an entry that
requires you to enter data, such as when setting the margins.  When the
cursor moves to a position where you need to enter data, then activate
the PC Cursor and press Say Line to read the instructions, and then
enter the data.
 
     Whenever you are in doubt about what to do, you can always use
the JAWS Cursor to read the screen.  Many mistakes can be avoided
and the quality of documents produced by WordPerfect will be improved
by taking the time to read the instructions until you are sure of what
WordPerfect is asking you to do.  The WP Help system provides one or
more screens of information about most any feature you are using.  If
you get stuck and don't know what to do in a specific situation, then
press F3.  It does not matter that you are in the middle of an activity. 
WordPerfect can usually interrupt the current task, so that you can read
its help screens.  When you are through reading the help screen with
the JAWS Cursor, then press ENTER, and you will be returned to the
document, menu, or task on which you were working.  
 
     There is really no substitute for working with manuals and
training tapes when it comes to learning about the fine points of a
complex program like WordPerfect.  The JAWS training tapes, and
Wordperfect books on tape and computer disk will be important tools
when learning to use WordPerfect.  Proficient use of any program comes
from studying the proper use of program functions, practicing the use
of those functions, and from creatively playing with the software to see
what will happen if you do things.  There is no substitute for study,
practice, and play.
 
     If a macro, such as the spell check macro or menu reading macro
is not working, then this is most likely being caused by the macro
looking for the wrong color.  WordPerfect has changed the colors it uses
as it has changed version numbers and it is also possible that other
users of your software may have changed the color palette used to
display text.  The JAWS macros for WP version 5.1 search for a
background color of red or for the reverse video enhancement.  WP
macros for version 5.0 and 4.2 search for the background color of white. 
If you have this problem, then please refer to the section on macros that
do not work in the chapter on the macro manager in PART TWO of this
manual for information about fixing this problem.  This chapter also
shows and analyzes the macro used with the WordPerfect spell checker.
 
     The size of the frames in the JAWS configuration files for
WordPerfect will not be the cause of macros not functioning properly
unless someone has changed the dimensions of the frames.  All the
versions of WordPerfect use a similar screen display, and we use the
same frame file for all versions.  It is named "WP.JFF".
 
If you wish to examine the macros used with WordPerfect you can
examine then individually by using the JAWS Macro Editor, or you can
print a copy of the macros by using the "J_MTEXT.EXE" program.  For
example, type the following line of information at the DOS prompt to
create the macro text file for WordPerfect Macros.  The file that is
created is called "WP.JMT".  This file can be printed or read with
WordPerfect.
 
     J_MTEXT.EXE WP.JMF
