Using JAWS With Alternate Input Devices

The program J_AINPUT.EXE makes JAWS compatible with Dragon Dictate,
VoiceType, and many other devices such as Morse Code keys that
allow using a computer without a keyboard. In all cases, JAWS should
be loaded first, then the program which allows non-keyboard input, and 
finally J_AINPUT. Order is very important if maximum performance is
to be achieved.

Devices that simulate a keyboard cannot generate certain key
combinations such as Insert+Up-Arrow.  Since JAWS relys on many such
key combinations for successful operation, the J_AINPUT driver
provides a means of simulating them.  For this purpose, certain keys
are assigned special meaning:

KEY			MEANING
F11			Cursor-Pad 5
F12			Insert Key
Alt-F12			Alt Key
Shift-F12		Shift Key
Ctrl-F12		Control Key

For example, in order for a device simulating a keyboard to get JAWS
to do a Say Word, it would send F12 followed by F11, the equivalent
of Insert+Cursor-pad 5.  To do a Say Prior Sentence, Shift-F12
followed by Left-Arrow, the equivalent of Shift+Left-Arrow.

When the J_AINPUT	 driver is loaded, a regular keyboard can
still be used, but some macro keys will have their labels spoken
twice when invoked from the keyboard.

Getting Maximum Performance Out Of JAWS With Dragon Dictate

Do the following to get JAWS and Dragon Dictate to interact properly:

Set JAWS Keyboard echo to Characters

Turn Screen echo off.  

Turn Smart Screen on.


Change the Dragon Dictate Hot and Fix keys to not conflict with keys
used by JAWS.  We recommend making the Hot key Alt-0 and the Fix key
Alt-9 (0 and 9 on the top row of the keyboard). 
For example, if you normally type 

	dd  your-name

to invoke Dragon, change the command to

	dd /ha-0 /fa--your-name

The above command can be placed in the AUTOEXEC.BAT file so that it will
be invoked automatically each time you turn on your computer.

In the Dragon Dictate parameters menu, set Pop Always to 0, and Keys
Immediate to 1. This will prevent the list of choices from appearing
each time you say a word.

Place the file JAWS.DCM in the DRAGON subdirectory.  It
contains voice commands to perform the most common JAWS functions.
(This is a standard ASCII text file and can be augmented with
additional commands, by following the procedure outlined in the Dragon
Dictate manual in the chapter discussing Voice Macros.) These macros
need only be explicitly loaded one time, using the command

	DCOM @JAWS

You will then be prompted to train the system to recognize each
command.  Once prompting is completed, the vocabulary should be
saved (Save from the Voice Console).  This will make the JAWS commands
a permanent part of the system and will allow them to be recognized in
the future.

Once the JAWS commands are part of the vocabulary, they can be
interspersed with text being dictated.  This allows a blind user to
easily navigate around the screen by voice.

When dictating text, each word will be verbally spelled out after it
has been recognized.  If this is not the word desired, you should say

	"Begin Spell Mode"

and use the International Communications alphabet to spell out the
word desired.  

In our tests, the above method has proven the most
efficient for a blind user. Note that with Pop Always set to 0, the list of
possible words is only presented when the Fix Key is pressed, or when
"Spell Mode" is entered. You may want to experiment with setting Pop
Always to 1, and using JAWS Smart-Screen to speak the current
item in the Word  Choice box. However, if this is done, sentence and
paragraph speaking commands will no longer work when the PC cursor is on.

