LIST OF FILES CONTAINING THE JAWS MANUAL
(Second Edition, Printing #2, January 1994)
 

** begin list of files in print and on disk *** 
 
00FRONT.MAN = cover, Legal Notices, and License Agreement
00TABLE.MAN = Organization of Manual and Table of Contents
 
11POWER.MAN = Chapter 1, Power of JAWS
12INSTAL.MAN = Chapter 2, JAWS Installation
13QUICK.MAN = Chapter 3, Quick Start Up
14LEARN.MAN = Chapter 4, Learning Strategies
 
21CURSOR.MAN = Chapter 1, Cursor Movement and Screen Reading
22MENU.MAN = Chapter 2, JAWS Menu System
23VOICE.MAN = Chapter 3, Voice Control
24FIND.MAN = Chapter 4, Finding Screen Locations, Text, Colors 
25FRAMES.MAN = Chapter 5, Frame Manager
26SMART.MAN = Chapter 6, Smart Screen Manager
27MACRO1.MAN = Chapter 7, Macro Manager
28CONFIG.MAN = Chapter 8, Configuration Manager
 
31EXPRES.MAN = Appendix 1, Specialized Expressions and Glossary
32SUPPLE.MAN = Appendix 2, Supplemental Information for
     Problems 
 
*** end of files in print and on disk ***
 
*** begin list of files only on disk ***
 
33MACRO2.MAN = Appendix 3, Macro Functions
34COMAND.MAN = Appendix 4, Command Line Switches and
     Program Files
 
41WP.MAN = Application 1, WordPerfect
42LOTUS.MAN = Application 2, Lotus 1-2-3
 
51INTERN.MAN = Installation Reference 1, Internal Synthesizers 
52EXTERN.MAN = Installation Reference 2, External Synthesizers
53SYNTH.MAN = Installation Reference 3, Speech Synthesizers Etc. 
54TOSH.MAN = Installation Reference 4, Toshiba Computers
55HIGH.MAN = Installation Reference 5, Notes for Loading JAWS
     into  High Memory
 
61JAWS20.MAN = Update 1, ABOUT JAWS VERSION 2.0 
62JAWS22.MAN = Update 2, ABOUT JAWS 2.2
63JAWS23.MAN = Update 3, ABOUT JAWS 2.3
 
*** end of files in print and on disk ***
 
** begin list of files in print and on disk *** 
 
71INDEX.MAN = Index
