PART ONE:  Chapter 1
THE POWER OF JAWS
(Also contained in the disk file "11POWER.MAN".)
 
 
Last revision:  November 1993.
 
 
 
CONTENTS OF CHAPTER 1
 
    1.   POWER AND COMMITMENT . . . . . . . . .1-1.1
    2.   SIMPLE DESIGN. . . . . . . . . . . . .1-1.1
    3.   UNLIMITED ACCESS AND FLEXIBILITY . . .1-1.2
    4.   TWO SEPARATE CURSORS ARE THE KEY . . .1-1.2
    5.   CONCENTRATED CONTROL OF READING. . . .1-1.3
    6.   SINGLE COMMANDS FOR MANY USES. . . . .1-1.3
    7.   MACROS MAKE EVERYTHING EASY. . . . . .1-1.4
    8.   AUTOMATIC MACROS RESPOND TO
         SCREEN CHANGES . . . . . . . . . . . .1-1.4
    9.   SOFT KEYS ARE NEVER IN CONFLICT. . . .1-1.5
    10.  EXPANSION OF KEYBOARD UTILIZATION. . .1-1.5
    11.  POPULAR MENU SYSTEM DESIGN . . . . . .1-1.6
    12.  UNIVERSAL SEARCH AND FIND. . . . . . .1-1.7
    13.  SMARTER THAN THE AVERAGE SCREEN
         READER . . . . . . . . . . . . . . . .1-1.8
    14.  SAY IT ALL AND STOP. . . . . . . . . .1-1.8
    15.  AUTOMATIC LOADING OF SPECIAL
         CONFIGURATIONS . . . . . . . . . . . .1-1.9
    16.  STRONG COMPUTER COMPATIBILITY. . . . .1-1.9
    17.  COMPREHENSIVE SPEECH SYNTHESIZER
         COMPATIBILITY. . . . . . . . . . . . 1-1.10
    18.  GRAPHIC CHARACTERS AND GRAPHICS
         DISPLAYS . . . . . . . . . . . . . . 1-1.10
    19.  THE NEXT STEP IS INSTALLATION. . . . 1-1.11
1.  POWER AND COMMITMENT
 
    JAWS is a powerful software program that transforms a personal
computer and a speech synthesizer into a talking computer system
for
people who are blind.  The power of JAWS begins with its name.  Job
Access With Speech, or simply JAWS, emphasizes our commitment to
men and women who depend on screen reading software for employment
and professional development.  Any personal computer (PC) screen
reading software package will read the screen (monitor), but JAWS
offers the comprehensive set of tools that are needed by visually
impaired people to work, study, and be creative.  JAWS is serious
software for those who want every possible advantage they can
obtain
when competing in the workplace or in the classroom.  JAWS is for
those who understand that top performance goes hand in hand with
the
quality of the tools they use.  JAWS is for people who need
powerful
computer access and unlimited performance potential.
 
 
2.  SIMPLE DESIGN
 
    JAWS' simple design reduces frustration for the first time
computer user and is one of the strongest assets for the advanced
computer user or computer trainer.  JAWS makes it possible to use
regular computer software programs designed to run on ordinary IBM
and IBM compatible PC's.  JAWS is the connection between people who
are blind and the software and hardware that has been designed for
sighted computer users.  Sighted people use the keyboard for input
and
their eyes for output.  Blind people use the keyboard for input and
JAWS for obtaining output.  JAWS users trust the power and advanced
features of JAWS because they know that they can depend on this
screen reading tool to achieve the same level of competency and
efficiency as sighted PC users.  
 
 
3.  UNLIMITED ACCESS AND FLEXIBILITY
 
    JAWS sets the standard for screen reading flexibility.  Blind
computer users need the ability to read any portion of the screen
at any
time, and JAWS provides this comprehensive flexibility.  Sighted
users
can look at instructions located at the top of the screen while
entering
data at the bottom of the screen, and easily refer to preceding
paragraphs of a business letter while composing new paragraphs. 
The
unique screen reading approach of JAWS also enables blind computer
users to read any part of the screen whenever they need to do so. 
This
high standard of screen access is unmatched by any other screen
reading software.
 
 
4.  TWO SEPARATE CURSORS ARE THE KEY
 
    It is the Dual Cursor design that gives the JAWS user
unrestricted ability to read the screen.  JAWS uses its PC Cursor
to
follow the regular cursor of the personal computer and adds the
strength
of the JAWS Cursor.  The JAWS Cursor is a free-moving cursor that
can read anything on the screen at any time.  This means the JAWS
Cursor can be used to read information on line 2 of the screen
while
data is simultaneously being typed on line ten.  New computer users
may not fully appreciate the value of this, but long time computer
users
will quickly realize the immense power offered by these two
cursors.
 
    The JAWS Cursor is significantly more advanced than the "review
cursor" that is offered by other screen reading software.  The JAWS
Cursor performs a review function like other screen readers, but
with
one remarkable difference.  The JAWS Cursor absolutely does not
interfere with the computer's normal operation!  By not stopping or
freezing the working of the computer, application software is left
alone
to work without interruption, even while the user moves the JAWS
Cursor to read the screen.  The user always has the ability to know
exactly what his or her software application is doing.  Function
keys can
be pressed, regular typewriter keys can be tapped, and the
software's
PC Cursor can be moved -- all while the JAWS Cursor is in use. 
There
is never any need for changing modes or pressing special keys.
 
    In contrast, screen reading software programs that use a
"review
mode" will temporarily freeze or suspend activity of other software
when
review mode is used.  Data cannot be entered and messages cannot be
received.
 
 
5.  CONCENTRATED CONTROL OF READING
 
    The easy to use JAWS screen reading functions are concentrated
on the numeric keypad.  The innovative enhancement of the numeric
keypad doubles the number of screen reading keys for the JAWS user,
and creates a truly versatile speech control keypad.  The JAWS
Speech
Pad is a source of high performance screen reading power that
becomes
a tool of precise control.  Characters, words, lines, sentences,
paragraphs, blocks of information and menu choices can all be read
with
the simplest of commands.  Efficiency and pure ease of operation
are
gained through the one-handed access to most screen reading
features.
 
 
6.  SINGLE COMMANDS FOR MANY USES
 
    The keys that control the movement of the PC Cursor also
control
the JAWS Cursor as well as movement within the internal JAWS Menu
System.  The keys are laid out in a very logical and easy to
remember
fashion.  One simple set of key strokes does it all--separate
keyboard
layouts and key functions need not be learned.  Additionally,
either
cursor can be routed to the other when the need arises.  Thus, the
JAWS Cursor can automatically locate and move to the position of
the
PC Cursor, and in many application software programs, the PC Cursor
can move to the JAWS Cursor.  JAWS always provides a way to move
the cursor and read what is needed.
 
 
7.  MACROS MAKE EVERYTHING EASY
 
    JAWS can relieve the user from entering repetitious screen
reading commands with the use of macros.  A Macro is like a short
tape
recording of key strokes.  The macro feature of JAWS allows a
series of
recorded key strokes to be triggered and replayed just like a
cassette
tape can be rewound and replayed again and again.  A macro is
assigned to a specific key and the macro is replayed every time
that key
is pressed.  For example, when a spell checking program is being
used,
a single key stroke can be used to trigger a macro that:  searches
for the
misspelled word, moves the JAWS Cursor to the word, reads the word,
spells the word, and reads the possible spelling alternatives.  
 
 
8.  AUTOMATIC MACROS RESPOND TO SCREEN CHANGES
 
    Macros can also be tied to the ability of JAWS to watch the
screen
for changes.  For example, JAWS can watch the bottom line of
information on the screen, and whenever the line turns from blue to
red,
a JAWS macro will be instantly triggered.  The automatic macro
might
turn-on the JAWS Cursor, move the JAWS Cursor to the top of the
screen and read line one.  A different macro could be triggered if
the
color turns to white.  JAWS has the ability to automatically tell
the user
about anything that is happening on the screen or to take a
specific
action in response to screen activity.
 
    Macros are very popular with sighted computer users because
macros make the keyboard more efficient and easier to use.  Since
a
blind person uses the keyboard more intensely than a sighted
person,
macros are even more important.  The easy to use JAWS Macro Editor
offers full speech interaction and makes the process of creating
macros
a snap.  The Macro Editor can be used within any application
program
so that macros can be written, tested, revised, and saved at any
time.
 
 
9.  SOFT KEYS ARE NEVER IN CONFLICT
 
    JAWS has an unbeatable method for avoiding conflicts that arise
when an application program wants to use the key strokes reserved
for
screen reading.  JAWS uses the Soft Key concept to eliminate the
possibility of keyboard conflicts.  The user can rearrange the
keyboard
with the JAWS Macro Editor because every JAWS key is actually a
macro.  The location of screen reading keys can therefore be
changed for
a specific application program whenever the user desires.  Thus, if
a
telecommunications program such as PROCOMMPLUS wants to use the
number pad for controlling the downloading or uploading of files,
then
the JAWS user can create a different set of screen reading keys. 
Use of
JAWS Soft Keys can instantly solve the kinds of keyboard conflicts
that
are an inconvenience to the users of many other screen readers.
 
    Some PC users prefer to keep their fingers on the home row and
object to moving their right hand over to the number pad to read
the
screen.  With JAWS' soft keyboard feature, the cursor/reading keys
can
be placed anywhere on the keyboard.  This means the UP/DOWN
ARROWS, LEFT/RIGHT ARROWS, PAGE UP/DOWN, HOME, END,
DELETE, and dozens of other special speech control functions can be
moved to any other key or key combination on the keyboard.  Many
people overlay a complete copy of the screen reading functions from
the
numeric pad onto the alphabet keys.  In this way, the alphabet keys
can
be used to type normal alphabet characters until the ALT key is
held
down.  When the ALT key is pressed, then the alphabet keys can turn
into cursor keys that move either the PC or JAWS Cursor.  This
makes
it possible to move the cursor and read the screen from either the
home
row location or from the numeric keypad.  
 
 
10. EXPANSION OF KEYBOARD UTILIZATION
 
    JAWS gives extra screen reading control to people who use a
one-
hundred-and-one keyboard.  These keyboards have the regular numeric
keypad that can be used for moving the cursor or for entering
numbers,
and they have an extra ten keys that are exclusively used as a
separate
cursor pad.  This cursor pad contains the UP/DOWN ARROWS,
LEFT/RIGHT ARROWS, INSERT, DELETE, HOME, END, PAGE UP,
and PAGE DOWN functions.  On most systems these keys are just
duplicate keys that perform the same task as the keys on the
numeric
keypad.  Now JAWS makes the cursor keypad and numeric keypad
independent and distinct so they can perform separate screen
reading
tasks.  The arrow keys on the numeric keypad could move the JAWS
Cursor to read the screen while the arrow keys of the cursor keypad
are
used to move the application software's cursor.  One delete key
could
report the position of the cursor while the other could delete a
character,
etc.  Many experienced computer users have been seeking a few more
macro keys for a long time -- now JAWS provides the extra keys for
creating powerful new macros.
 
 
11. POPULAR MENU SYSTEM DESIGN
 
    The Lotus style menu system pops-up at any time and provides
immediate access to all the features of JAWS.  The arrow keys move,
speak, and highlight the current menu option as the cursor is
moved. 
Helpful information about the current option is displayed for each
menu
and sub menu selection.  There is no need to memorize the menu,
because everything is spoken.  The JAWS Menu System is fully
compatible with JAWS macros, and users of JAWS commonly include
menu functions within their macros to create powerful screen
reading
tools.
 
    The JAWS Menu System is displayed on the screen where it can
be both read by JAWS and seen by sighted co-workers or trainers. 
The
menu system allows the user to:  adjust the way the speech
synthesizer
speaks; search the screen for text or a certain color; load special
JAWS
configurations for specific software programs; define a section of
the
screen for reading or monitoring; edit macros; edit speech
synthesizer
pronunciation rules; and perform numerous other functions.  No
other
screen reading program offers a menu system that is as feature
packed
as the JAWS Menu System.
 
 
12. UNIVERSAL SEARCH AND FIND
 
    Searching for text or colors on the screen is a common activity
for
screen readers; however, JAWS does more than just search-and-find. 
When JAWS successfully finds the desired string of text or color of
screen enhancement, it reads it and places the JAWS Cursor on it. 
The
user can then use the screen reading features of the JAWS Cursor to
read the data in detail.  This is very important when using a spell
checker because simply hearing the word is not sufficient.  When
spell
checking, JAWS users can easily read the misspelled word in context
by
reading full lines of text and then get the details of the spelling
and
capitalization with the Spell Word, Phonetic Pronunciation, and
other
cursor keys.  Other screen readers will read the data but will not
place
the cursor there.
 
    JAWS can search from the top of the screen to the bottom, from
the bottom to the top, or either up or down from the current cursor
location.  Searches can examine the entire screen or can be limited
to a
section of the screen.  Upper case, lower case, and graphic
characters
can be included in a search as well as screen attributes such as
color
and reverse video.  JAWS automatically senses if the computer is
using
a color or monochrome display and presents the proper selection of
color
or monochrome choices when starting a search.  All 16 colors, both
in
the foreground and background can be located.  These search
capabilities can allow the user to specifically define the search
criteria
and precisely zero-in on the exact piece of data to be read.  A
macro can
be used to streamline searches and put incredible power at your
fingertips.  The intelligence of JAWS macros can additionally
determine
if a search was successful and decide what to do.  If the search
was
successful, then the macro might automatically perform a secondary
search; otherwise, the macro might pause while the screen is
allowed to
change and then repeat the search again.
 
 
13. SMARTER THAN THE AVERAGE SCREEN READER
 
    Pull-down menus, pop-up windows, color bars, and unexpected
placement of important messages pose significant problems for most
screen reading software.  JAWS uses its Smart Screen, Smart Focus,
Color Bar Tracking, and other sophisticated Smart features to solve
screen access problems of unfriendly software.  The Smart features
can
find an error message that appears in a pop-up window when the
window overlaps text that was already on the screen, and can sense
when to read text by following a color bar and when to use a
cursor. 
When the Smart Screen feature is active, JAWS will constantly watch
the screen for changes.  When a change is sensed, JAWS evaluates
the
new information, determines how much of the new data should be
spoken, and then speaks the information automatically.  The Smart
Screen features are a powerful set of tools that work together to
solve
the problems that many other screen readers ignore.
 
 
14. SAY IT ALL AND STOP
 
    Screen readers are increasingly being used to read documents
that are stored on computer disk.  Books, computer manuals,
magazines,
company reports and numerous other text documents are being read
with JAWS and a computer.  The Say All feature facilitates
continuous
reading without interruption.  JAWS will stop reading as soon as
the
CONTROL key is tapped.  Many speech synthesizers have a feature
that
enables JAWS to place the cursor back on the last word spoken. 
This
means if the user interrupts continuous reading by tapping the
CONTROL key to reread a phrase or to answer the phone, then he or
she can resume reading with the last word that was spoken.  Users
can
also interrupt reading at any time, back up and listen to a phrase
again,
and then continue reading just by pressing a single key.
 
 
15. AUTOMATIC LOADING OF SPECIAL CONFIGURATIONS
 
    Each software program has a unique set of problems which must
be overcome to maximize the efficiency of screen reading.  Special
configuration files are provided with JAWS to solve the unique
problems
of many commonly used software packages.  Users can easily create
their own configuration files for software packages that are not
included
with JAWS.  Configuration files can be loaded, modified, or saved
at any
time through the JAWS Menu System and are automatically loaded
when starting application software programs.  Thus when you type
"WP"
to start WordPerfect, both the JAWS configuration for WordPerfect
and
the WordPerfect software are loaded.  When you exit WordPerfect,
then
the original JAWS configuration is automatically restored. 
Configuration files may contain a dictionary file to improve
pronunciation, synthesizer settings to fine tune speech, and
specific
macros needed for the application software.
 
 
16. STRONG COMPUTER COMPATIBILITY
 
    JAWS is fully compatible with the IBM PC, PC XT, PC AT, PS/2,
PS/1, and all other true IBM compatible personal computers.  JAWS
is
compatible with DOS versions 3.0 through 6.X.  DOS versions 5.0 or
6.0
are recommended, because they utilize a smaller amount of
conventional
memory and can be loaded into high memory on computers with
expanded memory.
 
    640K of conventional RAM memory is recommended because
many contemporary software programs utilize 300K to 500K of memory,
and JAWS, DOS, and application software will need to share
conventional memory.  Under special situations, JAWS can be loaded
into high memory.
 
    An 80 column color or monochrome monitor is required, and CGA,
EGA, VGA, and other high-resolution video boards are supported in
addition to the larger screen displays that exceed 25 lines and 80
columns of text.
 
    JAWS will run on a two floppy disk drive system (without a hard
drive), but we strongly recommend the use of a computer with a hard
drive.  Several commonly used application software packages will
occupy
one or more megabytes of disk space and may not run from a floppy
drive.  Additionally, a hard drive will free the user from the
frequent
requirement of changing floppy disks whenever the application
program
needs to load extra program functions or the user needs to find a
specific
data file.  
 
 
17. COMPREHENSIVE SPEECH SYNTHESIZER
    COMPATIBILITY
 
    A large number of speech synthesizers are supported by JAWS
and will accurately convert JAWS commands into speech.  They range
in price from a few hundred dollars to well over a thousand
dollars, and
have a variety of capabilities.  The most expensive synthesizers
will
usually have the most human sounding voice quality, although this
characteristic is not always the most important consideration when
selecting a speech synthesizer.  

18. GRAPHIC CHARACTERS AND GRAPHICS DISPLAYS
 
    Graphic characters, also known as extended ASCII characters,
are
frequently used by many application programs to draw boxes and
lines
on the screen while the video display remains in a standard text
mode. 
These characters will generally be ignored by JAWS, although they
can
be identified if needed.  Other video enhancements such as reverse
video
or colors are generally ignored until they need to be located by
the user.
 
    Application software programs cannot be allowed to use a pure
graphics display mode when JAWS is being used.  If the application
program uses a complete graphics mode for displaying text on the
screen, then JAWS will not function normally.  JAWS may still
operate
on a limited basis, but many of the screen reading functions and
the
JAWS Menu System will not function.  This means that drawing
programs, video games, and some publishing programs will not
operate
properly with JAWS.  Application software that uses standard text
modes, will be fully accessible to JAWS.  Thus, most word
processors,
spread sheets, and data bases will all work properly.
 
 
19. THE NEXT STEP IS INSTALLATION
 
    Now that you have been introduced to the power of JAWS, we
hope you are ready to begin using its many features.  The next
chapter
will help you get started by explaining how to use the JAWS
Installation Program to prepare JAWS for your computer.  The
remaining chapters in PART ONE, will present a quick reference for
screen reading and synthesizer set-up, and will describe the
learning
resources that blind men and women can use to put the full power of
JAWS to work for them -- on the job, in school, and at home.

 
