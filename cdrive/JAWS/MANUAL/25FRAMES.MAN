PART TWO:  Chapter 5
FRAME MANAGER
(Also contained in the disk file "25FRAMES.MAN".)
 
 
(Last Revision:  November 1993.)
 
 
 
CONTENTS OF CHAPTER 5
 
    1.   INTRODUCTION . . . . . . . . . . . .2-5.133
    2.   FRAMES MENU. . . . . . . . . . . . .2-5.135
         2.1  Revise. . . . . . . . . . . . .2-5.135
                   Sample Frame Definitions
         2.2  Speak . . . . . . . . . . . . .2-5.138
         2.3  Activate. . . . . . . . . . . .2-5.139
         2.4  Quiet . . . . . . . . . . . . .2-5.140
         2.5  Monitor . . . . . . . . . . . .2-5.141
    3.   STRATEGY FOR QUIETING CHATTER. . . .2-5.142
    4.   LARGE SCREEN DISPLAYS. . . . . . . .2-5.143
    5.   PRIORITY OF FRAME DEFINITIONS. . . .2-5.144
    6.   SAVING FRAME DEFINITIONS . . . . . .2-5.145
1.  INTRODUCTION
 
    Many software application programs selectively use portions of
the screen to display certain information such as menu selections or
error messages.  These sections of screen are given names such as
pop-up menu, menu box, pull down menu, data field, status line, help
window, or any number of similar sounding names.  We will use the
term Window to generically represent any section of screen that a
software application isolates for a particular purpose.  The term window,
as it is used here, does not refer to the "Windows" software program; it
only refers to a portion of the screen display that appears to be
separated from the remainder of the screen.
 
    Screen colors, monochrome enhancements, and the decorative use
of lines and graphic symbols are used to made a window stand out from
the rest of the screen display.  Some windows, such as those used for
displaying information about a program's status, are fixed in a specific
screen location and are easily read with a screen reading program. 
Other windows that pop-up in a variety of locations and temporarily
replace a portion of the current screen display, are often unfriendly to
blind PC users.  Topics related to the use of screen colors and
monochrome enhancements in screen displays were discussed in the
preceding chapter.  Concepts from the preceding chapter and concepts
presented in this chapter will be used together to develop strategies for
working with pop-up windows.
 
    Pop-up windows do not have to be a major obstacle to blind PC
users.  JAWS, for example, uses a pop-up window to display the JAWS
Menu System.  The JAWS software illustrates that a pop-up window can
be designed for efficient use by both blind and sighted PC users, if the
windows are properly designed.  Unfortunately, most pop-up windows
are often a source of frustration for blind PC users.  Nevertheless,
pop-up windows are increasing in popularity and JAWS has the tools for
using them.
 
    One of the tools that JAWS uses is a tool called a frame.  A frame
can be thought of as a box that JAWS places around a window so that
screen reading can be more easily managed.  As will be explained, when
frames enclose pop-up windows and status lines, the information they
contain can be read from within the limits of the frame boundaries.  A
status line is a line of information that is usually located on the last
few or top few lines of the screen, that provides information about
software activity and files.  The status line could at times contain a
prompt, a menu display, a name of a text file, help information, etc.
 
    When frames are used, JAWS essentially draws a box around the
window of information and then focuses its attention on what is
happening within the box.  These boxes or frames are like picture
frames, they surround what is important, and give meaning to what is
enclosed within their boundaries.  
 
    JAWS provides a set of 100 frames that are controlled by the
frame manager.  Frame zero surrounds the entire screen and is not
designed for routine adjustment.  Frames 1 through 99 are initially
defined to surround the entire screen, but their dimensions can be easily
changed to enclose any portion of the screen.  The set of frames can be
saved in a JAWS configuration file and be used again whenever you use
a particular software program.  Information on saving JAWS frames is
briefly described at the end of this chapter.  A complete description of
configuration files is presented in the chapter on the configuration
manager contained in PART TWO. 
 
    Frames can be spoken, activated, revised, quieted, or monitored
by selecting "Frames" from the JAWS main menu.  Frame functions can
be thought of as the frame manager, because the functions work
together to organize and control frames.  Frames fill many needs.  For
example, a window that causes unwanted speech can be quieted.  A
window in the middle of the screen can be read without hearing the text
that is displayed to the left and right of the window.  Dozens of frames
can be used to build a system for reading important prompts as the
cursor moves from place to place on structured screen displays.  A frame
can be used to limit the area of a color search or text search and to limit
the movement of the JAWS Cursor when reading information.  A frame
can even be monitored and macros can be triggered when information
or screen attributes within the frame change.  The frames sub menu
also includes the menu for Smart Screen.  Smart Screen is a feature
designed for use with software that emphasizes the use of color bars and
menu displays.  Smart Screen will not be described in this chapter, but
will instead be addressed in a separate chapter in PART TWO of this
manual.
 
 
2.  FRAMES MENU
 
** begin figure **
 
JAWS frame menu
Speak  Activate  Quiet  Revise  Monitor  smarT
Revise the boundaries of a frame

** end figure **
  
    The JAWS frame manager is accessed by selecting "Frames" from
the JAWS main menu.  The choices offered by the frame manager are: 
"speak", "activate", "quiet", "revise", "monitor", and "smart".  "Smart"
refers to Smart Screens and is the topic for a separate chapter.
 
2.1 Revise
 
    Selecting "Revise" from the frames menu, and specifying a frame
number, begins the process of changing the boundaries of a frame. 
Initially, all frames are preset to the dimensions of the full screen.  It,
therefore, is usually necessary to refine the dimensions of a frame in
order for a frame to provide meaningful information when spoken,
activated, quieted, or monitored.  Since frame zero is intended to
surround the entire screen, then operations which require the entire
screen can routinely use frame zero.
 
    When "revise" is selected, the prompt "Enter frame number:" is
displayed on the selection line of the menu display.  The prompt is
followed by the number of the most recently revised frame.  Any number
from 1 to 99 can be entered at this prompt to identify the frame that is
to have its size and location adjusted.  If ENTER is pressed without
typing a different frame number, then the Referenced Frame Number
is used.  The term "referenced frame number" refers to the frame
number that was most recently revised.  
 
    To see which frame numbers have already been defined, select
"revise".  Then, when the prompt appears, and the most recently revised
frame number is referenced, you can use the Say Next Word or Say
Prior Word to read the frame numbers that have already been defined. 

 
    After you have identified a frame number and pressed ENTER,
then the current definition of the frame is displayed on the selection
line.  The selection line contains five items as illustrated below.  The
first 4 selections are for defining the boundaries of the frame, and the
last item is used to indicate your acceptance of the boundaries.
 
** begin figure **
  
Frame Revise
Top row: 0  Bottom row: 25  Left column: 0  Right column: 80  Accept
Enter the number of the upper row of the frame

** end figure **
  
    You can move the highlighted menu bar to any of the four frame
boundaries and modify the row or column number that is shown, or you
can let JAWS automatically move the menu bar from item to item as
you type a row or column number and press ENTER.  The help line
reminds you of the boundary you are defining.
 
    If you will be defining each of the frame boundaries, then just
type the appropriate numbers and press enter after each number.  The
highlighted menu bar will start with the entry at the left (top row) and
move to the right each time you press ENTER.  You begin with the top
boundary and move on to define the bottom boundary, left boundary,
and right boundary.  When a boundary on the selection line is
highlighted, then the value for that boundary can either be changed, or
accepted without modification by pressing ENTER.
 
    The preceding illustration shows the default boundaries that are
assigned to a default frame before it has been modified.  The default
boundaries form a frame that marks the top of the screen (row 1),
bottom of the screen (row 25), left most column (column 0), and the right
most column (column 80).  The top row number must naturally be less
than or equal to the bottom row number and the left column number
must be less than or equal to the right column number.  Frames can
overlap, touch, and completely surround one another.  In general, it is
a good practice to use higher numbers for frames that have very specific
functions, such as quieting and monitoring, and to use lower numbers
for frames that may be used for more general purposes, such as frame
activation, speaking, and searching.
 
    The standard cursor movement keys function as they do in other
menus.  The ENTER or DOWN ARROW can be used to complete an
entry and move to the next item.  The UP ARROW, ESCAPE, and other
menu movement keys can be used to abort the definition process.
 
    Once you have defined the four sides of the frame, you must then
confirm your definition by selecting "accept" from the menu.  You will
be automatically prompted to do so after you have defined the fourth
side of the frame.  If you only need to modify the location of one frame
dimension, then you can make that change and move the menu bar to
"accept" and press ENTER.  The definition is confirmed and you are
returned to the JAWS frame menu.  At this point, the frame definition
has been saved in your PC's memory, but it has not been permanently
saved in a configuration file for later use.  If you wish to use the frame
definition again the next time you load JAWS, then you must save the
configuration.  The last section is this chapter explains how to do this.
 
Sample Frame Definitions
    Three sample frame definitions are shown below, and an
explanation of the way each might be used is suggested.
 
a.  Top row:  1, Bottom row:  1, Left column:  0, Right column 80.
    This definition builds a frame around line one of the screen.  This
    might be used to read a status line that appears on line 1, or to
    read the screen titles that change as new screen displays are
    presented.
 
b.  Top row:  24, Bottom row:  25, Left column:  0, Right column:  80.
    This frame encloses the last two lines of the screen display, and
    might be used to read help information that is routinely displayed
    in that location for some software programs.  
 
c.  Top row:  2, Bottom row:  15, Left column:  5, Right column:  75.
    This is a frame that might be used with a pop-up menu.  Often
    a menu of this type pops up in the middle section of the screen
    and overlaps the existing screen display so that the text on the
    left or right of the window seems to run together with the text of
    the pop-up menu.  This frame could exclude both the surrounding
    text and decorative boarders of the pop-up window so that just
    the text within the pop-up menu could be read.  A frame of this
    type would likely be used with the frame activate function that
    will be described later in this chapter.
 
2.2 Speak
 
    Selecting "Speak" from the Frames menu and entering a frame
number, instructs JAWS to speak the contents of the specified frame. 
Any frame number, from 0 to 99 can be specified.  The most recently
referenced frame will be the default.  The most recently referenced
frame number is the last frame number that was used with the frame
functions:  "speak", "activate", "quiet", or "monitor".  If you use "speak"
to read frame 12, then the number "12" becomes the most recently
referenced frame when you next use these frame functions.  If frame
zero or an unmodified frame is specified, then the entire screen will be
read.  
 
    When "speak" is selected, the prompt "Enter frame number:"
appears on the selection line, and the default frame number is
referenced.  If ENTER is pressed without indicating a different frame
number, then the referenced frame is spoken.  Alternatively, if a
different frame number is entered, then the contents of that frame is
spoken, and the new frame number becomes the new referenced or
default frame number.
 
    It is not always necessary to indicate a frame number when using
the speak function.  JAWS can speak the frame that a cursor is in when
you supply one of the letters "A", "J", or "P" in the place of a frame
number.  The letter "A" means to speak the frame containing the active
cursor.  The letter "J" means to speak the contents of the frame
containing the JAWS Cursor.  The letter "P" means to speak the
contents of the frame containing the PC Cursor.  If the cursor you select
lies within more than one frame, as when frames overlap, then the
frame with the highest number will be spoken.
 
2.3 Activate
 
    Selecting "Activate" from the JAWS frames menu results in the
activation of the frame that you specify.  The Active Frame is a frame
that contains the boundaries that JAWS uses to restrict the movement
of the JAWS Cursor.  All cursor movement and screen reading that is
done with the JAWS Cursor is restricted to the section of screen
enclosed by the active frame.  The default active frame is frame zero
which includes the entire screen.  Thus the JAWS Cursor can read
anywhere on the screen.  If the active frame enclosed a small portion of
the screen, then the JAWS Cursor could only move within that small
area.  The PC Cursor is unaffected by the restriction of the active frame
and is free to move normally; subject only to the limitations of the
application software.
 
    When "activate" is selected, the prompt "Enter frame number:"
appears on the selection line.  The most recently referenced frame
number (last used frame) is displayed, and you can either use this
default frame number or indicate a different frame.  If ENTER is
pressed, then the referenced frame number is activated.  If a different
frame number is entered, then the new number is activated, and it also
becomes the new referenced frame number for other menu selections in
the frame manager.  Also, the letters "A", "J", and "P" can be used to
identify the frame to be activated.  The use of these letters was
presented above in the discussion of "speak".
 
    Any frame number, from 0 to 99 may be specified.  The most
recently referenced frame will be displayed as the default frame number
for this function.  The most recently referenced frame number identifies
the frame that was last used in the frame manager.
 
2.4 Quiet
 
    Selecting "Quiet" from the frame manager and specifying a frame
number will cause the specified frame to be silenced.  This option is
used to prevent unwanted speech output.  This stops information from
a specific frame from being automatically echoed to the screen and
spoken.  Programs such as word processors and some PC utility
programs frequently produce unwanted speech output, by writing to the
screen through BIOS.  A discussion of BIOS and screen echo was
presented in the chapter on the voice menu in PART TWO. 
 
    One example of unwanted speech might be the output generated
by the status lines of some word processing programs.  A new row and
column position is displayed, echoed, and spoken each time the cursor
moves.  This constant chatter would interfere with the ability to hear
important speech output.  Similarly, some utility programs display a
digital clock on the screen that causes the time to be spoken every time
the number of seconds or minutes change.
 
    Two methods can be used to eliminate unwanted speech activity. 
The first is to simply use the voice menu to turn off screen echo.  This
drastic solution has the unfortunate consequence of also silencing the
things that you may want to hear.  The second method, and certainly
the preferred method, is to quiet the portion of the screen that is
generating the unwanted speech.  This is done by defining a frame that
encloses the offending area and then to silence the frame with the
"quiet" menu selection.
 
    When "quiet" is selected, the prompt "Enter frame number:"
appears on the selection line.  The most recently referenced frame
number is displayed, which you can either use or modify, before pressing
ENTER to complete your entry.  The frame number you enter is
immediately silenced and unwanted speech stops.  If a frame number
other than the referenced frame number is entered, then the silenced
frame becomes the new default.  If a frame enclosed the entire screen
and that frame were to be quieted, then the entire screen would be
silenced.  If you need to silence the entire screen to stop unwanted
speech output, then it, of course, is better to set "screen echo" in the
voice menu to "off".
 
    Quieting a frame only prevents its contents from being
inadvertently spoken when data is written to the screen through BIOS
and spoken by the screen echo of JAWS.  If the frame number is
specified in a speak command, or if the JAWS Cursor is used within the
frame, then the data can still be read.  Before quieting a frame, it is
recommended that "speak" be used to listen to the contents of the frame
to insure that you are not quieting a larger section of the screen than is
necessary.
 
    Frame quieting can be canceled by entering the number of the
quieted frame in the "revise" function.  To remove quieting, select
"revise", enter the number of the quieted frame at the frame revise
prompt and press ENTER.  Next move the menu bar to "accept" and
press ENTER.  This accepts the dimensions of the frame as they were
originally defined and removes quieting from the frame.
 
2.5 Monitor
 
    When you select "Monitor", you can instruct JAWS to watch a
specific frame for changes.  Frame Monitoring means to periodically
examine the data and screen attributes contained in a frame, and to
activate a macro if something changes.  Frame monitoring is another
name for auto macros.  When a frame is monitored, JAWS watches for
changes in data, color, and monochrome enhancement, and when a
change is observed, JAWS activates a macro.  The macro might search
the screen, read the contents of the frame or perform any number of
other activities.  This is a very powerful JAWS feature, and is at the
heart of many automatic screen reading routines.  The JAWS function
that ties a macro to the monitoring of a frame is available in both the
frame manager and within the macro manager.  These functions, frame
monitoring and auto macros, are identical functions.  The two functions
are located within different menus and provide convenience to JAWS
users when defining frames or editing macros.  To fully understand how
these functions work, it is necessary to understand how macros work. 
The subject of macros is presented in a separate chapter within PART
TWO, and as a result, we will present the discussion of frame
monitoring and auto macros in that chapter.
 
 
3.  STRATEGY FOR QUIETING CHATTER
 
    Let us assume that you are attempting to configure JAWS to
work with a word processing program that uses line 1 of the screen for
the status line, and that this program has the unfortunate feature of
speaking all changes that occur on the status line.  This means that any
time a letter is typed or the PC Cursor is moved, then the new
coordinates are spoken.  The Read Top Line function can be used to read
the status line whenever the user wishes to do so, but the uninvited
speaking of cursor coordinates never stops.  
 
    The unwanted speech output can be stopped by defining a frame
around the status line and then quieting the frame.  This can be done
by invoking the JAWS Menu System and following these steps.
 
a.  Press F (to display the frames menu).
 
b.  Press R (to use the revise function for defining a frame).
 
c.  Type a different frame number than the default frame number
    that is offered (such as 95) and press ENTER.  If this frame is
    undefined, the coordinates - Top row:  1, Bottom row:  25, Left
    column:  1, and Right column:  80, should appear.
 
d.  Since, in this example, the status line occupies all of line 1 and
    the cursor cannot normally move there, then we can silence the
    entire line without interfering with other speech activities.  Enter
    the following boundaries to define a frame that contains all of line
    one, and press ENTER to "accept" the definition.  
 
         Top row:  1   Bottom row:  1
         Left column:  1Right Column:  80
 
e.  Type S (to select speak).  The referenced frame number should be
    the frame you just revised, so simply press ENTER.  JAWS
    should speak all of line one, which should include all the
    information that you wish to silence.
 
f.  Assuming the frame now properly surrounds the status line, it is
    time to silence the frame.  Activate the JAWS menu System again
    and press F (for frames).
 
f.  Press Q (for quiet), and Press ENTER.  This will select and quiet
    the referenced frame with which you have been working.
 
h.  Now exit the JAWS Menu System.  The uninvited speech output
    has been silenced.  You can still read line 1 with the Say Top
    Line or with the Read Line X function whenever you wish, but
    unwanted chatter has stopped.
 
 
4.  LARGE SCREEN DISPLAYS
 
    Some screen displays are larger than the standard 25 rows (lines)
by 80 columns.  JAWS automatically determines the dimensions of your
screen display when your PC boots-up, and adjusts the default active
frame to fit those dimensions.  Some software application programs,
however, change the screen size when you load them, or even as you use
them.  If this happens to you, then you can use the frame activate
function to reactivate frame zero.  JAWS uses this opportunity to
reinterpret the size of the screen display.  
 
    Older versions of JAWS (version 2.14 and earlier) require a
different technique to inform JAWS of the new screen size.  In this
situation you would use the revise function to modify a frame such as
frame 1.  When prompted for the frame dimensions, you should supply
the full dimensions of the larger screen display.  These dimensions could
be something such as:  top row:  1, bottom row:  43, left column:  1, right
column:  133.  The next step is to activate the frame and make it the
new default frame.  You should then save the new frame definition so
it can be automatically loaded when you next use the software
application that uses the large screen display.  You may also need to use
this technique for newer versions of JAWS if the method for reactivating
frame zero, described earlier, does not work properly because of non
standard video cards.
 
 
5.  PRIORITY OF FRAME DEFINITIONS
 
    Occasionally, situations arise when it is important to know what
will happen if two or more frames overlap.  JAWS uses a Frame
Priority System to determine which frame is used for specific activities. 
Priorities are based on frame numbers.  Lower numbered frames have
a lower priority than do higher number frames.  Frame "99" has the
highest priority while frame "0" has the lowest priority.  The results of
two potential frame number conflicts are shown below to illustrate the
relationship between frame numbers.
 
a.  What happens if you use the letters "J", "P", or "A" to identify the
    frame you wish to speak, activate, or monitor; and the cursor is
    located within the overlapping area of two or more frames?  In
    situations where the location of the specified cursor could
    potentially target more than one frame, JAWS simply selects the
    frame that has the highest number.  You can verify the frame
    number that was used when you selected one of these frame
    identification letters, by returning to the frame manager and re-
    selecting the frame function.  The prompt on the selection line
    will display the most recently referenced frame number, which is
    the frame number that was selected in response to your last use
    of "J", "P", or "A".  
 
b.  What happens if a quieted frame is used to silence line 1 of the
    screen, and a new frame (that includes line 1) is revised or
    activated?  The quiet status of a frame is independent, and is
    maintained while other frames are modified.  The quieting of a
    specific frame remains in effect until the quieting is deliberately
    removed.  Quieting is removed by selecting "revise" from the
    frame manager and reconfirming the definition of the frame by
    pressing ENTER to accept the frame boundaries and to indicate
    your acceptance of the definition.
 
 
6.  SAVING FRAME DEFINITIONS
 
    If you have defined frames that you wish to save for future use,
then follow the steps presented below for saving frame related
information.
 
a.  Invoke the JAWS Menu System.
 
b.  Press C (for configuration files).
 
c.  Press S (for save).
 
d.  Press F (for frame configuration file).
 
e.  Press ENTER (to accept the default file name for the frame
    configuration file).
 
f.  Press ESCAPE (to exit the menu system).
 
