PART FIVE:
Installation Reference 4
TOSHIBA COMPUTERS
(Only contained in the disk file "54TOSH.MAN".)
 
 
(Last revision:  1992.)
 
 
 
CONTENTS OF INSTALLATION REFERENCE 4
 
    1.   INTRODUCTION
    2.   ORIENTATION TO THE TOSHIBA KEYBOARD
    3.   TOSHIBA COMMAND LINE PARAMETER: 
         "/T"
    4.   CREATING MACRO FILES FOR EXTENDED
         KEYBOARDS
    5.   RESUME MODE
    6.   TOSHIBA 3100
    7.   TOSHIBA 1000 SE
    8.   TOSHIBA 1200
1.  INTRODUCTION
 
    The Toshiba family of computers are different from most other
PC's in several ways.  As a result, we provide the following
information
to help with the installation of software and hardware, and to
orient
new Toshiba users to the very unique functions of the Toshiba's
keyboard.  Toshiba produces a wide selection of computers, each of
which has slightly different features.  We cannot address each
model in
this installation reference but we will present general information
that
applies to all of them, and offer some specific information on
representative models.
 
    The installation process described in PART ONE of this manual
can be used with Toshiba computers.  Be sure to indicate that you
will
be using a Toshiba keyboard when the installation program prompts
you
for keyboard information.  You will receive instructions about
copying
device drivers and modifying your CONFIG.SYS files during the
installation process.  Be sure you copy the device drivers as
instructed. 
Failure to do so will prevent JAWS from speaking.
 
 
2.  ORIENTATION TO THE TOSHIBA KEYBOARD
 
    The easiest way to find the JAWS keys on the Toshiba keyboard
is to turn on Help Mode.  Hold down the INSERT key (usually the
second key to the right of the SPACE BAR) and press the "1" on the
numbers row (above the "Q").  You can explore the keyboard, and the
keys will speak their names when pressed.  The Toshiba key board
layout is presented below:
 
Prior Line = UP ARROW
Next Line = DOWN ARROW
Say Current Line = SHIFT + UP ARROW
Prior Word = SHIFT + LEFT ARROW
Next Word = SHIFT + RIGHT ARROW
Say Current Word = SHIFT + DOWN ARROW
Prior Character = LEFT ARROW
Next Character = RIGHT ARROW
Say Current Character = SHIFT + END
Say up to Cursor = SHIFT + ENTER
Say from Cursor = SHIFT + PAGE DOWN
JAWS Cursor on = Key in top right corner, "sys Request".
PC Cursor on = Second from right top corner, "print screen".  
    (On some models, like the T3100, the JAWS and PC Cursor keys
    are reversed.)
Say all = CONTROL Sys Request, usually the top right key.
Read top line = SHIFT + HOME
Read bottom line = SHIFT + PAGE UP
Route JAWS to PC = SHIFT + JAWS cursor on, top right corner
Route PC to JAWS = SHIFT + PC Cursor on, second from top right
corner.  
Position of active Cursor = Period/Delete, to left of left arrow.
Position of in-active Cursor = INSERT + Period/Delete 
 
    With the help mode on, you can also discover the locations of
special keys for certain applications, like the WordPerfect macros
for the
spell checker or the files menu/revealed codes mode.  Try
control-up
arrow, control-down arrow, and alt-end for some of these functions.
 
    Be sure that num lock is off for best performance, having it on
may produce different keys, especially SYS REQUEST and PRINT
SCREEN.  If you do not use the "/T" command line parameter
described
below, then the keys will not perform as indicated.  Read the
section on
the Toshiba Command Line Parameter for more information about this.
 
 
3.  TOSHIBA COMMAND LINE PARAMETER:  "/T"
 
    The "/T" command line parameter gives JAWS access to the
extended Toshiba keys.  If you do not use this, the keyboard will
not
have access to all the necessary JAWS keys.  This parameter is
automatically placed in your JAWS.BAT file when you use the
installation program and specify that you will be using a Toshiba
keyboard.  If you do use the "/T" and still have trouble you may be
using
a JAWS Macro File that has not been updated for use with the
extended
keyboard.  If your DOWN ARROW says "down arrow" instead of acting
like a Say Next Line function, then it has not ben updated to work
with
the extended keyboard.  You can use a newer version of the macro
file
(from a more recent JAWS update), or update your own files
yourself.
 
 
4.  CREATING MACRO FILES FOR EXTENDED KEYBOARDS
 
    On a Toshiba the extended keys are the regular arrow keys
(below
the ENTER key), the four cursor keys to the right of the ENTER key
(HOME, END, PAGE UP/DOWN), and the keys on the top row to the
right of the function keys.  Naturally this varies from Toshiba
model to
model since none of them are the same.  The non-extended keys are
the
imbedded numeric/cursor pad, accessed by holding down the "FN" key
which is left of the space bar.  When num lock is off, this
imbedded pad
or "overlay" is a cursor pad with all the JAWS speech pad
functions. 
When num lock is on it becomes a numeric pad.
  
    The correct set of macro files will be installed when you use
the
JAWS installation program.  If you need to update your own macro
files
simply use the macro duplicate function in the macros menu.  When
it
asks for the key to be duplicated hold down the FN key and press
the
desired speech pad key (see list below), when it asks for the key
to
receive the definition press the desired key or key combination,
e.g.  the
up arrow key for Prior Line.  Use this technique to transfer the
JAWS
keys in the imbedded overlay to the key you want.  The layout we
have
chosen is described in the section "Toshiba keyboard layout".  You
can
follow this setup or create your own.  Of course you do not have to
use
the Duplicate function, in some cases it may be easier to simply
re-create each key yourself with the Macro Revise menu.
 
Toshiba imbedded/overlay cursor keys (num lock off):
 
Home = fn-7
Prior Line = fn-8
Page Up = fn-9
Prior Character = fn-U
Say Current Character = fn-I
Next Character = fn-O
JAWS cursor on = fn-P
End = fn-J
Next Line = fn-K
Page Down = fn-L
PC Cursor on = fn-semicolon
Route JAWS to PC = shift-fn-P
Route PC to JAWS = Shift-fn-semicolon
Position of active Cursor = fn-Period
Position of in-active Cursor = insert-fn-Period
 
    If you are looking for a key/function we have not mentioned,
just
turn on Help mode and look around for it by pressing keys until you
hear the one you want.  If you know the Desk Top macro layout you
will
have an idea where to look and what key combination is likely to
contain the JAWS function you want.
 
 
5.  RESUME MODE
 
    Some Toshiba lap top computers have a "Resume Mode" which
enables the user to turn the computer off and on without rebooting. 
The
active copy of JAWS and the application software you are using are
all
saved each time the computer is turned off.  When you turn it back
on,
every thing is restored just as you left it.  Various Toshiba
models use
different set up utilities to toggle between resume mode and boot
mode. 
Information on setting resume mode for the Toshiba 3100 and 1000 is
presented in this chapter.  If you are using a different Toshiba
model,
then please refer to your Toshiba users manual for information on
using
resume mode.
 
 
6.  TOSHIBA 3100
 
    If you have a Toshiba 3100 lap top you may want to use the hard
drive for your JAWS software.  The installation program will
install
JAWS to the hard drive
 
    You may also want to use resume mode.  To set up resume mode,
type "c:test3", enter "0", and press ENTER.  Move the cursor down
to
"Resume mode" and use the LEFT ARROW or RIGHT ARrOW to select
"resume".  Press F10, then "9", then ENTER.  Now when you turn it
off
you will hear two beeps, this means that you are in resume mode. 
When you turn it back on it will not need to re-boot, it will
already be
up and running where you left it.
 
 
7.  TOSHIBA 1000 SE
 
    If you have a Toshiba 1000 SE lap top you may want to use the
RAM drive for some of your JAWS software.  We recommend that you
leave the JAWS program itself on the floppy drive:  This is named
"J_ACNTPC.EXE" for the Accent synthesizer, or "J_SONIX.EXE" if you
have a Symphonix synthesizer.  You should also leave the Accent
driver
"SPKEMS.DVC" or the Symphonix driver on the floppy.  These files
take
up a lot of memory and you do not need them on the RAM drive.
 
    If your system was not installed by Henter-Joyce follow these
steps:
 
1.  Install the synthesizer in the modem slot.  This can be very
    complex so be careful.
 
2.  Boot up your system without a floppy in the disk drive.
 
3.  Create a boot disk:  put in a new diskette in the floppy drive;
type
    "c:" press ENTER, then type "format A:/s".  If the PC says you
    have the wrong parameters then you probably do not have a high
    density floppy disk.  High density floppies have 1.4 megabytes,
    low density have 720 kilobytes of storage.  Use the syntax
below
    for low-density floppies.
 
Type "format a:/s/t:80/n:9"
 
 4. Then copy all the files from the JAWS distribution disk:  Put
the
    JAWS disk in the disk drive; Enter "xcopy a:\ b:\/s"; when it
says
    to put in the destination disk or the "B" disk put in the
    newly-formatted disk we did above; When it asks to put in the
    source or "A" disk put in the JAWS disk again; Repeat these
steps
    until the copy is complete.
 
5.  Now copy the autoexec.t1s file and the config.t1s file to the
proper
    file names (this step may not be necessary, the install
procedure
    may have already done it).  The following commands will do
this,
    if you do not want to use our configuration then you should
look
    at our files and add the appropriate commands to your files to
get
    the configuration you want.
 
Type "copy \autoexec.t1s \autoexec.bat" add press ENTER. 
Type "copy \config.t1s \config.sys" and press ENTER. 
 
    Now you should have a boot disk that will also load up the
Accent
driver and JAWS.  Re-boot the computer with the JAWS boot disk in
the
A drive and let it load up with speech.  You should hear it say
"Accent
Ready" followed by your name and the JAWS welcome message. 
 
    If your computer is already in Resume mode you will need to
press the reset button just behind the power supply input
connection. 
Use a skinny object to press in the recessed button inside the
small hole. 
It will beep once, press enter, then wait for it to load after the
next
beep.    Then we want to format the D drive and copy the JAWS
utilities over to it:
 
1.  Enter "c:format d:", press "y" when asked and press enter.  It
may
    ask for the current volume label, just type it in.  If you do
not
    know this press enter, let it abort, then enter "c:label d:"
and it
    will tell you the current label.  Then repeat the step above to
    format the D drive.  
 
2.  Enter "a:jcopyse", this will copy the JAWS utility files over
to the
    D drive.
 
3.  Re-boot the computer with your JAWS boot disk in the a drive. 
    It should boot up with speech and be defaulted to the D drive.
 
    Now you should be able to use the JAWS utilities like the macro
editor without having the boot floppy in the drive, it will be
loaded from
the RAM drive.
 
    You should at this time set up the Toshiba to use "resume"
mode: 
enter "c:setup10"; move the cursor down to "Resume mode"; use left
or
right arrow to select "resume"; press enter and "y".  Now when you
turn
it off you will hear two beeps, this means that you are in resume
mode. 
When you turn it back on it will not need to re-boot, it will
already be
up and running where you left it.  Just give it about 5 seconds and
it
will be ready to speak.  
 
T1000SE Keyboard
 
    When loading JAWS for your Toshiba 1000SE use the "/T"
parameter.  This will enable the extended keyboard support except
that
the insert key will remain the JAWS shift key.  The two keys on the
top
row, top right, Pause (on the right) and Print Screen (second from
the
right) are now the JAWS Cursor and PC Cursor respectively.  With
the
Shift-Key Down these become Route JAWS To PC and Route PC To
JAWS.  
 
    The function keys F10, F11, and F12 do double duty with the FN
Key (the first key to the left of the space bar).  If you hold down
FN: 
Function Key 12 becomes Scroll Lock On/Off, Function Key 11 becomes
Num Lock On/Off, Function Key 10 becomes Overlay On/Off.  The
"Overlay" is the imbedded numeric pad:  J K L become 1 2 3, U I O
become 4 5 6, etc.  If the Overlay is On, these keys take on their
"Overlay" function (numbers or cursor Pad keys).  When overlay is
Off
these keys resume their letter key functions.  Holding down the FN
Key
reverses the Overlay status:  with Overlay Off, holding down the FN
Key and pressing a J will get you a 1.  
 
    Function Key 11 with the FN Key held down toggles Num Lock
On/Off.  If Num Lock is On the imbedded numeric pad will give you
numbers, with Num Lock Off the imbedded numeric pad will act like
a
cursor pad (Arrow Keys, Home, End, Page Up, Page Down, etc.) 
 
    JAWS will say, "Num Lock" for both Function Key 10 and
Function Key 11 when the FN Key is held down.  This "FN" Key is
unique to Toshiba and it does not use the center keyboard port. 
Therefore JAWS cannot sense when the FN Key is down and "special"
things are about to happen.  Thus it recognizes both Function Key
10
and 11 as the Num Lock Key when the FN Key is down.  It will say,
"Num Lock On" or "Num Lock Off." Just remember that when you are
pushing Function Key 11 with the FN Key you are toggling Num Lock,
and when you are pressing F10 with the FN Key you are toggling the
Overlay.  
 
Toshiba 1000 SE JAWS Keyboard Summary
 
JAWS Cursor On = Pause.
PC Cursor On = Print Screen.
Scroll Lock On/Off equals FN-F12.  
Num Lock On/Off = FN-F11.
Overlay (Imbedded Numeric Pad) On/Off = FN-F10.
 
    The other "Cursor Pad" keys, like Error keys, Home, End, Page
Up, Page Down, have the same function as the generic Toshiba
keyboard.  The embedded numeric pad/cursor pad (hold down the FN
Key and press the right hand letter key) has a speech pad layout
similar
to the JAWS for desk top computers.
 
    The FN Function Keys 10, 11, and 12 are not extended keys, they
work the same whether we use the "/T" command line parameter or
not. 
However, the Insert, Delete, the Arrow Keys, Home, End, Page Up,
Page
Down are all extended keys.  Thus the normal Arrow Keys, Home, End,
Page Up, Page Down can be defined differently than the Overlaid Num
Pad Keys.  This gives you a whole extra set of keys if you hold
down the
FN Key or when you enable the Overlay.
 
 
8.  TOSHIBA 1200
 
    The keys on the top right corner of the keyboard, from right to
left, should be:
 
JAWS Cursor On (Sys Request)
PC Cursor On (star)
 
 
Scroll Lock On/Off, and Num Lock On/Off
 
    When Num Lock is Off you will get the "JAWS Keys" such as
JAWS Cursor On/Off or PC Cursor On/Off.  When Num Lock is On you
will get Sys Request and Star.  Also, if Num Lock is on the Insert
Key
will be an insert, instead of the special JAWS shift key. 
Therefore, for
normal operation you will want to have Num Lock turned off.  
 
    The Num Lock Key (just to the right of F10, or to the left of
the
Scroll Lock Key) also toggles the numeric keypad overlay.  If you
hold
the FN Key (to the left of the space bar) and press the Num Lock
Key
it will toggle (turn on/off) the embedded overlay.  When the
embedded
overlay is on the J K L Key become 1 2 3, and the keys above that
are
4 5 6, and above that 7 8 9.  If the Overlay is turned on and Num
Lock
is turned off, then the J K L keys become End, Down Arrow, and Page
Down.  And the keys above that are similar to the keys on a regular
numeric pad.  When the Overlay is turned off you will get the
regular
letter keys.  If the Overlay is turned off but you hold down the FN
Key,
then you will get the Overlay Keys.  They will be numbers if Num
Lock
is on, and they will be Cursor Pad Keys if Num Lock is turned off. 
This
can be a bit confusing, of course, but it is the way Toshiba's are
designed.  Unfortunately, toggling the Overlay does not say
anything,
so you just have to press the keys and then test to see which is on
or off.
