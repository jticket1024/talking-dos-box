FULLTALK.DOC
APRIL 19, 1994

FullTalk is a program for sending and receiving Telephone Device
for the Deaf (TDD) messages on your computer.  It works with
JAWS, so that a blind person using the computer can communicate
with a deaf person using a TDD.  The file named "FULLTALK.JEF"
makes the interaction more convenient.

The FullTalk disk comes with configuration files for JAWS.  These
should be copied into the JAWS subdirectory.  Then the file from
the JAWS distribution disk, named "FULLTALK.JEF", should be
copied into the JAWS subdirectory.  This file will be loaded with
the other configuration files.  It will cause JAWS to speak each
word received from the TDD as soon as the space character
following the word is received.  Please refer to the FullTalk
documentation for more details on installing and using JAWS with
FullTalk.

We also recommend that you load JAWS with the "/I60" command line
parameter.  This will cause JAWS to wait about 3 seconds before
it automatically speaks data received from the TDD.  Without this
JAWS will only wait about 1/5 seconds, which might cause
incomplete words to be spoken, i.e. the words will be broken up. 
Naturally this time delay is adjustable, each number represents
about 1/20 of a second, so after you test it with "/I60" you can
adjust it to suit your needs.

Please call us for help or comments.  This combination of JAWS
and FullTalk is quite rare, so we do not have much test
information or feedback so far.  We will be glad to assist you to
make it work to your liking, and we welcome your comments.
