PART SIX:  Update 3
ABOUT JAWS VERSION 2.3
(Only contained in the disk file "63JAWS23.MAN".)
 
 
(Last Revision:  February 1993.  Information about changes made to
the JAWS software since this date can be found in the file
"CHANGES.DOC".  If you installed JAWS using a standard
installation, then this file would be located in your "\JAWS"
subdirectory.)
 
 
 
CONTENTS OF UPDATE 3
 
     1.   INTRODUCTION
     2.   NEW FUNCTIONS FOR READING TEXT
          2.1  Say Sentence
          2.2  Say Prior/Next Sentence
          2.3  Say Paragraph
          2.4  Say Prior/Next Paragraph
          2.5  Prior/Next Sentence and Prior/Next
               Paragraph
          2.6  Sentence and Paragraph Macros 
          2.7  Units of Text
          2.8  Say Unit
          2.9  Toggle Units
          2.10 Set Units
          2.11 Say Prior/Next Unit
          2.12 Say Units Macros 
     3.   NEW FEATURES FOR SAY ALL
          3.1  Rewind
          3.2  Skip Ahead
          3.3  Changing Voice Characteristics
     4.   NEW THINGS TO DO WITH MACROS
          4.1  Time and Date
          4.2  Margin Bell
          4.3  Slow Speech Rate for Spelling Words
          4.4  Beep
          4.5  CONTROL + UP ARROW
     5.   NEW SETTINGS IN THE JAWS VOICE MENU
          5.1  Dual Interruptability
          5.2  Line Pauses
          5.3  Standby or Sleep Mode
     6.   NEW FEATURES FOR FINDING SCREEN
          COLOR AND TEXT
          6.1  Testing for a Color/Attribute or a
               Character String
          6.2  Case Sensitive String Searches
     7.   A NEW TYPING SAFEGUARD
     8.   CREATE A TEXT COPY OF A MACRO FILE 
     9.   ON-LINE DICTIONARY EDITOR
          9.1  Create a Dictionary Rule
          9.2  Modifying a Dictionary Rule
          9.3  Removing a Dictionary Rule
          9.4  Saving Dictionary Rules
          9.5  Single Character Dictionary Rules
     10.  NEW FEATURES FOR THE WORDPERFECT
          CONFIGURATION
          10.1 Additional DELETE Key
          10.2 Read the Blocked Text
     11.  LOTUS 1-2-3 MACROS
     12.  VOICE CONFIGURATION FILES
     13.  BUG FIXES AND OTHER CHANGES
          13.1 Smart Screen Frame Number
          13.2 Beep on Routing
          13.3 Extended Arrow Keys
          13.4 Macro Pause
          13.5 Menu Display and Backspacing
          13.6 Keyboard Word Echo
          13.8 Single Character Prompts
          13.9 Speaking Punctuation Symbols
          13.10     Macro Duplicate and Macro Erase
          13.11     Place Cursor on the Last Word
                    Spoken
          13.12     Pitch Changes
          13.13     Pausing for Punctuation
          13.14     Kurzweil Personal Reader
          13.15     Multi Voice
          13.16     Prose 4000
1.   INTRODUCTION
 
     This appendix contains notes on new features that have been
introduced in JAWS version 2.3, as compared to JAWS version 2.2. 
The
information presented here is not designed to introduce new JAWS
users to the JAWS program.  Chapter 1 in PART ONE, (The Power of
JAWS) presents an overview of JAWS.  This chapter will familiarize
JAWS users with some of the new features of JAWS version 2.3.
 
 
2.   NEW FUNCTIONS FOR READING TEXT
 
2.1  Say Sentence
 
     This reads the sentence the cursor is in and does not move the
cursor.  A "Sentence" is defined as a block of text that ends with
a
period, question mark, or exclamation point followed by:  two or
more
spaces, a single digit (footnote number), a quote mark, or a blank
line. 
If the cursor is on a blank line it says "blank" when this function
is
used.
 
     If the PC Cursor is active, then JAWS will scroll the screen
to
find the start or end of a sentence, assuming that the application
program lets the screen scroll.  If the screen does not scroll,
then JAWS
will beep to indicate the failure and read what it can.  This also
happens
at the top or bottom of a document, because the screen does not
scroll.
 
2.2  Say Prior/Next Sentence
 
     This moves the cursor to the start of the sentence you are in
and
speaks it.  If you are already at the start of a sentence it will
move to
the start of the prior sentence and speak it.  The Say Next
Sentence
function moves the cursor to the start of the next sentence and
speaks
it.
 
2.3  Say Paragraph
 
     This function speaks the paragraph the cursor is in.  A
"Paragraph" is defined as a block of text that is bordered by a
blank
line, at the top and bottom.  JAWS does not recognize the indent at
the
start of a paragraph, it must see a blank line at the start and the
end
of the paragraph.  As with Say Sentence, the paragraph functions
try to
scroll the screen to find the start or end of the paragraph.  If
the screen
does not scroll, then it assumes it has found the start or end of
the
paragraph.
 
2.4  Say Prior/Next Paragraph
 
 
     The Say Prior Paragraph function moves the cursor to the start
of the paragraph you are in and speaks it.  If you are already at
the
start of a paragraph, then it will move to the start of the prior
paragraph and speak it.  The Say Next Paragraph function moves the
cursor to the start of the next paragraph and speaks it.
 
2.5  Prior/Next Sentence and Prior/Next Paragraph
 
     These functions move the cursor, but do not speak.  We provide
all these features/choices to give the maximum power and
flexibility to
the user; a HJ trademark.  For extra power you can combine the
"move"
and the "speak" functions in one macro.  If you prefer to use your
word
processor's paragraph logic you can combine the word processor
command for prior paragraph with the JAWS function for Say
Sentence,
thus reading the first sentence of each paragraph.  Or combine a
"move"
function like prior sentence with a "speak" function like Say Word,
thus
you will hear only the first word of each sentence.  The sentence
and
paragraph functions can be found in the macro revise program, with
the
other JAWS macro functions.
 
2.6  Sentence and Paragraph Macros 
 
Say Sentence = SHIFT + 5 (speech pad) 
Say Prior Sentence = SHIFT + LEFT ARROW
Say Next Sentence = SHIFT + RIGHT ARROW
Say Paragraph = SHIFT + PLUS
Say Prior Paragraph = SHIFT + PAGE UP
Say Next Paragraph = SHIFT + PAGE DOWN
 
2.7  Units of Text
 
     A "Unit of Text" can be a character, word, field, line,
sentence or
paragraph.  This feature has been enhanced with version 2.3 to be
much
more useable.
 
2.8  Say Unit
 
     This function speaks the current unit of text.  The unit of
text
that is spoken depends on the text unit that has been set.  You can
change the unit with the Toggle Units function or the Set Units
functions.
 
2.9  Toggle Units
 
     This function toggles or switches the text unit setting from
Characters to Words to Fields to Lines to Sentences to Paragraphs.
The
unit changes to a new setting each time you press the defined key.
 
2.10 Set Units
 
     This sets or turns on a specific unit of text, and determines
what
is read by the Say Units function.  The actual function names are: 
Set
Character Units, Set Word Units, Set Field Units, Set Line Units,
Set
Sentence Units, and Set Paragraph Units.  Naturally the speech
produced when the Next Unit and Prior Unit functions are used will
be
based on the unit that is selected with this function.
 
2.11 Say Prior/Next Unit
 
     These functions move the cursor up or down and speak the unit
of text.  The unit of text is selected by Toggle Units or Set
Units.  When
Character Units, Word Units, and Field Units are selected, the
cursor
moves up or down, before speaking the unit.  This may be different
than
you might expect.  This behavior makes it easy to read up/down a
column and read the character or word or field where the cursor
lands.
 
2.12 Say Units Macros 
 
     In the default JAWS 2.3 macro file, (JAWS.JMF), the Unit
functions are situated as follows:
 
Say Unit = SHIFT + MINUS
Say Next Unit = SHIFT + DOWN ARROW
Say Prior Unit = SHIFT + UP ARROW
Toggle Units = SHIFT + HOME 
 
 
3.   NEW FEATURES FOR SAY ALL
 
3.1  Rewind
 
     During Say All, you can press the speech pad MINUS and JAWS
will rewind (back up) the cursor about 20 words from the last word
spoken, and then resume reading.  This is very handy for those
little
items you did not quite understand and want to re-read.  This only
works with indexing synthesizers:  DECtalk, Accent, Braille 'n
Speak,
Audapter, Kurzweil, and Prose.
 
3.2  Skip Ahead
 
     The speech pad PLUS key moves the cursor ahead about 20 words
and then reading continues during Say All screen reading.  You can
still
use the ALT key to skip ahead as was the case with JAWS 2.2.  Press
it once to silence and skip ahead, press it again to re-start
speaking. 
The CONTROL key continues to stop Say All reading as it did with
earlier versions, and still places the cursor on the last word
spoken. 
This feature can only work with indexing synthesizers.
 
3.3  Changing Voice Characteristics
 
     When Say All reading is in progress, the F1 through F4 will
increase the volume, rate, pitch and tone of the synthesizer.  F5
through
F8 decrease these voice characteristics.  These keys return to
their
standard functions as soon as Say All speaking has been stopped. 
When
you press one of these keys during Say All, JAWS will route the
cursor
back to the line containing the last word spoken, change the voice,
and
then continue reading.  Naturally it will repeat a few words, just
to
maintain your rhythm and place in the text.
 
 
4.   NEW THINGS TO DO WITH MACROS
 
4.1  Time and Date
 
     A Time function and a Date function can be put into a macro to
speak the current time or date.
  
4.2  Margin Bell
 
     This new feature beeps when the cursor hits the margin you
set. 
To set the Margin Bell, move the cursor to the column where you
want
to hear the margin bell and press this macro key.  If it was "off",
then
it will be turned "on" and record the column.  If it was "on", then
it will
be turned "off".  When it is "on" and you are entering data, JAWS
will
beep whenever the PC Cursor moves past the column that was set. 
This
macro is INSERT + B in the default JAWS.JMF macro file.
 
4.3  Slow Speech Rate for Spelling Words
 
     JAWS will now automatically slow down the speech rate when
you spell a word.
 
4.4  Beep
 
     This macro function causes the PC to beep.  When you put this
function into a macro, then the computer will beep when the macro
processor gets to the Beep function.  It is useful to indicate an
error or
failure of some sort.
 
4.5  CONTROL + UP ARROW
 
     This macro is used after the DOS "DIR" command has been used
to display the names of files in a directory.  The macro finds and
reads
the first file name in a DOS directory listing, assuming it is
visible on
the screen.  This macro is just a little time saver that
demonstrates the
kinds of things that can be accomplished when a bit of creativity
is
combined with the flexibility of JAWS macros.
 
 
5.   NEW SETTINGS IN THE JAWS VOICE MENU
 
5.1  Dual Interruptability
 
     JAWS has always had the ability to interrupt synthesizer
speech,
so you do not have to wait until everything is spoken.  This
parameter
was found in the keyboard voice menu and in the speech pad voice
menu, but the functions were combined, not independent.  Thus, when
you set one you were also setting the other.  Now the functions are
separate and independent.  the keyboard interruptable setting
affects
the letter and number keys on the "typewriter" keyboard, and the
speech pad interruptable setting affects the speech pad keys. 
Thus, you
can quickly read around the screen with the speech pad keys (speech
pad interruptable "on"), and then also here every letter or word
you type
(keyboard interruptable "off").  Alternatively, you can read around
the
screen slowly, hearing every item (speech pad interruptable "off")
and
have the flexibility to type fast while the speech keeps up with
your
typing (keyboard interruptable "on").
 
5.2  Line Pauses
 
     Some synthesizers are very noticeable when they start to
speak,
it sounds as if there is a period or end of sentence.  JAWS pauses
at the
end of each line, whether it is the end of a sentence or not.  Now,
if you
wish, you can turn Line Pauses "off", and JAWS will only pause at
the
appropriate punctuation characters, thus sounding more natural. 
This
feature is found in the universal voice menu.
 
     This feature is especially useful if you have a DECtalk, Multi
Voice, KPR, Prose or other natural-sounding synthesizer.  Because
some
synthesizers wait until they are told to speak there is potential
for the
synthesizer to "hang", to pause longer than usual or seem to lock
up, if
it gets too many words without a sentence ending punctuation
symbol. 
This should not be a problem with most English text, but weird or
long
and poorly-written phrases could cause it.  If your system seems to
hang
up, just press the ALT key and it should continue.  If you notice
unusual
phrasing, it is because JAWS will arbitrarily put in a start speak
if it
thinks too much text has gone by without a sentence-ending
punctuation
symbol.  The Sentence and Paragraph functions automatically turn
line
pauses off temporarily, so they sound more natural.
 
5.3  Standby or Sleep Mode
 
     This temporarily turns off JAWS, which prevents all screen
reading until JAWS is reactivated by pressing CONTROL + /.  This
feature is useful when a sighted person wants to use the computer. 
Standby allows JAWS to remain in memory, unlike the J_UNLOAD
command and the ALT + SHIFT + F1 command which remove JAWS
from memory.  The J_UNLOAD command should be used only at the
DOS prompt, but Standby or Sleep Mode can be used anytime.  Standby
is found in the Voice Global menu.  You can invoke Standby from DOS
or a batch file with the J_UNLOAD.EXE file.  Use "/S" as a command
line parameter for the J_UNLOAD command.
 
 
6.   NEW FEATURES FOR FINDING SCREEN COLOR AND
     TEXT
 
6.1  Testing for a Color/Attribute or a Character String
 
     JAWS has always been able to search an area of the screen for
a 
certain color or string of characters.  Now you can "test" for a
specific
color or character string at the cursor's present location without
moving
the cursor.  You can do this by using the "x" option when the
string
search or enhancement search asks for "Options".  When the X option
is used, then the test will use the active cursor, (the one that is
"on").
 
     For example, to tell if the cursor is at the start of the word
"Hello"
enter the JAWS Menu System, press S for Search, type Hello when
prompted for the character string and press ENTER.  Next when
prompted for search options: type X, followed by ENTER.  You can
use
a lower or upper case X, it does not matter.  If the test is
successful, and
the cursor is on the "H", then JAWS will say "Hello", otherwise
JAWS
will give an error beep.
 
6.2  Case Sensitive String Searches
 
     If you want a string search to differentiate between uppercase
and lowercase letters, then you can use a "U" when prompted for
search
options.  The "U" for case sensitivity can be used in any string
search. 

 
 
7.   A NEW TYPING SAFEGUARD
 
     If you have CAPS LOCK "on" and hold down the SHIFT and then
press a letter key; the letter will be lower case.  This may be
what you
want, but usually it is not.  JAWS now beeps when this situation
occurs.
 
 
8.   CREATE A TEXT COPY OF A MACRO FILE 
 
     The J_MTEXT.EXE program will convert a JAWS macro file to
a text file, which can be read with a word processor or text
editor.  The
entries in the file will tell you the name of the macro key;
indicate
whether the macro is a BIOS macro; identify the monitored frame
number if it is an auto macro; and list all functions and key
strokes that
are recorded in that macro.  It is very convenient for deciphering
a
macro file and valuable for looking at macros that you want to
learn
more about.  Type J_MTEXT at the DOS prompt, and you will be
provided with a list of options and proper syntax.
 
 
9.   ON-LINE DICTIONARY EDITOR
 
     When you select "Revise" from the dictionary menu in the
universal voice menu, you will be presented with the selections of:
"Add", "Change", and "Delete".  The ESCAPE key and the UP ARROW
will get you out of any of these menu functions, and return you to
the
Dictionary Editor menu.  The ENTER and DOWN ARROW keys can be
used to select an option or complete your entry.
 
9.1  Create a Dictionary Rule
 
     When you select "Add" you will be prompted for the first part
of
the rule, the word or phrase whose pronunciation you want to
change. 
Type the word or phrase and press ENTER.  You will be prompted for
the second part, which is the new way of spelling.  After typing
the
pronunciation rule, press ENTER, and you will be returned to the
menu.
 
9.2  Modifying a Dictionary Rule
 
     To modify an existing pronunciation rule, select "Change". 
The
change process is similar to the processed used for "Add".  You are
first
prompted to find the first part of the dictionary entry (rule). 
You can
use the LEFT ARROW and RIGHT ARROW keys to find the entry, or
simply begin to type the word/phrase and the Dictionary Editor will
find
it for you.  Once you have found the entry, then press ENTER and
you
are ready to edit the existing second part of the rule.  You can
use the
arrow keys, the DELETE, the BACKSPACE, and type new characters.
 
9.3  Removing a Dictionary Rule
 
     When you select "Delete" from the Dictionary Editor menu, you
are following a process that is identical to the first half of
"Change". 
You need to find the rule to be deleted the same way you find the
rule
to be changed.  When you find the rule you wish to delete, then
press
ENTER and it will be erased.
 
9.4  Saving Dictionary Rules
 
     The Dictionary Editor simply edits the dictionary rules that
are
held in the PC's memory.  The dictionary rules will be lost if you
turn
off your computer or load another dictionary configuration file. 
To save
the new rules, use the Config Files menu, press S for Save, D for
Dictionary, and press ENTER when prompted for the file name.
 
9.5  Single Character Dictionary Rules
 
     The dictionary will now recognize a single character, like a
punctuation symbol, so you can change what is spoken for these
symbols.  Follow the usual technique for entering a dictionary
rule.
 
 
10.  NEW FEATURES FOR THE WORDPERFECT
     CONFIGURATION
 
10.1 Additional DELETE Key
 
     The extended DELETE key is now a macro that will perform the
Delete function.  It deletes the current character and reads the
character that takes its place.  This way you do not need to use
the ALT
+ DELETE on the speech pad.
 
10.2 Read the Blocked Text
 
     The INSERT + Q macro in WordPerfect 5.1 will read the text
that
you have "blocked" for moving, copying, deleting, etc.  It defaults
to
reading the text that is red in the background, just like the spell
check
macro.
 
 
11.  LOTUS 1-2-3 MACROS
 
     The INSERT + T macro, which reads the title displayed on line
5 now works the way it should.  We changed the speech pad DELETE
macro (the position macro), so it will read the cell coordinates
for all
versions of 1-2-3, even the three-dimensional model.
 
 
12.  VOICE CONFIGURATION FILES
 
     The voice configuration files used with JAWS version 2.2 will
not
be compatible with version 2.3.  If you try to use them, then you
will get
the message "Voice file is not compatible" when you try to load
them. 
The same is true of the environment configuration files.  The JAWS
installation program will convert these files to the new format if
it finds
them in the default directory where JAWS is installed.  If your
configuration files are located elsewhere, then you can run our new
conversion program, "J_CVT23.EXE" from the DOS prompt in the
subdirectory where the old files are stored.
 
 
13.  BUG FIXES AND OTHER CHANGES
 
13.1 Smart Screen Frame Number
 
     We changed the default frame number from "0"" to "1" for the
frame that the Smart Read macro uses.  By making this change, you
can
now change the dimensions of the frame to ignore a portion of the
screen.  Thus, you can eliminate sources of constant chatter, such
as a
clock on a status line.
 
13.2 Beep on Routing
 
     This feature now defaults to "on" for the "JAWS.JMF" and
"WP.JMF" macro files.  When you Route PC to JAWS or use the silence
key to skip ahead during a Say All, then your computer will beep
for
each line the cursor passes.  If you do not want it to beep then
create a
macro with this function and turn it off.  This also affects the
"Rewind"
beeps.
 
13.3 Extended Arrow Keys
 
     These macro keys only move the PC Cursor.  They activate the
PC Cursor whenever they are pressed and then use it for screen
reading, while the JAWS Cursor is left at its current location.
 
13.4 Macro Pause
 
     If you activated a macro (pressed the macro key) while in the
JAWS Menu System and that macro had a Pause Macro function in it,
then you might have gotten stuck in the menu system.  This is no
longer
a problem. This was the problem that caused JAWS to lock up when
WordPerfect was being used and you exited the Macro Editor and
accidently pressed a SHIFT + CONTROL + UP ARROW while in the
JAWS menus.
 
13.5 Menu Display and Backspacing
 
     If you had used a DOWN ARROW instead of ENTER when in the
JAWS Menu System to select a config file, search string, or to find
an
enhancement, then the new menu item would not have appeared on the
screen.  It sounded fine, but it did not look fine.  Now the menu
display
is correct.  The backspace problem has also been fixed.  When you
use
the backspace to edit a file name in the config files menu, JAWS
will
now speak the character deleted.
 
13.6 Keyboard Word Echo
 
     Selecting "Word Echo" in the keyboard voice menu causes JAWS
to speak the words you type when you press the space bar.  The
BACKSPACE key, the Menus key, and others would not cause JAWS to
speak the accumulated characters.  Thus, when they did get spoken
it
was often confusing.  Now the accumulated characters are spoken as
soon as any "unusual" key is pressed.
 
13.8 Single Character Prompts
 
     JAWS will echo all prompts written through the BIOS, including
single character prompts such as the asterisk in EDLIN.  Some
earlier
versions of JAWS did not speak single-character prompts.
 
13.9 Speaking Punctuation Symbols
 
     Now JAWS will say the punctuation symbols when reading a
character and/or a word at a time, or in keyboard echo, even if
Symbols
is not set to "All".  When reading longer units of text, such as
lines or
sentences, the symbols will not be spoken unless "All" is selected.
 
13.10Macro Duplicate and Macro Erase
 
     Some earlier versions of JAWS could not duplicate or erase a
single key macro, such as UP ARROW.  This is now fixed.  
 
13.11Place Cursor on the Last Word Spoken
 
     Pressing the CONTROL key causes JAWS to stop reading when
the Say All function is in use, and to place the cursor on the last
word
spoken.  Occasionally JAWS would not find the last word spoken,
which
either produced a routing-failure beep, or resulted in the cursor
being
stopped a few words before or after the correct word.  We have made
improvements in this function; please let us know if you think it
needs
adjusting, or if you ever get the error beep telling you it failed.
 
13.12Pitch Changes
 
     JAWS should do a better job of controlling the pitch of the
synthesizer.  Previously it could allow the synthesizer to get
stuck at a
higher than usual pitch, as is heard when JAWS speaks uppercase
characters. 
 
13.13Pausing for Punctuation
 
     Formerly JAWS would not pause for some punctuation symbols
when the Symbols menu was set to "Most" or "Some".  The symbols
were
being removed from the output string.  We think it is fixed now in
most
of the synthesizer versions.  Please test your version for us.
 
13.14Kurzweil Personal Reader
 
     The Say All function and screen echo work properly now.  JAWS
will now stop on the last word spoken when the CONTROL key is
pressed during Say All.  The synthesizer will silence instantly,
and it
will not go into "phonetic mode" and speak garbage.  Please check
it out,
give it your toughest test and let us know if any problems develop. 
The
names of the JAWS files for the KPR are J_DECTLK.EXE and
J_DECTLK.BIN.  The KPR synthesizers are actually DECtalk
synthesizers and therefore use the DECtalk version of JAWS.
 
13.15Multi Voice
 
     The JAWS version for the DECtalk synthesizers has been
combined with the version for the Multi Voice synthesizer.  They
all use
J_DECTLK.EXE and J_DECTLK.BIN.  You may have to change your
JAWS.BAT file and perhaps the AUTOEXEC.BAT file to reflect this new
name.  The old Multi Voice version were named J_MULTI.EXE and
J_MULTI.BIN.
 
     We think the phonetic mode problem, that resulted in garbled
speech is now fixed.  Please report any problems.
 
13.16Prose 4000
 
     We have made extensive internal changes to the JAWS version
for the Prose.  It is faster, sounds better, and is more reliable
than
before, plus it takes less memory.  Please test it with things that
may
have caused you problems in the past, and try out the new features
mentioned above.
