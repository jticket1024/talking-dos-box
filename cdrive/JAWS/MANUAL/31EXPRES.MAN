PART THREE:  Appendix 1
SPECIALIZED EXPRESSIONS
(Also contained in the disk file "31EXPRES.MAN".)
 
 
(Last revision:  November 1993.)
 
 
 
TABLE OF CONTENTS FOR APPENDIX 1
 
     1.   DOCUMENTATION CONVENTIONS . . . . . . 3-1.213
          1.1  Typographical Conventions. . . . 3-1.213
          1.2  Keyboard Conventions . . . . . . 3-1.214
     2.   PHONETIC EQUIVALENTS FOR LETTERS. . . 3-1.215
     3.   GLOSSARY. . . . . . . . . . . . . . . 3-1.216
1.   DOCUMENTATION CONVENTIONS
 
     Words and expressions used in computer software manuals often
take on very specific meaning because of the technical nature of the
concepts and instructions being presented.  There are several common
practices or Documentation Conventions that are used in this manual
to provide clear written communication.  Documentation conventions are
designed to emphasize what is being said, and to tell the user exactly
what is to be done, and how it is to be done.
 
1.1  Typographical Conventions
 
Bold Type Style
     Bold type is used for command names, command line switches,
     and any text that the user must type exactly as it appears in
     print.
 
Italic Type Style
     Light Italic type is used to add emphasis to certain concepts and
     activities that are being explained.  Light italic type is also used
     for the name of parameters.  For example, if we say "Type
     filename press ENTER", this means that "filename" is a
     parameter and that you should type the name of the file, instead
     of typing the word filename.
 
Bold Italic Type Style
     Bold italic type is used to introduce new terms, and indicates
     that an explanation of the term is also presented within the
     paragraph.  
 
Capital Letters
     ALL CAPITAL LETTERS (uppercase letters) will be used for the
     names of directories, files, keys of the keyboard, and acronyms. 
     When typing directory names, filenames, or DOS commands it is
     not necessary to actually type the words in uppercase letters.  
 
1.2  Keyboard Conventions
 
Expression:  Type
     The word "type" means to use the alphabet and number keys to
     create text on the screen that exactly matches the words and
     symbols that follow the word "type".
 
Expression:  Press
     The word "press" indicates that you should push down the key
     that is identified immediately after the word "press".  Press can
     mean either to tap the key or to hold the key depending on the
     key stroke being described.  Please see the special uses for the "+"
     (plus sign) and "," (comma) that are described below.  
 
Key:  ENTER
     The word "ENTER" indicates that the ENTER key should be
     pressed.  The ENTER is sometimes also called the "return key". 
     Thus, the instruction, "press ENTER" means to tap the ENTER
     key.
 
Key:  PLUS or "+"
     The word "PLUS" refers to the PLUS key located on the numeric
     keypad.  A "+" (plus sign) is used to show a connection between
     two keys.  When a "+" appears between the names of two keys,
     then this means to hold the first key while pressing the second
     key.  Thus, "ALT + RIGHT ARROW" means to hold down the
     ALT key and press the RIGHT ARROW key.  
 
Key:  COMMA or ","
     When a "," (comma) appears between the names of two keys, then
     this means to press the first key and release it before the second
     key is pressed.  Thus, "HOME, UP ARROW" means press the
     HOME key and release it before pressing the UP ARROW.
 
Key:  MINUS or "-"
     "MINUS" refers to the MINUS key on the numeric keypad.
 
Key:  SHIFT
     "SHIFT" refers to the SHIFT key.  The abbreviation Shft will not
     be used in this manual.
 
Key:  CONTROL
     "CONTROL" refers to the CONTROL key.  The abbreviation Ctrl
     will not be used.
 
Key:  ALT
     "ALT" refers to the ALT key.
 
Key:  INSERT
     "INSERT" refers to the Ins (insert) key on the numeric keypad.
 
Key:  SLASH "/" and BACK SLASH "\"
     The slash key - "/" is often confused with the back slash "\". 
     Neither your software nor the computer's operating system will
     allow you to confuse these two symbols.  They must be used
     exactly as written when they appear in a command.
 
Key:  Function Keys "F"
     The upper case "F" immediately followed by a number (1 through
     12) refers to a function key.  Thus, "press F1" means Function
     key number one.
 
 
2.   PHONETIC EQUIVALENTS FOR LETTERS
 
     A phonetic equivalent for a letter is a word that is used to
represent that letter.  The following list shows the phonetic equivalents
that will be spoken for each letter in the alphabet.  For additional
information on the use of phonetic equivalents, please refer to the pages
identified in the index under the topics of "phonetic" or "characters".   
 
Alpha = A
Bravo = B
Charley = C
Delta = D
Echo = E
Foxtrot = F
Gulf = G
Hotel = H
India = I
Juliette = J
Kilo = K
Lima = L
Mike = M
November = N
Oscar = O
Papa = P
Quebec = Q
Romeo = R
Sierra = S
Tango = T
Uniform = U
Victor = V
Whiskey = W
X-ray = X
Yankee = Y
Zulu = Z
 
 
3.   GLOSSARY
 
Application
     A specialized software program, such as a word processor or
     spreadsheet, that the user can run under the operating system of
     the computer.  WordPerfect is an application software program
     that can be run under the operating system of DOS.
 
AUTOEXEC.BAT
     A special file containing DOS commands that are automatically
     carried out by a personal computer when the computer is started. 
     The use of this file prevents the computer user from having to
     type a long set of commands each time the computer is turned on. 
     The AUTOEXEC.BAT file contains the DOS PATH command and
     may contain commands to load software programs.
 
Boot
     In general terms, it means to start the computer.  This usually
     involves the loading of the computer operating system.  Related
     terms are warm boot and cold boot.  Cold boot refers to turning
     on the electrical power so the computer can start.  Warm boot
     refers to having the computer cycle through the process it uses to
     restart without turning off the electrical power.  On most IBM
     and compatible computers, the command for doing a warm boot
     is CONTROL + ALT + DELETE.  A warm boot is quicker than a
     cold boot and is often used after the computer has frozen up for
     some reason.
 
Buffer
     A temporary storage location for data in the computer's memory. 
     When  data is moved from one application to another, it might be
     placed in a buffer.
 
BUFFERS 
     This is a DOS command used within a CONFIG.SYS file.  It
     specifies the number of buffers that are to be used by your
     computer.  Some software programs require a minimum number
     of buffers to function properly.
 
     Example:  BUFFERS=25
 
CHKDSK (check disk)
     A DOS command that checks the hard disk or a floppy diskette
     for errors.  When it is typed at the DOS prompt, it displays any
     error messages and then gives a status report showing how
     memory is being used and how much memory is free.
 
CONFIG.SYS
     A file of commands that lets the user configure DOS for use with
     the devices and applications being used by his/her computer. 
     This file can be changed by the user to modify the maximum
     number of buffers that can be used and the maximum number of
     files that DOS can leave open for temporary storage.  This file
     can also contain the command that loads the device driver for a
     speech synthesizer when a device driver is required.
 
COPY
     A DOS command typed at the DOS prompt that is used to
     duplicate a file or set of files.
 
     Example:  COPY C:\*.* A:
     With most computer systems, this means to copy all of the files
     in the root directory of the hard disk drive (C:\*.*) to a floppy
     diskette drive (A:).
 
Disk Drive
     A general term describing a device used to store and retrieve
     computer data.  (See:  hard disk drive and floppy disk drive.)
 
Default
     A value or setting that a software program will automatically use
     unless the user specifies a different value.  For example, the
     default voice selection for the DECtalk synthesizer is Perfect
     Paul.
 
DEL
     A command typed at the DOS prompt used to erase a file stored
     on the hard disk or on a floppy diskette.  It performs the same
     DOS function as ERASE.
 
     Example:  DEL (path)\(filename)
     The command is used by typing the name of the file to be deleted
     after the command.  Wild card characters can be used to delete
     a group of files.  (See your DOS User's Guide and Reference
     Manual for additional information.)
 
Device Driver
     A software program that manages interactions between the
     computer and peripheral devices such as a printer or a speech
     synthesizer.  The command to load a device driver is usually
     contained in the CONFIG.SYS file.
 
Directory
     A storage area of a hard disk drive or a floppy diskette.   These
     storage areas are usually named after the software programs that
     use them and contain groupings of related files.  For example,  a
     directory named "WP" might contain WordPerfect files.  A
     directory can also have additional (secondary) storage areas
     within it called subdirectories.  (See subdirectory.)
 
DOS
     An abbreviation for Disk Operating System.  It is the set of
     programs that control the overall operation of a computer and
     permit the user to run specialized applications.
 
DOS Prompt
     The combination of letter and punctuation symbols that appear
     on the screen when interacting with the Disk Operating System
     by the use of typed commands.  Examples:  C:\>, C:\WS>, 
     A:>, etc.  The first letter in the prompt identifies the disk drive. 
     The information that follows the back slash "\" identifies the sub
     directory.  Thus, when "C:\WS" is displayed on the screen when
     entering DOS commands, this means that the current (active)
     disk drive is "C" and the current (active) directory is "WS".
 
Drive Letter
     A letter assigned by DOS to represent a specific disk drive. 
     Generally, floppy diskette drives use the letters "A" or "B", and
     hard disk drives use the letters of "C" or higher.
 
ERASE
     A command typed at the DOS prompt used to delete a file stored
     on the hard disk or on a floppy diskette.  It performs the same
     DOS function as DEL.
 
     Example:  ERASE (path)\(filename)
 
File
     Information stored on a diskette or stored within a hard disk
     drive is organized into groups of data called files.  Computer files
     are similar to paper file folders because they both organize so it
     will not be mixed up.  A file may contain data, software
     programs, or both.  Files are stored in directories.  The directory
     can be thought of as a file cabinet for computer files.
 
FILES
     This is a DOS command used within a CONFIG.SYS file.  It
     specifies the number of files that DOS can keep open.  Some
     software programs require a minimum designation of open files
     to function properly.
 
     Example:  FILES=20
 
Floppy Disk
     A flat flexible disk made of an electromagnetic material that can
     be used to store computer data.  The 3.5 inch or 5.25 inch disk is
     enclosed within a flat rectangular enclosure.  
 
Floppy Disk Drive
     A part of a computer into which floppy diskettes can be inserted. 
     This device is used for copying or retrieving information used by
     a computer.
 
Frozen or Locked Keyboard
     When the computer does not accept input from the keyboard, the
     keyboard is said to be frozen or locked up.
 
Graphics Mode
     One of two methods used to display information on a computer
     monitor.  It can display pictures and text at the same time. 
     When graphics mode is used, even the letters, numbers, and
     punctuation characters  are pictorial representations. This is in
     contrast to text mode which cannot display graphic images. 
     Generally, the information that is printed by a word processing
     program that uses graphic mode will be virtually identical to that
     which appears on the computer monitor.  (See text mode.)
 
Hard Disk Drive
     Refers to a non-removable part of the computer used to store data
     and program files.  With most computers the hard disk drive is
     given a drive letter of "C" or greater.
 
Hardware
     The physical parts of a computer consisting of such elements as: 
     disk drives, memory boards, power supplies, keyboards, and
     monitors.
 
Hot Key
     A key or series of keys that, when pressed, will activate (invoke)
     a TSR software program, or will cause a pop-up window to appear
     on the screen.  Examples:  the ALT + INSERT invokes the
     WordScholar Menu System, the CONTROL + SLASH invokes the
     JAWS Menu System, and the CONTROL + ALT + D is the hot
     key for the American Heritage Dictionary.
 
Insert Mode
     One of two modes of operation used when typing data with a
     keyboard.  When text is typed in this mode, then any text that
     already exists on the screen to the right of the cursor is  moved
     over to the right.  Thus, new text can be inserted within an
     existing block of text without losing any of the existing
     characters.  (Also see typeover mode.)
 
Installation Program
     A special software program that is used to copy and prepare the
     files of an application software program.  Original files are
     usually stored on diskettes until the installation program
     prepares the files for use and then copies them onto a hard disk
     drive.
 
K
     Stands for Kilobyte.  One K is equal to 1024 bytes of memory. 
     The expression "640K RAM memory" refers to six hundred and
     forty kilobytes of RAM memory.
 
Load
     The process of placing a software program in memory so that it
     can be run on a computer.  Software is loaded from files stored on
     diskettes or a hard disk drive.
 
MB
     Stands for Megabyte. One Megabyte is equal to 1024 kilobytes of
     computer memory.
 
Memory
     The portion of computer hardware that is used to store data and
     software programs so it can be rapidly accessed.  Memory can be
     either volatile or permanent.  Anything that is held in volatile
     memory will be lost when the computer is turned off.  Information
     in permanent (battery backed) memory is saved for future use.
 
Free Memory
     The amount of computer memory that is available for use by
     software programs.  This is usually calculated by the DOS
     CHKDSK command (or by other programs of this type) after DOS
     is loaded and before application programs and their data are
     loaded.
 
Memory-Resident Program
     A software program which has been stored in memory is called
     memory-resident.  WordScholar, JAWS, and the American
     Heritage Dictionary  are programs of this type.
 
Overtype Mode
     (See Typeover Mode.)
 
Paragraph
     For the purpose of JAWS and WordScholar, a Paragraph is a
     block of text that is preceded and followed by a blank line.  An
     indention produced by several blank spaces or a tab stop at the
     beginning of a line of text is not recognized by WordScholar or
     JAWS as the beginning point for a paragraph.
 
PATH
     A DOS command that tells the computer which directories it
     should examine to find files after the current (active or working)
     directory has been searched.  The PATH command is usually part
     of the AUTOEXEC.BAT file.  A sample PATH command is shown
     below.
 
     PATH C:\;C:\DOS;C:\WS;C:\WS\AHD;C:\WP;
 
Pop Up
     The activation of a Terminate and Stay Resident (TSR) software
     program.  (See Terminate and Stay Resident Program.)
 
RAM
     An acronym for Random Access Memory.  Computer software and
     data can be held in this type of memory to allow for very quick
     processing.
 
RAM-Resident
     A program which has been stored in RAM is RAM-resident.  The
     American Heritage Dictionary is a RAM-resident program.  This
     term may be used interchangeably with memory resident or TSR
     (terminate  and stay resident).
 
Reboot
     To restart the computer.  (See boot.)
 
Root Directory
     Please read the description of "subdirectory" for an explanation.
 
Sentence
     For the purpose of JAWS and WordScholar, a sentence is defined
     as a block of text that ends with a period, question mark, or
     exclamation point, and which is either followed by 2 spaces, or a
     hard carriage return (graphic character indicating the end of a
     line of text).  The period, question mark, and exclamation point
     also can be followed by a quote mark or a single digit footnote
     number.  Thus, the examples that follow would be recognized as
     sentences as long as the quote mark or foot note number is
     followed by either two blank spaces or a hard carriage return.
 
     Jane said, "No, I don't know anyone by the name of Dick."
 
     Our company's growth does not follow industry trends.2
 
Software
     A complex set of interrelated commands and routines used by a
     computer written in a symbolic language.  Software is used to
     organize and perform work done on a computer.  Word processors,
     spread sheets, and data bases all use software to perform their
     functions and activities.
 
Subdirectory
     A storage area of a hard disk drive or a floppy diskette.  These
     storage areas are also called directories.  When a directory is
     subdivided into additional storage areas, then these secondary
     storage areas are called subdirectories.  Files can be stored in
     directories and subdirectories.  For example, WordScholar files
     are usually stored in the directory "\WS" and JAWS files are
     usually stored in the "\JAWS" directory.  A directory can also
     have subdirectories.  The computer disk version of this manual
     could be contained in "\JAWS\MANUAL" or "\WS\MANUAL". 
     The network of directories and subdirectories descends downward
     like the roots of a tree.  The highest level is called the root
     directory.  All directories descend from the root directory.
 
Terminate and Stay Resident (TSR)
     This type of software program is loaded into the computer's
     memory, and may operate in two ways.  It can either perform
     automatic activities without interfering with application software
     programs, or it can stay dormant until the user presses a hot key
     to pop it up from memory.  WordScholar, JAWS, and the
     American Heritage Dictionary are Terminate and Stay Resident
     Software programs.  These types of programs remain in memory
     until they are intentionally removed from memory.
 
Text Mode
     One of two methods for displaying information on a computer
     monitor.  When text mode is used, letters, numbers, blank spaces,
     and punctuation characters are displayed in rows and columns. 
     Graphics pictures cannot be displayed in text mode.  The
     information that is printed from a document displayed in text
     mode may look very different from the representation of the
     document that was displayed on the computer screen.  (See
     Graphics mode.) 
 
TSR
     (See Terminate and Stay Resident Program.)
 
Typeover Mode
     One of two modes used when typing text from a keyboard.  When
     typeover mode is used, text that is positioned to the right of the
     cursor will be replaced by new text as it is typed.  (Also see Insert
     Mode.)
 
Window
     A portion of the display screen reserved for a special function,
     such as a menu of user choices, user input, or a second
     application.  This term does not refer to the Windows software
     program produced by the Microsoft Corporation.
 
