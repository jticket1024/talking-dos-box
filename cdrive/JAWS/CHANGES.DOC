CHANGES.DOC
June 1, 1994

This file contains information that is not necessarily in the
manual and specifically addresses new features, additions,
changes, and bug fixes for JAWS.  Read it to see if the
improvement you want or the bug that has been bugging you has
been fixed.


JAWS Version 2.31 Changes and New Features, June 1, 1994

The new release of JAWS 2.3, which is Version 2.31, has several
new features and is designed to work with additional emulators.
Emulators are software/hardware products that make the PC act
like a mainframe terminal.  Some emulators, such as the Avatar
PA3270 or AttachMate EXTRA 2.3, have worked well with JAWS, but
others  that take over the keyboard have not.  There has been a
complete rewrite of the JAWS keyboard handling code, in order to
be compatible with the Novel 3270 emulator being used by
Florida's Division of Blind Services, and other emulators that
use similar techniques.  Try this new JAWS with any emulators or
other programs that tend to  "steal" interrupts away from JAWS. 
The improved Watchdog function  (command line switch /W) will
grab back the keyboard, and new parameters /VEC9USE and /VEC9GET
will give JAWS access to the built-in BIOS code.  The PC BIOS
Macros allow JAWS to control the emulator and still read around
the screen with the JAWS cursor.


Improved Watchdog Command Line Parameter

If an emulator tries to take over the keyboard, as most try to
do, then JAWS will try to grab it back when the "/W" command line
parameter is used.  JAWS must be able to take back keyboard
control so that it can maintain use of the keyboard.  When JAWS
knows that other programs are running, it can handle the keys
properly and share them with other programs.  Most emulators do
not know or care that another program is running and therefore
they do not share the keys.  Simply add a "/W" to the JAWS
command line to try this feature.  See the chapter on Command
Line Parameters in the manual for more information.


/VEC9USE

In order to bypass the emulator code and go directly to the PC's
BIOS code we have provided this new option.  Its need is
dependent on the particular emulator being used.  If your
emulator is getting keys that JAWS should get, such as when in
the JAWS menus, then you should try this option.

First, you must let JAWS discover where the original BIOS code
is.  Remove any special resident programs, like emulators, from
the system and load JAWS with "/VEC9GET" as a command line
parameter.  JAWS will record the addresses it needs.  You must
then save the "Environment" file on disk, by entering the JAWS
menus (control+/), then "C" for Config files, "S" for Save, and
"E" for Environment file.  It will display the file name of
"JAWS", press enter to save with this file name.

Now you can try JAWS with your systems normal configuration,
including emulator, just use "/VEC9USE" on the JAWS command line. 
It will load the Environment file you saved above and get the
information it needs out of it.  Then when you use the JAWS menus
they should performed normally and the emulator will not get the
keys.  You must repeat the "/VEC9GET" steps above if you move
JAWS to another computer.  Note that the parameter is VEC9GET
when setting up, and VEC9USE when actually using it.


PC BIOS Macros

Special BIOS macros that work differently when  the PC Cursor is
on.  Used when emulators get their cursor movement keys  directly
from the keyboard and not from Interrupt 16.  By using
Control+Shift+F5 in the Macro Identify Window in the Macro Revise
program you  can specify a macro to be a PC BIOS Macro. 
Control+Shift+F6 makes it a BIOS macro, and Control+Shift+F7
makes the macro being edited a regular macro, which is the
default.

When do you need a PC BIOS or a BIOS macro?  Whenever the other
program (emulator, note taker, calculator, etc) takes over the
keyboard at interrupt 9 and does not put keys in the kb buffer at
interrupt 16.  How do you know?  If JAWS locks up whenever you
press an arrow key on the numpad and the PC cursor is on, and it
does not lock up if you do the same with the JAWS cursor on, then
we are pretty sure you should use a PC BIOS macro.  Pressing Caps
Lock after the "lock up" should clear the kb buffer and free JAWS
, if it is experiencing this problem.  Other keys that do not act
different for the two cursors, like backspace, should be BIOS
macros.

You can make any macro a PC BIOS macro (or BIOS macro) by
selecting Macro Revise from the menus, pressing the desired macro
key, then pressing Control+Shift+F5 (or F6) while in the Revise
window, then save it with Control+Shift+Enter.  Control+Shift+F7
returns the macro to "regular" status and function.

The file named "JAWSPCB.JMF" is a good starting point if you need
PC BIOS or BIOS macros.  It already has the usual ones defined. 
Use Config files and Load from the menus to load this file, then
try it with your emulator.

The above three items, /W, /VEC9USE, and PC BIOS Macros, are
provided specifically for emulation problems.  Below are general
purpose improvements and new features.


G option added to enhancement search to ignore graphics and 
spaces:

When "G" is used as one of the enhancement search options, then
the Find Enhancement and Find a  Different Enhancement functions
will ignore all graphics characters and spaces. The cursor will
only stop on real printable ASCII characters that fit the other
search criteria.  This helps cut through the clutter and allows
you to go directly to the desired information.  For example, a
message box with a graphics border may use the color red for both
the border and the data.  If Jaws searches for red it will
normally find the border, not the data, unless you use the "G"
option.

Alt+Number Key Echo:

Pressing the ALT key and a number key on the top row previously
would not  echo the key.  Now it does.  this is a minor change
that will make some of our users happy.

Silent config Load:

When loading a new configuration file via a macro, JAWS will not 
speak nor beep with a successful load.  Thus configuration files
can be loaded automatically by auto-macros, without interfering
with the users thought process.  Previously this loading would be
announced.  If you desire verbal confirmation you can still add
it to the macro.  This is very helpful with our Quicken
configuration, and others.

Spawning improvements:

The spawning code has been improved, and the error messages will
provide a little more information if a problem occurs.  The
spawning code is what loads "external" non-resident JAWS utility
programs like Macro Revise/Erase/Duplicate/Identify, Frame
Revise/Monitor, and Dictionary Revise.  Sometimes these programs
cannot be loaded or run because the "other" program is keeping
DOS busy or has used up all the file handles or buffers.


New Macro Compiler:

This program creates a text file from the macros that are
contained in a JAWS macro file.  The text file then can be read
and edited with a text editor or word processor.  After editing
the macros in the text file, you can then "compile" the revised
text back into a JAWS macro file.    

With this new feature, you will be able to use your favorite text
editor to create/modify macros.  You will be able to create a
special macro or set of macros in one application, and then
export them to a text file and then include them in all of your
other macro files.  Special keyboard configurations will be able
to be updated more easily with new macros or new features.  It
will be easier to study complex macro configurations to learn
about the relationship between macros, frames, etc., and to
discover new methods for solving screen reading challenges.

Use J_MTEXT.EXE to compile and decompile macro and related frame
files.  Read "J_MTEXT.DOC" for details.


New Macro files

The JAWS.JMF and WP.JMF for WordPerfect 5.1 have a few new Help
labels for Sentence and Paragraph functions, to make learning the
keyboard easier.  These additions have bumped up the memory
requirements in the macro buffer, so you may need to add "/B5" 
to the command line, if it has not already been increased by the
installation program.


Alternate Input

The file named "J_AINPUT.DOC" in the Manual subdirectory contains
information on alternate methods of input, other than the key
board.  the program named "J_AINPUT.EXE" is very useful for voice
input devices or morse-code input devices, and others.


WordPerfect 6.0

WordPerfect 6.0 configuration files are discussed in the file
named "WP60.DOC" in the MANUAL subdirectory.


Telephone Device for the Deaf

JAWS configuration files for use with FullTalk are discussed in
"FULLTALK.DOC" in the MANUAL subdirectory.  FullTalk allows your
computer to communicate with someone using a TDD.


DECtalk Portable 2

When Using the DECtalk driver with the DECtalk Portable 2 overall 
operation should be improved.


New Release for Prose Synthesizers

We have an improved version for the Prose synthesizers.   Earlier
versions of JAWS for the Prose would sometimes pause for a few
seconds while waiting for indexes (internal communication between
JAWS and Prose) which would sometimes interrupt typing or screen
reading.  The new version maintains the high-powered features
available from JAWS versions for other indexing synthesizers
while coaxing a little more performance and reliability out of
this old hardware.
 

New command line switch /INDEXxxyy

The xx is the number of words  that JAWS will allow to get out to
the synthesizer before pausing to check the  indexes coming back. 
The yy is the maximum number of words that JAWS  will allow to be
sent out that don't contain punctuation or a start  speak
character.  This is a very interesting change with JAWS.  In the 
past, we "hard-coded" these values into each Synthesizer driver. 
By  using combinations of different values, you can tailor the
speech for  your type of documents.  Most users won't have any
need to change these  settings, but we are providing a easy way
to do so with the command-line  switch.  Further explanation
follows, and the current defaults.

Sometimes when the JAWS Say All function was used for reading
large documents in a word processing program, you might have
encountered an unexpected pause where speech totally stops.  This
situation would occur with indexing synthesizers like the DECtalk
PC or Kurzweil, when the menu setting for "Line Pauses" was
turned off.  When this  setting is off, then the synthesizer
produces more natural sounding reading.

However, if a string of 25 or more words is encountered and the
string of words does not contain periods or commas, then the
synthesizer could stop speaking for no apparent reason.  The
computer system has not locked up, rather, the synthesizer is
just waiting for a "end of phrase" signal, (punctuation)  before
it starts to speak, and JAWS is waiting for a signal from the
synthesizer before it tells the synthesizer to speak additional
words.  This system of closely synchronized communication between
JAWS and indexing synthesizers produces more natural reading
inflection, but this system is not foolproof.

The standoff that can occur between a synthesizer and JAWS could
be broken by pressing the ALT, CONTROL, or some other key. 
However, the waiting problem can be prevented through the use of
the "index" command line switch.  When this switch is used, you
can reduce the number of words that JAWS sends out before it
forces the synthesizer to speak.  Remember, this problem only
occurs when the Line Pauses menu is set to off, and you are using
an indexing synthesizer.  When Line Pauses are On then JAWS tells
the synthesizer to start speaking after each line.

Here are the default values for the indexing synthesizers:

Synthesizer     Indexes Out Max (xx)    Words Since Max (yy)
J_ACNTPC                25                        41 
J_ACNTSA                25                        41
J_APOLXT                25                        25
J_AUDAPT                15                        31
J_BNS                   20                        20
J_DECPC                 35                        20
J_DECTALK               25                        20
J_KRE                   40                        30
J_PROSE                 35                        25
J_VOXPC                 35                        35
J_VOXXT                 35                        35

Be careful with the values you choose.  Some synthesizers,
notably the  DECtalks, don't send back indexes until a start
speak is received.  If  the Words Since Max value is greater than
the Indexes Out Max, then there  may be a deadlock condition with
JAWS waiting for indexes that will  never come because of no
start speak.  Make only small changes to these  values and test
thoroughly.


New Unload program:

J_UNLOAD.EXE has been rewritten as J_UNLOAD.COM.  Using a .COM
file format makes the program smaller and load faster.  If you do
install this version over a existing copy of JAWS, please delete
J_UNLOAD.EXE so there is no confusing over which on is being
used.

You can also use the /S  parameter to put JAWS into Standby and
then run J_UNLOAD /S again to  take JAWS out of standby.  Standby
can also be initiated in the Global Voice menu, or by using the
JAWS Macro Function "Standby".


Mixed Case, Blank Lines, and Freeze mode:

The "Mixed case flag" has been added.  It is controlled by the M
option to On Off Toggle, and it has its own macro function name. 
When this flag is on, words like MixedCaseFlag are spoken as if
there is a space before each capital letter.

The "Blank line flag" has been added.  It is controlled by the B
option to On Off Toggle, and it has its own macro function name. 
When this flag is on, blank lines are spoken as "blank" by Frame
Speak.  When off (the default) blank lines are ignored just like
in Say All.

A "Freeze mode" has been added, to freeze or stop the screen. 
This is handy for people developing configurations and macros,
the screen can be stopped so you can "look" at it.  The
"JAWS.JMF" macro file has Insert+G defined to turn freez mode on
and off.  It automatically turns on the JAWS cursor, and it
cannot be invoked from the menus, although the menus can be
invoked while in freeze mode.  Turning on the PC cursor exits
freeze mode, as does pressing the key/function again to toggle it
off.  It can be checked by the "If" statement.


The above flags have been added to the j_funct.txt file so that
their defaults can be set in a .jmt file, which the macro
compiler can use.

Fixed on_off_rnt to only update curchoice if called from within a
menu.  Prior to this change, calling on Off Toggle from within a
macro after JAWS is loaded but before the menus have been invoked
at least once, could result in system lock-up.


The On/Off/Toggle description below is paraphrased from the
manual:

This macro function provides a way of setting or changing menu
items without going into the menus, and other non-menu items. 
The On/Off Toggle function must be followed by two parameters,
the first parameter is a letter that represents the item to be
toggled or set:  A=acronyms, D=dictionary, I=keyboard
interruptable, L=line pause, P=speech pad interruptable, R=key
repeat, S=screen echo, U=autoload, M=Mixed case, B=Blank lines. 
The second parameter is the setting:  0=on, 1=off, 2=toggle. 
Zero is used for "On", even though this is not routine usage.

The example below first speaks the label or item to be toggled,
then performs the On/Off Toggle function followed by its
parameters:  "M" for Mixed Case, "2" for toggle.  The Say On/Off
function completes the macro by saying if it is "on" or "off".

[Macro Key:  ALT + M] [Label begin] Mixed case [Label end]
[On/Off Toggle] M 2
[Say on or off]
[Macro end] 


Fixing Sticky ALT and CONTROL KeyS

Some JAWS users may have noticed their ALT or CONTROL keys acting
as if they have gotten stuck in the down position.  These keys,
of course, are not physically stuck down, but when other keys are
pressed, then the commands and characters that are produced could
only be created if the ALT or CONTROL were being held down. 
Usually this would show up if you happened to hold down both ALT
keys or both CONTROL keys at the same time.  In over six years of
using JAWS on our test computers, we never noticed this behavior. 
This was probably because we only used the LEFT ALT or LEFT
CONTROL keys.  The correction to this problem may also fix other
sticky key complaints that we have heard from time to time.  


Unjam key:


The Keyboard unjam key is now Caps Lock instead of Control.  JAWS
will  appear to hang while trying to send the application a
keystroke and the  application doesn't retrieve the keystroke. 
If this happens, press Caps  Lock to free up JAWS from waiting.


Demo keys are now 1000 instead of 2000.  After 1000 keystrokes
JAWS will "lock up" for five minutes, then give you another 500
keys to save your work and exit.  When you re-boot it will start
over with 1000 keys.


Search from cursor:

Previously, when performing a search from the cursor, and it was
outside the active frame, the search  might go off the screen. 
Now it will not, and it will give the proper error message if the
item is not found.


Pass-Key-Through:

The Pass Key Through function (INSERT + 3) has been fixed. 
Previously JAWS 2.3 would not always pass through a shifted key
combination such as SHIFT + PAGE DOWN, etc.


Color Bar:

The Color Bar would not always sense when the cursor was turned
off by the application.  It is improved now, providing better
performance the Smart Screen feature.


Large Screen Read Line X:

Read Line X will now read more than 80 characters on video
cards/monitors which can display more than 80 columns.


Pitch control:

Sometimes the pitch of the voice would seem to have 3 levels,
instead of just 2 to represent upper and lower case.  This has
been changed to just 2 pitch levels.


Macro Duplicate:

Macro Duplicate will now work properly with long macros. 
Formerly a long macro would be truncated.  Macro revise is also
new, see the section on PC BIOS Macros for the new features.


Leading Zeroes:

JAWS Will say a number string that only has 0's even when
SKIP_ZERO and  Full numbers are set.  Formerly it would not say
the "leading" zeroes, thus not say anything if the number was all
zeroes.


We removed the compiler message in the Say Version
message(Insert+V with default JAWS config), it still says the
date.


Place Markers now work as expected with the JAWS Cursor on.


Audapter would sometimes speak a "24]".


The proper volume-setting command, :vs, is now sent to the synth
in DECtalk and Reading Edge.  This has two benefits.  The volume
is set properly, and screeching problems should have been
eliminated.



JAWS 2.3 Changes, as of 7/2/93:


Say Next Field:

You may have noticed that the "Say Next Field" function will
sometimes read over to column 81.  This is an error.  It happens
when a character is in column 80.  It can also happen with "Say
Next Word" when "Fields" are on.  The release dated 7/2 will fix
this.


Audapter

The 6/4 release of JAWS 2.3 for the Audapter synthesizer, and
maybe earlier versions, speaks a few extra numbers  upon boot-up
and when loading voice files.  You may hear a "24" when running
an application, or when exiting to DOS.  This does not cause a
problem, however we have fixed this with release 7/2.


Sound Blaster

To use JAWS with the Sound Blaster or its clones you must have
the "SMOOTHTALKER" text to speech program from First Byte.  This
usually comes with the Sound Blaster.  its file name is "SBTALK". 
Run "SBTALK" first then run the proper version of JAWS, which is
"J_SBLAST.EXE".  The Sound Blaster may be a very good synthesizer
for hobbyists, but it currently is not good for blind computer
users that use a screen reader.  It takes up a lot of memory, it
will not silence when asked, and it is lacking in the performance
and other features we have become used to.  Please do not call us
for help with this unit, instead call the manufacturer or vendor. 
We know it performs poorly, we do not sell it, and we do not want
to support it.  As improvements are made our opinion may change.

List Of JAWS 2.3 Changes, as of 6/4/93:

"alt ay" changed to "alt a" for all synthesizers, to correct
pronunciation problem on DECtalk-PC.  

Added a "menus active"  API call, so external program  can tell
if the JAWS menus or external programs are active.  

Changed the "speech flush" function to be the same as  "speech
silence".  This should cause JAWS to silence speech output  when
any key is pressed, not just the silence key.  Previously any 
key would silence the speech when interruptable was on, but if
you were displaying a long file to the screen, like using a
communications  package, the speech would come back on.  Now it
will stay silent until  another key is pressed.  It also dumps
the rest of the data to the  screen faster.

Toshiba arrow keys were changed to handle the key board
differently.   This fixes the problem with some menu programs
that would seem to lock up  when the arrow keys were used.  Use
the "/T" command line parameter to  invoke this new feature,
which is the commonly used Toshiba flag that is  put into the
JAWS batch file automatically by the install program if you 
select the Toshiba keyboard during the installation.
     
     Typing capital letter after backspace will now produce the
correct higher pitch.  Previously the pitch did not reflect the
upper  case status.
     
     DECtalk-PC, added "Citation" mode, "Punctuation" mode, an
"Proper name" mode to the "Extra" menus.  Citation mode causes it
to  say "A" instead of "ah".  Punctuation mode gives the user
control over  the DECtalk-PC punctuation choices.  To be sure
this works correctly,  put the JAWS "Symbols" choices in the
Universal menu to "Off".  This setting must be returned to the
default if you plan to use the "Symbols" choices in the Universal
menu to vary the amount of punctuation spoken, these two settings
work together to give you the widest number of choices.  Proper 
Names will cause names to be spoken better.  This is not
recommended  for general use, but only when reading text with a
lot of names.   

F9 and F10 are now "Rewind" and "Skip ahead".  The original keys
for this, NUMPAD- and NUMPAD+ are still functional also.  This
change is useful for Toshiba and other lap tops with no numeric
pad.      

"Say Time" and "Say Date" have been changed, they no longer are
invoked when you do not want them with the Sounding Board
synthesizer.  And they should not make "DOS busy".
     
     Manual correction: the BBS phone number was wrong, the
corrected number is (813) 528-8903.

Also in the manual, the correct key combination to remove JAWS
from memory is alt+shift+F1.  Be sure you are at the DOS prompt
when you do this, with no other TSR programs loaded after JAWS or
no interrupts "hooked".

There is a new "Command" file on the disk but not in the print
version.  This has more information on command line parameters. 
Its full name is "42COMAND.MAN".


     WordPerfect and Keyboard Echo set to Words:  If the cursor
was on row 1 and between column 1 and 21 the word just typed
would not be spoken when the space bar was pressed.  This was
caused by a  new WP configuration file that monitored line 1, to
provide more speech interaction in some of the WP menus.  Now it
has been fixed. 
    
    Braille 'N Speak version of JAWS has been modified to suit
the older BNS as well as the new 640 model.  During installation
a /NI will be added to the command line in the JAWS.BAT file if
you select the older BNS.  This stands for No Indexing and will
also disable some of the speech controls in the JAWS menu.  You
then must use the BNS keyboard to change the voice parameters. 
An earlier release of JAWS 2.3 would slow down the speech rate
when you did not want it to, the new one no  longer initializes
the speech rate to a slow speed, it just leaves  it alone.  If
you select the newer 640 model during the install  then all the
speech controls like pitch and rate will work in the  JAWS menus,
and the indexing will let you stop on the last word  spoken,
change speech during say all, and rewind/skip ahead.  


Cursor movement time-out and Breaking Up Words Using A Modem:

Sometimes when receiving data over a modem the words will be
broken up, they will be spoken in pieces.  This happens when the
data appears or comes in slower than JAWS expects.  To tell JAWS
to slow down add "/I:##" to the command line.  The higher the
number the more patient JAWS becomes.  Probably 20 up to 60 will
work well. Each number represents about 1/20th of a second, so
/I:20 is about one second, /I:60 is about 3 seconds, which is a
very long time in computers.  Please note that this number was
changed with this release, previously you needed a much larger
number.  This new technique is not dependent on the speed of the
computer.

This command line parameter also has the added benefit of slowing
down the cursor-movement time-out.  When JAWS asks the cursor to
move it sets a timer and waits only a certain amount of time.  If
the cursor does not move within that time period JAWS will beep
to let you know.  Sometimes, like when connected to a mainframe
or other remote computer, it takes a long time to move the
cursor.  Using the /I option will allow more time to move the
cursor.


Foreign Languages

The release of JAWS version 2.3 dated 7/2/93 and later supports
the foreign language synthesizers like the Infovox, the Apollo,
and the Prose.  They only work with version 2.3 so you must upgrade to that version as
well.  

The foreign languages require several extra characters not used
in english, like upside-down question marks, "umlauts" and
accented characters.  Unfortunately some of these characters
cause problems for some of the American/English synthesizers. 
The most common symptom is that data to the right of or following
one of these characters will not be spoken.  If this is happening
to you then use the "J_CVT23" program discussed below.  If you
have the 6/4/93 release of JAWS 2.3, see below.

If you want the special "foreign" characters to be sent out, i.e.
if you have one of the foreign language synthesizers, then run
the "J_CVT23" program with "/F" as a command line parameter.  The
"/F" stands for Foreign.  This will re-set the environment files
to enable these characters.  If you do not want the foreign
characters to be sent out, i.e. if you have an American/English
synthesizer and some of the data on your screen is not being
spoken as it should, then run J_CVT23 with the /A parameter.  the
"A" stands for American.  If you want to use environment files
that you have created be sure to run the convert program with the
/A parameter (or /F), to be sure all the environment files are
converted to "American" (or "Foreign").  For example:

J_CVT23 /A

or

J_CVT23 /F



Infovox internal synthesizer:

If you are using the internal infovox synthesizer you must run it
with the "/I" command line parameter to set up an interrupt
vector. Enter the following:

"VOXDRV /I"

then enter the command to load the proper JAWS version, which is:

"J_VOXPC /E"

The external Infovox is "J_VOXXT.EXE", and the external Apollo is
"J_APOLXT.EXE".


Free Update:

A new release of JAWS 2.3 is available at no charge for anyone
that has purchased the 6/4/93 release or that has purchased an
upgrade and was shipped the 6/4/93 release.  We want everyone to
stop using the 6/4 release because it has problems with foreign
language characters and untimely lockups.  Pressing insert+V when
JAWS is loaded and at the DOS prompt will cause JAWS to speak the
version number and the release date.  Listen carefully, if it
says "Compiled with MicroSoft C on Jun 4, 1993" then you have the
release in question.  You can call/write/fax us and we will ship
a new release, or you can also down-load a new release from our
BBS if you are in a hurry, just call ahead for access.


There were two problems with the 6/4 release discussed above. 
Rapid pressing of the back space key or the arrow keys in some
application programs will cause it to lock up.  It may also
happen when pressing the insert+R macro key.  And, the "Foreign"
characters were "locked in" in this version, thus they caused
certain American synthesizers to skip over words and phrases, as
discussed above.  We have tried to update everyone that received
this version as quickly as possible, but we are sure some have
not been notified.


Other changes:

Other changes may have been made that are not documented here. 
If you have a question about the performance of your update of
JAWS version 2.3 that is not discussed in this file or in the
manual and tapes please call or write or fax it to us.  For more
information on JAWS  refer to the manual, or the manual text
files that are installed (if you so choose) in the \JAWS\MANUAL
subdirectory.
