ANY NIGHT FOOTBALL 1995 - 1996
PRODUCED BY
PERSONAL COMPUTER SYSTEMS
WRITTEN BY
CARL MICKLA
VERSION 1.6
COPY WRITE 1996

INTRODUCTION:
A text based football game.  Which is simple to play, and the teams are
historically reflected in the game.  It includes 28 teams and their stats to
make a very realistic, enjoyable and easy to play game for the blind.
It follows the same rules as the National Football League with 24 offensive and
8 defensive plays and allows you to play against another person or against the
computer.

It was written with speech in mind to enable blind people to know exactly what
is going on in the game at all times. 
All the data used for each team is historically reflective of the
teams in the 1993-1994 season in which they appear.

GETTING STARTED:
This game can be run off the floppy or installed to the hard drive.
A.  To play off the floppy just type ANF at the prompt where the
game is located.
B.  If you would like to install the game on to your hard drive,
just type INSTALL at the prompt where the game disk is located. 
The game will create a directory called 'ANF' and will place
all the files in it.  
Type ANF from inside the games directory to run the game.

NOTE:  ALL PROMPTS IN THE GAME WILL BE ANSWERED WITH EITHER ONE
LETTER OR A NUMBER RESPONSE.
 After all numbered responses you must hit ENTER.  After the title
page is displayed The game will prompt you if you would like to
have the AUDIBLE mode off.  This will stop the computer from
inserting an offensive audible or a defensive shift.  It is
suggested that this mode be on, as it helps balance out the
computer play.  The next thing the game will ask you is if you
would like to have the REF WHISTLES turned off.  The referee whistles
can be changed at the prompt just after the update of the position
of the ball is given by hitting W.  
Next the game will ask if you would like to have the pre snap description off.
If you enter Y the breaking of the huddle, formation line up and the
motion descriptions will not be shown.  At the prompt after the
ball update you may toggle  the snap description on or off by
hitting D.  Next the game will ask you if you would like to
play a saved game.  If you do not want to play a saved game hit '
N ' and the game will go to the SELECT TEAM PHASE.  If you would
like to play a saved game hit  ' y '.  The game will display
the home and visiting teams, along with the score.  The computer
will prompt you to have the computer play the home team or not. 
Then the game will ask, if you would like to play this game? 
If you respond with a ' n ' the game will move to the select
teams phase. 

SELECT TEAM PHASE

The game will prompt you to play with the computer controlling the home team or
not.  If you play against the computer it will take most of the play calling
for the home team.  The computer will run the play calls for the kicking teams,
offence and defence.  During the penalty phase, there are a few unclear
choices you would have to make for the computer.  
Type Y or N when asked for your selection for computer play.
  
Then the game will ask you to pick a home team.  Just type in
the number that precedes the team you would like to have as the
home team.  Then you will do the same for the visiting team.

TEAMS IN THE GAME:
0  = 1993  GIANTS.     1  = 1993  COWBOYS.     
2  = 1993  REDSKINS.     3  = 1993  CARDINALS.     
4  = 1993  EAGLES.     5  = 1993  JETS.     
6  = 1993  BILLS.     7  = 1993  PATRIOTS.     
8  = 1993  DOLPHINS.     9  = 1993  COLTS.     
10  = 1993  FORTY NINERS.     11  = 1993  SAINTS.     
12  = 1993  FALCONS.     13  = 1993  RAMS.     
14  = 1993  RAIDERS.     15  = 1993  CHARGERS.     
16  = 1993  BRONCOS.     17  = 1993  SEA HAWKS.     
18  = 1993  CHIEFS.     19  = 1993  STEELERS.     
20  = 1993  OILERS.     21  = 1993  BENGALS.     
22  = 1993  BROWNS.     23  = 1993  LIONS.     
24  = 1993  BEARS.     25  = 1993  BUCCANEERS.     
26  = 1993  PACKERS.     27  = 1993  VIKINGS.     

The game will then display the home team and the visiting team
and ask you if these teams are OK.  If you reply with a N, the
game will allow you to pick other teams, and to have the computer play the home
team. 
Once you have the teams you want you type Y, and the coin toss will take place.

At the coin toss the game will ask the visiting team to pick H
or T.  After the prompt is given the game will announce the
winner of the flip and ask for the winners choice to kick or
receive.  After the response is given the game will move on to
the kickoff phase and you are off and playing!

After the kickoff is done, you will be up dated on the ball
location and the down.  To access a menu at this time  type M and a short list
of commands will appear. 
You can quit the game showing the score and stats.
You can fast quit and not show the score.
You can display the score, or the time, and turn referee whistles on ore off. 
You can also change the pre snap description on ore off, and save the game. 
there are five items which can be accessed at any time.  
They are, T which will show the time remaining, S which will display the score,
W referee whistle on or off, D pre snap description on or off, and V save the
game.  If you do not want to display the menu hit any key  and  you will be
able to pick the plays.  

PLAY CALL.
When the computer prompts you for your play a numbered play or one
of four letter responses could be typed.  the letters which can
be used are T for time update, S for score update, P for a play
list, and L to look up a play description.  The numbered
plays correspond with available plays on the play list.  If you
would like a play list to appear type P and a list of all the plays
will be displayed.  If you would like to see a description of a
play enter L then the play number, and a short explanation will be given. 
After you have made a play choice the game will verify your selection.  If the
pick is correct hit any key and the game will go on.  Hit n if the
play is not right and you will be given the chance to enter another
play.
  
DESCRIPTION OF PLAYS:
1.  LINE PLUNGE,
     A running play that starts toward the middle of the line of
scrimmage.
2. TACKLE RUN,
     A running play which starts toward a hole created near the
tackles position.
3. END RUN,
     A running play that tries to get around the line of scrimmage
and up the sideline.
4.  DRAW,
     A running play which looks as if the quarterback is going to
throw and once the pass rush passes the play the ball runner takes
off up field.
5.  SCREEN,
     A passing play where the offence looks like it is going to
pass deep and they allow the pass rushers to get close to the
quarterback and he throws a short pass out to the receiver, who is
usually behind the line of scrimmage and has a few blockers in
front of him.
6.  SHORT PASS,
     A pass play looking to pick up a few yards and allowing the
receiver a chance to run with the ball.  This pass is thrown near
the line of scrimmage.
7.  MIDDLE PASS,
     A pass play that tries to get about 15 to 20 yards.
8.  LONG PASS,
     A pass play looking to get a big gain.
9.  SIDELINE PASS,
     A pass play thrown near the sideline to try and conserve time.
10.  PUNT,
     A play that tries to give the ball to the opponent as far down
field as possible.
11.  FIELD GOAL,
     A play where the offence will try to kick the ball threw the
uprights and receive 3 points.  This play is usually tried from
about 45 yards out from the goal line and closer.
12.  KICKOFF,
     A play looking to have the opponent start on offence as near
to there own goal line as possible.
This play is used only at the beginning of the game or third
quarter or after a score.
16.  ON SIDE KICK,
     A play used on a kickoff to try to enable the kicking team to
recover the ball.  This play is usually done when the kicking team
is far behind in score or if time is running out and the kicking
team must get the ball back for there offence.
17.  POINT AFTER TOUCHDOWN,
     A play used after the team has scored a touchdown and they are
given an opportunity to add to there score by kicking the ball
threw the uprights and receiving 1 more point.
18.  STANDARD DEFENCE,
     A defensive alignment that tries to cover the every offensive
play called.
19.  SHORT YARDAGE DEFENCE,
     A defensive alignment that is looking for a inside running
play or a short yardage play.
20.  SPREAD DEFENCE,
     A defensive alignment looking for a short offensive play and
very good against the end run.
21.  PASS PREVENT SHORT,
     This defensive alignment is looking to stop the short pass.
22.  PASS PREVENT LONG,
     This defensive alignment is used against long passing
situations.
23.  BLITZ,
     This defensive play is used against passing plays and can stop
a run in the back field at times, but it is a gamble because if it
does not work the offence can get a big play.
24.  TIME OUT,
     This is a time saving tool and is used to conserve time during
the game.  Use this after a time consuming play and you will use no
time on the next play. Thus saving half of the time from the
previous play.
25.  HURRY UP OFFENCE,
     This will place the offence in a hurry up mode.  This may
cause more penalties and fumbles, but it will only use half the
time that would be spent on a play which did not go out of bounds. 
Be shore to choose this before you pick a offensive play.  You
might want to pick this when you are going for a play that will not
have a good chance of going out of bounds.
26.  PASS OUT OF BOUNDS,
     This might be used to stop the clock when you do not want to
use a time out.  This will enable you to save time by loosing a
down.  When using this you do not have to take the increase chances
of fumbles or penalties that might occur when using the hurry up
offence.
27.  RUN OUT OF BOUNDS,
     This must also be picked before any other play.  When you pick
this the ball carrier will try to get out of bounds at any cost. 
The down side to this is that there is a loss of 5 yards on the
final play result.  You would also use this to conserve time.
28.  WASTE TIME OFFENCE,
     This is used to spend as much time as possible.  The chances
of getting penalties is increased but the time used up off the
clock is 45 seconds.  This is used before any play is picked.
29.  QUARTERBACK KNEEL DOWN,
     This play is used to run out the clock.
30.  STAY IN BOUNDS,
     This is just the opposite of the run out of bounds play.  This
must be called before any offensive play is picked.  There is also
a 5 yard reduction is assess at the end of the plays result.
31.  FAKE PUNT,
     This is a mode used before picking the punt play.  If the fake
has a chance then you will be informed and asked to pick a fake
play.  If the fake fails then pick the type of punt or field goal
try you wish to do.
32.  QUARTERBACK SNEAK,
     When this play is used the quarterback will make 1 yard.  If
the quarterback fails then no gain is the result.
33.  RAZZLE DAZZLE,
     These are a few trick plays that are randomly called.  You
cannot pick 1 particular play but the computer will pick 1 for you.
This play can be very risky to run, so be careful when you use it.
34.  TWO POINT TRY,
     This play is available after a touchdown.  You run a play at
scrimmage and if the play gains 2 yards then 2 points are added to
the score of the team which scored the touchdown.  This play may be
used in place of the point after try.
35.  POOCH PUNT,
     This play is used to try backing up the opponent to there goal
line.  The punter tries to place the ball out of bounds or have the
ball downed near the goal line with out going into the end zone. 
This play is done when the ball is too far away for a field goal
try and too short for a full strength punt. 
36.  SQUIB KICK,
     If this play is picked during the kickoff the ball will be
kicked short and the return is usually less then 25 yards.  This
play is used to prevent a big return.
37.  GAMBLING DEFENCE,
     When the defence plays this strategy they are trying to pick
the offenses same play.  If the defence picks right the play will
be stopped for no gain or a loss.  If the defence picks wrong then
the offensive play result is doubled.
38.  BLOCK KICK,
     The defence uses this when they wish to increase there chances
to block a punt or field goal.  No return of the punt can be
attempted, because there was no man back to receive the ball and
the kicking team downs the ball.
Any offensive play that fails to make it out of the end zone is a
safety.

STATS:
Included with the game is a program called STATS.  This program
will give some stats of a teams performance in several categories
like running, passing, kicking, defensive, and special teams plays.

The tracking of fumbles, quarterback traps, interceptions,
incomplete, blocked kicks, force plays, out of bounds, touchdowns,
longest field goal, fumble recovery rating, and breakouts are the
categories which can be examined.  On the offensive and special
teams charts all factors are based on 30 attempts.  On the
defensive charts the factors are based on 5 plays.  The force
factor is a result that will over ride most all other results.  on
offence the force is all ways plus yardage on defence the force can
be plus or minus yardage. force is a very important factor and
should be considered when deciding a strategy.  For example:  A
team with 10 out of 30  force factors in the line plunge play.
Going against a defence which is strong against the run and has the
capability of creating fumbles, 1 out of five. This offensive team
might use the line plunge, as the force factor would over ride the
fumble factor.  The out of bounds is the chance of a play going out
of bounds.  Quarterback traps is when the qb has to scramble or
gets sacked.  The interception on the defensive chart is preceded
by a number of how many yards down field  the interception might
take place.  The force on defence charts is preceded by a number
which displays how many yards the defence will allow unless the
offence has an over riding result.  

If you have any problems or questions you may contact Carl Mickla in any way at
the address or phone number provided below.  

IN THE FUTURE FOR ANY NIGHT FOOTBALL.
Real football sounds will be included in future versions.
Using players names is being looked at.
Modem and or bulletin board play might also be available.    

OTHER GAMES BY PCS:
MONOPOLY. A very speech friendly Monopoly game with multi media sounds.
MONOPOLY sells for $30.00

MOBIUS MOUNTAIN.  A very speech friendly math adventure game with multi media
sounds .  It was written with speech in mind to enable blind children to know
exactly what is going on in the game at all times.
MOBIUS sells for $20.00

TENPIN. A very speech friendly bowling alley game with multi media
sounds.  Use ear and hand skills to throw a bowling ball!
TENPIN sells for  $30.00

 You can purchase these programs through Ann Morris Enterprises.

If you have any suggestions, comments, ideas for other games, or
problems feel free to contact Carl at the address below.

The market base for programs to supply the visually impaired and
blind community is small.  If programmers are going to write fun
and enjoyable programs then your support is very important.    Giving or
receiving unpaid programs and using them only makes it very hard for people to
produce products for this small base of people.  For example, a
very good leisure game has been on the market for several years,
and in all that time only 250 to 300 games have been sold.  The
game is very good and either there are many unpaid for games out
there, or not many people like it.  Programmers are not
looking for any hand outs. Just pay them for what is do them. 
Thank you for your time and support and if you would like to give
me some feedback on this or any other topic feil free to contact
me.

Carl Mickla 
551 Compton ave.
Perth Amboy N.J.  08861
Phone (9 0 8) 8 2 6 - 1 9 1 7


         