             TD 2.0 UPGRADE PROCEDURE AND CHANGE LOG

TD 2.0 costs $70.  It may be obtained for a $20 upgrade fee from
any registered user of a previous version.

You can upgrade your system to TD 2.0 either with commands at the
DOS prompt or with the install batch file.  You cannot do so,
however, with the previous version of TD, since the main program
file (TD.EXE) cannot be replaced while it is loaded in memory.  As
with any upgrade procedure, it is wise to make a backup just in
case you need to restore the previous version.

To upgrade from the DOS prompt (or another DOS shell), copy the 4
new executable files TD.EXE, TD1UTIL.EXE, TD2UTIL.EXE, and
TDSET.BAT into your TD program directory (the directory containing
the previous TD.EXE).  By default, this directory is C:\TD.  Thus
if you are in the directory containing the new files, you could
enter the following 2 commands:

copy *.exe c:\td
copy *.bat c:\td

To upgrade with the batch file, run the INSTALL command from the
DOS prompt and then answer the questions, pressing F10 to accept
default values.  The install procedure can be run either from a
floppy disk or a temporary directory on your hard disk, depending
on where the new files are located.

                         TD 2.0 CHANGES

TD 2.0 is a major upgrade.  Many features have been added, and all
bugs discovered in previous versions have been corrected.  The main
documentation file has been significantly revised to incorporate
the new features.

As a bonus included with the registered version of TD 2.0, Jim
Ansley (a beta tester) contributed a complimentary copy of the
"Perfecto" macro he developed for WordPerfect 5.1.  Since many TD
users are also users of this word processor, it will be helpful to
them in automatically proofreading and correcting common formatting
errors in documents.  The included archive, PERF255T.ZIP, contains
the macro and documentation.  Please send the author any feedback
on your experience with it.  Thanks Jim!

The Tab and Shift-Tab keys can now be used to go to the next or
previous file in directory mode, to the next or previous paragraph
in view mode, and to the next or previous field when in a "dialog
box" (such as in the finance calculator).  Enter works the same as
Tab, except it exits a dialog when pressed on the last field.

The finance calculator now uses the maximum decimal precision that
the Clipper language supports.  For easier reading, however, the
displayed answer (corresponding to the field that one left blank)
is rounded.
 
The finance calculator retains its flexibility, but is more
friendly now.  Tips to guide common calculations appear
parenthetically after some input fields.

Since an interest rate is usually given in terms of a year, a user
may not know the monthly rate, which TD needs if a monthly time
period is used.  TD will do the conversion for you if you indicate
a yearly rate by appending the letter 'y' (upper or lower case) to
the value in the percent change field.  Without a trailing 'y', TD
interprets the percent change literally and calculates it every
time period. To calculate an interest rate in annual terms using a
monthly period as an input, put a 'y' in the percent change field
rather than leaving it blank.

Arithmetic calculations can be done "on the fly" with any of the
input fields.  For example, to express a time period of years in
terms of months, enter the number of months followed by "/12" to
indicate division by 12.

TD runs fine under Windows 95, either from the DOS prompt without
loading Windows, or from a DOS box after Windows is loaded.

The trial version of TD now gives periodic registration reminders,
after the first 10 minutes of use, after the next 5, after the next
2.5, etc.  It will not interrupt a command, however, including
View).

The batch Delete and Move commands now return the file pointer to
the current file before the batch operation occurred, provided that
file was not one of the ones deleted or moved.

An example of the flexibility of this calculator follows.  If you
want to know how many months it will take to pay back a loan at a
certain interest rate and payment level, then enter the loan as the
initial amount, the effective monthly interest rate as the percent
change, a negative number for the constant adjustment (indicating
subtraction of a payment from the outstanding loan), blank for the
time periods, and 0 for the resulting amount (meaning you've paid
back the loan.)

TD is getting ready for the 21st century!  Input prompts for dates
now provide 4 digits for the century rather than assuming a prefix
of 19.

I've improved the finance calculator feature.  The fields are now
initial amount, constant adjustment, percent change, time periods,
and resulting amount.  The model assumes an initial amount (e.g.,
a principal sum) that has modifications made to it over a number of
time periods (e.g., months or years).  Two kinds of modifications
may be made at the end of each time period.  A constant adjustment
is made, which may be a positive or negative number (e.g., annuity
payment).  A percent change is made, which may also be positive or
negative (e.g., growth due to interest or inflation).  The
resulting amount is what remains following the modifications
calculated after the last time period.As before, leave one and only
one of these values blank, and TD will calculate it.  Other fields
may be specified as 0 if appropriate, e.g., if there is no constant
adjustment.

I've tried to make this model flexible enough that, with
appropriate values, it can be used for calculating, say, how long
it will take to pay back a loan (leave time periods blank) or what
payments are needed to save for the college tuition of a child
(leave constant adjustment blank), or how much one will earn with
a certain interest rate (leave resulting amount blank).  Due to
computer rounding errors, the answer is not exact.

As with the calendar option, previous values are retained so that
one can easily change a single variable.  Also, like other input
prompts in TD, typing a new value (or tapping the Spacebar) will
erase the previous value.  To edit a previous value, use a cursor
key first (including Home or End to go to the beginning or end of
the field).

Previously if you renamed a directory in TD, it lost track of
descriptions of files in that directory and its subdirectories. 
This has been corrected.

After reading the DOS documentation for the share.exe utility, I
recommend removing or commenting out this command if present in
your autoexec.bat file, since it prevents TD from being loaded more
than once.  If one needs it for Windows applications, I suggest
including it an a batch file that loads Windows.

The "delete after archiving" option no longer has to reread the
current directory after completion.  It tracks which files were
deleted and adjusts the directory list accordingly.

As well as initializing the index or database, one can initialize
the configuration file TD.CNF.

The statistics command is invoked by Alt-S.

The # command performs some calendar, phone number, or arithmetic
calculations.

The calendar feature lets you calculate a day from a date or vice
versa.  In the day, week, month, and year fields, you can enter a
positive number (to move forward), negative number (to move
backward), unsigned number (to set this value), or word alternative
(e.g., Saturday or December instead of a numeric day or month). 
Leaving a field blank will generally indicate the current value.

For example, if you put 14 in the day field, 12 in the month, and
63 in the year, you would get my birthdate!  The resulting date
format would display not only the day, month, and year, but the day
of the week and the occurence of that day within the month.

if you want to know when is Memorial Day in 1997, you could enter
Monday for the day, 4 for the week, 5 for the month, and 97 for the
year (the 4th Monday in May 1997).  The best way to learn this
feature is by experimenting with different values for the fields.

The phone number feature lets you calculate the digits
corresponding to a vanity phone number.  Just enter the combination
of letters and numbers, and TD will produce an equivalent sequence
with the letters translated into numbers.

The arithmetic feature lets you perform some basic arithmetic.  Use
+ for addition, - for subtraction, * for multiplication, and / for
division.  If a valid number is calculated, the result may be
edited for subsequent calculations.

If your descriptions seem out of sync with the file names, or you
are getting some other kind of database error, you can now
re-initialize the TD index and database files from within TD. 
Press Alt-I to invoke the Initialization command.  Select I to
initialize the index only (TD.CDX), or D to initialize the database
and index (TD.DBF, TD.DBV, AND TD.CDX).  If you proceed without
aborting, you will be told the current number of file descriptions
and bookmarks when the operation is complete.

Data security has been increased for operations involving file
descriptions and bookmarks.  Changes in the TD database that stores
this information are saved to disk immediately.

Automatic quoting will now recognize and quote self executing .ZIP
files (with a .EXE extension).

Since TD's database of file descriptions is in a compressed format,
the benefit of automatically describing your whole hard disk may be
worth it.  I have a gigabyte drive that is at least 50% bigger
because of drivespace compression.  Unfortunately, I have less than
20 Megs free at this time!  When I did the
td /quote
command to automatically describe all non-described files (whenever
possible), the total size of the td.dbf/td.dbv/td.cdx database was
about 4 megabytes.

If you ever want to start over with your file description database,
simply delete the above mentioned files and TD will automatically
recreate them.

The List command has been enhanced so that you can skip the
"More/Continuous" prompt by pressing a C immediately after the L.

Automatic quoting is smarter--it will remove lines containing only
punctuation marks and nonprintable characters from the beginning of
the quoted material.  This means that a meaningful description is
more likely to be displayed in directory mode (since the first line
of the description is displayed to the right of the file name).

The # statistics command now works inside as well as outside .ZIP
archives.

Once I started using an external utility (td1util.exe) to convert
word processor files, TD lost the general ability to get a quick
view of a file by pressing V twice in succession.  This approach
still works on plain ASCII text files, which TD does not pass to
the external utility before entering view mode.  An enhancement to
the + show command now accomplishes essentially the same thing.  If
the current file has a description, it shows it as before.  If it
does not, then TD shows what the " command would produce as an
automatic description.  It does not actually attach this
description.  To do so, use the " command.

A new batch file, tdset.bat, replaces four previous ones which you
may delete:  hoston.bat, hostoff.bat, dualon.bat, and dualoff.bat. 
Copy tdset.bat into your TD directory and run it without any
parameters for a help screen.

This 2.0 version of TD will automatically convert file descriptions
of a previous version so that the volume label is only associated
with files on floppy disks.  This means that you can copy your
TD.DBF and TD.DBV description database to another computer and your
file descriptions will appear when the same directory structure is
present (even if the volume labels differ between hard disks on the
two computers).  A description of a file on a floppy disk is still
referenced by the volume label as well as the path of the file, so
that one can have files with the same names but different
descriptions on different floppy disks.  This change solves a
problem where some users lost their file descriptions when copying
their TD installation from one computer to another--not realizing
that the volume labels on the hard disks were significant then.

Problems were discovered in operations involving viewing,
unarchiving, or renaming files with paths inside .ZIP archives. 
Sometimes, memory corruption would eventually result.  I think I've
now corrected these problems.

When executing a user defined command or shell command involving
the current file, its size, date, and time variables are now
refreshed upon returning to directory mode--in case the command
changed the file.  This means you now longer have to refresh the
whole directory with the . command in order to check these
attributes (with the Size or Time command) of a file you've
changed.

The 
td /quote
command (note that the word quote is used here rather than the
punctuation mark) now should complete quoting all available drives
and directories (it previously ran into a memory problem after a
while).

The author of the TD1UTIL.EXE utility has supplied me with a new
version (included in this archive) which does a better job
converting Microsoft Word documents.

The W and numeric commands can be executed without the % or ^
parameters by combining them with the Alt key.  For example, to
load your word processor with a blank screen rather than with the
current file in TD, press Alt-W.  Similarly, if you have an ASCII
text editor defined for the 1 command, you could press Alt-1 and
load it without the current file.

The " command now works with any file that can be viewed, not just
.zip files.  This is a quick, automated way of getting a
description attached to a file.  It takes the first 22 lines of
text (1 screen worth).  You could tag all files in a directory and
then execute the " command on them.  You can also enter
td /quote

at the DOS prompt to attach descriptions to all viewable files on
your hard disk.  When you come across a description you think needs
improvement, you can manually change it with the @ command, perhaps
removing the quoted description first with the - command.

The Jump command is enhanced so that you can jump to a file with a
particular attribute by specifying the appropriate punctuation
symbol as follows:

\ for a subdirectory
> or < for a tagged or untagged file
] or [ for a read-only or read-write file
} or { for a system or general file
) or ( for a hidden or visible file
@ for a file with an attached description
; for a file with a bookmark set

As usual, if the previous parameter is accepted by just pressing
Enter, the Jump command will go to the next occurence in the
directtory.

The most requested TD enhancement has been implemented:  a bookmark
capability!  When in view mode, press S to set a bookmark, D to
delete it, or semicolon (;) to go to it.  If a file has a bookmark,
a ; will precede its description (if any) when in directory mode.

There is a new Alt-S command that computes some file statistics I
have often found useful when deciding whether to read or share a
file.  Press # or Alt-S to get an estimate of the number of pages,
lines, and characters on a line that a file contains.  It will be
most accurate with DOS text files.

TD now sorts directory as well as file names in the index order
selected.  Directories will still be grouped separately from files. 
The Index command is much faster.

TD now handles more kinds of file conversions using an updated
TD1UTIL.EXE, which I've included in this archive.  It should handle
HTML files from the web, Microsoft Word 6-7 documents, and
Microsoft Publisher files that are not too complex.  An HTML file
must begin with the characters
<html>
to be recognized.  The Alt-V command now creates an ASCII text file
with carriage returns only at the ends of paragraphs (rather than
after every line), which makes it better for importing into a word
processor document.

TD now runs in protected mode, using about 250K of conventional
memory and up to about 2M of extended memory.  A batch file in the
TD directory allows changing and identification of the current
mode.It is called "dual mode," because TD will still run in real
mode if insufficient extended memory is available or less than a
286 processor is in use (gasp!).  I have found that TD experiences
fewer random memory corruption problems when running in protected
mode.

TD is now a multi-user program.  More than one copy can be loaded
at the same time.  Thus you could load WordPerfect from TD, then
shell to DOS and load TD again for some file management operations.

the Go command of TD now handles file specifications as flexibly as
the Jump command.  You can use wildcards in ways that DOS doesn't
support.  For example, if you want to load a directory of files
that contain "td" anywhere in the name or extension, you could
enter the following specification:
*td*

Support has been added for system and hidden files, as well as
those with the readonly attribute.  A system file will be followed
by a } character and a hidden file by a ) character.  The { and }
commands make files system or general, while the ( and ) commands
hide or reveal files.  Thus, for example, most computers will have
the following file in the root directory of drive c:
msdos.sys]})

meaning it has the readonly, (or write protected), system, and
hidden attributes set.

In addition to the % symbol, there is now support for three more
special symbols in the definitions of numeric commands.  The ^
symbol means to substitute the current file name, but without its
extension.  This is because some programs or batch files assume a
particular extension and do not work properly if the extension is
specified.  Think of the ^ symbol being adjacent to and similar to
the % symbol.

The ~ symbol means that TD should prompt for parameter(s) to enter. 
TD will substitute whatever the user enters for the ~ symbol in the
command definition.  Think of ~ meaning "pause for input," and
recall that ~ is a half-second pause symbol for modems.

the ` symbol means that TD should display a "Press any key to
continue" message after the program runs, thus allowing you to
inspect any of its screen display before returning to TD.  It
should be the last character in the command definition and be
preceded by a space.  A parenthetical command description can still
follow it.  Think of the accent adding emphasis to the command by
retaining its screen of output before returning to TD.


                         TD 1.2 CHANGES

The TD 1.2 version corrects a few minor bugs and adds some feature
enhancements.

Alt-U will test the integrity of a .ZIP archive.  You might use it
to test before unarchiving a .ZIP file whose integrity may be in
doubt.  It works even if the file extension is something other than
.ZIP.  This allows you to check with a file with a .EXE extension
is actually a self executing .ZIP archive.  The Zoom command now
also works with self executing .ZIP files.  You do not have to test
an archive you just created with the Archive command, since TD will
indicate whether there was an error in archiving.

Some other .ZIP related enhancements are as follows.  You can now
Zoom into a self extracting .ZIP archive.  If a .ZIP file includes
subdirectory information, this will now be shown when you Zoom
inside it.  When you unarchive a .ZIP file that contains
subdirectory information, you will be asked whether to create
subdirectories.  You can now execute user defined commands besides
View when inside a .ZIP file.  If the command definition contains
a %, it will be ignored.  For example, pressing W would simply load
your word processor without passing it the current file (since it
is actually inside the .ZIP archive).

The View command will now read almost any DOS or Windows word
processor format with the help of two utilities TD1UTIL.EXE and
TD2UTIL.EXE which must be in the same directory as the main
executable TD.EXE.  If these files are not present in the TD
program directory, the View command will work like before with
ASCII text and WordPerfect 5.1 formats.  With these utilities, TD
will view any WordPerfect, Microsoft Word, and Lotus Amie Pro
formats, as well as most Windows help files.  

The new Alt-V command will generate a viewable output file, rather
than entering view mode.  You will be prompted for the name of the
ASCII text file to create.  Its lines will be no more than 65
characters in length, thus enabling it to be loaded into a word
processor, such as WordPerfect, without causing line wrap problems.

TD now more intelligently determines whether a command has added a
file to the current directory.  For example, if you create a note
file within view mode, you will be placed on that file when you
return to directory mode.  Similarly, if you archive without
deleting, you will be brought to the archive.  (The same behavior
of rereading the current directory still holds if you delete after
archiving.)  Likewise, if you create a viewable output file in the
current directory, you will be brought to it.  In all of these
cases, the new file is inserted just before the file you were on
before you executed the command that created the file.  The new
file is not placed in proper sort order.  This allows you to
conveniently press Enter to return to the file you were on before
(since it will be the next file after the inserted one).


                         TD 1.1 CHANGES

The following is a list of features new or modified in TD 1.1: 

The default index order is now used whenever TD displays a new
directory.  This order is remembered from one TD session to
another.  Thus, for example, you could always have directories
presented in descending order by the time and date a file was last
updated.  To identify the current order, press the ? key in
directory mode.

As before, the > and < keys tag or untag the current file.  Now,
however, they automatically go to the next file.  This is a quicker
way to selectively tag files in a directory.

Three new commands provide quicker ways of performing common
directory changes.  The . command, as before, redisplays the
current directory, reading information from disk into memory.  This
is useful when you've added, modified, or deleted files in the
current directory in a way that TD doesn't know about--e.g., by
using the / shell command and invoking an external program.  The ,
command next to it goes to the parent directory, if any.  This is
useful if you went to a child directory by other than the Zoom
command and then you want to go to the parent directory (since Quit
will not do this in this case).  The : command allows you to select
a drive to go to.  The \ command goes to the root directory of the
current drive.

As before, the T command says when a file was last updated.  A new
command, Alt-T, displays the current time according to the system
clock.

The Archive command now lets you easily specify an archive name
similar to the current file name.  By leaving the name blank, the
base name of the current file will be used with a .ZIP extension. 
For example, JAMAL.TXT would be archived to a file called
JAMAL.ZIP.  After specifying the name of the archive, a new prompt
asks whether to delete after archiving.  If yes, the current
directory is automatically re-displayed and a jump is issued to the
archive file (if in the current directory).

All new commands are available on the directory mode menu, invoked
with the Alt-F1 or Alt-H keys.

In view mode, the U and L commands define the upper and lower lines
of a block to copy, as before.  Now, however, TD pays attention to
the cursor position on the line.  It marks the beginning of the
block on the upper line and the end of the block on the lower line. 
Another change is that the copied block will preserve the original
formatting better by replacing "soft carriage returns" with nothing
rather than "hard carriage returns."  This allows wrapping to be
done more smoothly when the note file is loaded into a word
processor.

The search commands in view mode (Forward, Reverse, and Again) now
place the cursor at the beginning of the search string when found,
rather than at the beginning of the line.  As before, the Home and
End keys go to the beginning and end of the line, respectively.  In
addition, Control-Home now goes to the beginning of the first line
on the screen and Control-End goes to the end of the last.

The index file is now TD.CDX rather than TD.CMX and the file of
attached descriptions is TD.DBV rather than TD.DBT.  These file
formats are compressed and more efficient.  For example, my TD.DBV
file is a third the size of the previous TD.DBT file.  Conversion
to these new formats occurs automatically when the old formats are
detected.  It may take a few minutes.