These are the instructions for LOOK.
 
Look provides an electronic index for FLIPPER.  To use LOOK,
just start it up with the command:
 
LOOK INDEX
 
or, if you are using a hard disk,
 
LOOK \flipper\index
 
You can then use the arrow keys, up and down to browse through
Flipper's index.  If you find a topic of interest, you can then
press L for look up.  You will then be popped into the
appropriate part of the instructions for Flipper.  If you want,
you can then press T to try different sections which may also be
relevant, or the escape key to go back to the index.  You can
also look at these instructions by pressing the H key.  Press
escape to leave help.  You can also use LOOK to look at other
text files.  You can not change files with look, you can only
browse through them.
 
 
 
The following commands are available.  They are laid out in
two columns, first the command, and then the explanation of
what it does.
 
Commands for LOOK:
 
  Command                                   Action
  Q                                         Quit LOOK
  H                                         look at the documentation
  L                                         look up index listing
  T                                         try another listing
  I                                         go back to the index
  ESC                                       go back to the index
 
 
  up arrow                                  line up.
  down arrow                                line down.
  right arrow                               right character.
  left arrow                                left character.
  control right arrow                       right word.
  control left arrow                        left word.
  home                                      beginning of line.
  end                                       end of line.
  control home                              beginning of file.
  control end                               end of file.
  page up                                   page up.
  page down                                 page down.
 
  control F                                 simple Find.
  control L                                 repeat find.
  control U                                 Abort command.
 
 
  control K control Q                       Quit look.
 
  control K control M                       Set a Marker.
  control K 1                               Set marker 1.
  control K 2                               Set marker 2.
  ...                                       ....
  control K 9                               Set marker 9.
 
  control Q control F                       Find pattern (with options).
  control Q control J                       Jump to Marker.
  control Q 1                               Jump to marker 1.
  control Q 2                               Jump to marker 2.
  ...                                       ....
  control Q 9                               Jump to marker 9.
 
  control J control L                       Jump to a Line.
  control J control C                       Jump to a Column.
 
Options for the find command include the following letters:
  u                                         ignore case
  b                                         search backwards
  w                                         find whole words only
 