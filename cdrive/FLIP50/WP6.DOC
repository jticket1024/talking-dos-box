These are the basic instructions for Wordperfect.  Autoloading
should be on (function key four from review) to have the proper
flipper configuration loaded.

After Wordperfect is installed, you start editing a file by typing
WP followed by the name of the file that you want to edit.  If you
want to start a new document, you need to make up a unique name for
it which is 8 characters long, followed by a period and up to three
additional characters.  

When you start Wordperfect with a new document, the screen is
mostly empty, with just the name of the file in the lower left hand
corner, and information about where you are in the document in the
lower right hand corner.  There is also menu on the top line (line 1).
It works a lot like a typewriter, with
what you type going directly on the screen.  You do not need to hit
return within a paragraph, wordperfect will automatically go to a
new line when it is needed.  Unlike a typewriter, however, it is
easy to read back what you have typed and correct it.  If you make
a mistake, for example, backspace will erase the last character
typed and say it.  

If you move a line at a time up or down with the arrow keys,
Flipper will read the lines.  You can move to the right or left a
word at a time by holding down the control key and pressing the
left or right arrow keys.  Flipper will read the words as you move.
You can hold down alt while pressing down the right or left arrow
keys and Flipper will move one sentence forwards or backwards, and
read the corresponding sentence.  If you hit alt up arrow, Flipper
will read the current sentence.  If you hit control down arrow,
wordperfect will move to the next paragraph, and Flipper will read
the first sentence.  If you hit control up arrow, Wordperfect will
move back a paragraph and Flipper will read the first sentence of
that paragraph.  If you press alt down arrow, Flipper will start
reading through the text until it gets to the end, or until you
stop it by hitting alt. Control backspace will delete and read the
current word.  After you have moved to somewhere that you want to
add something, you can just start typing, and the text will be
inserted.  You can press delete to delete characters.
Alt 6 reads the last pop up box.  Alt 7 reads the bottom status
line, line 25.  Alt 8 reads the doc, page, line, position.

Most Wordperfect commands start with a function key, and you then
follow with numbers or letters for menu choices.  Pressing function
key 7, for example, will exit wordperfect, or exit any menu level. 
Control function key 2 will start the spelling checker.  Function
key 2 will let you search for some text.  After you have typed what
you want to look for, hit function key two again to start the
search.  Searching is a very efficient way to move around a
document, if you know what to look for.  Alt function key 2 will
let you do a search backward.  

After you have typed a document, you can print it to your printer
by pressing shift function key 7, and then F for the full document.
You can also use the pull down menus for Wordperfect.  To get the
menus, press alt equal.  You can then move through the menus with
the arrow keys, right and left to change major categories, and up
and down for specific items.  Once you have found the item you
want, press return.  

The help mode for Wordperfect contains a lot of information about
using wordperfect.  To start Wordperfect's help mode, press
function key three.  The first screen, which is a template, is not
well organized for reading, but the other information is easy to
access.  If you then press a command key, Wordperfect will
present a page of information about that command.  Each page can
be read by pressing alt p to read the current page.  You may
also need to go into review mode to read the screen line by
line.  Press a function key, and a page of information will be
put up about that function.  Flipper will also read the label
for the function key, so you can simply look for the one you
want this way.  If you press an alphabetic key, Wordperfect will
show you the portion of its index starting with that letter.
You can, in this way, find the key sequence that will do the
function that you want.  To see pages for a letter beyond the
first, hit the same letter again.

To spell check your document, press control function key 2, then D
for document.  The first misspelled word that wordperfect has found
will be spoken.  You can reread the word with alt K, or spell it
with alt comma.  To read the line containing the misspelled
word, hit alt I.  Wordperfect will give you a number of possible
correct spellings.  You can read move through the possible
correct spellings with the arrow key.  Each choice is labeled
with a letter of the alphabet, starting with A, and you can just
press the corresponding letter to replace the misspelled word
with the correct spelling.  If the word is not correct, but
Wordperfect does not give you a correct alternative, you can
edit the word instead.  Press function key 7 when you are done
editing and you want to continue to the next misspelled word.

When wordperfect needs to reformat, the line you move to with a
down arrow may not be complete.  The Flipper disk includes a file
called ALTR.WPM which is a macro for wordperfect that you can put
in your wordperfect directory, so that when you hit alt R it will
reformat the whole document at once.

The files WP.FLP and WP.FAC are the configuration files for
WordPerfect.


Some technical information about how wp60.flp is configured:
 
Windows:
 
Window 6 is assigned to alt 6, it reads the last pop up box.
 
Window 7 is assigned to alt 7, it reads the bottom status line, line 25.
 
Window 8 is assigned to alt 8, it is set to read the doc, page, line, position.

Cursors:

The first Cursor is a DOS Cursor.  It is normally disabled, it is enabled
when reaction zone 9 sees block mode turning on.  It is turned off when
reaction zone 10 sees block mode turned off.
 
The second Cursor is a DOS Cursor which is valid only on a red enhancement.
This is used for most command lightbars, in menues and dialogue boxes.  It
is disabled during help mode.
 
The third Cursor is any Red Enhancement, it is used for spell checking,
and help mode.
 
Cursor Four is set for enhancement number 71, on lines one through seven.
It is used for the spell check for the word in context.
 
Cursor five is searches from top to bottom for enhancement number 71, it is
used for reveal codes mode.
 
Cursor six is a DOS cursor valid any where  on the screen.  It is used for
normal editing.
 
Cursor Seven looks for a small right arrow on a specific enhancement number,
it is used by the Tab key to read out the commands it points to in spell
mode, the file menu, etc.
 
Cursor Eight is a DOS cursor, it is used when the tab is used and there is
no small right arrow.
 
REACTION ZONES
 
Reaction Zone One is unused.
 
Reaction zone two and three turn turn reaction zone four on and off, as help
as the Help Title appears and disappears from line three.
 
Reaction zone four reacts only to red enhancements, and reads window 43,
which is set to read all the red enhancements.
 
Reaction Zone five reacts to increases in the number of windows on lines
one through twenty four, and then reads window one, which is the top box.
 
Reaction Zone Six reacts to the message checking on line 17, and reads the
message "checking".
 
Reaction Zone Seven
 
Reaction Zone Eight is unused.
 
Cursor one is turned on when reaction zone 9 sees block mode turning on.
It is turned off when reaction zone 10 sees block mode turned off.
 
 
 

