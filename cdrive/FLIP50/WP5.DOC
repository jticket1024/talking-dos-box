These are the basic instructions for Wordperfect.  Autoloading
should be on (function key four from review) to have the proper
flipper configuration loaded.

After Wordperfect is installed, you start editing a file by typing
WP followed by the name of the file that you want to edit.  If you
want to start a new document, you need to make up a unique name for
it which is 8 characters long, followed by a period and up to three
additional characters.  

When you start Wordperfect with a new document, the screen is
mostly empty, with just the name of the file in the lower left hand
corner, and information about where you are in the document in the
lower right hand corner.  It works a lot like a typewriter, with
what you type going directly on the screen.  You do not need to hit
return within a paragraph, wordperfect will automatically go to a
new line when it is needed.  Unlike a typewriter, however, it is
easy to read back what you have typed and correct it.  If you make
a mistake, for example, backspace will erase the last character
typed and say it.  

If you move a line at a time up or down with the arrow keys,
Flipper will read the lines.  You can move to the right or left a
word at a time by holding down the control key and pressing the
left or right arrow keys.  Flipper will read the words as you move.
You can hold down alt while pressing down the right or left arrow
keys and Flipper will move one sentence forwards or backwards, and
read the corresponding sentence.  If you hit alt up arrow, Flipper
will read the current sentence.  If you hit control down arrow,
wordperfect will move to the next paragraph, and Flipper will read
the first sentence.  If you hit control up arrow, Wordperfect will
move back a paragraph and Flipper will read the first sentence of
that paragraph.  If you press alt down arrow, Flipper will start
reading through the text until it gets to the end, or until you
stop it by hitting alt. Control backspace will delete and read the
current word.  After you have moved to somewhere that you want to
add something, you can just start typing, and the text will be
inserted.  You can press delete to delete characters.

Each of the function keys will read wordperfect's label for them
when pressed.  Alt 7 reads the status line, which is good for re-
reading prompts as well, and alt 8 will read the page number,
vertical and horizontal position.  

Most Wordperfect commands start with a function key, and you then
follow with numbers or letters for menu choices.  Pressing function
key 7, for example, will exit wordperfect, or exit any menu level. 
Control function key 2 will start the spelling checker.  Function
key 2 will let you search for some text.  After you have typed what
you want to look for, hit function key two again to start the
search.  Searching is a very efficient way to move around a
document, if you know what to look for.  Alt function key 2 will
let you do a search backward.  

After you have typed a document, you can print it to your printer
by pressing shift function key 7, and then F for the full document.
You can also use the pull down menus for Wordperfect.  To get the
menus, press alt equal.  You can then move through the menus with
the arrow keys, right and left to change major categories, and up
and down for specific items.  Once you have found the item you
want, press return.

The help mode for Wordperfect contains a lot of information about
using wordperfect.  To start Wordperfect's help mode, press
function key three.  The first screen, which is a template, is not
well organized for reading, but the other information is easy to
access.  

If you then press a command key, Wordperfect will present a page of
information about that command.  Each page can be read by pressing
alt p to read the current page.  You may also need to go into
review mode to read the screen line by line.  Press a function key,
and a page of information will be put up about that function. 
Flipper will also read the label for the function key, so you can
simply look for the one you want this way.  If you press an
alphabetic key, Wordperfect will show you the portion of its index
starting with that letter.  You can, in this way, find the key
sequence that will do the function that you want.  To see pages for
a letter beyond the first, hit the same letter again.

To spell check your document, press control function key 2, then D
for document.  The first misspelled word that wordperfect has found
will be spoken.  You can reread the word with alt K, or spell it
with alt comma.  To read the line containing the misspelled word,
hit alt I.  Wordperfect will give you a number of possible correct
spellings, starting on line 15.  You can read the choices by
hitting alt 9.  Each choice is labeled with a letter of the
alphabet, starting with A, and you can just press the corresponding
letter to replace the misspelled word with the correct spelling. 
You can also go into review and jump to line 15 with the command 15
I, and then move through the choices with the arrow keys.  When you
have found the correct spelling, leave review with the alt spacebar
and then type the letter.  If the word is not correct, but
Wordperfect does not give you a correct alternative, you can edit
the word by hitting 4 to edit.  Press function key 7 when you are
done editing and you want to continue to the next misspelled word. 

When wordperfect needs to reformat, the line you move to with a
down arrow may not be complete.  The Flipper disk includes a file
called ALTR.WPM which is a macro for wordperfect that you can put
in your wordperfect directory, so that when you hit alt R it will
reformat the whole document at once.

The files WP.FLP and WP.FAC are the configuration files for
WordPerfect.
 

