
SECTION 6.0 OPTIONS INDEX

Flipper allows you to select many options.  The options are
selected by pressing the function keys during the review mode. 
All of these options give you two choices and alternate between
them when you press their key.  For example, if you press
function key nine once, it will say keyboard echo is now on if
you press it again, it will say keyboard echo is now off.  If you
press it a third time, it will come back to keyboard echo is now
on.  Every option has exactly two alternatives.  This allows you
to check current settings by simply pushing a toggle key twice. 
The option will always be back where it was, and the message
will tell you its setting.

Two complete sets of options are maintained inside of Flipper, 
and others can be created in the computer's memory.  See
section 5.10 for instruction to use multiple configurations and
quickly flip between them.



Function key 1

automatic output on.

no automatic output.

This allows you to select whether things that are sent to the
screen by a program are spoken automatically, or not.  If this
option is off then Flipper will only speak when you ask it to or
to echo keystrokes.  The automatic output will only work for
some programs, and it does not usually work for editors.  Some
programs will speak too much if this option is on.  This output
can be shut up temporarily (until the next keystroke) by pressing
alt.  Function key 2 will change the way that automatic output
is caught by Flipper.  This key will toggle the automatic output
on and off, whether it is set to extended mode, or normal.

Function key 2

extended automatic output method.

normal automatic output.

This option changes the method Flipper uses to read text sent to
the screen, which we call automatic output.

When turned on, many programs which would not read
automatically before will read their prompts and other
information.  Try it with WordPerfect, for example.  In some
cases this option will read too much, and you will need to turn
it off or set up quiet windows.

Function key 3

non stop review mode.

freeze screen and application during review mode.

When you are in review mode, the application is normally
frozen, so that you can examine the screen without it changing. 
If non stop review is chosen, the application continues to run
and the screen may change while you are looking at it.  This is
useful for processes where you want to monitor while the
computer is doing something, such as downloading a file or
compiling.

Function key 4

do not automatically load configurations. 

allow configurations to be automatically loaded.

If you are creating configurations, you usually do not want them
to be automatically loaded, because they would overwrite the
one you are working on before it is saved.  

Function key 5

over ride all symbols on.

say only selected symbols.

Flipper can read the full symbol set that can appear on the
computer's screen, including graphics symbols, decorative
symbols, and others.  However, you will often want to turn
some of them off so that you won't get overwhelmed.  This
option allows you to temporarily turn all symbols back on. You
turn off symbols by pressing delete and then selecting symbols
from the keyboard or, for symbols which aren't on the
keyboard, from a list of special symbols.  You can turn on
selected symbols after you press insert.  Symbols can also be
turned on or off in blocks by using the function keys after either
insert or delete. If you are reading out one character at a time,
for example when you are spelling, anything is read, whether it
is on or not.
To go through the list of special symbols, you use control keys
after pressing the insert key.  If you press control F, control M,
control D, or control A, you will be given a list of form
symbols, math symbols, decorative symbols or accent and other
foreign symbols, respectively, and asked to select the ones you
want.


Function key 6

delayed time out for listening to remote typing.

normal automatic output response.

If you are using a computer to communicate with another person
live, for example chatting with a bulletin board operator, they
can not usually type fast enough so that Flipper thinks that the
output all goes together, and it will read parts of words or
individual characters.  This option slows Flipper down so that
what the other person types will come out in phrases.

Function key 7

Magic Cursor.

This toggles between magic cursor and custom cursor.
When set to magic cursor, flipper automatically finds the
cursor in most applications.  When set to custom cursor,
Flipper will use the cursor defined in the menus.

Function key 8

dos cursor used if it is on the screen. 

use the cursor defined in the cursor menu.

Sometimes both the dos cursor and some kind of enhancement
cursor will be available on the screen, and you will want to be
able to switch back and forth between them.  This toggle allows
you to set up the cursors in the cursor menu to find the other
cursor type, and then switch back and forth between them.


Function key 9

keyboard echo is now on.

no keyboard echo.


This allows you to turn off the keyboard echo for the whole
keyboard.  If you want to turn keyboard echo off only for
specific keys, you can define them with D K, and say that you
do not want them echoed.  If the next option (word echo) is on
and this option is off, Flipper will only echo words as they are
completed, and nothing else.  The only exception is the caps
lock and num lock on and off messages, which are turned off
separately.

Function key 10

echo keystrokes as words.

fast single key echo.

This allows you to have the alphabetic keys echoed quickly as
each key is pressed, or to accumulate letters into words before
echoing them.  (Echoing simply means to say what was
pressed). If you press any key besides a letter the word is
considered to be finished and the word is spoken along with the
name of the other key that was pressed.

Control function key 1

Always say period, comma, apostrophe and question mark if
they are on. 

If the period, comma, apostrophe, and question mark are on say
them only if they are not in their proper places.

If this option is on, Flipper will read out the punctuation marks:
period, comma, question mark, and apostrophe, only if Flipper
does not consider them to be in their proper place.  For
example, if an embedded period appears, like in
FLIPPER.DOC, Flipper will read it off.  Note that Flipper will
read . as point in this circumstance.  (That is, not after a letter
and before a space.)  If this option is off Flipper will read it as
period if it is at the end of a sentence, and point otherwise. It is
also possible to turn off these symbols completely by pressing
delete in the review mode and then the punctuation that you
want to turn off. (However, the decimal point in numbers can
never be turned off)

Control function key 2

Controls whether repeated symbols (like *****) are counted, or
ignored altogether.

Control function key 3

spell everything.

do not spell everything.

Spell everything.  Sometimes, for proofreading or programming,
it is necessary to have Flipper spell to you instead of reading
words.  This option has Flipper spell everything that it reads off
the screen.  If you want automatic output spelled, you need to
turn that on with control function key 4.

Control function key 4

spell automatic output.

do not spell automatic output.

This toggle can cause all automatic output, that is messages from
the program which go through the bios and are read, to be
spelled.

Control function key 5

spaces are counted when reading lines.

do not count spaces when reading lines.

Sometimes you need to know exactly how many spaces occur
between words or symbols on the screen.  If you turn this option
on, Flipper will tell you how many spaces are on the screen
whenever you read a line at a time or a window a time.

Control function key 6

spaces are counted when reading words.

do not count spaces when reading words.

Flipper counts all repeated symbols.  However, spaces are only
counted at specific times. If you are reading a word at a time
and this option is on it will count the number of spaces between
the words or symbols on the screen.  There is an associated
option (control function key 5) which will make Flipper count
spaces when reading whole lines at a time.

Control function key 7

fore ground color enhancements on.

turn off fore ground colors mode.

When this option is on, Flipper uses the color the characters
would appear on a color screen to describe enhancements instead
of things like high intensity, underlined, etc.  If both foreground
and background colors are off, Flipper will describe
enhancements as they would appear on a monochrome screen.

Control function key 8

back ground color enhancements on.

turn off back ground colors mode.

This option is similar to foreground colors on, except
enhancements are described in terms of the changes to the
background rather than the character. You can turn both on at
the same time.

Control function key 9

inverse is any back ground color except black.

black fore ground symbols are defined as inverse.

This option changes what Flipper refers to as inverse video. The
two options are to call a character inverse video if the
foreground color is black, or, to call it inverse video only if the
background is any color except black.

Control function key 10

enhancement changes are now being spoken.

no enhancement changes are spoken.

Text can be printed as plain white on black but it can also be
printed blinking, underlined, bright, or inverse (black on white). 
On a color screen it can also be printed in different colors.  This
option allows you to choose to have this indicated to you when
it is encountered.  Other options, control function keys 7 and 8,
allows you to select whether these enhancements are described
as they appear on a monochrome screen or as they appear on a
color screen, foreground, background, or both.

Alt function key 1

beep when the computer stops after alt key. 

turn off beep after alt key.

After you press alt to silence the automatic output, Flipper will
wait until the program is ready for keystrokes again, and then it
will beep.  This toggle will turn off this beep.

Alt function key 2

click when something appears on screen.

do not click when the screen changes.

When this option is on, Flipper will click about once per second
whenever the screen is changing, even if it is being written to
directly and the automatic output doesn't work.


Alt function key 3

cursor beeps on.

turn off cursor beeps.

When this option is on, you will hear a beep each time the
cursor is moved to a new line, with the tone indicating where on
the screen the cursor is, higher tones for higher on the screen,
and lower for lower on the screen.


Alt function key 4

click during disk access.

do not click when the disk is accessed.

Alt function key 5

Not currently used.

Alt function key 6

Not currently used.

Alt function key 7

capital and small letters match in a search.

capital letters do not match small letters during a search.

During a search, capital letters are usually treated as being
identical to small letters.  If you want to search for an exact
match, including capitalization, turn on this option.

Alt function key 8

move back to previous location after aborting current paragraph.

stay where reading was stopped when current paragraph.

Alt function key 9

echo shift, alt, and control.

do not echo shift, alt and control.

Alt function key 10

turn off caps lock message.

say caps lock on or off.

Alt+control function key 1

auto key interactive mode.

silent auto key.

This option, which is more fully documented in the file
AUTOKEY.DOC, limits the messages that are spoken from the
AUTOKEY mode.  This is appropriate when you are playing
back macros and do not want things spoken like enter search
pattern or press key to be defined. This option is not stored in
configurations.  If you want to change it from a batch file, use
the FLIP program.

Alt+control function key 2

keyboard protection on. 

no keyboard recapture.

If a program, particularly a resident program, takes over control
of the keyboard, Flipper will regain control so that you can
continue to have access to Flipper commands.  This option
allows you to turn off that protection. 

Alt+control function key 3

highlight what flipper reads in review.

mark review cursor position only in review.

This feature is designed for people with reading difficulties or
low vision who want a full highlighting of what flipper is
reading in review mode.


Alt+control function key 4

This feature allows you to define the keys on the keypad
separately from the cursor keys, so you can define more
functions.  Remember that laptop computers usually do not have a
keypad, so if you are developing configurations for general use,
keep this in mind.

Not currently used.  

Alt+control function key 5

read line numbers in review.

do not say line numbers in review.

When you read a line at a time in the review mode, Flipper
usually reads out the line number.  This option prevents that.

Alt+control function key 6

use margins set using the w key, when reading lines or the
whole page.

do not use margins, read to edge of screen on both sides.

When you read a line at a time, you may not want to read from
one edge of the screen to the other.  For example, you may not
want to have a border decoration used by your word processor
read.  You may also want to limit Flipper to reading a column. 
You can tell Flipper what part of each line to read by pressing
the M key (for margin) while in the review mode and Flipper
will ask for the left and right hand edges of the portion that you
want read. This option toggle allows you to turn that feature off
temporarily, and cause Flipper to read the whole line without
changing the margin settings.

Alt+control function key 7

Not currently used.  

Alt+control function key 8

Not currently used.  

Alt+control function key 9

Not currently used.  

Alt+control function key 10

Not currently used.

Shift function key 1

faster.

Raise the speed of the synthesizers for all voices.


Shift function key 2

slower.
Lower the speed of the synthesizers for all voices.

Shift function key 3

say blank for blank lines.

do not say blank for blank lines.

Flipper ordinarily says nothing if a line is blank.  If this option
is on, Flipper will say blank if you try to read a blank line.

Shift function key 4

If two synthesizers and their drivers are installed, this key will
shift from one to the other.  The primary synthesizer will be the
one which is lower in memory.  (the one loaded first if you are not
using memory management.

Shift function key 5 

Say cap before capital letters.

do not say cap before capital letters except when spelling.

Flipper ordinarily only tells you when a letter is capitalized if
you are spelling. This option allows you to have Flipper say
capital before capital letters all the time.  Flipper will say all
caps at the start of a word which is all capitalized.

Shift function key 6
say cap before capital letters in spell mode.

change pitch for capital letters in spell mode.

Capital letters will be read with a higher pitch when you are
spelling rather than saying cap before each capital letter.

Shift function key 7

spell part numbers which contain letters and digits.

read words with embedded numbers as words.

When this option is on, if Flipper comes across a string of
characters which are mixed letters and digits, it will spell it
character by character.  If there are three or more letters at the
start, Flipper will read them as a word, and then spell out the
remaining characters.  This is also good for HAM call signs.

Shift function key 8

pass through all ascii characters to synthesizer.

process characters normally.

Flipper usually does a lot of work to send text to the synthesizer
which will be spoken clearly and consistently. This includes
changing digit strings to spelled out numbers, counting repeated
symbols, and converting special symbols to words.  This option
allows you to send text to the synthesizer directly, so that you
can use the synthesizer's features to do this kind of processing.

Shift function key 9

now reading digit strings as numbers.

now reading off digits separately.

For example one two three, or as an amount, one hundred
twenty three. This option allows you to choose which is done. 
If a number is incorrectly formatted, for example, if it has
commas in the wrong place, then it will be spoken as digits
anyway.

Shift function key 10

send numbers to the synthesizer without interpretation. 

have flipper interpret numbers into spoken english before
sending to synthesizer.

Voice commands:

Voice selection is started by pressing V during the review mode.
Each of Flipper's three voices can be changed together, or
independently. Pressing a function key by itself changes all three
voices.  Pressing a function key with the shift key changes the
automatic output voice alone.  Pressing a function key with the
alt key depressed changes the keyboard echo voice.  Pressing a
function key with the control key depressed changes the
command voice by itself. Press the enter key to leave the voice
control, and return to the review mode.  The function keys
control the following voice characteristics:

Function key 1

Raise the speed of the synthesizer.

Function key 2

Lower the speed of the synthesizer.

Function key 3

Raise the volume of the synthesizer.

Function key 4

Lower the volume of the synthesizer.

Function key 5

Raise the filter of the synthesizer.
Function key 6

Lower the filter of the synthesizer.

Function key 7

Raise the inflection of the synthesizer.

Function key 8

Lower the inflection of the synthesizer.

Function key 9

Raise an auxiliary parameter for the synthesizer.

Function key 10

Lower an auxiliary parameter for the synthesizer.
