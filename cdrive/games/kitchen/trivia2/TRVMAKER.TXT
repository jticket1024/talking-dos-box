     The trvmaker program can be used to create your own question and
     answer files for the trivia game.  To start the program simply type
     trvmaker and press enter.  The program will first search the
     directory for files that you have worked on and saved.  If it finds
     such files it will ask if you want to work on or convert one of the
     files or start a new file.  If you want to work on or convert a
     saved file press number 2 as your choice.  You will then be asked
     if you want to add to a saved file or convert a file.  You will
     then be given a list of the saved files that were found, just enter
     the number of the file that you want to add to or convert.  If you
     choose to work on a new file or if there are no saved files found
     you will be asked to enter a file name for the question and answer
     file.  The file name can not be more than 8 letters long and should
     not have an extension.  You will then be asked for a title for the
     file.  The title can be as long as 78 characters.  Next you will be
     asked for your name.  Again your name can be as long as 78
     characters.  Now you are ready to start entering questions and
     answers.  The questions can be up to 9 lines long but no more than
     78 characters per line.  You press enter on a blank line to end the
     question.  Now you can enter up to 9 possible answers.  The answers
     can only be one line long and only up to 77 characters.  Again
     press enter on a blank line to end entering answers.  Next you will
     be asked to enter the number of the correct answer and to confirm
     it.  You will next be asked to enter comment lines that will come
     up after the question is answered in the trivia game.  Again press
     enter on a blank line to end entering comment lines.  You don't
     even have to enter any comment lines, just press enter on the first
     line.  Next you will be shown the questions and answers so that you
     can check to see if you will want to write them to the question and
     answer file.  Then you will be shown the comment lines.  Now you
     will be asked if you do want to write the question, answers and
     comment lines to the question and answer file.  If you answer no
     the question, answers and comment lines will be deleted.  At this
     point you will be asked if you want to add more questions and
     answers, convert the file, exit the program or start the program
     over from the start.
     
     If you would rather use your favorite word processor to write a
     question and answer file please read the convert2.txt file.
     
     If you want to please send me the file that you create and I would
     very much like to put it up on my web site.
     
     If you have any trouble, a question or comment about this program
     please write or call me.
     
     jim.kitchen@pcohio.net
     http://users.nowonline.net/jkitchen
     1-440-286-6920
     Chardon Ohio
