     
     To run LIFE simply type life at the dos prompt and press enter.  If
     you start the life game with a /s switch it will run without the
     sound effects.  You can also toggle the sound effects while playing
     the game by pressing the S key.
     
     If you are using the JAWS screen reader I have included a Jaws
     Dictionary File.  As with all JDF files it should be put into your
     Jaws directory.
      
     If you have any trouble, a comment or a question about the
     program, my e-mail address is jimkitchen@simcon.net 
     Or you may write or call.
               
                        Jim Kitchen
                        385 Center Street Apt. 13
                        Chardon, Ohio  44024
                        (440) 286-6920
               
