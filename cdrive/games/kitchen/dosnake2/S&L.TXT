     
     To run the Snakes and Ladders program simply type s&l and press
     enter.  please read the instructions in the game the first time
     that you play the game.  You can adjust the delays in the program
     from the command line with the /d switch.  So that your synthesizer
     is not speaking at the same time that the wave files are being
     played.  Such as:
     
     s&l /d 100  100 is the default.
     
     If you have any problems, questions or comments please write or
     call.
     
     jimkitchen@simcon.net
     http://simcon.net/public/jkitchen
     1-440-286-6920
     Chardon Ohio
     
