DECLARE SUB lp (a#, b#, a$)
DECLARE SUB sb (a$, b$)
DECLARE SUB cs ()
DECLARE SUB dl (a, b)
DECLARE SUB lc (a)
DIM sak(11)
OPEN "o", #1, "con"
co$ = LCASE$(COMMAND$)
q = 1
IF co$ <> "" THEN GOSUB setq
spt$(1) = "wplay": spt$(2) = "plany": spt$(3) = "nusound /v:10"
cn$(1) = "Ira": cn$(2) = "Theo": cn$(3) = "Cryton": cn$(4) = "Joshua"
cn$(5) = "Hal": cn$(6) = "Selma": cn$(7) = "R 2 D 2": cn$(8) = " C 3 P O"
GOTO getcolor
bfgc:
CLS
SCREEN 0
WIDTH 80
ON ERROR GOTO nocfg
OPEN "i", #2, "s&l.cfg"
INPUT #2, sd$
CLOSE #2
gotcfg:
CLS
sb sd$, "title"
lp 1, 19, " Snakes and Ladders         By Jim Kitchen"
inyorn:
lp 3, 1, " Would you like to read the instructions Y or N"
GOSUB gkey
IF g$ = "y" THEN GOTO inpage
IF g$ <> "n" THEN GOTO inyorn
startgame:
GOSUB setsl
re1:
lp 5, 1, " Please enter the number of players 1 to 8"
LOCATE 5, 44: LINE INPUT nop$
nop = VAL(nop$)
IF nop < 1 OR nop > 8 THEN lc 5: sb sd$, "afraid": GOTO re1
FOR x = 1 TO nop
playerspace(x) = 1
NEXT x
x = 6
FOR getname = 1 TO nop
x = x + 1
LOCATE x, 1: PRINT #1, " Please enter the name of player number";
PRINT #1, getname;
LOCATE x, 43: LINE INPUT plan$(getname)
NEXT getname
x = CSRLIN + 1
re2:
cn = INT(RND(1) * 8) + 1
IF cn = cnold THEN GOTO re2
cnold = cn
LOCATE x, 1: PRINT #1, " May i " + cn$(cn) + " the computer play Y or N";
GOSUB gkey
IF g$ = "n" THEN GOTO nocomp
IF g$ <> "y" THEN LOCATE x, 1: PRINT SPC(78); : GOTO re2
nop = nop + 1
plan$(nop) = cn$(cn)
playerspace(nop) = 1
compnum = nop
nocomp:
GOSUB setwave
whosup = 1
gameon = 1
joy = 1
mainloop:
cs
lp 3, 1, " " + plan$(whosup)
lp 5, 1, " you are at space number"
PRINT #1, playerspace(whosup)
IF whosup = compnum THEN dl q, 2: GOTO autoroll
lp 7, 1, " press space bar to roll the die"
GOSUB gkey
autoroll:
RANDOMIZE TIMER
sb sd$, "dieroll"
dr = INT(RND(1) * 6) + 1
lp 9, 1, " You roll a"
PRINT #1, dr
dl q, 1
IF playerspace(whosup) + dr > 100 THEN GOTO over
FOR x = 1 TO dr
sb sd$, "step"
IF sd$ = "plany" THEN dl q, .3
NEXT x
playerspace(whosup) = playerspace(whosup) + dr
IF playerspace(whosup) = 100 THEN GOTO win
said = 0
checkforladorsak:
cek$ = RIGHT$(STR$(playerspace(whosup)), 1)
IF cek$ = "0" THEN GOTO upladder
IF cek$ = "5" THEN GOTO downsak
IF said = 1 THEN GOTO skipsay
GOSUB showspace
skipsay:
lp 23, 1, " press space bar to continue"
GOSUB gkey
whosup = whosup + 1
IF whosup > nop THEN whosup = 1
GOTO mainloop
over:
lp 13, 1, " sorry " + plan$(whosup)
lp 15, 1, " but you need to land exactly on 100 to win"
GOTO skipsay
leavegame:
sb sd$, "title"
LOCATE 23, 1, 1
SYSTEM
win:
cs
GOSUB showspace
dl q, 1.5
sb sd$, "iwon"
lp 19, 1, " " + plan$(whosup)
lp 21, 1, " You WON!"
dl q, 1
sb sd$, "clap"
GOTO leavegame
showspace:
lp 11, 1, " " + plan$(whosup)
lp 13, 1, " you are now at space"
PRINT #1, playerspace(whosup)
RETURN
upladder:
said = 1
sb sd$, u$
whatlad = playerspace(whosup) / 10
playerspace(whosup) = lad(whatlad)
GOSUB showspace
dl q, 1.5
GOTO checkforladorsak
downsak:
said = 1
sb sd$, d$
whatsak = playerspace(whosup) / 10 + .5
playerspace(whosup) = sak(whatsak)
GOSUB showspace
dl q, 1.5
GOTO checkforladorsak
gkey:
DO: LOOP UNTIL INKEY$ = ""
DO: LOOP UNTIL STRIG(0) = 0 AND STRIG(4) = 0
dd = 0
DO
IF joy = 1 AND STRIG(0) <> 0 THEN joy = 2: EXIT DO
IF joy = 1 AND STRIG(4) <> 0 THEN joy = 2: EXIT DO
g$ = INKEY$
LOOP UNTIL g$ <> ""
IF joy = 2 THEN joy = 1: sb sd$, "joy": RETURN
g = VAL(g$)
dd = ASC(RIGHT$(g$, 1))
g$ = LCASE$(g$)
IF dd = 27 AND gameon = 1 THEN GOTO exitornot
RETURN
setsl:
RANDOMIZE TIMER
xx = 99
FOR ld = 1 TO 9
xx = xx - 10
lad(ld) = INT(RND(1) * xx) + (ld * 10) + 1
NEXT ld
xx = 0
FOR sn = 1 TO 10
xx = xx + 5
repick:
sak(sn) = INT(RND(1) * xx) + 1
IF sak(sn) = xx THEN GOTO repick
bad = 0
IF RIGHT$(STR$(sak(sn)), 1) = "0" THEN GOSUB check0
IF bad = 1 THEN GOTO repick
NEXT sn
RETURN
check0:
c4s = xx * 10 - 5
c4l = sak(sn) / 10
IF c4l = c4s THEN bad = 1
RETURN
setwave:
cs
lp 3, 1, " Would you like to play with this set of wave files"
dl q, 2.5
sb sd$, "upa"
dl 1, .5
sb sd$, "downa"
dl 1, .1
lp 5, 1, " or this set"
dl q, 1
sb sd$, "upb"
dl 1, .5
sb sd$, "downb"
dl q, .5
lp 7, 1, " Please enter 1 for the first set or 2 for the second set"
LOCATE 7, 70: LINE INPUT ws$
ws = VAL(ws$)
IF ws < 1 OR ws > 2 THEN GOTO setwave
IF ws = 1 THEN u$ = "upa": d$ = "downa"
IF ws = 2 THEN u$ = "upb": d$ = "downb"
cs
RETURN
exitornot:
lp 25, 1, " Press E to exit game S to shell to dos or any other key to return to game"
DO: LOOP UNTIL INKEY$ = ""
DO
g$ = LCASE$(INKEY$)
LOOP UNTIL g$ <> ""
IF g$ = "e" THEN CLS : GOTO leavegame
IF g$ = "s" THEN GOTO shellpro
RETURN
shellpro:
kps$ = CURDIR$
CLS
PRINT #1, " O K shelling to dos"
PRINT #1, " "
SHELL "command /k prompt=Type exit to return to Snakes and ladders.$_$p$g"
FOR rep = 333 TO 666 STEP 111
SOUND rep, 2
NEXT rep
rdr$ = LEFT$(kps$, 2)
SHELL rdr$
ps = LEN(kps$)
rcd$ = "cd" + RIGHT$(kps$, (ps - 2))
SHELL rcd$
CLS
lp 1, 19, " Snakes and Ladders         By Jim Kitchen"
lp 3, 1, " Press space bar to continue"
GOTO gkey
nocfg:
RESUME nc0
nc0:
ON ERROR GOTO 0
nc1:
LOCATE 15, 1: PRINT SPC(78);
LOCATE 16, 1: PRINT SPC(78);
LOCATE 8, 1: PRINT #1, " Please press the number of the sound player program to try"
LOCATE 10, 1: PRINT #1, " 1 = wplay"
LOCATE 11, 1: PRINT #1, " 2 = plany"
LOCATE 12, 1: PRINT #1, " 3 = nusound"
GOSUB gkey
IF (g < 1 OR g > 3) THEN GOTO nc1
sd$ = spt$(g)
nc2:
sb sd$, "aha"
sb sd$, "clap"
LOCATE 15, 1: PRINT #1, " Press y to use this sound player"
LOCATE 16, 1: PRINT #1, " or if you didn't hear anything then press n to try a different player"
GOSUB gkey
IF (dd > 58 AND dd < 64) THEN GOTO nc2
IF g$ <> "y" THEN GOTO nc1
OPEN "o", #2, "s&l.cfg"
PRINT #2, sd$
CLOSE #2
GOTO gotcfg
getcolor:
ON ERROR GOTO noco
x = SCREEN(1, 1, 1)
dfc = (x AND 128) \ 8 + (x AND 15)
dbc = (x AND 112) \ 16
COLOR dfc, dbc
renoco:
ON ERROR GOTO 0
GOTO bfgc
noco:
RESUME renoco
setq:
j = INSTR(co$, "/d")
IF j = 0 THEN RETURN
k = LEN(co$)
k = k - (j + 1)
q = VAL(RIGHT$(co$, k))
q = q / 100
RETURN
inpage:
lp 3, 5, " After you choose how many players are going to play, enter all of"
lp 4, 5, " the names and decide if you want the computer to play along as well"
lp 5, 5, " as choose which set of wave files you want to use it will be time"
lp 6, 5, " to start the game.  All players will start at space number 1.  You"
lp 7, 5, " will take turns rolling the die and moving that number of spaces. "
lp 8, 5, " The object of the game is to be the first person to land on space"
lp 9, 5, " number 100.  If you land on space number 5, 15, 25, 35, 45, 55, 65,"
lp 10, 5, " 75, 85 or 95 you will land on a snake and slide downward.  The"
lp 11, 5, " length of the snakes will be different every game.  If you land on"
lp 12, 5, " space 10, 20, 30, 40, 50, 60, 70, 80 or 90 you will land on a"
lp 13, 5, " ladder and will climb upward.  Like with the snakes the ladders"
lp 14, 5, " lengths will be different every game.  Sometimes a snake may land"
lp 15, 5, " on a ladder or another snake.  The same goes for the ladders.  So"
lp 16, 5, " if you land on a snake or ladder you may go up and down more than"
lp 17, 5, " once in one turn.  Good luck and I hope that you like the game."
lp 19, 5, " press the f1 key to repeat the instructions"
lp 21, 5, " Press any other key to continue"
GOSUB gkey
cs
IF dd = 59 THEN dl 1, .5: GOTO inpage
GOTO startgame

SUB cs
FOR clearscreen = 2 TO 25
LOCATE clearscreen, 1
PRINT SPC(79);
NEXT clearscreen
LOCATE 2, 1
END SUB

SUB dl (a, b)
zz = a * b
st = TIMER
DO
LOOP UNTIL TIMER > st + zz
END SUB

SUB lc (a)
LOCATE a, 1
PRINT SPC(79);
END SUB

SUB lp (a#, b#, a$)
LOCATE a#, b#
PRINT #1, a$;
END SUB

SUB sb (a$, b$)
SHELL a$ + " " + b$ + ".wav > nul"
END SUB

