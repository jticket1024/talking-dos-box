     To play the Battle Ship game simply type bship at the dos prompt
     and press enter.  Please answer y to read the instructions the
     first time that you play the game.  If the game doesn't seem to
     work right you may need to add a line such as
     device=c:\dos\ansi.sys to your config.sys file.
     
     If you have any trouble, a comment or a question about the
     program, my e-mail address is jimkitchen@simcon.net 
     Or you may write or call.
               
                        Jim Kitchen
                        385 Center Street Apt. 13
                        Chardon, Ohio  44024
                        (440) 286-6920
               
