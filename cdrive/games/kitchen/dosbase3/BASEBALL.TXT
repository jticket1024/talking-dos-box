     If you would like to play a game with less scoring start the game
     with the /s xx switch.  xx being a number between 1 and 15.  The
     higher the number the lower the scoring.  You can also set the
     delay time with the /d xxx switch.  The default setting is 100.
     The function keys 1 through 6 will repeat important information for
     you.  You may want to try the batpract program to get a little
     batting practice in before you start a game of baseball.
     When asked to press the number of your choice just press the
     number.  You don't need to press the enter key.
     If you have a sound player program that you like better than the
     ones that came with this game you can edit the baseball.cfg file
     and just put the name of that program in there.

     If you have any trouble, a comment or a question about the
     program, my e-mail address is jimkitchen@simcon.net 
     Or you may write or call.
               
                        Jim Kitchen
                        385 Center Street Apt. 13
                        Chardon, Ohio  44024
                        (440) 286-6920
