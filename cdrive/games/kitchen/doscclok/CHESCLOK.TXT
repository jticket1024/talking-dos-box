     To run the chess clock program simply type chesclok and press
     enter.
     
     In addition to the left shift key for the left player, the left
     player can use the tab key or the left arrow key.  Similarly the
     right player besides the right shift key can use the enter key or
     the right arrow key.
     
     If you have any trouble, a comment or a question about the
     program, my internet address is jimkitchen@simcon.net
     Or call me at 1-440-286-6920
     
     Down-load my other programs from my web site at:
     http://www.simcon.net/public/jkitchen
