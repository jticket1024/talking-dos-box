     To use the TCB calculator simply type tcb at the dos prompt and
     press enter.  Pressing the esc key will exit the program, but your
     work will still be on the screen.  Use the asterisk or shift 8 key
     to multiply.  Use the / key to divide.  If you are not multiplying,
     dividing or subtracting, the enter key is the same as pressing the
     + key.  Pressing the enter key twice is the same as pressing the =
     key.  Or you can use the p, m, t, d and e keys.  Pressing the c key
     will clear the total.  The default setting is for two decimal
     points.  If you need the calculator to be more precise, start the
     program with the number of decimal points that you need
      such as tcb 5
          
     If you have any trouble, a comment or a question about the
     programs, my internet address is jimkitchen@simcon.net
     Or you may write or call.
               
                        Jim Kitchen
                        385 Center Street Apt. 13
                        Chardon, Ohio  44024
                        (440) 286-6920
               
