     To play Slot36 simply type slot at the dos prompt and press
     enter.  If the game doesn't seem to work right, you may need to add
     a line such as DEVICE=c:\dos\ansi.sys to your config.sys file.  The
     slot machine has three wheels.  Each wheel has three windows.  The
     center window is the one that counts.  It will be repeated if you
     press the F2 key.  It is at row number seven and the type is white
     on black.  In row five are the wheel characters that were about to
     come down.  The F1 key will repeat This top window.  In row nine
     are the wheel characters that just passed the center line.  The F3
     key will repeat this bottom window.    Both line five and line nine
     are blue on black.  You can change the wheel characters to what
     ever you want them to be.  There is a file that came with this game
     called sltwheel.dat.  You can use your text editor to change this
     file.  It contains the wheel characters.  The file now reads LEMON,
     CHERRY, PLUMB, ORANGE, * BAR *, and JACKPOT.  They need to be in
     order from lowest pay to highest.  Please note that the characters
     have a maximum length of seven letters.  You will also see a
     comment in rows eight, ten and twelve.  This information starts at
     column sixty two.  Press the F4 key to repeat the comment rows. 
     You can create your own comments by editing the comment.dat file. 
     Each comment can have up to three lines with a maximum of seventeen
     characters per line.  At the beginning of the line that follows
     each comment just place a greater than sign.  ( just keep the file
     format the same as it is now )  There is also information in row
     twenty and twenty two.  this information starts at column
     twenty.  The F5 key will repeat this information.  You can toggle
     the speaking of the messages by pressing the T key when you are
     asked to insert a coin.  Every time that you start Slot36 or move
     to a different machine, the three wheels are randomly created.So it
     is truly a new game every time.
          
     if you have a sound file player program that you like better than
     the ones that came with this game, you can edit the slot36.cfg
     file and type in the name of the program.

     If you have any trouble, a comment or a question about the game, my
     internet address is jimkitchen@simcon.net 
     Or you may write or call.
          
                        Jim Kitchen
                        385 Center Street Apt. 13
                        Chardon, Ohio  44024
                        (440) 286-6920
