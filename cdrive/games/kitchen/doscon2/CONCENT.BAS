DECLARE SUB sb (a$, b$)
DECLARE SUB lp (a, b, a$)
DECLARE SUB dl (a, b)
DIM prizes$(150), bord(41), bd(41), bs(21)
DIM pzwon(3, 2, 20), npzw(3, 2), vp(150)
DIM james$(150), kitchen(150), jr(150)
DIM kp$(41), ks(41), ks$(41)
spt$(1) = "wplay /q": spt$(2) = "plany": spt$(3) = "nusound /v:10"
z = 1
cmd$ = LCASE$(COMMAND$)
k = LEN(cmd$)
IF k = 0 THEN GOTO noswitch
j = INSTR(cmd$, "/d")
IF j = 0 THEN GOTO noswitch
k = k - (j + 1)
z = VAL(RIGHT$(cmd$, k))
z = z / 100
noswitch:
gmtp = 2
OPEN "con" FOR OUTPUT AS #1
OPEN "i", #2, "concent.dat"
ON ERROR GOTO er1
gl:
g = g + 1
LINE INPUT #2, james$(g)
IF james$(g) = "" THEN g = g - 1: GOTO gl1
LINE INPUT #2, herman$
kitchen(g) = VAL(herman$)
IF kitchen(g) <= 0 THEN GOTO er2
gl1:
IF EOF(2) THEN GOTO sl
IF g >= 150 THEN GOTO sl
GOTO gl
sl:
CLOSE #2
IF g > 65 THEN GOTO sr
DO
g = g + 1
READ james$(g)
READ herman$
kitchen(g) = VAL(herman$)
LOOP UNTIL g > 65
sr:
tnumofpz = g
ON ERROR GOTO nocfg
OPEN "i", #2, "concent.cfg"
INPUT #2, sd$
CLOSE #2
gotcfg:
gam = 1
GOSUB 9000
30 REM main loop
pp1 = 0: pp2 = 0
90 co$ = " Press the first letter "
GOSUB pc
GOSUB gkey
GOSUB cc
k1$ = g$
IF k1$ = "a" THEN r1 = 5: ra1 = 0: GOTO 100
IF k1$ = "b" THEN r1 = 7: ra1 = 8: GOTO 100
IF k1$ = "c" THEN r1 = 9: ra1 = 16: GOTO 100
IF k1$ = "d" THEN r1 = 11: ra1 = 24: GOTO 100
IF k1$ = "e" THEN r1 = 13: ra1 = 32: GOTO 100
co$ = " THAT WAS AN INVALID LETTER PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 90
100 co$ = " Press the first number "
GOSUB pc
GOSUB gkey
GOSUB cc
k2 = VAL(g$)
IF (k2 > 0 AND k2 < 9) THEN 110
co$ = " THAT WAS AN INVALID NUMBER PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 100
110 REM check for blank grid location
pz1 = ra1 + k2
IF bord(pz1) > 0 THEN 120
cl = LEN(player$(wt)) + 2
co$ = " THAT WAS AN INVALID SPACE PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 90
120 COLOR 31, 0
col1 = (k2 - 1) * 2 + 12
LOCATE r1, col1: PRINT "*";
COLOR 7, 0
pp1:
LOCATE 18, 10: PRINT #1, " " + k1$;
PRINT #1, k2;
PRINT #1, "= ";
IF spe = 3 THEN GOTO noplay1
dl z, .5
sb sd$, "panopen"
noplay1:
c$ = UCASE$(LEFT$(prizes$(bord(pz1)), 1))
GOSUB 6950
LOCATE 18, 17: PRINT #1, aoran$(aoran);
PRINT #1, prizes$(bord(pz1));
LOCATE 20, 10: PRINT #1, " Value = ";
amt = vp(bord(pz1))
GOSUB pu
IF spe = 3 THEN spe = 0: GOTO gkey
pp1 = 1
140 co$ = " Press the second letter "
GOSUB pc
GOSUB gkey
GOSUB cc
k3$ = g$
IF k3$ = "a" THEN r2 = 5: ra2 = 0: GOTO 150
IF k3$ = "b" THEN r2 = 7: ra2 = 8: GOTO 150
IF k3$ = "c" THEN r2 = 9: ra2 = 16: GOTO 150
IF k3$ = "d" THEN r2 = 11: ra2 = 24: GOTO 150
IF k3$ = "e" THEN r2 = 13: ra2 = 32: GOTO 150
co$ = " THAT WAS AN INVALID LETTER PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 140
150 REM get second number
co$ = " Press the second number"
GOSUB pc
GOSUB gkey
GOSUB cc
k4 = VAL(g$)
IF (k4 > 0 AND k4 < 9) THEN 170
co$ = " THAT WAS AN INVALID NUMBER PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 150
170 REM check second set for valid
IF (k1$ = k3$ AND k2 = k4) THEN 180
pz2 = k4 + ra2
IF bord(pz2) > 0 THEN 190
180 REM invalid second space
co$ = " THAT WAS AN INVALID SPACE PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 140
190 REM print second prize
col2 = (k4 - 1) * 2 + 12
COLOR 31, 0
LOCATE r2, col2: PRINT "*";
COLOR 7, 0
pp2:
LOCATE 22, 10: PRINT #1, " "; k3$;
PRINT #1, k4;
PRINT #1, "= ";
IF spe = 3 THEN GOTO noplay2
dl z, .5
sb sd$, "panopen"
noplay2:
c$ = UCASE$(LEFT$(prizes$(bord(pz2)), 1))
GOSUB 6950
LOCATE 22, 17: PRINT #1, aoran$(aoran);
PRINT #1, prizes$(bord(pz2))
LOCATE 24, 10: PRINT #1, " Value = ";
amt = vp(bord(pz2))
GOSUB pu
IF spe = 3 THEN spe = 0: GOTO gkey
pp2 = 1
IF bord(pz1) = 1 THEN 2000
IF bord(pz2) = 1 THEN 2010
IF bord(pz1) = bord(pz2) THEN 300
co$ = " SORRY BUT THAT IS NOT A MATCH"
GOSUB pc
dl z, 4
sb sd$, "miss"
co$ = " Press any key to continue"
GOSUB pc
GOSUB gkey
sb sd$, "panclose"
cc = (r1 - 5) / 2 + 9
COLOR cc, 0
LOCATE r1, col1: PRINT "?";
cc = (r2 - 5) / 2 + 9
COLOR cc, 0
LOCATE r2, col2: PRINT "?";
COLOR 7, 0
GOSUB clean
GOTO 1000
300 REM made a match
co$ = " YES THAT IS A MATCh"
GOSUB pc
dl z, 3
sb sd$, "match"
co$ = " Press any key to continue"
GOSUB pc
GOSUB gkey
301 REM for ski
GOSUB clean
cc = (r1 - 5) / 2 + 9
COLOR cc, 0
LOCATE r1, col1: PRINT "-";
cc = (r2 - 5) / 2 + 9
COLOR cc, 0
LOCATE r2, col2: PRINT "-";
COLOR 7, 0
IF ski = 1 THEN 302
npzw(gam, wt) = npzw(gam, wt) + 1
pzwon(gam, wt, npzw(gam, wt)) = bord(pz1)
bord(pz1) = 0
bord(pz2) = 0
302 ski = 0
310 pp1 = 0: pp2 = 0
LOCATE 16, 10: PRINT #1, " " + player$(wt) + " ";
LOCATE 18, 10: PRINT #1, " The Secret Number is between 0 and 100";
LOCATE 20, 10: PRINT #1, " Enter the Number of your Guess ";
LOCATE 20, 42: LINE INPUT gu$
guess = VAL(gu$)
GOSUB clean
IF guess > 0 AND guess < 100 THEN GOTO 400
LOCATE 18, 10: PRINT #1, " Sorry that was an invalid guess"
LOCATE 18, 1: PRINT SPC(78);
GOTO 310
350 GOSUB clean
co$ = " THAT WAS AN INVALID GUESS PRESS ANY KEY TO CONTINUE "
GOSUB pc
GOSUB gkey
GOSUB cc
GOTO 310
400 REM check for a win
IF guess = winnum THEN 450
p1$ = " Sorry but"
IF guess < winnum THEN p2$ = "is too low "
IF guess > winnum THEN p2$ = "is too high "
sp2:
LOCATE 20, 10: PRINT #1, p1$;
PRINT #1, guess;
PRINT #1, p2$;
IF spe = 2 THEN GOTO noplay3
dl z, 1.5
sb sd$, "miss"
noplay3:
PRINT #1, " But it is still your turn ";
co$ = " PRESS ANY KEY TO CONTINUE "
GOSUB pc
IF spe = 2 THEN GOTO gkey
spe = 2
GOSUB gkey
spe = 0
GOSUB clean
GOTO 30
450 REM win
co$ = " YOU GOT IT THATS A WIn"
GOSUB pc
dl z, 2
sb sd$, "win"
co$ = " Press any key to continue"
GOSUB pc
GOSUB gkey
CLS
rd1:
rd = 1
tot = 0
LOCATE 1, 10: PRINT #1, " " + player$(wt) + " you have won these prizes ";
LOCATE 3, 1
FOR t = 1 TO npzw(gam, wt)
PRINT " #";
PRINT #1, t;
p$ = prizes$(pzwon(gam, wt, t))
c$ = UCASE$(LEFT$(p$, 1))
GOSUB 6950
PRINT #1, aoran$(aoran) + p$;
LOCATE CSRLIN, 66
amt = vp(pzwon(gam, wt, t))
GOSUB pu
tot = tot + vp(pzwon(gam, wt, t))
PRINT
IF npzw(gam, wt) < 9 THEN PRINT
NEXT t
PRINT
LOCATE CSRLIN, 58: PRINT #1, "TOTAL = ";
amt = tot
GOSUB pu
co$ = " PRESS ANY KEY TO CONTINUE "
pcs = 1
GOSUB pc
IF spe = 4 THEN spe = 0: GOTO gkey
GOSUB gkey
pcs = 0
rd = 0
CLS
wwwg(gam) = wt
LOCATE 3, 10: PRINT #1, " SCORE ";
p1g = 0
p2g = 0
FOR t = 1 TO 3
IF wwwg(t) = 1 THEN p1g = p1g + 1
IF wwwg(t) = 2 THEN p2g = p2g + 1
NEXT t
rd2:
rd = 2
LOCATE 5, 10: PRINT #1, " " + player$(1) + " ";
LOCATE 5, 50: PRINT #1, "Has Won";
PRINT #1, p1g;
PRINT #1, "Game";
IF p1g <> 1 THEN PRINT #1, "s";
LOCATE 7, 10: PRINT #1, " " + player$(2) + " ";
LOCATE 7, 50: PRINT #1, "Has Won";
PRINT #1, p2g;
PRINT #1, "Game";
IF p2g <> 1 THEN PRINT #1, "s";
IF p1g = 2 THEN 500
IF p2g = 2 THEN 500
co$ = " PRESS ANY KEY TO START THE NEXT GAME OR THE ESC KEY TO EXIT "
GOSUB pc
IF spe = 5 THEN spe = 0: GOTO gkey
GOSUB gkey
rd = 0
gam = gam + 1
kwt = wt
CLS
GOSUB 7000
GOSUB 9110
wt = kwt
GOTO 30
500 REM match is won
LOCATE 11, 10: PRINT #1, " " + player$(wt) + " ";
PRINT #1, "HAS WON THE MATCH ";
co$ = " PRESS ANY KEY "
GOSUB pc
IF spe = 5 THEN spe = 0: GOTO gkey
GOSUB gkey
CLS
rd3:
rd = 3
tot = 0
cp = 0
LOCATE 1, 10: PRINT #1, " " + player$(wt) + " You have won these prizes ";
LOCATE 3, 1
FOR dap = 1 TO 3
IF wwwg(dap) <> wt THEN 510
FOR t = 1 TO npzw(dap, wt)
cp = cp + 1
PRINT #1, "#";
PRINT #1, cp;
p$ = prizes$(pzwon(dap, wt, t))
c$ = UCASE$(LEFT$(p$, 1))
GOSUB 6950
PRINT #1, aoran$(aoran) + p$;
LOCATE CSRLIN, 66
amt = vp(pzwon(dap, wt, t))
GOSUB pu
tot = tot + vp(pzwon(dap, wt, t))
PRINT
NEXT t
510 NEXT dap
PRINT
LOCATE CSRLIN, 58: PRINT #1, "TOTAL = ";
amt = tot
GOSUB pu
co$ = " PRESS ANY KEY TO START A NEW MATCH OR THE ESC KEY TO EXIT "
pcs = 1
GOSUB pc
IF spe = 6 THEN spe = 0: GOTO gkey
GOSUB gkey
pcs = 0
rd = 0
gam = 1
FOR t = 1 TO 3
wwwg(t) = 0
FOR st = 1 TO 2
npzw(st, t) = 0
NEXT st
NEXT t
CLS
GOSUB 7000
GOSUB 9110
GOTO 30
1000 REM switch players ect
IF wt = 1 THEN wt = 2 ELSE wt = 1
GOSUB clean
GOTO 30
clean:
FOR deb = 16 TO 24 STEP 2
LOCATE deb, 1
FOR sprow = 1 TO 79
PRINT " ";
NEXT sprow
NEXT deb
RETURN
2000 REM first pick is a wild card
IF bord(pz2) = 1 THEN 2100
wpw = bord(pz2)
GOTO 2020
2010 REM second pick is a wild card
wpw = bord(pz1)
2020 REM adjust wild win
co$ = " YES THATS A MATCh"
GOSUB pc
dl z, 3
sb sd$, "match"
co$ = " Press any key to continue"
GOSUB pc
GOSUB gkey
bord(pz1) = 0
bord(pz2) = 0
FOR gwp2 = 2 TO 40
IF bord(gwp2) = wpw THEN wp2 = gwp2
NEXT gwp2
rww = 5
IF wp2 > 8 THEN rww = 7
IF wp2 > 16 THEN rww = 9
IF wp2 > 24 THEN rww = 11
IF wp2 > 32 THEN rww = 13
      cww = wp2
2025 IF cww < 9 THEN 2030
cww = cww - 8
GOTO 2025
2030 cc = (rww - 5) / 2 + 9
COLOR cc, 0
LOCATE rww, (cww - 1) * 2 + 12: PRINT "-";
npzw(gam, wt) = npzw(gam, wt) + 1
pzwon(gam, wt, npzw(gam, wt)) = bord(wp2)
bord(wp2) = 0
ski = 1
GOTO 301
2100 REM both picks are wild cards
vp(1) = 500
LOCATE 20, 10: PRINT " Value = $ 500.00 ";
LOCATE 24, 10: PRINT " Value = $ 500.00 ";
GOTO 300
6500 REM delay display
pcs = 1
co$ = " PRESS ANY KEY TO CONTINUE DISPLAYING PRIZES "
GOSUB pc
GOSUB gkey
CLS
RETURN
6950 REM check for a or an
aoran = 1
IF c$ = "A" THEN aoran = 2
IF c$ = "E" THEN aoran = 2
IF c$ = "I" THEN aoran = 2
IF c$ = "U" THEN aoran = 2
RETURN
7000 REM set prizes in bord
RANDOMIZE TIMER
PRINT : PRINT
PRINT #1, " The Prizes are being set into the board";
IF gam > 1 THEN 7005
prizes$(1) = "WILD CARD"
vp(1) = 0
tnop = tnumofpz
FOR st = 1 TO tnumofpz
jr(st) = st
NEXT st
FOR scram = 2 TO tnumofpz
picnum = INT(RND(1) * tnop) + 1
prizes$(scram) = james$(jr(picnum))
vp(scram) = kitchen(jr(picnum))
tnop = tnop - 1
FOR sk = picnum TO tnop
jr(sk) = jr(sk + 1)
NEXT sk
NEXT scram
7005 rp = 1
trd = INT(tnumofpz / 3)
addto = ((gam - 1) * trd) + 1
DO UNTIL rp = 21
7010 bs(rp) = INT(RND(1) * trd) + addto
IF bs(rp) = 1 THEN 7010
cp = 1
IF rp = 1 THEN 7020
7015 IF bs(rp) = bs(cp) THEN 7010
cp = cp + 1
IF cp < rp THEN 7015
7020 rp = rp + 1
LOOP
bs(1) = 1
FOR s = 1 TO 40
bd(s) = s
NEXT s
nosa = 40
FOR x = 1 TO 20
p1 = INT(RND(1) * nosa) + 1
bord(bd(p1)) = bs(x)
nosa = nosa - 1
FOR fx = p1 TO nosa
bd(fx) = bd(fx + 1)
NEXT fx
NEXT x
FOR x = 1 TO 20
p2 = INT(RND(1) * nosa) + 1
bord(bd(p2)) = bs(x)
nosa = nosa - 1
FOR fx = p2 TO nosa
bd(fx) = bd(fx + 1)
NEXT fx
NEXT x
RETURN
8000 REM set screen
CLS
SCREEN 0: WIDTH 80
RETURN
9000 REM start screen
CLS
SCREEN 0
WIDTH 80
COLOR 7, 0
LOCATE 1, 18: PRINT #1, " Concentration                By Jim Kitchen"
dl z, 2
tits = INT(RND(1) * 2) + 1
IF tits = 1 THEN sb sd$, "title1" ELSE sb sd$, "title2"
LOCATE 3, 2, 1
co$ = " Press Y if you would like to read the instructions or N if not"
GOSUB pc
GOSUB gkey
GOSUB cc
IF g$ = "n" THEN 9100
rd4:
rd = 4
LOCATE 3, 1: PRINT #1, " First you will be asked to enter the names of two players.                 "
PRINT #1, " A grid of eight numbered colums by five lettered rowswill be drawn."
PRINT #1, " The object is to make matches by pressing the letter and number of two"
PRINT #1, " grid locations that have matching prizes."
PRINT #1, " If you make a match then you try to guess the secret number,"
PRINT #1, " which is between 0 and 100."
PRINT #1, " The player that guesses the secret number keeps the prizes that they have."
PRINT #1, "Pressing the ESC key when you are asked to press any key will exit the game"
PRINT #1, " or allow you to shell to dos."
PRINT #1, " use the function keys F1, F2 and F3 to review the screen."
co$ = " press any key to continue"
GOSUB pc
IF spe = 7 THEN spe = 0: GOTO gkey
GOSUB gkey
rd = 0
9100 REM
LOCATE 14, 1
GOSUB 7000
LOCATE 18, 10: PRINT #1, " Enter Name of Player #1 "
LOCATE 18, 35: INPUT player$(1)
LOCATE 20, 10: PRINT #1, " Enter Name of Player #2 "
LOCATE 20, 35: INPUT player$(2)
l = LEN(player$(1))
IF l > 22 THEN player$(1) = LEFT$(player$(1), 22)
l = LEN(player$(2))
IF l > 22 THEN player$(2) = LEFT$(player$(2), 22)
LOCATE 16, 1: PRINT #1, " ";
9110 CLS
LOCATE 1, 19: PRINT "Concentration                By Jim Kitchen"
LOCATE 3, 12: PRINT "1 2 3 4 5 6 7 8"
LOCATE 5, 10: PRINT "A";
LOCATE 7, 10: PRINT "B";
LOCATE 9, 10: PRINT "C";
LOCATE 11, 10: PRINT "D";
LOCATE 13, 10: PRINT "E";
cc = 8
FOR r = 5 TO 13 STEP 2
cc = cc + 1
FOR c = 12 TO 26 STEP 2
COLOR cc, 0
LOCATE r, c: PRINT "?";
NEXT c
NEXT r
aoran$(1) = "A  "
aoran$(2) = "An "
wt = INT(RND(1) * 2) + 1
winnum = INT(RND(1) * 98) + 1
RETURN
9999 REM
GOSUB cc
coo$ = co$
lp 16, 1, " Press Y to quit S to shell to dos any other key will return you to the game"
rd = 5
GOTO gkey
rd5:
IF g$ = "y" THEN GOTO quit
IF dd = 59 THEN GOSUB pc: GOTO gkey
IF g$ = "s" THEN GOTO shellout
co$ = coo$
GOSUB cc
GOSUB pc
rd = 0
GOTO gkey
quit:
IF tits = 1 THEN sb sd$, "title2" ELSE sb sd$, "title1"
CLOSE #1
SCREEN 0
WIDTH 80
CLS
SYSTEM
pu:
PRINT #1, USING "$##,###,###.##"; amt;
RETURN
pc:
IF pcs = 1 THEN pcx = 25 ELSE pcx = 16
LOCATE pcx, 1: PRINT SPC(78);
LOCATE pcx, 1: PRINT #1, " " + player$(wt) + co$;
RETURN
cc:
LOCATE 16, 1
PRINT SPC(79);
RETURN
gkey:
DO: LOOP UNTIL INKEY$ = ""
DO
g$ = INKEY$
LOOP UNTIL g$ <> ""
g$ = LCASE$(g$)
dd = ASC(RIGHT$(g$, 1))
IF rd = 5 THEN GOTO rd5
IF dd = 27 THEN GOTO 9999
IF (dd = 59 AND spe = 1) THEN GOTO 310
IF (dd = 59 AND spe = 2) THEN GOTO sp2
IF dd = 59 THEN GOSUB pc: GOTO gkey
IF (dd = 60 AND pp1 = 1) THEN spe = 3: GOTO pp1
IF (dd = 61 AND pp2 = 1) THEN spe = 3: GOTO pp2
IF (dd = 60 AND rd = 1) THEN spe = 4: GOTO rd1
IF (dd = 60 AND rd = 2) THEN spe = 5: GOTO rd2
IF (dd = 60 AND rd = 3) THEN spe = 6: GOTO rd3
IF (dd = 60 AND rd = 4) THEN spe = 7: GOTO rd4
IF (dd > 58 AND dd < 65) THEN GOTO gkey
RETURN
shellout:
GOSUB shellpro
LOCATE 3, 12: PRINT "1 2 3 4 5 6 7 8"
LOCATE 5, 10: PRINT "A";
LOCATE 7, 10: PRINT "B";
LOCATE 9, 10: PRINT "C";
LOCATE 11, 10: PRINT "D";
LOCATE 13, 10: PRINT "E";
cc = 8: pzc = 0
FOR r = 5 TO 13 STEP 2
cc = cc + 1
FOR c = 12 TO 26 STEP 2
COLOR cc, 0
pzc = pzc + 1
LOCATE r, c
IF bord(pzc) = 0 THEN PRINT "-";  ELSE PRINT "?";
NEXT c
NEXT r
rd = 0
co$ = coo$
GOSUB pc
GOTO gkey
shellpro:
kps$ = CURDIR$
CLS
PRINT #1, " O K shelling to dos"
PRINT #1, " "
SHELL "command /k prompt=Type exit to return to the concentration game.$_$p$g"
FOR rep = 333 TO 666 STEP 111
SOUND rep, 2
NEXT rep
rdr$ = LEFT$(kps$, 2)
SHELL rdr$
ps = LEN(kps$)
rcd$ = "cd" + RIGHT$(kps$, (ps - 2))
SHELL rcd$
gcl = 1
CLS
lp 1, 18, " Concentration                By Jim Kitchen"
RETURN
er1:
RESUME sl
nocfg:
RESUME nc0
nc0:
ON ERROR GOTO 0
nc1:
lp 8, 1, " Please press the number of the sound player program to try"
lp 10, 1, " 1 = wplay"
lp 11, 1, " 2 = plany"
lp 12, 1, " 3 = nusound"
beg = 1
GOSUB gkey
beg = 0
g = VAL(g$)
IF (g < 1 OR g > 3) THEN GOTO nc1
sd$ = spt$(g)
nc2:
sb sd$, "aha"
sb sd$, "match"
lp 15, 1, " Press y to use this sound player"
lp 16, 1, " or if you didn't hear anything then press n to try a different player"
beg = 1
GOSUB gkey
beg = 0
IF (dd > 58 AND dd < 64) THEN GOTO nc2
IF g$ <> "y" THEN GOTO nc1
OPEN "o", #2, "concent.cfg"
PRINT #2, sd$
CLOSE #2
GOTO gotcfg
er2:
REM input from file a zero value for prize value
DATA registered copy of JAWS 2.31, 650
DATA registered copy of Telix, 49.95
DATA registered copy of Readit, 20
DATA Accent SA speech synthesizer, 950
DATA Panasonic nine pin printer, 200
DATA slate and stylist, 20
DATA talking scale, 199.95
DATA picture of dogs playing poker, 29.95
DATA candy bar, .50
DATA scanner, 859.95
DATA pair of sunglasses, 1.98
DATA can of pop, .50
DATA Braille Monopoly game, 75
DATA Braille Scrabble game, 50
DATA joystick, 25.95
DATA pen shaped stylist, 6.95
DATA diamond ring, 7250
DATA kitchen sink, 150
DATA refrigerator, 1195.95
DATA hammer, 9.95
DATA electric screwdriver, 39.95
DATA big screen T. V., 3999.99
DATA box of Braille paper, 10
DATA bundle of clothes line, 1.98
DATA can of cream corn, .95
DATA watermelon, 3.75
DATA stereo, 1500
DATA satellite dish, 8999.99
DATA shower massage unit, 49.95
DATA monkey wrench, 5.95
DATA electric fan, 29.95
DATA air conditioner, 595.95
DATA roll of magnetic tape, 10
DATA three roll package of Dymo tape, 4.95
DATA folding long white cane, 20
DATA marshmallow cane tip, 2.95
DATA Power Mate surge protector and power supply, 99.95
DATA box of floppy disks, 5.95
DATA tape recorder, 29.95
DATA Braille embosser, 5999.95
DATA talking compass, 150
DATA talking alarm clock, 29.95
DATA talking pocket watch, 39.95
DATA three pair package of cotton briefs, 5.25
DATA bunch of frozen bananas, 3.55
DATA brand new mobile home, 22795.99
DATA totally computerized car, 1000000
DATA brand new brick home, 455999.95
DATA pair of pajamas, 14.98
DATA can of beer nuts, 3.98
DATA hamburger, .65
DATA large pizza with everything, 14.95
DATA six pack of Old English 800 malt liquor, 4.25
DATA box of spaghetti noodles, .98
DATA crystal orb, 1595.99
DATA Sound Blaster sound card, 149.95
DATA package of hotdogs, 3.15
DATA tube of toothpaste, 2.25
DATA tube of high marks, 4.95
DATA 28.8 fax modem, 195.95
DATA complete 486 home computer with large hard drive, 2295.99
DATA peanutbutter and jelly sandwich, 1.98
DATA large Dairy Queen Blizzard, 2.40
DATA beeny hat with propeller, 2.98

SUB dl (a, b)
st = TIMER
zz = a * b
DO
LOOP UNTIL TIMER > st + zz
END SUB

SUB lp (a, b, a$)
LOCATE a, b
PRINT #1, a$;
END SUB

SUB sb (a$, b$)
SHELL a$ + " " + b$ + ".wav > nul"
END SUB

