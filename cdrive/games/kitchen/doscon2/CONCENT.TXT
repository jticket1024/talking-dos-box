     To play this concentration game, simply type concent at the dos
     prompt and press enter.  Please press Y to read the instructions
     the first time that you play the game.
     
     You can adjust the delays in the program from the command line with
     the /d switch.  So that your synthesizer is not speaking at the
     same time that the wave files are being played.  Such as:
     concent /d 100  100 is the default.
     
     The game has 65 built in prizes and will operate just fine the way
     it is.  However you may edit and add to the concent.dat file. 
     Every prize in the concent.dat file will replace one in the built
     in section.  So if you have over 65 prizes in the concent.dat file
     the game will use only your prize list.  For variety the game will
     import and use 150 prizes from the concent.dat file.  Please be
     sure to maintain the simple format of the concent.dat file.  The
     game may not work right if the concent.dat file is amiss.  
     The format is simply
     
     first line prize name
     second line prize value (no dollar sign please)
     third line prize name
     fourth line prize value (no dollar sign please)
     etc etc etc
     
     If you have any trouble, a comment or a question about the
     game please write or call
     
     jimkitchen@simcon.net
     http://www.simcon.net/public/jkitchen/
     1-440-286-6920
     Chardon Ohio
     
