DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
'
'  Set up variable table
'
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE


'Com1
PRINT "Your command?->"; : LINE INPUT ""; a0$: IF a0$ = "" THEN a0$ = cz$
a0$ = RTRIM$(LTRIM$(a0$)): cz$ = a0$: a$ = LCASE$(a0$): l = 1
IF s%(4) = 1 THEN CLS :  ELSE LOCATE CSRLIN - 1, 16

'    Parse command; verb, subject, 2nd subject
'   -------------------------------------------
c = 0: v$ = "": s$ = "": s2$ = "": IF a$ = "" THEN GOTO Err0
a = INSTR(a$, " ")
IF a > 0 THEN
  v$ = LTRIM$(RTRIM$(LEFT$(a$, a - 1))): s$ = LTRIM$(RTRIM$(MID$(a$, a + 1)))
  a$ = s$: a0$ = LTRIM$(RTRIM$(MID$(a0$, a + 1)))
END IF
IF v$ = "" THEN v$ = a$: a$ = "": GOTO Com2
IF v$ = "at" THEN v$ = "a"
a = INSTR(s$, " to ") + INSTR(s$, " on ") + INSTR(s$, " in ") + INSTR(s$, " at ")
IF a > 0 THEN
  s2$ = LTRIM$(RTRIM$(MID$(s$, a + 4))): s$ = LTRIM$(RTRIM$(LEFT$(s$, a - 1)))
END IF
IF s2$ <> "" THEN GOTO Com2
a = INSTR(s$, " from ") + INSTR(s$, " with ") + INSTR(s$, " into ")
IF a > 0 THEN
  s2$ = LTRIM$(RTRIM$(MID$(s$, a + 6))): s$ = LTRIM$(RTRIM$(LEFT$(s$, a - 1)))
END IF
Com2:
FOR k = 1 TO nc
   m = INSTR(c$(k), v$)
   IF m > 0 AND (m = 1 OR RIGHT$(c$(k), LEN(v$)) = v$) THEN c = k: k = nc
NEXT
COLOR 7: PRINT UCASE$(c$(c)); " "; UCASE$(a$): COLOR 3: PRINT
IF c = 0 THEN GOTO Err0




' Hints
'------
Hints:
PRINT "Your question?": GOSUB Lp4
PRINT "    1- "; h$(1): GOSUB Lp2
a = 1: m = 1: IF nh > 1 THEN a = 2: m = nh
IF a > 1 THEN
  FOR r = a TO m
     IF r < 10 THEN PRINT " ";
     PRINT "   "; LTRIM$(RTRIM$(STR$(r - (a - 2)))); "- "; h$(r): GOSUB Lp2
  NEXT
END IF
GOSUB Lp1: PRINT "Your choice?:"; : LINE INPUT ""; a$: r = INT(VAL(a$)): GOSUB Lp6a
IF r > 1 THEN r = r + (a - 2)
IF r <> 1 AND ((r > 1 AND r < a) OR r > m OR r < 1) THEN GOTO Main
'
GOSUB Lp1: COLOR 2: PRINT h$(r): COLOR 3: GOSUB Lp4
OPEN "HINTS.DSC" FOR RANDOM AS #1 LEN = 255
a = h%(r, 1) + h%(r, 2) - 1: s$ = ""
FOR m = h%(r, 1) TO a
   GET #1, m, DS: a$ = RTRIM$(DS.TEXT): GOSUB Lp10
   IF m < a THEN
     PRINT "Another? (Y/N):";
     DO
       s$ = LCASE$(INKEY$)
     LOOP UNTIL s$ = "y" OR s$ = "n"
     LOCATE CSRLIN, 1: PRINT SPC(16); : LOCATE CSRLIN, 1: l = 0
     IF s$ <> "y" THEN m = a
   END IF
NEXT
CLOSE #1: GOTO Main0


' Save game
'-----------
GameSave:
OPEN "GAMEDIR.DAT" FOR INPUT AS #1: INPUT #1, a: IF a < 0 THEN a = 0
FOR m = 1 TO 5: LINE INPUT #1, gm$(m): NEXT: CLOSE #1
a$ = LEFT$(a0$, 2): s$ = MID$(a0$, 3)
IF s$ = "" AND a$ <> "" THEN s$ = "Quick Saved Game"
IF VAL(a$) > 0 AND VAL(a$) < 6 THEN GOTO GameSave0
a$ = "": s$ = "": PRINT "Saved games:": GOSUB Lp4
FOR m = 1 TO 5
   PRINT " "; LTRIM$(RTRIM$(STR$(m))); " - "; gm$(m): GOSUB Lp2
NEXT: PRINT 6; "- (Don't save, return to game)": GOSUB Lp4
'
PRINT "Enter 1-6 for saved position:"; : LINE INPUT ""; a$: GOSUB Lp6a
GameSave0:
m = INT(VAL(a$)): l = 0: IF m = 6 THEN GOTO Main
IF m < 1 OR m > 5 THEN s$ = "": GOTO GameSave
IF m > a + 1 THEN
  m = a + 1: PRINT "(Using #"; LTRIM$(STR$(m)); " instead)": GOSUB Lp4
END IF
'
IF m = a + 1 THEN a = a + 1
IF s$ <> "" THEN
  IF gm$(m) <> "(none)" AND s$ = "Quick Saved Game" THEN s$ = gm$(m)
  gm$(m) = s$: PRINT "[QUICK SAVE"; RTRIM$(STR$(m)); ": "; s$; "]"
  GOTO GameSave0A
END IF
IF gm$(m) <> "(none)" THEN
  PRINT "Enter a new description for save?"; : GOSUB Af1
  IF s$ <> "y" THEN GOTO GameSave1
END IF
PRINT "Enter new description:": GOSUB Lp4: PRINT "-->"; : LINE INPUT ""; gm$(m)
GameSave0A:
GOSUB Lp6a: gm$(m) = LEFT$(gm$(m), 40)
'
GameSave1:
OPEN "GAMEDIR.DAT" FOR OUTPUT AS #1: PRINT #1, a
FOR k = 1 TO 5: PRINT #1, gm$(k): NEXT: CLOSE #1
'
a$ = "GAME" + LTRIM$(RTRIM$(STR$(m))) + ".SAV"
OPEN a$ FOR OUTPUT AS #1
FOR a = 1 TO na
   FOR m = 0 TO 8: PRINT #1, a%(a, m): NEXT
NEXT
FOR m = 0 TO nm
   FOR a = 0 TO 15: PRINT #1, m%(m, a): NEXT
NEXT
FOR r = 1 TO nr
   FOR m = 0 TO 11: PRINT #1, r%(r, m): NEXT
NEXT
FOR a = 1 TO 4: PRINT #1, t(a), sa%(a), sp%(a): NEXT
FOR a = 1 TO 5: PRINT #1, wa%(a): NEXT
FOR a = 0 TO 50: PRINT #1, d(a): NEXT: FOR a = 0 TO 50: PRINT #1, d%(a): NEXT
PRINT #1, ac, ae, ar, bank, ch, die, ea, gold, ls, lt, nl, r3, ro, sex, sh
PRINT #1, Speed, wt
CLOSE #1: PRINT "Game saved.": GOSUB Lp4: GOTO Main


' Restore game
'--------------
GameRestore:
OPEN "GAMEDIR.DAT" FOR INPUT AS #1: INPUT #1, a
FOR m = 1 TO 5: LINE INPUT #1, gm$(m): NEXT: CLOSE #1
PRINT "Saved games:": GOSUB Lp4
FOR m = 1 TO 5
   PRINT " "; LTRIM$(RTRIM$(STR$(m))); " - "; gm$(m): GOSUB Lp2
NEXT: PRINT 6; "- (Don't restore, return to game)": GOSUB Lp4
PRINT "Enter 1-6 to restore a position:";
LINE INPUT ""; a$: m = INT(VAL(a$)): GOSUB Lp6a
IF m < 1 OR m > 6 THEN GOTO GameRestore
IF m = 6 THEN
  IF die = 1 THEN GOTO Dead
  GOTO Main
END IF
IF m > a THEN : PRINT "There is no saved game there.": GOTO GameRestore
'
a$ = "GAME" + LTRIM$(RTRIM$(STR$(m))) + ".SAV"
OPEN a$ FOR INPUT AS #1
FOR a = 1 TO na
   FOR m = 0 TO 8: INPUT #1, a%(a, m): NEXT
NEXT
FOR m = 0 TO nm
   FOR a = 0 TO 15: INPUT #1, m%(m, a): NEXT
NEXT
FOR r = 1 TO nr
   FOR m = 0 TO 11: INPUT #1, r%(r, m): NEXT
NEXT
FOR a = 1 TO 4: INPUT #1, t(a), sa%(a), sp%(a): NEXT
FOR a = 1 TO 5: INPUT #1, wa%(a): NEXT
FOR a = 0 TO 50: INPUT #1, d(a): NEXT
FOR a = 0 TO 50: INPUT #1, d%(a): NEXT
INPUT #1, ac, ae, ar, bank, ch, die, ea, gold, ls, lt, nl, r3, ro, sex, sh
INPUT #1, Speed, wt
CLOSE #1: cz$ = "": r2 = ro: GOTO Move2



' SellWep:

PRINT "Enter the number of the weapon to sell:"; : LINE INPUT ""; a$: GOSUB Lp6a


Lp6a:
zz$ = INKEY$: l = 0: IF s%(4) = 1 THEN CLS
RETURN


'
' Get Y/N input
'---------------
Af1:
PRINT " (Y/N):"; : LINE INPUT ""; s$: GOSUB Lp6a: s$ = LTRIM$(RTRIM$(LCASE$(s$)))
s$ = LEFT$(s$, 1): IF s$ <> "y" AND s$ <> "n" THEN GOTO Af1
GOTO Lp1


'spellcast

a$ = m$(0) + MID$("'s'", ABS(LCASE$(RIGHT$(m$(0), 1)) = "s") * 2 + 1, 2)


' Check/get s$
'------------------
CheckSub:
IF s$ <> "" THEN RETURN
CheckSub1:
PRINT UCASE$(LEFT$(c$(c), 1)); MID$(c$(c), 2);
PRINT " who or what?"; : LINE INPUT "", s$: s$ = LCASE$(LTRIM$(RTRIM$(s$)))
GOSUB Lp6a: PRINT : l = 1: GOTO CheckSub


' Check/get s2$
'---------------
CheckSub2:
IF s2$ <> "" THEN RETURN
PRINT a$; "?"; : LINE INPUT ""; s2$: s2$ = LTRIM$(RTRIM$(LCASE$(s2$)))
GOSUB Lp6a: PRINT : l = 1: GOTO CheckSub2


