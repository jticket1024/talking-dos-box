                          The Stronghold of Khar-Dur{15}
                              by Derek C. Jeter



Reviewed By: Thomas Ferguson{02}
Special Features: Difficulty scales to character's level, Atmospheric effects
Playing Time: 1-2 hours
  Reviewer Rating: 9                   Average Rating: 8.5 (from 2 reviews)
Difficulty Rating: 7                   Average Rating: 7


Comment: "Stronghold of Kahr-Dur" was initially described to me as having a {07}
"classic Eamon" flavor and I think this is an apt assessment. The plot is a 
straightforward "damsel in distress" scenario, paying homage to the classic 
dungeon crawls of yesteryear, with a heroic companion by the player's side and 
nasty enemies to defeat. "Stronghold," however, is by no means a derivative 
work.
<paws>

Where this adventure shines, I think, is in its execution. The descriptions of 
rooms and artifacts are detailed and moody while avoiding being verbose. The 
design is likewise excellent; the different areas of the map are very distinct 
from one another and are naturally laid out. While the puzzles tend to involve 
a fair amount of backtracking to complete, the layout of the map prevents this 
from making such forays overly tedious.

There is a very effective blend of fighting the bad guys and solving puzzles. 
On the former point, the author has added a nice feature that assigns the 
attributes of the enemies based on the values of the adventurer's strongest 
weapon; this ensures that the difficulty scales to meet the experience of the 
character. Though going through "Stronghold" with a brand-new character saw him 
swiftly trounced, playing with my old standby adventurer, wielding a souped-up 
canister of Smylex (retrieved from "Batman!!" in the Worst of the Classic 
Adventures collection), still proved a challenge. The puzzles can be 
intuitively solved without resorting to using the HINTS command though still 
demand some reasoning on the part of the player. In short, little in 
"Stronghold" is outright given to the player; success must be earned.
<paws>



"Stronghold" also takes pains to incorporate a number of effects and special 
features and they are employed successfully due to their subtlety. Often, 
special effects in Eamon, when present, tend to come off as gimmickry; in the 
case of "Stronghold," effects are employed seamlessly to add to the atmosphere 
of the map. I was surprised after looking "under the hood" at the number of 
effects programmed into the adventure; I'd been enjoying the adventure too much 
during the playthroughs to notice that they were being employed. And this is 
precisely how it ought to be.
<paws>


A further, satisfying feature of the adventure is the clearly outlined quest 
and the explicit indication in the endgame of having "won" or "lost." I'd have 
preferred to have read a bit more storyline detailing what happens after the 
player leaves the fortress- Eamon in general tends to be unbalanced in its 
proportion of prologue to epilogue- but the mere acknowledgement that the player
has been successful is enough to make "Stronghold" stand in distinction to most 
of the [Eamon] canon.

The writing and programming of "Stronghold of Kahr-Dur" are top notch. While its
author doesn't try and reinvent the wheel, the adventure succeeds in refining 
many of the traditional tropes of Eamon to produce one of the best "dungeon 
crawls" Eamon has seen.
<paws>
                          The Stronghold of Khar-Dur{15}
                              by Derek C. Jeter


Reviewed By: Luke Hewitt{02}
Reviewer Rating: 8                     Average Rating: 8.5 (from 2 reviews)
Difficulty Rating: 7                   Average Rating: 7


Comment: The best summation of stronghold is that it is what many of the more {07}
childish, combat-heavy classic adventures hoped to be when they grew up. The 
plot is absolutely typical of the genre: An evil necromancer is causing havoc 
in a far off town, kidnapping an important lovely damsel along the way (these 
evil sorcerers really should learn to only kidnap ugly, unimportant people). 
However, what really stands out in this game is the staggeringly good 
atmosphere that Derek has managed to create. Though there are many, many empty 
rooms in the large map, I never felt bored or uninterested since even the 
majority of empty rooms still have detailed descriptions which are not limited 
to visible objects and features, but distinct smells and sounds as well. A nice 
touch of special programming also comes into play as it randomly flags up extra 
atmospheric effects such as gusts of wind, distant howling, feelings of fear 
and other contributions to the already stunning environment.
<paws>

I was extremely pleased to find that the dusty, abandoned castle where you 
first start out is not the only environment you visit in this map either, as I 
later progressed to a dark and rugged series of caves, a gloomy forest, and 
other unique settings. As I mentioned already, the overall map is quite large 
and this is definitely an Eamon for explorers since several later portions of 
the game can become rather mazelike; though I encountered no "trick" exits or 
random room connections: and it's possible to navigate without getting lost as 
long as you keep track of where you have been and where you haven't explored 
yet.

One thing I did find a little jarring in this adventure is the total absence of 
embedded artifacts. I was occasionally quite sorry to have a fascinating object 
such as a statue of a warrior or a wizard's tome mentioned in a description, 
but not be able to examine it. Though, once I stopped trying to EXAMINE things, 
I came to appreciate how the artifacts that did exist were used in clever and 
interesting ways to create puzzles without the use of embedded objects. I can 
clearly understand the logic and reasoning behind the author's choice of 
artifact usage, even though it's not a choice I would have made myself.
<paws>


One minor disappointment was that, despite the extremely well crafted 
atmosphere and clear quest, the plot still felt a bit unfinished and 
anti-climatic in some respects. For example, though the intro story mentioned 
that many local citizens had been disappearing, but I only actually found the 
"brother" I started the game with and the aforementioned damsel in distress for 
companionship. Also it was never made explicitly clear who exactly the evil 
necromancer was or what his nefarious motivations were. Even when within his 
lair, I found no altars to mysteriously nasty gods nor preparations for 
unpleasant enchantments, plans for domination, etc.-- just the captive damsel 
who was quite two dimensional and really seemed to be there just for decoration 
(after all, the lair of any good necromancer has to have an attractive, chained 
up maiden as part of the decor).
<paws>

As I mentioned, your only companion is your loyal "brother"-- whether that is a 
friendly title to describe a comrade-in-arms or a literal relation was never 
explained, but I personally chose to interpret it as the first option, since I 
prefer to imagine my characters' background and family tree myself. 
Unfortunately, this "brother" is almost as two dimensional as the damsel in 
distress; this is an area that could have greatly benefited from some special 
programming, perhaps allowing him to speak to you or comment on what was 
happening throughout the adventure, it felt particularly flat when I rescued 
what may or may not have been his daughter (and therefore my character's niece).

The puzzles aren't overly challenging provided you take care to pay attention 
to the descriptions. The map layout can get rather complex especially towards 
the end, which certainly added to the challenge. One tip: though not exactly a 
puzzle, the author probably should have mentioned somewhere in the game that a 
"knock spell" is a magical enchantment used to open stubborn doors and/or other 
barriers. I learned this myself from playing other RPG games, but "Stronghold" 
could have been a bit unfair, had I not already known this.
<paws>

I give this one a 7 (out of 10) for its difficulty rating. The high rating, 
however, is not merely based upon the moderately challenging puzzles or the 
large, twisty map alone, but rather comes mostly from the combat. I give extra 
credit to Derek here as well because, instead of loading the place with hoards 
of weaker enemies, he intersperses the largely empty map with interesting 
monsters and very tough battles, which which adds far more significance to the 
standard Eamon hack'n'slash approach. My medium strength character (with a 
2D6 weapon) was often down to the wire, and it took the proper use of spells 
and artifacts to win several fights, despite having the included "brother" 
(who is a very hardy companion) from the start.

The big boss fight itself also deserves special mention as it is one of the 
better examples of special programming being used in combat that I've ever 
encountered, and very much stays consistent to the theme of facing off against 
a powerful, evil sorcerer who is capable of using dreadful magic against you. 
I'd definitely advise people pump up before trying this one, especially on 
their magic spells. 
<paws>


There are several tough fights, which are actually completely avoidable through 
clever navigation and which really could have used some sort of small reward or 
mention to generate a deserved sense of accomplishment (indeed given that one 
of them was against a group of animated skeletons).

All in all though, Stronghold of Kahr-Dur is an extremely well written Eamon 
Deluxe adventure, both in a literary sense as well as the inclusion of special 
programming. It has a classic "dungeon crawl" flavored plot, a clear quest, 
multiple combat fests against interesting opponents, challenging exploration 
and a fantastic atmosphere which is continuous from start to finish. Despite 
the few shortcomings I mentioned, it's still an outstanding game overall and I 
highly recommend it. With the dramatic combat, strong atmosphere, puzzle 
solving, or just the challenge of exploration and mapping, there is something 
for everyone. I give it an 8 out of 10 for the overall enjoyment rating.
<paws>

