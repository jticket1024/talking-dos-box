'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�   YE OLDE TEST BENCH 5.0                Revision: 27 JAN 2012  �
'�     By Senor Black                        Update: 04 DEC 2012  �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�
'
' Copyright (C) 2013 Frank Black Productions
'
' This program is free software; you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation; either version 2 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program; if not, write to the Free Software
' Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
'

DECLARE SUB EDX.Select.Adventure (c, a$)
DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)
ON ERROR GOTO ErHndlr

TYPE RECORD
  cname AS STRING * 40: status AS INTEGER: A1 AS INTEGER: A2 AS INTEGER
  a3 AS INTEGER: SP1 AS INTEGER: SP2 AS INTEGER: SP3 AS INTEGER
  SP4 AS INTEGER: WA1 AS INTEGER: WA2 AS INTEGER: WA3 AS INTEGER
  WA4 AS INTEGER: WA5 AS INTEGER: ae AS INTEGER: sex AS INTEGER: gold AS LONG
  bank AS LONG: ac AS INTEGER: WN1 AS STRING * 20: WN2 AS STRING * 20
  WN3 AS STRING * 20: WN4 AS STRING * 20: WCOMPLX1 AS INTEGER
  WCOMPLX2 AS INTEGER: WCOMPLX3 AS INTEGER: WCOMPLX4 AS INTEGER
  WTYPE1 AS INTEGER: WTYPE2 AS INTEGER: WTYPE3 AS INTEGER: WTYPE4 AS INTEGER
  WDICE1 AS INTEGER: WDICE2 AS INTEGER: WDICE3 AS INTEGER: WDICE4 AS INTEGER
  WSIDES1 AS INTEGER: WSIDES2 AS INTEGER: WSIDES3 AS INTEGER: WSIDES4 AS INTEGER
END TYPE

TYPE adv
   ANAME AS STRING * 78: NUMBER AS INTEGER
END TYPE
DIM SHARED tc AS RECORD, adv AS adv

DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

READ numchars: m = numchars
DIM na$(m), stat(m), hd(m), ag(m), ch(m), sp%(m, 4), wa%(m, 5), ae(m), sex(m)
DIM gold(m), bank(m), ac(m), wn$(m, 4), wo%(m, 4), WT%(m, 4), wd%(m, 4)
DIM ws%(m, 4), wp$(5)
GOSUB Init: m = 0

DIM SHARED em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0)
FOR x = 1 TO em0: READ em$(x): NEXT
em$(0) = CHR$(13) + CHR$(32) + CHR$(27)  '(Enter, Space, Escape)
SCREEN 0: WIDTH 80: mc1 = 1: mc2 = 2: mc3 = 7
'm = 4  ' <-- Make Brutis default for fast testing

'                  MAIN MENU
'                 -----------
Menu:
GOSUB Header: COLOR 15: PRINT : PRINT "     Character: "; : COLOR 14
IF m = 0 THEN PRINT "None":  ELSE PRINT n$(m)
em$(2) = "  Change to " + MID$("femalemale", ABS(sex(1) = 1) * 6 + 1, 6)
em$(2) = em$(2) + " characters  ": PRINT

c = 10: c1 = 15: COLOR 7
CALL EDXMenuM(c, c1, 1, 5, mc3, 0)
IF c1 = 3 OR c = 5 THEN CHDIR "..": CHDIR "DD": RUN "DDMENU"
IF (c = 3 OR c = 4) AND m = 0 THEN GOTO Menu

ON c GOTO Pick, Change, ViewChar, Test

'
'                     Select character
'                    ------------------
Pick:
CLS : PRINT "Select a character:": PRINT : PRINT
FOR r = 1 TO numchars
   em$(5 + r) = " " + n$(r)
   em$(5 + r) = em$(5 + r) + SPACE$(36 - LEN(em$(5 + r))) + cm$(r) + " "
NEXT

c = 0: c1 = 2: CALL EDXMenuM(c, c1, 6, 5 + numchars, 3, 0)
IF c1 = 3 THEN GOTO Menu
m = c: GOTO Menu

'
'                   Toggle male/female
'                  --------------------
Change:
IF sex(1) = 0 THEN
  FOR x = 1 TO numchars: sex(x) = 1: n$(x) = f$(x): NEXT: GOTO Menu
END IF
FOR x = 1 TO numchars: sex(x) = 0: n$(x) = m$(x): NEXT: GOTO Menu

'
'                  View character stats
'                 ----------------------
ViewChar:
FOR a = 1 TO 4
   sp$(a) = LTRIM$(STR$(sp%(m, a))) + "%": IF sp%(m, a) < 1 THEN sp$(a) = "None"
NEXT
a = INT(ac(m) / 2): sh = ABS((a < ac(m) / 2))
a = ABS((a < 5)) * a + ABS((a > 4)) * 4: ar$ = ar$(a)
IF sh > 0 THEN ar$ = ar$ + " & Shield"

CLS
PRINT "Name: "; n$(m); : LOCATE CSRLIN, 46: PRINT "Sex: "; sex$(sex(m))
PRINT : PRINT "Hardiness: "; hd(m): LOCATE 3, 20: PRINT "Agility: "; ag(m)
LOCATE 3, 37: PRINT "Charisma: "; ch(m); "  (";
a = (ch(m) - 10) * 2: PRINT MID$("+", ABS((a < 1)) * 1 + 1, 1);
PRINT LTRIM$(STR$(a)); "% Likeability Bonus)": PRINT

PRINT "Weapon Abilities:"; : LOCATE 5, 41: PRINT "Spell Abilities:"
PRINT "1. Axe:  "; wa%(m, 1); : LOCATE 6, 42: PRINT "Blast..."; sp$(1)
PRINT "2. Bow:  "; wa%(m, 2); : LOCATE 7, 42: PRINT "Heal...."; sp$(2)
PRINT "3. Mace: "; wa%(m, 3); : LOCATE 8, 42: PRINT "Speed..."; sp$(3)
PRINT "4. Spear:"; wa%(m, 4); : LOCATE 9, 42: PRINT "Power..."; sp$(4)
PRINT "5. Sword:"; wa%(m, 5); : LOCATE 10, 24: PRINT "Gold:"; gold(m)
LOCATE 10, 56: PRINT "In bank:"; bank(m): PRINT

a$ = RTRIM$(STR$(ae(m))) + "%"
PRINT "Armor:  "; ar$: LOCATE 12, 35: PRINT "Armour Expertise: "; a$
PRINT :  a = hd(m) * 10: a$ = LTRIM$(RTRIM$(STR$(a)))
PRINT "Weight Carryable: "; a$; " Gronds  (";
a$ = LTRIM$(RTRIM$(STR$(a * 10))): PRINT a$; " Dos)": PRINT

PRINT "Weapon Names:             Complexity:         Damage:   Base Odds to Hit:"
PRINT STRING$(75, "-")
FOR w = 1 TO 4
   IF wn$(m, w) = "" THEN wn$(m, w) = "NONE"
   IF wn$(m, w) = "NONE" THEN GOTO WLoop
   IF LEN(wn$(m, w)) > 20 THEN wn$(m, w) = LEFT$(wn$(m, w), 20)
   PRINT wn$(m, w); SPC(31 - LEN(wn$(m, w)));
   a$ = LTRIM$(RTRIM$(STR$(wo%(m, w)))) + "%"
   PRINT a$; SPC(18 - LEN(a$));
   a$ = LTRIM$(RTRIM$(STR$(wd%(m, w)))) + "D" + LTRIM$(RTRIM$(STR$(ws%(m, w))))
   PRINT a$; SPC(14 - LEN(a$));
   '
   '          %% COMPUTE ARMOR FACTOR %%
   ar% = ac(m) / 2: sh% = ABS(ar% <> ac(m) / 2): IF ar% > 3 THEN ar% = 3
   af% = (-5 * sh% - ar% * 10 - (30 * ABS(ar% = 3)))
   '
   '          %% COMPUTE BASE ODDS TO HIT %%
   odds% = 50 + 2 * (ag(m) - ac(m))
   odds% = odds% + ((af% + ae(m)) * ABS(-af% > ae(m)))
   odds% = odds% + wa%(m, WT%(m, w)) / 4: odds% = odds% + wo%(m, w) / 2
   '
   PRINT LTRIM$(STR$(odds%)); "%"
WLoop:
NEXT: COLOR 3: PRINT : PRINT "(Hit any key to continue)";
DO
LOOP UNTIL INKEY$ <> ""
GOTO Menu

'
'                    Test an adventure
'                   -------------------
Test:
c = 3: ad$ = "DD"
CALL EDX.Select.Adventure(c, ad$)
IF c = 0 THEN GOTO Menu
'
'             Make sure directory exists
'            ----------------------------
CHDIR "..": CHDIR ad$
'
'       Make sure other characters aren't in adventure
'      ------------------------------------------------
OPEN "GAMEDIR.DAT" FOR INPUT AS #2: INPUT #2, a: CLOSE #2
IF a <> -1 THEN
  OPEN "NEW-MEAT" FOR RANDOM AS #4 LEN = 200: GET #4, 1, tc: CLOSE #4
  IF tc.status > -1 THEN
    COLOR 3: PRINT : PRINT "You already have a character adventuring there."
    PRINT "(Character: "; RTRIM$(tc.cname); ") "; : GOSUB GetKey
    CHDIR "..": CHDIR "DD": GOTO Menu
  END IF
  IF tc.status <> -m THEN
    k$ = RTRIM$(tc.cname): a$ = "him": IF tc.sex = 1 THEN a$ = "her"
    PRINT k$; " is already testing there. You'll have to use "; a$; ". ";
    GOSUB GetKey: CHDIR "..": CHDIR "DD": GOTO Menu
  END IF
END IF
'
'      Transfer character data and run adventure intro program
'     ---------------------------------------------------------
r = m
   tc.cname = n$(r): tc.status = -r: tc.A1 = hd(r): tc.A2 = ag(r)
   tc.a3 = ch(r): tc.ae = ae(r):   tc.sex = sex(r)
   tc.SP1 = sp%(r, 1): tc.SP2 = sp%(r, 2): tc.SP3 = sp%(r, 3): tc.SP4 = sp%(r, 4)
   tc.WA1 = wa%(r, 1): tc.WA2 = wa%(r, 2): tc.WA3 = wa%(r, 3)
   tc.WA4 = wa%(r, 4): tc.WA5 = wa%(r, 5)
   tc.gold = gold(r): tc.bank = bank(r): tc.ac = ac(r)
   tc.WN1 = wn$(r, 1): tc.WN2 = wn$(r, 2): tc.WN3 = wn$(r, 3): tc.WN4 = wn$(r, 4)
   tc.WCOMPLX1 = wo%(r, 1): tc.WCOMPLX2 = wo%(r, 2): tc.WCOMPLX3 = wo%(r, 3)
   tc.WCOMPLX4 = wo%(r, 4): tc.WTYPE1 = WT%(r, 1): tc.WTYPE2 = WT%(r, 2)
   tc.WTYPE3 = WT%(r, 3): tc.WTYPE4 = WT%(r, 4): tc.WDICE1 = wd%(r, 1)
   tc.WDICE2 = wd%(r, 2): tc.WDICE3 = wd%(r, 3): tc.WDICE4 = wd%(r, 4)
   tc.WSIDES1 = ws%(r, 1): tc.WSIDES2 = ws%(r, 2): tc.WSIDES3 = ws%(r, 3)
   tc.WSIDES4 = ws%(r, 4)
CHDIR "..": CHDIR ad$
OPEN "NEW-MEAT" FOR RANDOM AS #4 LEN = 200: PUT #4, 1, tc: CLOSE

OPEN "SETTINGS.DAT" FOR OUTPUT AS #1
    WRITE #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: WRITE #1, s%(x): NEXT
CLOSE
RUN "INTRO"

'          Get a single keypress
'         -----------------------
GetKey:
' VI Mode support added 27 JAN 2012
k$ = INKEY$: BS = POS(0): CC# = 0
DO
  IF s%(4) <> 1 THEN
    CC# = CC# + 1
    IF CC# = CC1# THEN PRINT "/"; : LOCATE CSRLIN, BS
    IF CC# = CC2# THEN PRINT CHR$(179); : LOCATE CSRLIN, BS
    IF CC# = CC3# THEN PRINT "\"; : LOCATE CSRLIN, BS: CC# = 0
  END IF
  k$ = UCASE$(INKEY$)
LOOP UNTIL k$ <> ""
PRINT k$: RETURN

'              AD Menu Header 5.0
'             --------------------
Header:
CLS : COLOR mc1, 0: PRINT " �"; STRING$(65, "�"); "�"
PRINT " �"; SPACE$(15); : COLOR mc2: PRINT "THE MARVELOUS WORLD OF EAMON DELUXE ";
COLOR mc1: PRINT SPACE$(14); "�": PRINT " �"; SPACE$(20); : COLOR mc2
PRINT "Adventure Test Program 5.0"; : COLOR mc1: PRINT SPACE$(19); "�": PRINT " �";
PRINT STRING$(65, "�"); "�": PRINT : COLOR 7: RETURN


'敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
'�                      Initialize data                          �
'青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
Init:
FOR r = 1 TO numchars
   READ m$(r), f$(r), cm$(r), hd(r), ag(r), ch(r), ae(r), gold(r)
   READ bank(r), ac(r): n$(r) = m$(r)
   FOR m = 1 TO 4: READ sp%(r, m): NEXT
   FOR m = 1 TO 5: READ wa%(r, m): NEXT
   FOR m = 1 TO 4
      READ wn$(r, m), wo%(r, m), WT%(r, m), wd%(r, m), ws%(r, m)
   NEXT
NEXT
sex$(0) = "Male": sex$(1) = "Female"
FOR x = 1 TO 5: READ wp$(x): NEXT
FOR x = 0 TO 4: READ ar$(x): NEXT
RETURN

'敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
'�                    Error handling routine                     �
'青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
ErHndlr:
CLS : BEEP: PRINT "Run-time error"; ERR; "   ";
IF ERR = 68 THEN PRINT "(Printer unavailable)";
IF ERR = 62 THEN PRINT "(Input past end of file)";
IF ERR = 53 THEN PRINT "(File not found)";
PRINT : PRINT : PRINT "Program terminated.": RESUME ErHndlr1
ErHndlr1:
ON ERROR GOTO 0: a$ = INKEY$: PRINT : PRINT "(Hit any key)"
DO
LOOP UNTIL INKEY$ <> ""
SYSTEM

'敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
'�                       Character data                          �
'青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
DATA 4
DATA Hard Hat Mack,Sally Van Weenus,(Weak character)
DATA 15,12,12,10,500,100,3
DATA 50,100,0,50
DATA 0,-10,20,10,0
DATA TrollsFire MMXII,5,5,1,10,mace,15,3,1,8
DATA pocket fuzz,0,3,1,2,rabbit's foot,30,3,1,1

DATA Stan,Wendy,"(Strong, well-rounded character)"
DATA 30,18,24,35,32647,9000,5
DATA 65,168,63,122
DATA 39,15,36,34,67
DATA Schward,10,1,5,4,Crystal Pepsi Sword,25,5,10,3
DATA Firefox,16,5,2,10,Droolinval,18,5,2,8

DATA Sir Lunchalot,Molly Millions,"(Stronger, well-rounded character)"
DATA 35,20,24,50,7266,1,7
DATA 200,200,50,200
DATA 30,40,50,20,70
DATA Grimtooth MMXII,25,5,2,12,lance,-5,4,2,8
DATA Seersucker,15,5,2,7,rubber beaver tail,10,3,1,12

DATA Brutis,Jane MacMurderkill-Jones,(The adventure won't know what hit it!)
DATA 200,30,24,65,80386,6502,11
DATA 500,500,100,500
DATA 60,60,60,60,60
DATA nuclear cannon,25,2,20,20,Eccentric MMXII,30,5,3,8
DATA rusty fork,15,4,1,10,magic murder bag,-10,2,30,2
  
DATA Axe,Bow,Club,Spear,Sword
DATA Clothes,Leather,Chain Mail,Plate Mail,Magic/Exotic

EDXMenuStrings:
DATA 9
DATA "  Select a Character  ","  Change Gender  "
DATA "  View Character  ","  Go on an Adventure  ","  Quit  "
DATA c,c,c,c

SUB EDX.Select.Adventure (c, a$)
' (Updated for VI Mode support 06 JAN 2012)
'            Load adventure names
IF c = 1 OR c = 3 THEN CHDIR ".."
OPEN "ADVENTRS" FOR RANDOM AS #2 LEN = 90
   GET #2, 1, adv: numadv = adv.NUMBER: numadv1 = numadv
   REDIM advn$(numadv + 2)
   FOR x = 2 TO numadv + 1
      GET #2, x, adv
      advn$(x - 1) = LEFT$(LTRIM$(RTRIM$(adv.ANAME$)), 38)
   NEXT
CLOSE

'            Load design names
IF c = 2 OR c = 3 THEN
  CHDIR "dd"
    OPEN "DGNDIR.DAT" FOR INPUT AS #1: INPUT #1, d1, d2: CLOSE
  CHDIR ".."
  IF d1 = 1 THEN
    CHDIR "DGN-1"
      OPEN "NAME.DAT" FOR INPUT AS #1: INPUT #1, x, x, x, x, k$: CLOSE
      numadv = numadv + 1: advn$(numadv) = LEFT$(k$, 26) + "  (Design 1)"
    CHDIR ".."
  END IF
  IF d2 = 1 THEN
    CHDIR "DGN-2"
      OPEN "NAME.DAT" FOR INPUT AS #1: INPUT #1, x, x, x, x, k$: CLOSE
      numadv = numadv + 1: advn$(numadv) = LEFT$(k$, 26) + "  (Design 2)"
    CHDIR ".."
  END IF
END IF
'             Return to current directory
IF (c = 1 OR c = 3) AND a$ <> "" THEN CHDIR a$

' VI Mode numeric menu
GetAdvCVI:
IF s%(4) = 1 THEN
  CLS : L = 0: PRINT "Please select an adventure:"
  FOR c = 1 TO numadv
     a$ = LTRIM$(RTRIM$(STR$(c))) + ": " + advn$(c)
     IF c < 10 THEN a$ = " " + a$
     IF c < 100 THEN a$ = " " + a$
     PRINT a$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       L = 0
     END IF
  NEXT
  PRINT : PRINT "Enter the number of your choice (0 to Exit):";
  LINE INPUT ""; a$: c = INT(VAL(a$))
  IF c < 0 OR c > numadv THEN CLS : GOTO GetAdvCVI
  IF c = 0 THEN RUN
  k = 2: GOTO SelectADV2
END IF
' Standard Menu
keyz$ = CHR$(27) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

SelectADVa:
GOSUB SelectADV0

SelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB SelectADV0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
SelectADV2:
IF k < 3 THEN
  k$ = LTRIM$(RTRIM$(STR$(c))): a$ = LEFT$("E000", 4 - LEN(k$)) + k$
  IF c = numadv1 + 2 OR (c = numadv1 + 1 AND d1 = 0) THEN a$ = "DGN-2": c = -2
  IF c = numadv1 + 1 THEN a$ = "DGN-1": c = -1
  IF k = 1 THEN c = 0: a$ = ""
  VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO SelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO SelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO SelectADV

'       Set up screen
SelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>    <Escape=Exit>    <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
'       Display choices
SelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
' (Updated with alt. numeric menu for VI Mode support 06 JAN 2012)
'
' VI Mode numeric menu
EDXMenuMCVI:
IF s%(4) = 1 THEN
  L = 2
  FOR x = be TO en
     x$ = LTRIM$(STR$(x - be + 1)) + ": " + LTRIM$(RTRIM$(em$(x)))
     IF x < 100 THEN x$ = " " + x$: IF x < 10 THEN x$ = " " + x$
     PRINT x$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       L = 0
     END IF
  NEXT
  PRINT : PRINT "Enter the number of your choice (0 to Exit):";
  LINE INPUT ""; k$: c = INT(VAL(k$)): c1 = c + (be - 1)
  IF c = 0 THEN c = be: c1 = 3: EXIT SUB
  IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMCVI
  en = c1: c1 = 1: EXIT SUB
END IF

' Standard Menu
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, tc2
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT

EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1
LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h: COLOR tc1, tc2: PRINT em$(c);

IF LEN(k$) > 1 THEN
  IF k = 71 THEN c = be
  IF k = 79 THEN c = en
  IF k = 72 OR k = 75 THEN c = c - 1
  IF k = 80 OR k = 77 THEN c = c + 1
  GOTO EDXMenuM
END IF

IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB
END SUB

