'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�                 MULTI-ADVENTURE INTRO PROGRAM                  �
'�                                                                �
'� The Lost Treasures of Eamon            Revision 1: 12 JUN 2012 �
'�      By TMF and FB                     Revision 2: 20 SEP 2012 �
'�                                            Update: 28 SEP 2012 �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�

DECLARE SUB EDX.Select.Multi.Adventure (c, a$)
DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)
DIM SHARED Adventure$, author$, q$, SEX
ON ERROR GOTO ErrHandler

TYPE RECORD
    CNAME AS STRING * 40: Status AS INTEGER: a1 AS INTEGER: a2 AS INTEGER
    A3 AS INTEGER: SP1 AS INTEGER: SP2 AS INTEGER: SP3 AS INTEGER
    SP4 AS INTEGER: WA1 AS INTEGER: WA2 AS INTEGER: WA3 AS INTEGER
    WA4 AS INTEGER: WA5 AS INTEGER: ae AS INTEGER: SEX AS INTEGER: gold AS LONG
    bank AS LONG: ac AS INTEGER: WN1 AS STRING * 20: WN2 AS STRING * 20
    WN3 AS STRING * 20: WN4 AS STRING * 20: WCOMPLX1 AS INTEGER
    WCOMPLX2 AS INTEGER: WCOMPLX3 AS INTEGER: WCOMPLX4 AS INTEGER
    WTYPE1 AS INTEGER: WTYPE2 AS INTEGER: WTYPE3 AS INTEGER: WTYPE4 AS INTEGER
    WDICE1 AS INTEGER: WDICE2 AS INTEGER: WDICE3 AS INTEGER: WDICE4 AS INTEGER
    WSIDES1 AS INTEGER: WSIDES2 AS INTEGER: WSIDES3 AS INTEGER
    WSIDES4 AS INTEGER
END TYPE
DIM SHARED Z AS RECORD

' Load system settings
DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
OPEN "SETTINGS.DAT" FOR INPUT AS #1
INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

RESTORE AdventureData: READ a
DIM SHARED a$(a), a1$(a), mp$(a), adv, adv1, adv2, ANAME$, dungeons
adv2 = a
OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, c, c, c, c, ANAME$, c, c, dungeons
IF dungeons > 1 THEN
    DIM SHARED dn$(dungeons), dn%(dungeons, 9)
    FOR a = 1 TO dungeons
        LINE INPUT #1, dn$(a): FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
    NEXT
END IF
CLOSE

' Load multi-adventure names
'----------------------------
FOR a = 1 TO adv2: READ a$(a), a1$(a), mp$(a): a1$(a) = "by " + a1$(a): NEXT

' Init choices for EDXMenuM
DIM SHARED em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0)
FOR x = 1 TO em0: READ em$(x): NEXT: em$(0) = CHR$(13) + CHR$(32) + CHR$(27) '(Enter, Space, Escape)
'  Load useful character data
OPEN "NEW-MEAT" FOR RANDOM AS #1 LEN = 200: GET #1, 1, Z: CLOSE
name$ = LTRIM$(RTRIM$(Z.CNAME$)): SEX = Z.SEX: q$ = CHR$(34)
author1$ = "Salvaged by Thomas Ferguson & Frank Black": author$ = author1$
Adventure1$ = "The Lost Treasures of Eamon": Adventure$ = Adventure1$

' See if graphic mode is available
'----------------------------------
ON ERROR GOTO ScreenError: IF s%(4) <> 1 THEN SCREEN 7
SkreenTest:
ON ERROR GOTO 0: SCREEN 0: WIDTH 80: CLS : GOSUB Title

OPEN "GAMEDIR.DAT" FOR INPUT AS #1: INPUT #1, a: CLOSE
IF ASC(MID$(name$, 1)) = 0 THEN
    COLOR 15: PRINT "WARNING! This adventure has a corrupt file. Repairing the ";
    PRINT "file and returning to": PRINT "Main System Menu...": GOSUB KeyWait
    OPEN "GAMEDIR.DAT" FOR OUTPUT AS #1: PRINT #1, -1
    FOR m = 1 TO 5: PRINT #1, "(none)": NEXT
    CLOSE : CHDIR "..": RUN "EAMONDX"
END IF
IF a = -1 THEN
    OPEN "GAMEDIR.DAT" FOR OUTPUT AS #1
    a = -2: PRINT #1, a: FOR m = 1 TO 5: PRINT #1, "(none)": NEXT
    CLOSE
    COLOR 2: LOCATE 6: a$ = "This is Official Eamon Deluxe Adventure #23"
    GOSUB Center: LOCATE 20: GOSUB KeyWait: GOSUB Intro: GOTO Menu
END IF
IF a = -2 THEN GOTO Menu
OPEN "ADV.DAT" FOR INPUT AS #1: INPUT #1, adv1: CLOSE
adv = adv1: Adventure$ = a$(adv): mp$ = mp$(adv): author$ = a1$(adv)
PRINT "Welcome back to "; Adventure$; "!": PRINT
PRINT "Do you want to read the intro story again? (Y/N):"; : GOSUB KeyWait3
IF k$ <> "Y" THEN GOTO IntroDone
ON adv GOSUB IceCave, Cronum, Underland, GenStore, Aqua
GOTO IntroDone

'              First-time intro
'             ------------------
Intro:
GOSUB Header: LOCATE 8: COLOR 2
PRINT "  Welcome to an unfinished copy of this adventure collection!": PRINT
PRINT "  Although the menu lists several adventures, not all of them have been fully"
PRINT "  converted to Eamon Deluxe yet. Currently, "; q$; "Crypt Crashers"; q$; ", "; q$; "Leadlight"; q$; ","
PRINT "  "; q$; "Tenement of the Damned"; q$; " and "; q$; "Beneath Mount Imagery"; q$; " are still undergoing"
PRINT "  conversion and/or testing."
LOCATE 20: GOSUB KeyWait
GOSUB Header: LOCATE 8: COLOR 2
PRINT "  Visit "; : COLOR 15: PRINT "www.EamonAG.org/pages/eamondx.htm"; : COLOR 2
PRINT " for the latest Eamon Deluxe updates.": PRINT : PRINT : PRINT
PRINT "  For information regarding the conversion status of these adventures or any"
PRINT "  other questions, comments, etc., please email:"; : COLOR 15
PRINT " eamondeluxe@gmail.com"
LOCATE 20: GOTO KeyWait

Menu:
author$ = author1$: Adventure$ = Adventure1$: GOSUB Title
FOR a = 1 TO 5
    IF a < 6 THEN COLOR 2
    PRINT a$(a); : COLOR 8: IF a + 5 < 6 THEN COLOR 2
    LOCATE CSRLIN, 40: IF a < 5 THEN PRINT a$(a + 5)
NEXT
COLOR 7: PRINT : PRINT : PRINT
k = 0: c1 = 4
CALL EDXMenuM(k, c1, 1, 5, 3, 0)
IF k > 5 OR c1 = 3 THEN CHDIR "..": RUN "EAMONDX"
IF k = 5 THEN GOSUB Intro: GOTO Menu
IF k = 4 THEN adv = 0: GOTO Reviews

GOSUB Pick
ON k GOTO Stories, IntroDone0, Reviews

Stories:
GOSUB Title
ON adv GOSUB IceCave, Cronum, Underland, GenStore, Aqua
GOTO Menu

Reviews:
'IF adv > 0 THEN  ' AND adv <> 1
'  COLOR 3: GOSUB Title1: PRINT : COLOR 2
'  PRINT "Reviews have been written for both the Eamon Deluxe version of this adventure"
'  PRINT "and the classic title from the original Apple II Eamon gaming system upon which"
'  PRINT "it was based. Please select the version of the adventure that you wish to read"
'  PRINT "the review for.": PRINT : PRINT
'  c = 12: c1 = 4
'  CALL EDXMenuM(c, c1, 7, 8, 3, 0)
'  IF c1 = 3 THEN GOTO Menu
'  IF c = 2 THEN GOTO DustyReviews
'END IF
'author$ = "Reviewed by Luke Hewitt"
'IF adv <> 2 THEN adventure$ = adventure$ + " Deluxe Edition"
'Adventure$ = Adventure$ '+ " Deluxe Edition"
author$ = author$: Adventure$ = Adventure$
IF adv = 0 THEN Adventure$ = Adventure1$
a$ = "": IF adv < 9 THEN a$ = "0"
a1$ = "ADVENTURE #" + a$ + LTRIM$(RTRIM$(STR$(adv)))
a$ = "": IF adv + 1 < 9 THEN a$ = "0"
a2$ = "ADVENTURE #" + a$ + LTRIM$(RTRIM$(STR$(adv + 1)))
OPEN "EDX023RV.TXT" FOR INPUT AS #1
DO
    LINE INPUT #1, a$
LOOP UNTIL a$ = a1$
COLOR 2: GOSUB Title1: COLOR 3
DO
    LINE INPUT #1, a$
    IF a$ = "<paws>" THEN
        PRINT : COLOR 2: GOSUB KeyWait2: COLOR 2: GOSUB Title1: COLOR 3
        LOCATE CSRLIN - 1: a$ = ""
    END IF
    IF a$ <> a2$ THEN PRINT a$
LOOP UNTIL a$ = a2$
CLOSE : GOTO Menu
DustyReviews:
GOSUB Title
ON adv GOSUB IceCave1, Cronum1, Underland1, GenStore1, Aqua1
GOTO Menu

IntroDone0:
GOSUB Title
ON adv GOSUB IceCave, Cronum, Underland, GenStore, Aqua
IntroDone:
a$ = "(Waking up the monsters...)"
PRINT a$: FOR x# = 1 TO dly# * 2: NEXT
OPEN "ADV.DAT" FOR OUTPUT AS #1: PRINT #1, adv: CLOSE
RUN mp$


'                     The Ice Caves Deluxe
'                    ----------------------
IceCave:
x$ = "girlfriend": xx$ = "Lovely Lil": xxx$ = "her"
IF SEX = 1 THEN x$ = "boyfriend": xx$ = "Gorgeous George": xxx$ = "him"
'
GOSUB Title: LOCATE 6: COLOR 2
PRINT "You have spent the last two years living in the woods with your "; x$; ","
PRINT xx$; ". But living in bliss can set you up for nasty surprises."
PRINT
PRINT "You came home from the weapons maker (his shop is near the Main Hall) and"
PRINT "found the cabin locked. This of course would not upset you except "
PRINT xx$; " is nowhere to be found and the place certainly looks like there"
PRINT "was a fight. After careful examination you find broom tracks and realize that"
PRINT "your old enemy Broomhandle the Witch stole "; xxx$; "."
PRINT
PRINT "Being a gentle natured person, you decide that you are going to have to kill"
PRINT "that &!^$* witch and find your "; x$; "."
PRINT
PRINT "You see a message in the dirt that says: "; q$; "ICE CAVE"; q$; "."
PRINT : PRINT "A challenge if ever there was one!"
LOCATE 20: GOTO KeyWait
IceCave1:
LOCATE 20: GOTO KeyWait

'                          Cronum's Castle
'                         -----------------
Cronum:
GOSUB Title: LOCATE 7: COLOR 2
PRINT "The story of "; q$; "Cronum's Castle"; q$; " is given by reading a scroll in the first room of"
PRINT "the adventure. The scroll reads:"
PRINT
PRINT "   "; q$; "The object of this quest is to find Cronum's golden ring and destroy "
PRINT "   the monster guarding it."; q$
PRINT
PRINT "Who is Cronum? What is the secret power of his ring? Why must it be captured?"
LOCATE 20: GOSUB KeyWait: COLOR 2: LOCATE 7
PRINT "To be honest, none of the answers to these questions will be revealed."
PRINT
PRINT "That's life in Eamon."
PRINT
PRINT "So prepare yourself and sharpen your sword (or machine gun). You are about to"
PRINT "storm "; q$; "Cronum's Castle."; q$
LOCATE 20: GOTO KeyWait
Cronum1:
LOCATE 20: GOTO KeyWait

'                        Lord of the Underland
'                       -----------------------
Underland:
GOSUB Title: LOCATE 7: COLOR 2
PRINT "The only introduction existing for "; q$; "Lord of the Underland"; q$; " is found in an"
PRINT "advertisement by PC-SIG, reading:": COLOR 7
PRINT
PRINT "   You must fight your way through "; q$; "The Underland,"; q$; " taking on many foes in "
PRINT "   order to realize the object of your quest - a powerful sword."
PRINT : COLOR 2
PRINT "The object seems to be to to travel to the "; q$; "Underland,"; q$; " an underground kingdom"
PRINT "north of the Main Hall, and eliminate its ruler, the wizard Mehan."
PRINT
PRINT "You will have opportunities to interact with some of the Underland's denizens,"
PRINT "rescue some captive Free Adventurers, and wreak havoc on a suburban family along"
PRINT "the way."
LOCATE 20: GOSUB KeyWait: COLOR 2: LOCATE 7
PRINT q$; "Wreak havoc on a suburban family along the way?,"; q$; " you ask."
PRINT
PRINT "That's Eamon for you."
PRINT
PRINT "So study your Blast spell and say a prayer. You're about to square off with the"
PRINT q$; "Lord of the Underland."; q$
LOCATE 20: GOTO KeyWait
Underland1:
LOCATE 20: GOTO KeyWait

'                  The Adventurer's General Store
'                 --------------------------------
GenStore:
GOSUB Title: LOCATE 8: COLOR 7
PRINT "[Editor's Note: This "; q$; "adventure"; q$; " is actually a full conversion of a failed"
PRINT "Apple II system called Eamon Pro. Thomas Ferguson combined all of the notable"
PRINT "aspects of Eamon Pro's Main Hall and single adventure into the form of an Eamon"
PRINT "Deluxe adventure.]"
LOCATE 20: GOSUB KeyWait: LOCATE 8: COLOR 2
PRINT "The Adventurer's General Store is a mall in which adventurers may buy various"
PRINT "weapons, from a morning star to an antimatter ring."
PRINT
PRINT "You may purchase items in the first level by using the BUY command. If you do "
PRINT "not have enough money you will not be able to purchase the item from the shop-"
PRINT "keeper. The shopkeeper cannot be harmed in any way."
LOCATE 20: GOSUB KeyWait: LOCATE 8: COLOR 2
PRINT "In addition, you may adventure in the mall's sewer system."
PRINT
PRINT "The sewer system has become the headquarters of a group of monsters known as"
PRINT "T.O.A.D. (The Official Agents of Destruction). You must be very careful because"
PRINT "T.O.A.D. likes no one and will attack upon sight."
LOCATE 20: GOSUB KeyWait: LOCATE 7: COLOR 2
PRINT "Under the former management, it was the policy of the Adventurer's General Store"
PRINT "that all weapons dropped or lost while in the mall itself (but not outside or"
PRINT "under it) became the property of the mall and were bought back (in the normal"
PRINT "manner) if the adventurer wished to obtain them once more. Adventurers were not"
PRINT "paid any money for dropping the dropped weapons, but they were merely "
PRINT "confiscated by the mall."
PRINT
PRINT "This policy has been rescinded by the new management."
LOCATE 20: GOTO KeyWait
GenStore1:
LOCATE 20: GOTO KeyWait

'                     The Sub-Aquan Laboratory
'                    --------------------------
Aqua:
GOSUB Title: LOCATE 6: COLOR 2
PRINT "This adventure takes place in the Barony Of Horn, part of the Northern"
PRINT "Michigan area of the contemporary United States. You and three other fearless"
PRINT "Free Adventurers have gathered from far around, hoping for news about the"
PRINT "mysterious "; q$; "Golden Marauders."; q$
PRINT
PRINT "Upon the request of the Mayor of Horn, one Hubert Frump, your party visits the"
PRINT "mayor at his estate, where he then promises you that if you learn anything of"
PRINT "significance in this venture, that he will pay you for your information, as well"
PRINT "as let you take the items that you find while working for him. When you accept,"
PRINT "he hands you over to his trusted advisor, Squire Briik, who then tells you the"
PRINT "story of these strange golden warriors:"
LOCATE 20: GOSUB KeyWait: LOCATE 6: COLOR 2
PRINT q$; "These marauders strike swiftly, and to this very day none of the outlying"
PRINT "villages have been able to resist their attacks. As one eyewitness reported,"
PRINT "'They move and act as one unit, and the town guards are practically useless in "
PRINT "combating them.' Indeed, no weapon brought to bear by the defenders had any"
PRINT "effect upon the glowing golden figures."
PRINT
PRINT q$; "Reports indicate that several villages have been razed by the marauders and,"
PRINT "after the attacks, the entire adult populace was captured and led away to the"
PRINT "Northwest of Horn. It is speculated that there may also be a connection between"
PRINT "the missing people, and the ever-increasing number of 'Golden Legionnaires', but"
PRINT "any conclusions are hard to draw at this time."
LOCATE 20: GOSUB KeyWait: LOCATE 7: COLOR 2
PRINT q$; "Rumors throughout Horn disclose that our Mayor has plans to deal with the "
PRINT "golden menace. As of yet the details are not well known, though naturally "
PRINT "speculation on how this feat will be accomplished runs rampant."
PRINT
PRINT q$; "One rumor tells of a man wearing golden armor being captured north of"
PRINT "Mucktown. The mayor discloses that the individual caught was actually an android"
PRINT "found to be in possession of a piece of mysterious gold metal- obviously a"
PRINT "section of armor or robotoid."
LOCATE 20: GOSUB KeyWait: LOCATE 6: COLOR 2
PRINT q$; "The android was found along the shores of the Great Mitchigoom about 10"
PRINT "kilometers north of Mucktown, where it was heading north. Once subdued, the"
PRINT "android was brought to Mucktown and questioned by authorities. When normal means"
PRINT "of interrogation were found to be useless against the cyberoid circuits"
PRINT "controlling the android's brain, a mnemonic drainer was employed. Unfortunately,"
PRINT "the slightly defective device killed the android, but a great deal of useful "
PRINT "information was gleaned from the prisoner's brain before its mind was destroyed."
PRINT
PRINT q$; "The android was on a mission connected in some way with the golden warriors."
PRINT "The exact nature of the affiliation is unknown. However, the android was "
PRINT "carrying a golden breastplate with the word 'REAPER' carved into it. According"
PRINT "to the mind probe the android was returning to its home base, which it called"
PRINT "'Samurai'. This base may be found approximately 20 kilometers north along the"
PRINT "coast...."; q$
LOCATE 20: GOSUB KeyWait: LOCATE 7: COLOR 2
PRINT "The mayor's cousin, a local wizard, is then summoned to create a magic stairway"
PRINT "which you and your companions may use to enter and exit the base. You make one"
PRINT "final visit to the mayor's fancy bathroom and then hurry through the portal into"
PRINT "the magic stairway, rushing to catch up with your fellow adventurers."
PRINT
PRINT "As you feel your feet land on the magical steps, your surroundings become blurry"
PRINT "and your head begins to swim while a dull throbbing sound fills your ears."
PRINT "Although you still seem to be moving, you can't tell if it's by your own power"
PRINT "or if something is propelling you."
LOCATE 20: GOSUB KeyWait: LOCATE 8: COLOR 2
PRINT "After a matter of what seems to be a few minutes, your head clears and you feel"
PRINT "a cool breeze on your face. A strong hint of salt tingles your senses awake and"
PRINT "you realize that your are on the middle of a solid white, glowing staircase with"
PRINT "clear exits back into reality at each end."
LOCATE 20: GOSUB KeyWait: LOCATE 8: COLOR 2
PRINT "Above you hovers a portal back to the Main hall and below you lies the top of a"
PRINT "tall cliff. At the base of the towering cliff you can make out a sandy beach"
PRINT "running along a lake of enormous proportions. You start to hear the excited"
PRINT "voices of your companions coming from the cliff top. It appears they have"
PRINT "already spotted a possible entrance to the base in the distance..."
LOCATE 20: GOTO KeyWait
Aqua1:
LOCATE 20: GOTO KeyWait

'                  Review header
'                 ---------------
Review1:
GOSUB Title: a$ = "The Eamon Adventurer's Guild": COLOR 14: GOSUB Center
COLOR 2: a$ = "www.EamonAG.org": GOSUB Center: COLOR 3: PRINT : PRINT
PRINT "The following is from the "; s$; " issue of the Eamon Adventurer's Guild"
PRINT "newsletter. The EAG supports the Apple II version of Eamon and not Eamon"
PRINT "Deluxe. The review is for the old Apple II version."
Review1a:
PRINT
PRINT "The EAG rating system is from 1-10 (10 being the best), the average rating"
PRINT "is displayed with the number of people who have rated it. e.g. 4.6/3 is an"
PRINT "average rating of 4.6 by the ratings of three people."
PRINT : COLOR 2
PRINT "Remember, these reviews were written for the OLD versions of these adventures,"
PRINT "most Eamon Deluxe adventures have been enhanced considerably!"
GOTO KeyWait

'                *********************
'                  NEUC Review header
'                *********************
Review2:
GOSUB Title: a$ = "The Eamon Adventurer's Guild": COLOR 14: GOSUB Center
COLOR 2: a$ = "www.EamonAG.org": GOSUB Center: COLOR 3: PRINT : PRINT
PRINT "The following is from the "; s$; " issue of the "; : COLOR 7
PRINT "National Eamon User's Club": COLOR 3
PRINT "newsletter which preceded the Eamon Adventurer's Guild. The ratings are from"
PRINT "the EAG. The NEUC supported the Apple II version of Eamon and not Eamon Deluxe."
PRINT "The review is for the old Apple II version.": GOTO Review1a

'                  Select adventure
'                 ------------------
Pick:
adv = 0: a$ = ""
CALL EDX.Select.Multi.Adventure(adv, a$)
Adventure$ = a$(adv): mp$ = mp$(adv): author$ = a1$(adv): RETURN


KeyWait:
COLOR 3
KeyWait1:
PRINT : PRINT
KeyWait2:
PRINT "Press any key to continue...";
KeyWait3:
k$ = INKEY$: BS = POS(0): CC# = 0
DO
    IF s%(4) <> 1 THEN
        CC# = CC# + 1
        IF CC# = CC1# THEN PRINT "/": LOCATE CSRLIN - 1, BS
        IF CC# = CC2# THEN PRINT CHR$(179): LOCATE CSRLIN - 1, BS
        IF CC# = CC3# THEN : PRINT "\": LOCATE CSRLIN - 1, BS: CC# = 0
    END IF
    k$ = UCASE$(INKEY$)
LOOP UNTIL k$ <> ""

Title:
COLOR 3
Title1:
CLS : a$ = Adventure$: GOSUB Center: a$ = author$:
GOSUB Center: PRINT STRING$(80, 196): PRINT : RETURN

Center:
PRINT SPC(INT((80 - LEN(a$)) / 2)); a$: RETURN

'
'     Emulate Apple II SPEED command
'    --------------------------------
SlowPrint:
FOR x = 1 TO LEN(a$)
    PRINT MID$(a$, x, 1);
    FOR x1# = 1 TO a#: NEXT
NEXT
PRINT : RETURN

'              AD Menu Header 5.0
'             --------------------
Header:
mc1 = 1: mc2 = 15: mc3 = 7: COLOR 7: CLS
IF s%(4) = 1 THEN RETURN
COLOR mc1, 0: PRINT " �"; STRING$(76, "�"); "�"
'PRINT " �"; SPACE$(76); "�"
PRINT " �"; SPACE$(20); : COLOR mc2: PRINT "THE MARVELOUS WORLD OF EAMON DELUXE ";
COLOR mc1: PRINT SPACE$(20); "�": PRINT " �"; SPACE$(24); : COLOR mc2
PRINT "The Lost Treasures of Eamon"; : COLOR mc1: PRINT SPACE$(25); "�"
'PRINT " �"; SPACE$(76); "�"
PRINT " �"; STRING$(76, "�"); "�": PRINT : COLOR 7: RETURN

'                       -----------
'                        Error Pit
'                       -----------
ErrHandler:
COLOR 15: PRINT : PRINT SPACE$(15);
PRINT "UH-OH, A PROGRAM ERROR JUST POPPED UP! (CODE"; STR$(ERR); ")": PRINT
k$ = "try and continue": IF ERR > 51 THEN k$ = "exit to Main Menu"
PRINT SPACE$(20); "Hit any key to "; k$; "...": k$ = INKEY$: k$ = ""
DO
    k$ = INKEY$
LOOP UNTIL k$ <> ""
PRINT : IF ERR > 51 THEN CHDIR "..": RUN "EAMONDX"
PRINT "ATTEMPTING TO CONTINUE...": PRINT : L = 0: COLOR 7: RESUME NEXT

ScreenError:
SCREEN 0: WIDTH 80: CLS : SkreenTest = 1
COLOR 0, 2: PRINT " Sorry, ": COLOR 3, 0: PRINT
PRINT "You won't be able see the graphic intro screen for "; q$; "[GAME NAME]"; q$; ","
PRINT "because compatible graphics hardware is not available."
GOSUB KeyWait: RESUME SkreenTest

AdventureData:
DATA 9
DATA "The Ice Caves Deluxe",Jon Walker,PCWALKER
DATA Cronum's Castle,"Matt Ashcraft & Richard Tonsing",PCWALKER
DATA Lord of the Underland,Justin Langseth,PCWALKER
DATA The Adventurer's General Store,Ryan Page,UNICORN
DATA The Sub-Aquan Laboratory,Michael Penner,SUB-AQUA
DATA "Crypt Crashers: The Tomb of Horrors",Michael Penner,MAINPGM
DATA Tenement of the Damned,"J.M. Menassanch",MAINPGM
DATA Beneath Mount Imagery,Roy Riggs,MAINPGM
DATA Leadlight,Wade Clarke,MAINPGM

EDXMenuStrings:
DATA 8
DATA "  Preview Adventure Intros          "
DATA "  START PLAYING AN ADVENTURE        "
DATA "  Read Adventure Reviews            "
DATA "  Read Review for This Collection   "
DATA "  Read Notes About This Collection  "
DATA "  Return to Eamon Deluxe Menu    "
DATA "  Review of the Eamon Deluxe Version   "
DATA "  Review of the Classic Eamon Version  "

SUB EDX.Select.Multi.Adventure (c, a$)
'/// Eamon Deluxe Multi-Adventure Selection
'
' (Updated for VI Mode support 06 JAN 2012)
'
'   Codes returned from SUB:
'      c - Number of adventure selected
'     a$ - Name of adventure selected

IF dungeons = 1 THEN c = 1: a$ = ANAME$: EXIT SUB

' VI Mode numeric menu
GetMAdvCVI:
IF s%(4) = 1 THEN
    CLS : L = 2: PRINT "Please select an adventure:"
    FOR c = 1 TO dungeons
        a$ = LTRIM$(RTRIM$(STR$(c))) + ": " + dn$(c)
        IF c < 10 THEN a$ = " " + a$
        IF c < 100 THEN a$ = " " + a$
        PRINT a$: L = L + 1
        IF L = 20 THEN
            PRINT "Hit any key to continue...";
            DO
            LOOP UNTIL INKEY$ <> ""
            L = 0
        END IF
    NEXT
    PRINT : PRINT "Enter the number of your choice (0 to Exit):";
    LINE INPUT ""; a$: c = INT(VAL(a$))
    IF c < 0 OR c > dungeons THEN CLS : GOTO GetMAdvCVI
    'IF c = 0 THEN RUN 0
    a$ = dn$(c): EXIT SUB
END IF


' Standard Menu
'            Load adventure names
REDIM advn$(dungeons)
FOR x = 1 TO dungeons
    advn$(x) = LEFT$(LTRIM$(RTRIM$(dn$(x))), 38)
NEXT
numadv = dungeons

keyz$ = CHR$(13) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

mSelectADVa:
GOSUB mSelectADV0

mSelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
    COLOR 7, 1
    IF VAL(MID$(TIME$, 4, 2)) <> min THEN
        hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
        IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB mSelectADV0
        k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
        LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
        PRINT "  "; STR$(k);
        PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
    END IF
    LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
    k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
IF k < 3 THEN
    a$ = dn$(c): VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO mSelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO mSelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO mSelectADV

'       Set up screen
mSelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>                     <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
'       Display choices
mSelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
    LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
' // Compact version of standard Eamon Deluxe selection menu
'      Allows only a single row of choices and has no error checking
'
' (Updated with alt. numeric menu for VI Mode support 06 JAN 2012)
'
'  Arguments passed to EDXMenuM:
'     c - Vertical cursor position to begin at (0=CSRLIN)
'    c1 - Horizontal cursor position to begin at  (0=POS)
'    be - Location in em$ array to begin at
'    en - Location in em$ array to end
'   tc1 - Text foreground color of menu
'   tc2 - Text background color of menu
'
' Values returned by EDXMenuM:
'     c - The number of the choice relative to the amount of choices
'    c1 - This holds the exit key code (Standard:1=Enter,2=Space,3=Escape)
'    en - The actual array number of em$() that was chosen
'
'  Required SHARED variables:
'    s%() - Eamon DX system settings (s%(4) = 1 is VI Mode Flag)
'   em$() - An array which holds all available choices. em$(0) must
'           contain exit keys
'    em0 - The maximum dimension of the em$() array.
'
'   Upon exit, the cursor will locate to the row just beneath the last
'   choice (or 25 if it exceeds screen), at column 1
'

' VI Mode numeric menu
EDXMenuMCVI:
IF s%(4) = 1 THEN
    L = 2
    FOR x = be TO en
        x$ = LTRIM$(STR$(x - be + 1)) + ": " + LTRIM$(RTRIM$(em$(x)))
        IF x < 100 THEN x$ = " " + x$: IF x < 10 THEN x$ = " " + x$
        PRINT x$: L = L + 1
        IF L = 20 THEN
            PRINT "Hit any key to continue...";
            DO
            LOOP UNTIL INKEY$ <> ""
            CLS : L = 0
        END IF
    NEXT
    PRINT : PRINT "Enter the number of your choice (0 to exit):";
    LINE INPUT ""; k$: c = INT(VAL(k$)): c1 = c + (be - 1)
    IF c = 0 THEN c1 = 3: EXIT SUB
    IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMCVI
    en = c1: c1 = 1: EXIT SUB
END IF

' Standard Menu
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, tc2
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT

EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1
LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
    k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h: COLOR tc1, tc2: PRINT em$(c);

IF LEN(k$) > 1 THEN
    IF k = 71 THEN c = be
    IF k = 79 THEN c = en
    IF k = 72 OR k = 75 THEN c = c - 1
    IF k = 80 OR k = 77 THEN c = c + 1
    GOTO EDXMenuM
END IF

IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB
END SUB

