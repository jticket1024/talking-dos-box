ADVENTURE #00




No reviews have been written for this adventure collection yet.











<paws>
ADVENTURE #01

Reviewed by Frank Black

Playing Time: 10-15 minutes
Overall rating: 3.0                Difficulty Rating: 1

Comment: From the intro: "A challenge if ever there was one!"

[Chuckling] That is quite the understatement actually. This is in fact a very
simple beginner-level adventure even for the loose standards of the early time
period of Classic Eamon in which it was written, with the biggest of the few
specials for this adventure being a hackneyed "find your missing lover" quest
that is powered by routines lifted straight out of Lair of The Minotaur and The
Abductor's Quarters.


<paws>


The only mildly challenging puzzle in the game is one you face at the very start
and involves you being locked out of your house. Once you find your keys, which
are a step away from being left out in plain sight, you can pass through your
unremarkable cabin, into your back yard and onward toward the Ice Cave.
(Although why you can only enter your sprawling backyard from the house is never
explained.) Just past your yard you stumble upon the life-companion-kidnapping
old witch from the intro who is supposedly your mortal enemy (also never
explained) and whom I dispatched easily with a couple swings of an average
Marcos sword.


<paws>

Yes, the "end boss" for this adventure is a total wimp and you dispose of her
before you even get to the Ice Cave. And speaking of the adventure title bearing
caverns, you would think that it would be hidden or require some sort of effort
for entry, but no, apparently: "A huge gate that once would have blocked your
way lies in disrepair." Sheesh.

All in all you are looking at 41 rooms, one locked door, one secret passage, a
handful of minor specials and 6 monsters (not counting your pal and their gender
twin dummies).

"Ice Cave" has been labeled as being written by "Anonymous" around the internet
but Thomas and I each sifted through the compiled executable code (as far as I
know, no BASIC source code exists anywhere for this one), and we both found text
strings indicating it was written by Jon Walker so, unless someone else comes
forth to claim this relic, I'm going to assume that he is indeed the author.

<paws>

This gets it's place in the Eamon Deluxe Museum with a "Lost Treasure" status
because it was an original Walker-PC-Eamon-only adventure that was designed for
that little known, sub-par Eamon port from the 80s and is actually somewhat rare
and unknown. Have no delusions though, the adventure material in and of itself
is far from a treasure. Thomas did an excellent job cleaning it up and fixed a
lot of bad room connections along with other minor surgeries to make it
playable, and I did a second sweep of enhancements afterwards to bring it up to
par as a very low grade Eamon Deluxe adventure. Were I currently reviewing the
Ice Cave in it's original format, I would have rated it even lower than I did.
All in all, it's not the worst adventure ever written but it is pretty far from
the best as well. Recommended for beginner's or those seeking a minor
distraction and for the experienced Eamon Adventurer: it's worth checking out at
least once for the sake of nostalgia as it does hold an obscure place in the
very early days of Eamon.

<paws>
ADVENTURE #02




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #03




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #04




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #05




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #06




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #07




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #08




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #09




Review for this adventure:

IT WAS GREAT!








<paws>
ADVENTURE #10




