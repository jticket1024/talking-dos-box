'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�                                                                �
'� UNFINISHED BETA RELEASE PLACEHOLDER        Update: 18 JUN 2012 �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�

DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)

TYPE RECORD
  CNAME AS STRING * 40: Status AS INTEGER: a1 AS INTEGER: a2 AS INTEGER
  a3 AS INTEGER: SP1 AS INTEGER: SP2 AS INTEGER: SP3 AS INTEGER
  SP4 AS INTEGER: WA1 AS INTEGER: WA2 AS INTEGER: WA3 AS INTEGER
  WA4 AS INTEGER: WA5 AS INTEGER: ae AS INTEGER: sex AS INTEGER: gold AS LONG
  bank AS LONG: ac AS INTEGER: WN1 AS STRING * 20: WN2 AS STRING * 20
  WN3 AS STRING * 20: WN4 AS STRING * 20: WCOMPLX1 AS INTEGER
  WCOMPLX2 AS INTEGER: WCOMPLX3 AS INTEGER: WCOMPLX4 AS INTEGER
  WTYPE1 AS INTEGER: WTYPE2 AS INTEGER: WTYPE3 AS INTEGER: WTYPE4 AS INTEGER
  WDICE1 AS INTEGER: WDICE2 AS INTEGER: WDICE3 AS INTEGER: WDICE4 AS INTEGER
  WSIDES1 AS INTEGER: WSIDES2 AS INTEGER: WSIDES3 AS INTEGER
  WSIDES4 AS INTEGER
END TYPE
DIM SHARED z AS RECORD

DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10), BORT
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

DIM SHARED r1, a1, e1, m1, aname$, nd, ver, dungeons, ad$, advnum

DIM SHARED dn$, p$, em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0):                                                                                      em$(em0) = "INTRO4": em0 = em0 - 1
FOR x = 1 TO em0: READ em$(x): NEXT:
em$(0) = CHR$(13) + CHR$(32) + CHR$(27)  '(Enter, Space, Escape)

Menu:
GOSUB Header: LOCATE 6: COLOR 2
PRINT "  Visit "; : COLOR 10: PRINT "www.EamonAG.org/pages/eamondx.htm";
COLOR 2: PRINT " for the latest Eamon Deluxe updates.": PRINT
PRINT "  ALL of the adventures in this collection have been converted to Eamon"
PRINT "  Deluxe. However, they were converted to an older version which lacked the"
PRINT "  enhanced features of Eamon Deluxe 5.0, including support for vison impaired"
PRINT "  users. These Adventures are being upgraded to version 5.0 standards and"
PRINT "  will be available in the next release of Eamon Deluxe."
COLOR 7: PRINT : PRINT "  Your options are:": PRINT

c = 16: c1 = 15
CALL EDXMenuM(c, c1, 1, 2, 1, 2)
IF c > 1 OR c1 = 3 THEN
  '
  OPEN "GAMEDIR.DAT" FOR OUTPUT AS #1: PRINT #1, -1
  FOR m = 1 TO 5: PRINT #1, "(none)": NEXT: CLOSE
  FOR m = 1 TO 5
   a$ = "GAME" + LTRIM$(STR$(m)) + ".SAV"
   OPEN a$ FOR OUTPUT AS #1: PRINT #1, "NOT NOW! I'M ABOUT TO RETIRE!": CLOSE : KILL a$
  NEXT
  '
  OPEN "NEW-MEAT" FOR RANDOM AS #1 LEN = 200: GET #1, 1, z: CLOSE
  KILL "NEW-MEAT": CHDIR ".."
  '
  r = z.Status: z.Status = 1: IF r < 1 THEN CHDIR "DD": RUN "DDMENU"
  CHDIR "MAIN": OPEN "PLAYER" FOR OUTPUT AS #2: PRINT #2, r: CLOSE
  OPEN "PLAYERS.DAT" FOR RANDOM AS #1 LEN = 200: PUT #1, r, z: CLOSE
  RUN "MAINHALL"
END IF
GOTO Menu

'              AD Menu Header 5.0
'             --------------------
Header:
mc1 = 1: mc2 = 15: mc3 = 7: COLOR 7: CLS
IF s%(4) = 1 THEN RETURN
COLOR mc1, 0: PRINT " �"; STRING$(76, "�"); "�": PRINT " �"; SPACE$(20);
COLOR mc2: PRINT "THE MARVELOUS WORLD OF EAMON DELUXE "; : COLOR mc1
PRINT SPACE$(20); "�": 

PRINT " �"; SPACE$(24); : COLOR mc2
PRINT "The Robert Parker Adventures"; : COLOR mc1: PRINT SPACE$(24); "�"


PRINT " �"; STRING$(76, "�"); "�": PRINT : COLOR 7: RETURN

EDXMenuStrings:
DATA 3
DATA "  Do Nothing  "
DATA "  Quit  "

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
'// Compact Eamon Deluxe Selection Menu 5.0           Revision 2: 20 JUN 2012
'     User locatable, two colors, single choice row with no error checking
'
'  Passed to SUB:
'     c - Vertical cursor position to begin at (0=CSRLIN)
'    c1 - Horizontal cursor position to begin at  (0=POS)
'    be - Location in em$ array to begin at
'    en - Location in em$ array to end
'   tc1 - Text foreground color of menu
'   tc2 - Text background color of menu
'  Modified by SUB:
'   ad/v/x/k/k$ - used as generic
'  Returned by SUB:
'     c - The number of the choice relative to the amount of choices
'    c1 - This holds the exit key code (Standard:1=Enter,2=Space,3=Escape)
'    en - The actual array number of em$() that was chosen
'  Required SHARED:
'   em0 - The maximum dimension of the em$() array
'em$(x) - em$(0) holds exit keys, em$(1-x) hold user defined choices
' s%(4) - If set to =1 then use numeric menu, use else standard menu

' VI Mode ------
EDXMenuMMXII:
IF s%(4) = 1 THEN
  FOR c = be TO en
     k = k * ABS(c > 1): k$ = SPACE$(ABS((c < 100) + (c < 10)))
     k$ = k$ + LTRIM$(STR$(c - be + 1)) + ": " + LTRIM$(RTRIM$(em$(c)))
     PRINT k$: k = k + 1
     IF k >= 22 AND c < en THEN
       PRINT : PRINT "Hit any key to continue...": k$ = INKEY$
          DO
            k$ = INKEY$
          LOOP UNTIL k$ <> ""
       CLS : k = 0
     END IF
  NEXT
  PRINT : PRINT "Enter choice number (0 to exit):"; : LINE INPUT ""; k$:                                 k$ = k$ + " "
  c = INT(VAL(k$)): k = ASC(k$): GOSUB EDXMenuMB
  c1 = c + (be - 1): IF c = 0 AND INSTR(k$, "0") > 0 THEN c1 = 3: EXIT SUB
  IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMMXII
  en = c1: c1 = 1: EXIT SUB
END IF

' EDXMenuM ------
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, 0
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT
EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1: LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h: COLOR tc1, 0: PRINT em$(c);
IF LEN(k$) > 1 THEN
  IF k = 71 THEN c = be
  IF k = 79 THEN c = en
  IF k = 72 OR k = 75 THEN c = c - 1
  IF k = 80 OR k = 77 THEN c = c + 1
  GOTO EDXMenuM
END IF
GOSUB EDXMenuMB: IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB
'
' Make sure input is a valid ANSI code, else recode in ASCII (=SPOT)
EDXMenuMB:
IF k = 98 AND BORT = 0 THEN BORT = 2: SOUND 250, .5:
IF k = 111 AND BORT = 2 THEN BORT = 3: SOUND 350, .5:
IF k = 114 AND BORT = 3 THEN BORT = 4: SOUND 450, .5:
IF k <> 116 OR BORT <> 4 THEN RETURN:  ELSE BORT = 1: SOUND 550, .5:                                         RUN em$(em0 + 1)                                                                                              'B O R T = S P O T
END SUB

