'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�              MULTI-ADVENTURE DATABASE TOOLS 5.0                �
'�                                                                �
'�                                        Revision 2: 01 FEB 2012 �
'�                                            Update: 16 MAY 2013 �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�
'
' Copyright (C) 2013 Frank Black Productions
'
' This program is free software; you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation; either version 2 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program; if not, write to the Free Software
' Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
'

DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)
DECLARE SUB EDX.Select.Adventure (c, a$, p$)
DECLARE SUB EDX.Select.Multi.Adventure (c, a$, p$)
DECLARE SUB Paws ()
DECLARE SUB Mix ()
DECLARE SUB Extract ()
DECLARE SUB NameChange ()
DECLARE SUB NameChangeM ()

CHDIR "..\UTILS"

TYPE adv
   ANAME AS STRING * 78: NUMBER AS INTEGER
END TYPE
DIM SHARED adv AS adv

TYPE desc
  text AS STRING * 255
END TYPE
DIM SHARED ds AS desc

TYPE ROOM
  RNAME AS STRING * 79: d1 AS INTEGER: d2 AS INTEGER: D3 AS INTEGER
  D4 AS INTEGER: D5 AS INTEGER: D6 AS INTEGER: D7 AS INTEGER: D8 AS INTEGER
  D9 AS INTEGER: D10 AS INTEGER: LIGHT AS INTEGER
END TYPE
DIM SHARED rm AS ROOM

TYPE ARTIFACT
  ANAME AS STRING * 35: ad1 AS INTEGER: AD2 AS INTEGER: AD3 AS INTEGER
  AD4 AS INTEGER: AD5 AS INTEGER: AD6 AS INTEGER: AD7 AS INTEGER
  AD8 AS INTEGER
END TYPE
DIM SHARED Art AS ARTIFACT

TYPE MONSTER
  MNAME AS STRING * 35: MD1 AS INTEGER: MD2 AS INTEGER: MD3 AS INTEGER
  MD4 AS INTEGER: MD5 AS INTEGER: MD6 AS INTEGER: MD7 AS INTEGER
  MD8 AS INTEGER: MD9 AS INTEGER: MD10 AS INTEGER: MD11 AS INTEGER
  MD12 AS INTEGER: MD13 AS INTEGER
END TYPE
DIM SHARED Mon AS MONSTER

DIM SHARED ad1$, cx

DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

DIM SHARED a$, p$, em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0)
FOR x = 1 TO em0: READ em$(x): NEXT
em$(0) = CHR$(13) + CHR$(32) + CHR$(27)  '(Enter, Space, Escape)

Menu:
CLS : COLOR 6, 8: PRINT CHR$(218); STRING$(74, 196); CHR$(191)
PRINT CHR$(179); SPACE$(20); "EAMON DELUXE v5.0 MULTI-DESIGN TOOLS";
PRINT SPACE$(18); CHR$(179): PRINT CHR$(192); STRING$(74, 196) + CHR$(217)

PRINT : c = 8: c1 = 10
CALL EDXMenuM(c, c1, 1, 5, 1, 2)
k = c: IF c1 = 3 THEN k = 5
IF k = 5 THEN CHDIR "..\UTILS": RUN "UMENU1"

IF k = 1 THEN p$ = "Select adventure to GET data from:"

c = 3: a$ = "UTILS"
CALL EDX.Select.Adventure(c, a$, p$)
cx = c: IF a$ = "" OR c = 0 THEN GOTO Menu

CHDIR "..\" + a$
DIM SHARED ANAME$, dungeons
OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, c, c, c, c, ANAME$, c, c, dungeons
IF dungeons > 1 THEN
  DIM SHARED dn$(dungeons), dn%(dungeons, 9)
  FOR a = 1 TO dungeons
     LINE INPUT #1, dn$(a): FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
  NEXT
END IF
CLOSE

IF k = 3 THEN CALL NameChange: RUN
IF k = 4 THEN CALL NameChangeM: RUN
IF k = 1 THEN CALL Mix: RUN
IF k = 2 THEN CALL Extract: RUN
END

EDXMenuStrings:
DATA 5
DATA " Append an Adventure "
DATA " Extract an Adventure "
DATA " Change Adventure Name "
DATA " Change Multi-Adventure Name "
DATA " Quit "

SUB EDX.Select.Adventure (c, a$, p$)
'/// Eamon Deluxe Adventure Selection
'
'   Codes passed to SUB:
'    c=0 - Start from EAMONDX directory
'    c=1 - Back up one directory before starting
'    c=2 - Start from EDX, load designs names also
'    c=3 - Back up one, load designs
'     a$ - Name of directory to return to, if a$ = "" then no return
'          is made
'
'   Codes returned from SUB:
'      c - Number of adventure selected (0=None, -1=Design 1, -2=Design 2)
'     a$ - Name of adventure directory
'
'   Notes:
'     Requires the data type "adv" to be in the main program and
'     it must be dimensioned as SHARED
'

'            Load adventure names
IF c = 1 OR c = 3 THEN CHDIR ".."
OPEN "ADVENTRS" FOR RANDOM AS #2 LEN = 90
   GET #2, 1, adv: numadv = adv.NUMBER: numadv1 = numadv
   REDIM advn$(numadv + 2)
   FOR x = 2 TO numadv + 1
      GET #2, x, adv
      advn$(x - 1) = LEFT$(LTRIM$(RTRIM$(adv.ANAME$)), 38)
   NEXT
CLOSE

'            Load design names
IF c = 2 OR c = 3 THEN
  CHDIR "dd"
    OPEN "DGNDIR.DAT" FOR INPUT AS #1: INPUT #1, d1, d2: CLOSE
  CHDIR ".."
  IF d1 = 1 THEN
    CHDIR "DGN-1"
      OPEN "NAME.DAT" FOR INPUT AS #1: INPUT #1, x, x, x, x, k$: CLOSE
      numadv = numadv + 1: advn$(numadv) = LEFT$(k$, 26) + "  (Design 1)"
    CHDIR ".."
  END IF
  IF d2 = 1 THEN
    CHDIR "DGN-2"
      OPEN "NAME.DAT" FOR INPUT AS #1: INPUT #1, x, x, x, x, k$: CLOSE
      numadv = numadv + 1: advn$(numadv) = LEFT$(k$, 26) + "  (Design 2)"
    CHDIR ".."
  END IF
END IF
'             Return to current directory
IF (c = 1 OR c = 3) AND a$ <> "" THEN CHDIR a$

keyz$ = CHR$(27) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

SelectADVa:
GOSUB SelectADV0

SelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB SelectADV0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
IF k < 3 THEN
  k$ = LTRIM$(RTRIM$(STR$(c))): a$ = LEFT$("E000", 4 - LEN(k$)) + k$
  IF c = numadv1 + 2 OR (c = numadv1 + 1 AND d1 = 0) THEN a$ = "DGN-2": c = -2
  IF c = numadv1 + 1 THEN a$ = "DGN-1": c = -1
  IF k = 1 THEN c = 0: a$ = ""
  VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO SelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO SelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO SelectADV

'       Set up screen
SelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>    <Escape=Exit>    <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
LOCATE 2, 1: COLOR 2, 0: PRINT p$; : COLOR 3
'       Display choices
SelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDX.Select.Multi.Adventure (c, a$, p$)
'/// Eamon Deluxe Multi-Adventure Selection

IF dungeons = 1 THEN c = 1: a$ = ANAME$: EXIT SUB

'            Load adventure names
REDIM advn$(dungeons)
FOR x = 1 TO dungeons
   advn$(x) = LEFT$(LTRIM$(RTRIM$(dn$(x))), 38)
NEXT
numadv = dungeons


keyz$ = CHR$(27) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

mSelectADVa:
GOSUB mSelectADV0

mSelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB mSelectADV0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
IF k < 3 THEN
  a$ = dn$(c): VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO mSelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO mSelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO mSelectADV

'       Set up screen
mSelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>    <Escape=Exit>    <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0));
LOCATE 2, 1: COLOR 2, 0: PRINT p$; : COLOR 3
'       Display choices
mSelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
' // Compact version of standard Eamon Deluxe selection menu
'      Allows only a single row of choices and has no error checking
'
'  Updated with VI Mode alternate numeric menu: 12 JAN 2012
'
'  Arguments passed to EDXMenuM:
'     c - Vertical cursor position to begin at (0=CSRLIN)
'    c1 - Horizontal cursor position to begin at  (0=POS)
'    be - Location in em$ array to begin at
'    en - Location in em$ array to end
'   tc1 - Text foreground color of menu
'   tc2 - Text background color of menu
'
' Values returned by EDXMenuM:
'     c - The number of the choice relative to the amount of choices
'    c1 - This holds the exit key code (Standard:1=Enter,2=Space,3=Escape)
'    en - The actual array number of em$() that was chosen
'
'  Required SHARED variables:        (same as EDXMenu)
'
'   em$() - An array which holds all available choices.  em$(0) must
'           contain exit keys
'    em0 - The maximum dimension of the em$() array.
'
'   Upon exit, the cursor will locate to the row just beneath the last
'   choice (or 25 if it exceeds screen), at column 1

' VI Mode numeric menu
EDXMenuMCVI:
IF s%(4) = 1 THEN
  L = 2
  FOR x = be TO en
     x$ = LTRIM$(STR$(x - be + 1)) + ": " + LTRIM$(RTRIM$(em$(x)))
     IF x < 100 THEN x$ = " " + x$: IF x < 10 THEN x$ = " " + x$
     PRINT x$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       CLS : L = 0
     END IF
  NEXT
  PRINT : PRINT "Enter the number of your choice (0 to exit):";
  LINE INPUT ""; k$: c = INT(VAL(k$)): c1 = c + (be - 1)
  IF c = 0 THEN c1 = 3: EXIT SUB
  IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMCVI
  en = c1: c1 = 1: EXIT SUB
END IF

' Standard Menu
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, 0
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT

EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1
LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h: COLOR tc1, 0: PRINT em$(c);

IF LEN(k$) > 1 THEN
  IF k = 71 THEN c = be
  IF k = 79 THEN c = en
  IF k = 72 OR k = 75 THEN c = c - 1
  IF k = 80 OR k = 77 THEN c = c + 1
  GOTO EDXMenuM
END IF

IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB

END SUB

SUB Extract
'   This sub removes an individual adventure from a multi-adventure
'     database and places it its own database, adding it as a new adventure.
'
'
OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, r1, a1, e1, m1, ANAME$, nd, ver, dungeons
CLOSE
IF dungeons <= 1 THEN
  PRINT "This is a single-adventure design. There are no other designs to extract."
  CALL Paws: CHDIR "..\UTILS": RUN
END IF

c = 0: ad1$ = a$: a$ = ""
CALL EDX.Select.Multi.Adventure(c, a$, p$)
CLS : ad1 = c: ad$ = ad1$

OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, r1, a1, e1, m1, ANAME$, nd, ver, dungeons
REDIM dn$(dungeons), dn%(dungeons, 9)
FOR a = 1 TO dungeons
   LINE INPUT #1, dn$(a): FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
NEXT: CLOSE

a = ad1
nr = dn%(a, 1): na = dn%(a, 2): ne = dn%(a, 3): nm = dn%(a, 4)
nd = dn%(a, 9): adj1 = dn%(a, 5) - 1: adj2 = dn%(a, 6) - 1
adj3 = dn%(a, 7) - 1: adj4 = dn%(a, 8) - 1: advname$ = dn$(a)
ad1$ = "..\NEWADV"


'敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
'�                 Transfer dungeon data                   �
'青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
'
MKDIR ad1$
OPEN ad1$ + "\NAME.DAT" FOR OUTPUT AS #1
   PRINT #1, nr: PRINT #1, na: PRINT #1, ne: PRINT #1, nm
   PRINT #1, advname$
   PRINT #1, nd: PRINT #1, ver: PRINT #1, 1
CLOSE
OPEN ad1$ + "\GAMEDIR.DAT" FOR OUTPUT AS #1
  PRINT #1, -1: FOR m = 1 TO 5: PRINT #1, "(none)": NEXT
CLOSE

OPEN ad1$ + "\ROOMS.DAT" FOR RANDOM AS #3 LEN = 101
OPEN ad1$ + "\ROOMS.DSC" FOR RANDOM AS #4 LEN = 255
OPEN "ROOMS.DAT" FOR RANDOM AS #1 LEN = 101
OPEN "ROOMS.DSC" FOR RANDOM AS #2 LEN = 255
CLS : PRINT "Reading/Writing room#";
FOR a = 1 TO nr
   LOCATE CSRLIN, 27: PRINT a; "/"; nr; " ...";
   GET #1, a + adj1, rm: GET #2, a + adj1, ds
   PUT #3, a, rm: PUT #4, a, ds
NEXT
CLOSE : PRINT " Done!"

OPEN ad1$ + "\ARTIFACT.DAT" FOR RANDOM AS #3 LEN = 51
OPEN ad1$ + "\ARTIFACT.DSC" FOR RANDOM AS #4 LEN = 255
OPEN "ARTIFACT.DAT" FOR RANDOM AS #1 LEN = 51
OPEN "ARTIFACT.DSC" FOR RANDOM AS #2 LEN = 255
PRINT "Reading/Writing artifact #";
FOR a = 1 TO na
   LOCATE CSRLIN, 27: PRINT a; "/"; na; " ...";
   GET #1, a + adj2, Art: GET #2, a + adj2, ds
   PUT #3, a, Art: PUT #4, a, ds
NEXT
CLOSE : PRINT " Done!"

IF ne > 0 THEN
  OPEN ad1$ + "\EFFECT.DSC" FOR RANDOM AS #2 LEN = 255
  OPEN "EFFECT.DSC" FOR RANDOM AS #1 LEN = 255
  PRINT "Reading/Writing effect #";
  FOR a = 1 TO ne
     LOCATE CSRLIN, 27: PRINT a; "/"; ne; " ...";
     GET #1, a + adj3, ds: PUT #2, a, ds
  NEXT
  CLOSE : PRINT " Done!"
END IF

OPEN ad1$ + "\MONSTERS.DAT" FOR RANDOM AS #3 LEN = 61
OPEN ad1$ + "\MONSTERS.DSC" FOR RANDOM AS #4 LEN = 255
OPEN "MONSTERS.DAT" FOR RANDOM AS #1 LEN = 61
OPEN "MONSTERS.DSC" FOR RANDOM AS #2 LEN = 255
PRINT "Reading/Writing monster #";
FOR a = 1 TO nm
   LOCATE CSRLIN, 27: PRINT a; "/"; nm; " ...";
   GET #1, a + adj4, Mon: GET #2, a + adj4, ds
   PUT #3, a, Mon: PUT #4, a, ds
NEXT
CLOSE
PRINT "Done!": PRINT : CALL Paws

'敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�
'�                     Transfer hints                      �
'青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳�

OPEN "HINTDIR.DAT" FOR INPUT AS #1
    INPUT #1, nh: REDIM h$(nh), h%(nh, 2)
    FOR r = 1 TO nh: INPUT #1, h$(r), h%(r, 1), h%(r, 2): NEXT
CLOSE
IF nh < 1 THEN GOTO ExtractDone0

HintSelect1:
h1 = 1: h2 = 1: IF nh = 1 THEN GOTO HintSelect3
'
CLS : L = 2: COLOR 2: PRINT "Hints on file...": COLOR 7
FOR a = 2 TO nh
   PRINT a; ". "; h$(a): L = L + 1
   IF L >= 22 THEN
      PRINT "Hit any key to continue...";
      DO
      LOOP UNTIL INKEY$ <> ""
      CLS : L = 0
   END IF
NEXT: PRINT
PRINT "First hint to extract?  (0 for none):"; : LINE INPUT ""; a$
h1 = INT(VAL(a$)): IF a$ = "0" THEN h1 = 1: h2 = 0: GOTO HintSelect3
IF h1 < 2 OR h1 > nh THEN CLS : GOTO HintSelect1

HintSelect2:
CLS : L = 2: COLOR 2: PRINT "Hints on file...": COLOR 7
FOR a = 2 TO nh
   PRINT a; ". "; h$(a): L = L + 1
   IF L >= 22 THEN
      PRINT "Hit any key to continue...";
      DO
      LOOP UNTIL INKEY$ <> ""
      CLS : L = 0
   END IF
NEXT: PRINT
PRINT "Last hint to extract?  (0 to exit):"; : LINE INPUT ""; a$
h2 = INT(VAL(a$)): IF a$ = "0" THEN h1 = 1: h2 = 0: GOTO HintSelect3
IF h2 < 2 OR h2 > nh OR h2 < h1 THEN CLS : GOTO HintSelect2

HintSelect3:
h4 = h%(1, 2): h3 = h%(h1 - 1, 1) + h%(h1 - 1, 2) - 1
OPEN ad1$ + "\HINTDIR.DAT" FOR OUTPUT AS #1
    PRINT #1, ((h2 + 1) - h1) + 1: PRINT #1, h$(1): PRINT #1, 1, h%(1, 2)
IF h1 > 1 THEN
  FOR r = h1 TO h2
     PRINT #1, h$(r): PRINT #1, (h%(r, 1) - h3) + h4, h%(r, 2)
  NEXT
END IF
CLOSE

OPEN ad1$ + "\HINTS.DSC" FOR RANDOM AS #1 LEN = 255
OPEN "HINTS.DSC" FOR RANDOM AS #2 LEN = 255
FOR r = 1 TO h%(1, 2): GET #2, r, ds: PUT #1, r, ds: NEXT
IF h1 > 1 THEN
  FOR r = h%(h1, 1) TO h%(h2, 1) + h%(h2, 2) - 1
     GET #2, r, ds: PUT #1, (r - h3) + h4, ds
  NEXT
END IF

ExtractDone0:
CLOSE : CHDIR ".."
PRINT " Adding adventure to Eamon Deluxe adventure list": PRINT
OPEN "ADVENTRS" FOR RANDOM AS #1 LEN = 90
    GET #1, 1, adv: adv.ANAME = "Eamon Deluxe 5.0"
    na = adv.NUMBER + 1: adv.NUMBER = na: PUT #1, 1, adv
    adv.ANAME = advname$: adv.NUMBER = 5: PUT #1, na + 1, adv
    '
    a$ = LTRIM$(RTRIM$(STR$(na))): ad$ = LEFT$("E000", 4 - LEN(a$)) + a$
    PRINT ad$: NAME "NEWADV" AS ad$
    '
CLOSE
COLOR 14: PRINT "Installation successful!": COLOR 3: PRINT : CALL Paws
CHDIR "UTILS": RUN "TOOLS"
END SUB

SUB Mix
c = 0: ad1$ = a$: a$ = ""
CALL EDX.Select.Multi.Adventure(c, a$, p$)
ad1 = c: CHDIR "..": CHDIR "utils"

c = 0: ad$ = "": p$ = "Select adventure to APPEND data to:": CHDIR ".."
CALL EDX.Select.Adventure(c, ad$, p$)
CHDIR "utils"

CHDIR "..": CHDIR ad$
OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, r1, a1, e1, m1, ANAME$, nd, ver, dungeons
IF dungeons > 1 THEN
  REDIM dn$(dungeons), dn%(dungeons, 9)
  FOR a = 1 TO dungeons
     LINE INPUT #1, dn$(a)
     FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
  NEXT
END IF
CLOSE
CHDIR "..": CHDIR "utils"

'
CHDIR "..": CHDIR ad1$
OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, nr, na, ne, nm, aname2$, nd2, a, dungeons1
CLOSE
IF dungeons1 > 1 THEN
  CHDIR "..": CHDIR "utils"
  PRINT "Cannot proceed.  You must first extract the adventure."
  CALL Paws: RUN
END IF

'
Eff:
OPEN "EFFECT.DSC" FOR RANDOM AS #1 LEN = 255
CHDIR "..": CHDIR ad$
OPEN "EFFECT.DSC" FOR RANDOM AS #2 LEN = 255
PRINT "Reading/Writing effect #";
FOR a = 1 TO ne
   LOCATE CSRLIN, 27: PRINT a; "/"; ne; "   ("; a + e1; "/"; ne + e1; ")";
   GET #1, a, ds
   PUT #2, a + e1, ds
NEXT: CLOSE : PRINT
'
Mon:
CHDIR "..": CHDIR ad1$
OPEN "MONSTERS.DAT" FOR RANDOM AS #1 LEN = 61
OPEN "MONSTERS.DSC" FOR RANDOM AS #2 LEN = 255
CHDIR "..": CHDIR ad$
OPEN "MONSTERS.DAT" FOR RANDOM AS #3 LEN = 61
OPEN "MONSTERS.DSC" FOR RANDOM AS #4 LEN = 255
PRINT "Reading/Writing monster #";
FOR a = 1 TO nm
   LOCATE CSRLIN, 27: PRINT a; "/"; nm; "   ("; a + m1; "/"; nm + m1; ")";
   GET #1, a, Mon
   GET #2, a, ds
   PUT #3, a + m1, Mon
   PUT #4, a + m1, ds
NEXT: CLOSE : PRINT
'
Art:
CHDIR "..": CHDIR ad1$
OPEN "ARTIFACT.DAT" FOR RANDOM AS #1 LEN = 51
OPEN "ARTIFACT.DSC" FOR RANDOM AS #2 LEN = 255
CHDIR "..": CHDIR ad$
OPEN "ARTIFACT.DAT" FOR RANDOM AS #3 LEN = 51
OPEN "ARTIFACT.DSC" FOR RANDOM AS #4 LEN = 255
PRINT "Reading/Writing artifact #";
FOR a = 1 TO na
   LOCATE CSRLIN, 27: PRINT a; "/"; na; "   ("; a + a1; "/"; na + a1; ")";
   GET #1, a, Art
   GET #2, a, ds
   PUT #3, a + a1, Art
   PUT #4, a + a1, ds
NEXT: CLOSE : PRINT
'
Rooms:
CHDIR "..": CHDIR ad1$
OPEN "ROOMS.DAT" FOR RANDOM AS #1 LEN = 101
OPEN "ROOMS.DSC" FOR RANDOM AS #2 LEN = 255
CHDIR "..": CHDIR ad$
OPEN "ROOMS.DAT" FOR RANDOM AS #3 LEN = 101
OPEN "ROOMS.DSC" FOR RANDOM AS #4 LEN = 255
PRINT "Reading/Writing room#";
FOR a = 1 TO nr
   LOCATE CSRLIN, 27: PRINT a; "/"; nr; "   ("; a + r1; "/"; nr + r1; ")";
   GET #1, a, rm
   GET #2, a, ds
   PUT #3, a + r1, rm
   PUT #4, a + r1, ds
NEXT
CLOSE : PRINT : PRINT "DONE!"

OPEN "NAME.DAT" FOR OUTPUT AS #1
PRINT #1, r1 + nr, a1 + na, e1 + ne, m1 + nm
PRINT #1, ANAME$
PRINT #1, nd, ver, dungeons + 1
IF dungeons = 1 THEN
  LINE INPUT "Name of first adventure:"; a$: a$ = LTRIM$(RTRIM$(a$))
  argh$ = a$: PRINT #1, a$
  PRINT #1, r1: PRINT #1, a1: PRINT #1, e1: PRINT #1, m1: PRINT #1, 1
  PRINT #1, 1: PRINT #1, 1: PRINT #1, 1: PRINT #1, nd
END IF
IF dungeons > 1 THEN
  FOR a = 1 TO dungeons
     PRINT #1, dn$(a)
     FOR m = 1 TO 9: PRINT #1, dn%(a, m): NEXT
  NEXT
END IF
PRINT #1, aname2$: PRINT #1, nr: PRINT #1, na: PRINT #1, ne: PRINT #1, nm
PRINT #1, r1 + 1: PRINT #1, a1 + 1: PRINT #1, e1 + 1: PRINT #1, m1 + 1
PRINT #1, nd2
CLOSE

OPEN "HINTDIR.DAT" FOR INPUT AS #1
CHDIR "..": CHDIR ad1$
OPEN "HINTDIR.DAT" FOR INPUT AS #2
INPUT #1, nh
INPUT #2, nh1
IF nh1 < 2 THEN GOTO Done
nh2 = nh + nh1 - 1
DIM h$(nh2), h%(nh2, 2)

FOR r = 1 TO nh
   INPUT #1, h$(r), h%(r, 1), h%(r, 2)
NEXT
lrec = h%(nh, 1) + h%(nh, 2) - 1

INPUT #2, a$, a, a

' Adjust for EDX 5.0 extra General Help paragraphs
a = ABS(INSTR(a$, "EAMON DELUXE 5.0") <> 0)
argh = 6 + (2 * argh)

FOR r = 1 TO nh1 - 1
   INPUT #2, h$(r + nh), h%(r + nh, 1), h%(r + nh, 2)
   h$(r + nh) = aname2$ + " -- " + h$(r + nh)
NEXT: CLOSE
lrec1 = h%(nh2, 1) + h%(nh2, 2) - 1

FOR r = 1 TO nh1 - 1
   h%(r + nh, 1) = h%(r + nh, 1) + lrec - argh
NEXT

CHDIR "..": CHDIR ad$
OPEN "HINTDIR.DAT" FOR OUTPUT AS #1
PRINT #1, nh2
FOR r = 1 TO nh2
   PRINT #1, h$(r)
   PRINT #1, h%(r, 1), h%(r, 2)
NEXT: CLOSE

OPEN "HINTS.DSC" FOR RANDOM AS #1 LEN = 255
CHDIR "..": CHDIR ad1$
OPEN "HINTS.DSC" FOR RANDOM AS #2 LEN = 255
FOR r = argh + 1 TO lrec1
   GET #2, r, ds
   PUT #1, r + lrec - argh, ds
NEXT
CLOSE

Done:
CLS : PRINT "Adventure sucessfully appended!": PRINT
PRINT aname2$: PRINT
PRINT "NR-"; nr; "  NA-"; na; "  NM-"; nm; "  NE-"; ne: PRINT
PRINT "adj1-"; r1; "  adj2-"; a1; "  adj3-"; m1; "  adj4-"; e1
IF dungeons = 1 THEN
  PRINT : PRINT argh$
  PRINT "NR-"; r1; "  NA-"; a1; "  NM-"; m1; "  NE-"; e1: PRINT
  PRINT "adj1-0  adj2-0  adj3-0  adj4-0"
END IF
PRINT : CHDIR "..": CHDIR "utils": CALL Paws: EXIT SUB
END SUB

SUB NameChange
OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, r1, a1, e1, m1, ANAME$, nd, ver, dungeons
IF dungeons > 1 THEN
  REDIM dn$(dungeons), dn%(dungeons, 9)
  FOR a = 1 TO dungeons
     LINE INPUT #1, dn$(a)
     FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
  NEXT
END IF
CLOSE

CLS
PRINT "Old name: "; ANAME$: PRINT
LINE INPUT "New name: "; ANAME$: ANAME$ = LTRIM$(RTRIM$(ANAME$))

OPEN "NAME.DAT" FOR OUTPUT AS #1
PRINT #1, r1, a1, e1, m1
PRINT #1, CHR$(34); ANAME$; CHR$(34)
PRINT #1, nd, ver, dungeons
IF dungeons > 1 THEN
  FOR a = 1 TO dungeons
     PRINT #1, dn$(a)
     FOR m = 1 TO 9: PRINT #1, dn%(a, m): NEXT
  NEXT
END IF
CLOSE : CHDIR ".."
OPEN "ADVENTRS" FOR RANDOM AS #2 LEN = 90
   GET #2, cx + 1, adv
   adv.ANAME = ANAME$
   PUT #2, cx + 1, adv
CLOSE

CHDIR "utils": CALL Paws: RUN
END SUB

SUB NameChangeM

OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, nr, na, ne, nm, aname2$, nd2, a, dungeons1
CLOSE
IF dungeons1 < 2 THEN
  CHDIR "..": CHDIR "utils"
  PRINT "This isn't a multi-adventure."
  CALL Paws: RUN
END IF

c = 0: ad1$ = a$: a$ = ""
CALL EDX.Select.Multi.Adventure(c, a$, p$)
ad1 = c

OPEN "NAME.DAT" FOR INPUT AS #1
INPUT #1, r1, a1, e1, m1, ANAME$, nd, ver, dungeons
IF dungeons > 1 THEN
  REDIM dn$(dungeons), dn%(dungeons, 9)
  FOR a = 1 TO dungeons
     LINE INPUT #1, dn$(a)
     FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
  NEXT
END IF
CLOSE

CLS
PRINT "Old name: "; dn$(ad1): PRINT
LINE INPUT "New name: "; dn$(ad1): dn$(ad1) = LTRIM$(RTRIM$(dn$(ad1)))

OPEN "NAME.DAT" FOR OUTPUT AS #1
PRINT #1, r1, a1, e1, m1
PRINT #1, CHR$(34); ANAME$; CHR$(34)
PRINT #1, nd, ver, dungeons
IF dungeons > 1 THEN
  FOR a = 1 TO dungeons
     PRINT #1, CHR$(34); dn$(a); CHR$(34)
     FOR m = 1 TO 9: PRINT #1, dn%(a, m): NEXT
  NEXT
END IF
CLOSE : CHDIR "..": CHDIR "utils": CALL Paws: RUN
END SUB

SUB Paws
PRINT : PRINT "[Hit any key to continue]": a$ = INKEY$
DO
LOOP UNTIL INKEY$ <> ""
END SUB

