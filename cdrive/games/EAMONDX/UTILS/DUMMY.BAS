'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�                                                                �
'� UNFINISHED BETA RELEASE PLACEHOLDER       Update: 12 JAN 2012  �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�
'
' Copyright (C) 2013 Frank Black Productions
'
' This program is free software; you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation; either version 2 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program; if not, write to the Free Software
' Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
'

DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)
DECLARE SUB EDX.Select.Adventure (c, ad$, p$)
DECLARE SUB EDX.Select.Multi.Adventure (c, aname$, p$)
DECLARE SUB Paws ()

TYPE adv
   aname AS STRING * 78: NUMBER AS INTEGER
END TYPE
DIM SHARED adv AS adv

DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

DIM SHARED r1, a1, e1, m1, aname$, nd, ver, dungeons, ad$, advnum

DIM SHARED dn$, p$, em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0)
FOR x = 1 TO em0: READ em$(x): NEXT
em$(0) = CHR$(13) + CHR$(32) + CHR$(27)  '(Enter, Space, Escape)

Menu:
CLS : COLOR 6, 8: PRINT CHR$(218); STRING$(74, 196); CHR$(191)
PRINT CHR$(179); SPACE$(16); "EAMON DELUXE v5.0 DUNGEON DESIGN UTILITIES";
PRINT SPACE$(16); CHR$(179): PRINT CHR$(192); STRING$(74, 196) + CHR$(217)
COLOR 7
PRINT "  (Beta Copy of Eamon Deluxe 5.0 - This program is still under development)"

PRINT : c = 7: c1 = 10
CALL EDXMenuM(c, c1, 1, 2, 1, 2)
IF c < 1 OR c > 1 OR c1 = 3 THEN RUN "UMENU"
GOTO Menu

' Load Adventure Names
xc = c: c = 3: ad$ = "DD"
p$ = "Select the adventure you wish to change the name of:"
CALL EDX.Select.Adventure(c, ad$, p$)
IF ad$ = "" OR c = 0 THEN GOTO Menu
ad$ = "..\" + ad$ + "\": advnum = c
' Get Adventure Data and branch accordingly
OPEN ad$ + "NAME.DAT" FOR INPUT AS #1
    INPUT #1, r1, a1, e1, m1, aname$, nd, ver, dungeons
    IF dungeons > 1 THEN
      DIM SHARED dn$(dungeons), dn%(dungeons, 9)
      FOR a = 1 TO dungeons
         LINE INPUT #1, dn$(a): FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
      NEXT
    END IF
CLOSE

EDXMenuStrings:
DATA 2
DATA "  Do Nothing  "
DATA "  Quit  "

SUB EDX.Select.Adventure (c, ad$, p$)
'/// Eamon Deluxe Adventure Selection
' (Updated for VI Mode support 11 JAN 2012)
'   Codes passed to SUB:
'    c=0 - Start from EAMONDX directory
'    c=1 - Back up one directory before starting
'    c=2 - Start from EDX, load designs names also
'    c=3 - Back up one, load designs
'     ad$ - Name of directory to return to, if ad$ = "" then no return is made
'     p$ - Message to display
'
'   Codes returned from SUB:
'      c - Number of adventure selected (0=None, -1=Design 1, -2=Design 2)
'     ad$ - Name of adventure directory
'
'   Notes:
'     Requires the data type "adv" to be in the main program and
'     it must be dimensioned as SHARED
'
'            Load adventure names
IF c = 1 OR c = 3 THEN CHDIR ".."
OPEN "ADVENTRS" FOR RANDOM AS #2 LEN = 90
   GET #2, 1, adv: numadv = adv.NUMBER: numadv1 = numadv
   REDIM advn$(numadv + 2)
   FOR x = 2 TO numadv + 1
      GET #2, x, adv
      advn$(x - 1) = LEFT$(LTRIM$(RTRIM$(adv.aname$)), 38)
   NEXT
CLOSE
'            Load design names
IF c = 2 OR c = 3 THEN
  CHDIR "dd"
    OPEN "DGNDIR.DAT" FOR INPUT AS #1: INPUT #1, d1, d2: CLOSE
  CHDIR ".."
  IF d1 = 1 THEN
    CHDIR "DGN-1"
      OPEN "NAME.DAT" FOR INPUT AS #1: INPUT #1, x, x, x, x, k$: CLOSE
      numadv = numadv + 1: advn$(numadv) = LEFT$(k$, 26) + "  (Design 1)"
    CHDIR ".."
  END IF
  IF d2 = 1 THEN
    CHDIR "DGN-2"
      OPEN "NAME.DAT" FOR INPUT AS #1: INPUT #1, x, x, x, x, k$: CLOSE
      numadv = numadv + 1: advn$(numadv) = LEFT$(k$, 26) + "  (Design 2)"
    CHDIR ".."
  END IF
END IF
'         Return to current directory
IF (c = 1 OR c = 3) AND ad$ <> "" THEN CHDIR ad$

' VI Mode Numeric Menu
GetAdvCVI:
IF s%(4) = 1 THEN
  CLS : L = 0: PRINT "Please select an adventure:"
  FOR c = 1 TO numadv
     vi$ = LTRIM$(RTRIM$(STR$(c))) + ": " + advn$(c)
     IF c < 100 THEN vi$ = " " + vi$: IF c < 10 THEN vi$ = " " + vi$
     PRINT vi$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       L = 0
     END IF
  NEXT
  PRINT : LINE INPUT "Enter the number of your choice (0 to Exit):"; vi$
  c = INT(VAL(vi$))
  IF c < 0 OR c > numadv THEN CLS : GOTO GetAdvCVI
  IF c = 0 THEN RUN
  k = 2: GOTO SelectADV2
END IF

' Standard Menu
keyz$ = CHR$(27) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

SelectADVa:
GOSUB SelectADV0

SelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB SelectADV0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0
LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
SelectADV2:
IF k < 3 THEN
  k$ = LTRIM$(RTRIM$(STR$(c))): ad$ = LEFT$("E000", 4 - LEN(k$)) + k$
  IF c = numadv1 + 2 OR (c = numadv1 + 1 AND d1 = 0) THEN ad$ = "DGN-2": c = -2
  IF c = numadv1 + 1 THEN ad$ = "DGN-1": c = -1
  IF k = 1 THEN c = 0: ad$ = ""
  VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO SelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO SelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO SelectADV

'       Set up screen
SelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>    <Escape=Exit>    <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
LOCATE 2, 1: COLOR 2, 0: PRINT p$; : COLOR 3
'       Display choices
'SelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDX.Select.Multi.Adventure (c, aname$, p$)
'/// Eamon Deluxe Multi-Adventure Selection
' (Updated 09 JAN 12 with VI Mode alternate numeric menu)

IF dungeons = 1 THEN c = 1: aname$ = aname$: EXIT SUB

' Load adventure names
REDIM advn$(dungeons)
FOR x = 1 TO dungeons
   advn$(x) = LEFT$(LTRIM$(RTRIM$(dn$(x))), 38)
NEXT
numadv = dungeons

' VI Mode numeric menu
GetMAdvCVI:
IF s%(4) = 1 THEN
  CLS : L = 2: PRINT "Please select an adventure:"
  FOR c = 1 TO dungeons
     vi$ = LTRIM$(RTRIM$(STR$(c))) + ": " + dn$(c)
     IF c < 10 THEN vi$ = " " + vi$
     IF c < 100 THEN vi$ = " " + vi$
     PRINT vi$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       L = 0
     END IF
  NEXT
  PRINT : LINE INPUT "Enter the number of your choice (0 to Exit):"; vi$
  c = INT(VAL(vi$))
  IF c < 0 OR c > dungeons THEN CLS : GOTO GetMAdvCVI
  IF c = 0 THEN RUN
  aname$ = dn$(c): EXIT SUB
END IF

' Standard Menu
keyz$ = CHR$(27) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

mSelectADVa:
GOSUB mSelectADV0

mSelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB mSelectADV0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
IF k < 3 THEN
  aname$ = dn$(c): VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO mSelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO mSelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO mSelectADV

'       Set up screen
mSelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>    <Escape=Exit>    <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
'       Display choices
mSelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
' // Compact version of standard Eamon Deluxe selection menu
'      Allows only a single row of choices and has no error checking
'
'  Updated with VI Mode alternate numeric menu: 12 JAN 2012
'
'  Arguments passed to EDXMenuM:
'     c - Vertical cursor position to begin at (0=CSRLIN)
'    c1 - Horizontal cursor position to begin at  (0=POS)
'    be - Location in em$ array to begin at
'    en - Location in em$ array to end
'   tc1 - Text foreground color of menu
'   tc2 - Text background color of menu
'
' Values returned by EDXMenuM:
'     c - The number of the choice relative to the amount of choices
'    c1 - This holds the exit key code (Standard:1=Enter,2=Space,3=Escape)
'    en - The actual array number of em$() that was chosen
'
'  Required SHARED variables:        (same as EDXMenu)
'
'   em$() - An array which holds all available choices.  em$(0) must
'           contain exit keys
'    em0 - The maximum dimension of the em$() array.
'
'   Upon exit, the cursor will locate to the row just beneath the last
'   choice (or 25 if it exceeds screen), at column 1

' VI Mode numeric menu
EDXMenuMCVI:
IF s%(4) = 1 THEN
  L = 2
  FOR x = be TO en
     x$ = LTRIM$(STR$(x - be + 1)) + ": " + LTRIM$(RTRIM$(em$(x)))
     IF x < 100 THEN x$ = " " + x$: IF x < 10 THEN x$ = " " + x$
     PRINT x$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       CLS : L = 0
     END IF
  NEXT
  PRINT : PRINT "Enter the number of your choice (0 to exit):";
  LINE INPUT ""; k$: c = INT(VAL(k$)): c1 = c + (be - 1)
  IF c = 0 THEN c1 = 3: EXIT SUB
  IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMCVI
  en = c1: c1 = 1: EXIT SUB
END IF

' Standard Menu
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, 0
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT

EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1
LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h: COLOR tc1, 0: PRINT em$(c);

IF LEN(k$) > 1 THEN
  IF k = 71 THEN c = be
  IF k = 79 THEN c = en
  IF k = 72 OR k = 75 THEN c = c - 1
  IF k = 80 OR k = 77 THEN c = c + 1
  GOTO EDXMenuM
END IF

IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB
END SUB

SUB Paws
PRINT : PRINT "[Hit any key to continue]"; : a$ = INKEY$
DO
LOOP UNTIL INKEY$ <> ""
END SUB

