'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                   by Frank Black Productions                   �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�    THE FRONT DESK AND                5.0 Revision: 29 FEB 2012 �
'�  NEW CHARACTERS ROUTINES           Final Revision: 03 JUL 2012 �
'�                                            Update: 27 NOV 2012 �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�
'
' Copyright (C) 2013 Frank Black Productions
'
' This program is free software; you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation; either version 2 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program; if not, write to the Free Software
' Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
'
DECLARE SUB EDX.Select.Char (c)

TYPE RECORD
CNAME AS STRING * 40: STATUS AS INTEGER: A1 AS INTEGER: A2 AS INTEGER
A3 AS INTEGER: SP1 AS INTEGER: SP2 AS INTEGER: SP3 AS INTEGER
SP4 AS INTEGER: WA1 AS INTEGER: WA2 AS INTEGER: WA3 AS INTEGER
WA4 AS INTEGER: WA5 AS INTEGER: AE AS INTEGER: SEX AS INTEGER
GOLD AS LONG: BANK AS LONG: AC AS INTEGER: WN1 AS STRING * 20
WN2 AS STRING * 20: WN3 AS STRING * 20: WN4 AS STRING * 20
WCOMPLX1 AS INTEGER: WCOMPLX2 AS INTEGER: WCOMPLX3 AS INTEGER
WCOMPLX4 AS INTEGER: WTYPE1 AS INTEGER: WTYPE2 AS INTEGER: WTYPE3 AS INTEGER
WTYPE4 AS INTEGER: WDICE1 AS INTEGER: WDICE2 AS INTEGER: WDICE3 AS INTEGER
WDICE4 AS INTEGER: WSIDES1 AS INTEGER: WSIDES2 AS INTEGER: WSIDES3 AS INTEGER
WSIDES4 AS INTEGER
END TYPE
DIM SHARED P AS RECORD, numchars

DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

READ nc: DIM comment$(nc): GOSUB ReadComs: q$ = CHR$(34)
OPEN "NUMPLRS.DAT" FOR INPUT AS #1: INPUT #1, numchars: CLOSE
DIM SHARED n$(numchars + 1), stat(numchars + 1)
OPEN "PLAYERS.DAT" FOR RANDOM AS #1 LEN = 200
FOR a = 1 TO numchars
 GET #1, a, P: n$(a) = LTRIM$(RTRIM$(P.CNAME)): stat(a) = P.STATUS
NEXT
CLOSE
'               Title
'
SCREEN 0: CLS : RANDOMIZE TIMER
LOCATE 2: COLOR 14: PRINT SPACE$(28); "WELCOME TO THE": PRINT
PRINT SPACE$(13); "EAMON DELUXE COMPUTERIZED FANTASY GAMING SYSTEM"
PRINT : PRINT : COLOR 3
'COLOR 7: PRINT "Contact: eamondeluxe@gmail.com"
PRINT "You are in the outer chamber of the hall of the Guild of Free Adventurers."
PRINT "Many men and women are guzzling beer and there is loud singing and laughter."
PRINT
PRINT "On the north side of the chamber is a cubbyhole with a desk. Over the desk is"
PRINT "a sign which says: "; : COLOR 2: PRINT "REGISTER HERE OR ELSE!"
COLOR 3: PRINT : PRINT
PRINT "Do you go over to the desk or join the men drinking beer?": PRINT

Choice:
PRINT " (Hit "; q$; "D"; q$; " for desk or "; q$; "M"; q$; " for men.):";
GOSUB GetKey: PRINT : IF a$ <> "D" AND a$ <> "M" THEN GOTO Choice
PRINT

IF a$ = "M" THEN
  COLOR 2: PRINT "As you go over to the men, you feel a sword being thrust through your back and"
  PRINT "you hear someone say, "; q$; "You really must learn to follow directions!"; q$
  PRINT : COLOR 3: GOSUB KeyWait: CHDIR "..": RUN "EAMONDX"
END IF
'
'     That ol' finger-eater...
'
PRINT "You are greeted there by a burly Irishman who looks at you with a scowl and"
PRINT "and asks you, "; q$; "What's yer name?"; q$: PRINT : GOSUB KeyWait

GetName:
CALL EDX.Select.Char(m)
COLOR 3: IF m = 0 THEN GOTO NewCharacters

IF stat(m) = 2 THEN
  PRINT "The Burly Irishman gets a sad look in his eyes and says, "; q$; "That character"
  PRINT "be dead.  You'll have to pick another."; q$: PRINT
  GOSUB KeyWait: m = 0: GOTO GetName
END IF

IF stat(m) = 3 THEN
  COLOR 14: PRINT "This character is off on an adventure right now...": PRINT
  COLOR 3: PRINT "If you wish, you can hire a wizard to return this character from the adventure"
  PRINT "so that you can use them again. (Warning: Any saved games for that adventure"
  PRINT "will be deleted!)": PRINT : COLOR 2
  PRINT "Do you want to do this? (Y/N)"; : GOSUB GetKey
  IF a$ <> "Y" THEN m = 0: GOTO GetName
  '
  '      Snatch
  ON ERROR GOTO SearchError
    CLS : COLOR 2: PRINT "The on-call wizard tonight is a creepy little fellow with a gnarled form. He"
    PRINT "rubs his eyes, still half asleep, and starts muttering an incantation..."
    PRINT : COLOR 7: PRINT "Searching: ": COLOR 7: f = 0: x = 0: p1$ = ""
   DO
     CLOSE : x = x + 1: c$ = LTRIM$(RTRIM$(STR$(x)))
     IF LEN(c$) < 3 THEN c$ = "0" + c$: IF LEN(c$) < 3 THEN c$ = "0" + c$
     c$ = "..\e" + c$: a$ = c$ + "\NEW-MEAT"
     IF s%(4) <> 1 THEN
       LOCATE CSRLIN - 1, 12: PRINT SPACE$(20); : LOCATE CSRLIN, 12
     END IF
     PRINT a$: IF s%(4) <> 1 THEN FOR x# = 1 TO dly# * 2: NEXT
     OPEN a$ FOR INPUT AS #1
     CLOSE #1
     IF ProgErr = 0 THEN
       OPEN a$ FOR RANDOM AS #1 LEN = 200: GET #1, 1, P: CLOSE #1
       IF LCASE$(LTRIM$(RTRIM$(P.CNAME))) = LCASE$(n$(m)) THEN p1$ = c$: f = 1
     END IF
     IF ProgErr > 0 AND ProgErr <> 53 THEN f = 2:  ELSE ProgErr = 0
   LOOP UNTIL f > 0 'OR p1$ <> ""
  ON ERROR GOTO 0
    ProgErr = 0: c$ = p1$
    IF p1$ = "" THEN
      COLOR 14: PRINT "Error.": PRINT : COLOR 3
      PRINT "Couldn't find character.": PRINT : COLOR 3: GOSUB KeyWait: RUN
    END IF
    OPEN c$ + "\GAMEDIR.DAT" FOR OUTPUT AS #1: PRINT #1, -1
    FOR a = 1 TO 5: PRINT #1, "(none)": NEXT: CLOSE #1
    FOR a = 1 TO 5
        a$ = "\GAME" + LTRIM$(RTRIM$(STR$(a))) + ".SAV"
       OPEN c$ + a$ FOR OUTPUT AS #1: PRINT #1, 1: CLOSE #1: KILL c$ + a$
    NEXT
    KILL c$ + "\NEW-MEAT": PRINT : PRINT : COLOR 14
    PRINT "In a smoking explosion of fire and brimstone, your character suddenly appears"
    PRINT "before the Irishman's desk, still half-crouched in a combat stance..."
    COLOR 2: PRINT : PRINT n$(m); " looks around, startled!": PRINT : COLOR 3
    wizard = 100: stat(m) = 1
END IF

'    Head on into the Main Hall...
'
CLOSE
OPEN "PLAYERS.DAT" FOR RANDOM AS #1 LEN = 200
    GET #1, m, P: P.STATUS = 1
    IF wizard > 0 THEN
      IF P.GOLD < wizard THEN
        PRINT "You don't have enough to pay the wizard! Uh-oh, he says he'll get even soon..."
      END IF
      IF P.GOLD >= wizard THEN
        PRINT "The on-call wizard charges you 100 gold coins for after hours service!"
        P.GOLD = P.GOLD - wizard: wizard = 0
      END IF
      COLOR 2: PRINT
      PRINT "Finally, the Burly Irishman slaps your confused character a few times until he"
      PRINT "hears a name he can look for...": COLOR 3
    END IF
    PUT #1, m, P
CLOSE
OPEN "PLAYER" FOR OUTPUT AS #2: WRITE #2, m: CLOSE

r% = (RND * nc) + 1: IF r% > nc THEN r% = nc
PRINT "He starts looking through his book, while muttering something about"
PRINT comment$(r%): PRINT
PRINT "Finally he looks up and says, "; q$; "Ah, here ye be.  Well, go and have"
PRINT "fun in the hall."; q$
PRINT : PRINT "Hit any key to enter the Main Hall:"; : GOSUB GetKey
RUN "MAINHALL"
'---------------------------------------------------------------------------
' THE WONDERFUL WORLD OF EAMON DELUXE
' CHARACTER GENERATOR AND INSTRUCTIONS
'
'    BY FRANK BLACK, DEC 1995
'
NewCharacters:
PRINT : PRINT "He hits his forehead and says, "; q$; "Ah, ye must be new here!  Well, wait just"
PRINT "a minute and I'll bring someone out to take care of ye."; q$
PRINT : GOSUB KeyWait

NewChars:
CLS : LOCATE 8: COLOR 2
LINE INPUT "Name of character:"; a$: PRINT : COLOR 3
Check:
a$ = LTRIM$(RTRIM$(LEFT$(a$, 40))): f = 0
IF LEN(a$) < 1 THEN GOTO NewChars
FOR a = 1 TO LEN(a$)
 k = ASC(MID$(a$, a, 1))
 IF k = 44 OR k = 58 OR k = 34 THEN
   a$ = LEFT$(a$, a - 1) + MID$(a$, a + 1)
   GOTO Check
 END IF
NEXT
FOR a = 1 TO numchars
   IF LCASE$(n$(a)) = LCASE$(a$) THEN f = 1
NEXT
IF f = 1 THEN
  PRINT q$; "Ye can't be impersonating anybody! Use yer real name!"; q$
  PRINT : PRINT "(You already have a character with that name.)": PRINT
  GOSUB KeyWait: GOTO NewChars
END IF
n$ = a$: PRINT

PRINT "The Irishman says, "; q$; "First I must know whether ye be male or female."
PRINT "Which are ye?"; q$
PRINT : PRINT "(Hit "; q$; "M"; q$; " for male or "; q$; "F"; q$; " for female:";
GOSUB GetKey: IF a$ <> "M" AND a$ <> "F" THEN GOTO NewChars
'
' Setup basic data
'
P.CNAME = n$: P.STATUS = 1: P.SEX = ABS(a$ = "F")
P.SP1 = 0: P.SP2 = 0: P.SP3 = 0: P.SP4 = 0
P.WA1 = 5: P.WA2 = -10: P.WA3 = 20: P.WA4 = 10: P.WA5 = 0
P.AE = 0: P.GOLD = 200: P.BANK = 0: P.AC = 0
P.WN1 = "NONE": P.WN2 = "NONE": P.WN3 = "NONE": P.WN4 = "NONE"
P.WCOMPLX1 = 0: P.WCOMPLX2 = 0: P.WCOMPLX3 = 0: P.WCOMPLX4 = 0
P.WTYPE1 = 0: P.WTYPE2 = 0: P.WTYPE3 = 0: P.WTYPE4 = 0
P.WDICE1 = 0: P.WDICE2 = 0: P.WDICE3 = 0: P.WDICE4 = 0
P.WSIDES1 = 0: P.WSIDES2 = 0: P.WSIDES3 = 0: P.WSIDES4 = 0
'
' Set prime attributes
'
SetPrime:
GOSUB RollDie: HD = x: GOSUB RollDie: AG = x: GOSUB RollDie: CH = x
IF HD < 15 OR AG < 12 OR HD + AG + CH < 42 THEN GOTO SetPrime
P.A1 = HD: P.A2 = AG: P.A3 = CH
'
' Put new character in file
'
numchars = numchars + 1
OPEN "PLAYERS.DAT" FOR RANDOM AS #1 LEN = 200: PUT #1, numchars, P: CLOSE
OPEN "NUMPLRS.DAT" FOR OUTPUT AS #2: WRITE #2, numchars: CLOSE
OPEN "PLAYER" FOR OUTPUT AS #2: WRITE #2, numchars
'
' Info
'
CLS : COLOR 3
PRINT "The Irishman walks away and in walks a man of possibly Elfish descent."
PRINT
PRINT "   He studies you for a moment and says, "; q$; "Here is a booklet of instructions"
PRINT "for you to read, and your prime attributes are--": PRINT
PRINT SPC(20); "Hardiness:"; : COLOR 10: PRINT HD: COLOR 3
PRINT SPC(20); "Agility:  "; : COLOR 10: PRINT AG: COLOR 3
a$ = LTRIM$(STR$(CH))
PRINT SPC(20); "Charisma:  "; : COLOR 10: PRINT a$; : COLOR 3: PRINT q$: PRINT
PRINT "   You read the instructions and they say--": PRINT
GOSUB KeyWait

GOSUB Title
PRINT "You will have to buy a weapon.  Your chance to hit with it will be determined"
PRINT "by the weapon complexity, your ability in that weapon class, how heavy your"
PRINT "armor is, and the difference in agility between you and your enemy."
PRINT
PRINT "The five classes of weapons (and your current abilities with each) are--"
PRINT SPC(22); "Club/Mace......20%"
PRINT SPC(22); "Spear..........10%"
PRINT SPC(22); "Axe.............5%"
PRINT SPC(22); "Sword...........0%"
PRINT SPC(22); "Bow...........-10%": PRINT
PRINT "Every time you score a hit in battle, your ability in the weapon class may go up"
PRINT "by 2%, if a random number from 1-100 is less than your chance to miss."
LOCATE 23: GOSUB KeyWait: GOSUB Title

PRINT "There are four armor types and you may also carry a shield.  These protections"
PRINT "will absorb hits placed upon you (almost always!) but they lower your chance to"
PRINT "hit.  The protections are--": PRINT
PRINT "Armor               Hits Absorbed     Odds Adjustment"
PRINT " None .....................0 .................0%"
PRINT " Leather ..................1 ...............-10%"
PRINT " Chain ....................3 ...............-20%"
PRINT " Plate ....................5 ...............-60%"
PRINT " Shield ...................1 ................-5%": PRINT
PRINT "You will develop an Armor Expertise, which will go up when you hit a blow"
PRINT "wearing armor and your expertise is less than the armor you are wearing."
PRINT "No matter how high your Armor Expertise is, however, the net effect of"
PRINT "armor will never increase your chance to hit."
LOCATE 23: GOSUB KeyWait: GOSUB Title

a$ = LTRIM$(STR$(HD * 10))
PRINT "You can carry weight up to ten times your hardiness, or, "; a$; " Gronds.  (A"
PRINT "measure of weight, one Grond = 10 Dos.)": PRINT
PRINT "Additionally, your hardiness tells how many points of damage you can survive."
a$ = LTRIM$(STR$(HD))
PRINT "Therefore, you can be hit with "; a$; " 1-point blows before you die."
PRINT
PRINT "You will not be told how many blows you have taken.  You will be merely told"
PRINT "such things as--": PRINT
PRINT SPC(3); q$; "Wow!  That one hurt!"; q$
PRINT "or "; q$; "You don't feel very well."; q$: PRINT
a$ = LTRIM$(STR$(CH))
PRINT "Your charisma ("; a$; ") affects how the citizens of Eamon react to you.  You affect"
PRINT "a monster's friendliness rating by your charisma less ten, difference times"
a$ = LTRIM$(STR$((CH - 10) * 2))
PRINT "two ("; a$; "%).": PRINT
PRINT "You start off with 200 gold pieces, which you will want to spend on supplies"
PRINT "for your first adventure."
LOCATE 23: GOSUB KeyWait: GOSUB Title

PRINT "After you begin to accumulate wealth, you may want to put some of your money"
PRINT "into the bank, where it cannot be stolen.  However it is a good idea to always"
PRINT "carry some gold with you for use in bargaining and ransom situations."
PRINT
PRINT "You should also hire a Wizard to teach you some magic spells.  There are four"
PRINT "spells you can learn--": PRINT
PRINT "Blast: Throw a magical blast at your enemies to inflict damage."
PRINT "Heal:  Remove damage from your body."
PRINT "Speed: Double your agility for a short time."
PRINT "Power: The exact effect of this spell is unpredictable and is different in"
PRINT "        each adventure.": PRINT
PRINT "Other types of spells may be available in various adventures, and items may"
PRINT "have special properties.  However, these will only work in the adventure where"
PRINT "they were found.  Thus it is best (and you have no choice but to) sell all"
PRINT "items found in adventures except for weapons and armor."
LOCATE 23: GOSUB KeyWait: CLS

PRINT "The man behind the desk takes back the instructions and says, "; q$; "It is now time"
PRINT "for you to start your life."; q$; "  He makes an odd sign with his hand and says, "; q$; "Live"
PRINT "long and prosper."; q$: PRINT
PRINT "You now wander into the Main Hall..."
LOCATE 23: GOSUB KeyWait: CLS
RUN "MAINHALL"
'
' Roll dice subroutine (3D8)
'
RollDie:
x = 0: FOR a = 1 TO 3: x = x + INT(RND * 7) + 1: NEXT: RETURN
'
' Titlebar subroutine
'
Title:
CLS : COLOR 0, 14
PRINT SPC(22); "Information about the world of Eamon"; SPC(22);
COLOR 3, 0: PRINT : PRINT : RETURN
'
' Alt. entry for GetKey
'
KeyWait:
PRINT "Hit any key to continue...";
'
' Get one keypress Subroutine
'
GetKey:
' VI Mode support added 27 JAN 2012
IF s%(4) = 1 THEN
  DO
    a = RND: a$ = UCASE$(INKEY$): xx = RND
  LOOP UNTIL a$ <> ""
  PRINT a$: RETURN
END IF
' Standard Routine
DO
   a = RND
   BS = POS(0)
   CC = CC + 1
   IF CC = CC1# THEN PRINT "/"; : LOCATE CSRLIN, BS
   IF CC = CC2# THEN PRINT CHR$(179); : LOCATE CSRLIN, BS
   IF CC = CC3# THEN PRINT "\"; : LOCATE CSRLIN, BS: CC = 0
   a$ = UCASE$(INKEY$): xx = RND
LOOP UNTIL a$ <> ""
PRINT a$: RETURN
'
'        The Burly Irishman's random comments
'       --------------------------------------
ReadComs:
FOR a = 1 TO nc: READ comment$(a): NEXT: RETURN

SearchError:
ProgErr = ERR: RESUME NEXT

DATA 19
DATA "being pulled away from the bar to look up names."
DATA "having seen ugly faces before but never anything like this."
DATA "the idiot on the other shift with the lousy handwriting."
DATA "how the days are so long since he quit adventuring."
DATA "how he was once young and foolish too."
DATA "weird young adventurers with even weirder names."
DATA "young greenhorns that think they know it all."
DATA "taking the barmaid out after her shift."
DATA "leaving this job to start a dungeon."
DATA "how he should have listened to his mother and been a brain surgeon."
DATA "how good fer nothin' young adventurers should be taken out an hung."
DATA "strange Saxon names ye can never pronounce, let alone spell."
DATA "how he should have listened to his mother and gone into taxidermy."
DATA "having to computerize this damn book business sometime."
DATA "how the know-it-all attitudes of the new adventurers will do them in."
DATA "how if he gets the chance, he's gonna get out of this rat race."
DATA "leaving this place and running off with the bar wench someday."
DATA "young adventurers never using their rightful names anyway."
'            Eamon Deluxe original
DATA "the Main Hall on the Apple II paying better wages."

SUB EDX.Select.Char (c)
'/// Eamon Deluxe Character Selection
'
' (Updated for VI Mode support 06 JAN 2012)
'
'   Codes returned from SUB:
'      c - Number of character selected (0=None)
'
'  Notes:
'   Requires number of characters to be in SHARED variable "numchars"
'   and n$() and stat() to be SHARED arrays which contain character names
'   and status.
'
'
' V.I. Mode numeric menu
GetCharCVI:
IF s%(4) = 1 THEN
  CLS : COLOR 7: L = 3: PRINT "Please select a character:": PRINT
  FOR c = 1 TO numchars
     PRINT LTRIM$(RTRIM$(STR$(c))) + ": " + n$(c): L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       L = 0
     END IF
  NEXT
  PRINT LTRIM$(RTRIM$(STR$(c))) + ": Create a brand new character"
  PRINT : LINE INPUT "Enter the number of your choice (0 to Exit):"; k$
  c = INT(VAL(k$))
  IF c < 0 OR c > numchars + 1 THEN CLS : GOTO GetCharCVI
  IF c = numchars + 1 THEN c = 0: EXIT SUB
  IF c = 0 THEN RUN
  EXIT SUB
END IF

' Standard Menu
keyz$ = CHR$(27) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
be = 1: en = numchars
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

SelectCHRa:
GOSUB SelectCHR0

SelectCHR:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT n$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB SelectCHR0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT n$(c);

'         Set arguments and exit
IF k < 3 THEN
  IF k = 1 THEN c = 0
  VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO SelectCHRa
IF k = 4 AND en < numchars THEN be = be + 42: GOTO SelectCHRa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO SelectCHR

'       Set up screen
SelectCHR0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "   <Enter=Accept>   <Escape=New Character>   <Page Up/Page Down=More Options>   "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
'       Display choices
SelectCHR1:
IF be < 1 THEN be = 1
IF be > numchars THEN be = numchars
en = be + 41: IF en > numchars THEN en = numchars
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT n$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

