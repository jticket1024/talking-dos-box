'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�    The Jim Jacobson Adventures Deluxe Visitors File Utility    �
'�                                                                �
'� By Frank Black                         Revision 2: 25 JUN 2012 �
'�                                            Update: 25 JUN 2012 �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�

DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)
ON ERROR GOTO ErrHandler

TYPE visitors
   pname AS STRING * 40: psex AS STRING * 3: v0 AS INTEGER: v1 AS INTEGER
   v2 AS INTEGER: v3 AS INTEGER: v4 AS INTEGER: v5 AS INTEGER: v6 AS INTEGER
   v7 AS INTEGER: v8 AS INTEGER: v9 AS INTEGER: v10 AS INTEGER: v11 AS INTEGER
   v12 AS INTEGER: v13 AS INTEGER: v14 AS INTEGER: v15 AS INTEGER
   v16 AS INTEGER: v17 AS INTEGER: v18 AS INTEGER: v19 AS INTEGER
   v20 AS INTEGER: v21 AS INTEGER: v22 AS INTEGER: v23 AS INTEGER
   v24 AS INTEGER: v25 AS INTEGER: v26 AS INTEGER
END TYPE
DIM SHARED visit AS visitors

DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10), BORT
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

OPEN "VISITORS.DAT" FOR RANDOM AS #1 LEN = 97
GET #1, 1, visit: nv = visit.v26

'nv = 100

DIM SHARED em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0 + nv)
FOR x = 1 TO em0: READ em$(x): NEXT
em$(0) = CHR$(13) + CHR$(32) + CHR$(27) '(Enter, Space, Escape)
FOR x = 1 TO nv: GET #1, x, visit: em$(em0 + x) = RTRIM$(visit.pname): NEXT

'FOR x = 1 TO nv: em$(em0 + x) = "Player" + STR$(x): NEXT

Menu:
GOSUB Header: LOCATE 7: COLOR 2
PRINT "Number of visitors: "; LTRIM$(STR$(nv)): PRINT : PRINT
IF nv = 0 THEN
  COLOR 3: PRINT "Hit any key to exit..."; : GOSUB KeyWait3: RUN "INTRO"
END IF

c = 0: c1 = 15
CALL EDXMenuM(c, c1, 1, 3, 7, 0)
IF c1 = 3 THEN RUN "INTRO"
GOSUB Header
' Zap the file!
IF c = 3 THEN
  LOCATE 8: COLOR 2
  PRINT "Are you sure you want to erase all visitor data? (Y/N):";
  GOSUB KeyWait3: PRINT : PRINT : IF k$ <> "Y" THEN GOTO Menu
  CLOSE : KILL "VISITORS.DAT"
  OPEN "VISITORS.DAT" FOR RANDOM AS #1 LEN = 97
  GET #1, 1, visit: visit.pname = "Nobody": visit.psex = "he": PUT #1, 1, visit: CLOSE
  COLOR 7: PRINT "Erasing visitors file..."; : COLOR 15: PRINT "DONE!": GOSUB KeyWait
  RUN "INTRO"
END IF
' List the roster...
IF c = 1 THEN
  COLOR 10: PRINT "Visitors on file --": PRINT : L = 6: COLOR 7
  FOR m = 1 TO nv
     PRINT LTRIM$(STR$(m)); ": "; em$(em0 + m): L = L + 1
     IF L > 18 THEN GOSUB KeyWait: GOSUB Header: L = 5: COLOR 7
  NEXT
  IF L > 5 THEN GOSUB KeyWait
  GOTO Menu
END IF

Menu1:
LOCATE 7: COLOR 2: PRINT "View history on which visitor?": PRINT
' (Use VI Mode for selecting from long list)
c = 0: c1 = 15: x1 = s%(4): IF nv > 15 THEN s%(4) = 1
CALL EDXMenuM(c, c1, em0 + 1, em0 + nv, 7, 0)
s%(4) = x1: k = c: IF c1 = 3 THEN GOTO Menu
GET #1, k, visit
'
'  Cave of the Mind
'
IF visit.v21 > 0 THEN
  GOSUB Header: LOCATE 7: COLOR 2
  PRINT RTRIM$(visit.pname); " has played the Cave of the Mind"; visit.v21; "time";
  IF visit.v21 > 1 THEN PRINT "s";
  PRINT ".": PRINT : PRINT : a$ = "The last time " + RTRIM$(visit.psex) + " met"
  IF visit.v0 + visit.v1 = 0 THEN a$ = a$ + " no friends"
  IF visit.v0 > 0 THEN a$ = a$ + " a fellow named A.J."
  IF visit.v1 > 0 THEN
    IF visit.v0 > 0 THEN a$ = a$ + " and"
    a$ = a$ + " a friendly chef."
  END IF
  IF visit.v0 = 2 THEN m$ = "A.J.": GOSUB M1
  IF visit.v1 = 2 THEN m$ = "the jolly chef": GOSUB M1
  IF visit.v0 = 1 THEN m$ = "A.J.": GOSUB M2
  IF visit.v1 = 1 THEN m$ = "the jolly chef": GOSUB M2
  GOSUB Lp0: GOSUB KeyWait
END IF
'
'  Zyphur
'
IF visit.v22 > 0 THEN
  GOSUB Header: LOCATE 7: COLOR 2
  PRINT RTRIM$(visit.pname); " has played the Zyphur Riverventure"; visit.v22; "time";
  IF visit.v22 > 1 THEN PRINT "s";
  PRINT ".": PRINT
  IF visit.v7 > 0 THEN
    a$ = "There " + RTRIM$(visit.psex)
    a$ = a$ + " met and disposed of the fearsom Black Warrior!"
    GOSUB Lp0
  END IF
  PRINT : a$ = "The last time " + RTRIM$(visit.psex) + " met"
  IF visit.v2 + visit.v3 + visit.v4 + visit.v5 + visit.v6 = 0 THEN a$ = a$ + " no friends"
  x = 0: IF visit.v2 > 0 THEN a$ = a$ + " a fellow named A.J.": x = 1
  IF visit.v3 > 0 THEN m$ = " a doctor named Benway": GOSUB M3
  IF visit.v4 > 0 THEN m$ = " a poor soul known only as 'prisoner #13'": GOSUB M3
  IF visit.v5 > 0 THEN m$ = " the famous Professor Axom": GOSUB M3
  IF visit.v6 > 0 THEN m$ = " an enormous warrior in gold colored armor": GOSUB M3
  a$ = a$ + ".": GOSUB Lp0: PRINT : a$ = ""
  IF visit.v2 = 2 THEN m$ = "A.J.": GOSUB M1
  IF visit.v3 = 2 THEN m$ = "Doctor Benway": GOSUB M1
  IF visit.v4 = 2 THEN m$ = "Prisoner #13": GOSUB M1
  IF visit.v5 = 2 THEN m$ = "Professor Axom": GOSUB M1
  IF visit.v6 = 2 THEN m$ = "the Gold Warrior": GOSUB M1
  IF visit.v2 = 1 THEN m$ = "A.J.": GOSUB M2
  IF visit.v3 = 1 THEN m$ = "Doctor Benway": GOSUB M2
  IF visit.v4 = 1 THEN m$ = "Prisoner #13": GOSUB M2
  IF visit.v5 = 1 THEN m$ = "Professor Axom": GOSUB M2
  IF visit.v6 = 1 THEN m$ = "the Gold Warrior": GOSUB M2
  IF a$ <> "" THEN a$ = a$ + "."
  GOSUB Lp0: PRINT : a$ = ""
  GOSUB KeyWait
END IF
'
'  Devil's Tomb
'
IF visit.v23 > 0 THEN
  GOSUB Header: LOCATE 7: COLOR 2
  PRINT RTRIM$(visit.pname); " has played The Devil's Tomb"; visit.v23; "time";
  IF visit.v23 > 1 THEN PRINT "s";
  PRINT ".": PRINT
  IF visit.v9 > 0 THEN
    a$ = "There " + RTRIM$(visit.psex): IF visit.v7 > 0 THEN a$ = a$ + " again"
    a$ = a$ + " met": IF visit.v9 = 2 THEN a$ = a$ + " and disposed of"
    a$ = a$ + " the fearsom Black Warrior!"
    GOSUB Lp0: PRINT : a$ = ""
  END IF
  IF visit.v8 > 0 THEN
    m$ = "The Gold Warrior"
    IF visit.v8 = 2 THEN GOSUB M1
    IF visit.v8 = 1 THEN GOSUB M2
  END IF
  GOSUB KeyWait
END IF
'
'  Abductor
'
IF visit.v24 > 0 THEN
  GOSUB Header: LOCATE 7: COLOR 2
  PRINT RTRIM$(visit.pname); " has played The Abductor's Quarters"; visit.v24; "time";
  IF visit.v24 > 1 THEN PRINT "s";
  PRINT ".": PRINT : PRINT : a$ = "The last time " + RTRIM$(visit.psex) + " met"
  IF visit.v10 + visit.v11 + visit.v12 = 0 THEN a$ = a$ + " no friends"
  IF visit.v10 > 0 THEN a$ = a$ + " the jolly chef again"
  IF visit.v11 > 0 THEN
    IF visit.v10 > 0 THEN a$ = a$ + " and"
    a$ = a$ + " Professor Axom again"
  END IF
  IF visit.v12 > 0 THEN
    IF visit.v10 > 0 OR visit.v11 > 0 THEN a$ = a$ + " and"
    a$ = a$ + " Prisoner #13 again."
  END IF
  IF visit.v10 = 2 THEN m$ = "the jolly chef": GOSUB M1
  IF visit.v11 = 2 THEN m$ = "Professor Axom": GOSUB M1
  IF visit.v12 = 2 THEN m$ = "Prisoner #13": GOSUB M1
  IF visit.v10 = 1 THEN m$ = "the jolly chef": GOSUB M2
  IF visit.v11 = 1 THEN m$ = "Professor Axom": GOSUB M2
  IF visit.v12 = 1 THEN m$ = "Prisoner #13": GOSUB M2
  GOSUB Lp0: GOSUB KeyWait
END IF

GOTO Menu

'                 Visitor event desc. helpers
'                -----------------------------
M1:
a$ = a$ + "  Unfortunatly, " + m$ + " died.": RETURN
M2:
a$ = a$ + "  Then " + RTRIM$(visit.pname) + " savagely killed " + m$ + "!": RETURN
M3:
IF x > 0 THEN m$ = " and" + m$
x = x + 1: a$ = a$ + m$: RETURN


'            Standard EDX 5.0 Keypress rtn
'           -------------------------------
KeyWait:
COLOR 3
KeyWait1:
PRINT : PRINT
KeyWait2:
PRINT "Press any key to continue...";
KeyWait3:
k$ = INKEY$: BS = POS(0): CC# = 0
DO
   IF s%(4) <> 1 THEN
     CC# = CC# + 1
     IF CC# = CC1# THEN PRINT "/": LOCATE CSRLIN - 1, BS
     IF CC# = CC2# THEN PRINT CHR$(179): LOCATE CSRLIN - 1, BS
     IF CC# = CC3# THEN : PRINT "\": LOCATE CSRLIN - 1, BS: CC# = 0
   END IF
   k$ = UCASE$(INKEY$)
LOOP UNTIL k$ <> ""


'                 Center text
'                -------------
Center:
PRINT SPACE$(INT((80 - LEN(a$)) / 2)); a$: RETURN


' Print a$ without wrap-around
'------------------------------
Lp0:
xm = 0: a$ = LTRIM$(a$)
IF LEN(a$) <= 79 THEN PRINT a$:  RETURN
FOR k = 79 TO 1 STEP -1
   IF MID$(a$, k, 1) = " " THEN
     PRINT LEFT$(a$, k - 1): a$ = RIGHT$(a$, LEN(a$) - k): k = 1: xm = 1
   END IF
NEXT: IF xm = 1 THEN GOTO Lp0

'              AD Menu Header 5.0
'             --------------------
Header:
mc1 = 1: mc2 = 15: mc3 = 7: COLOR 7: CLS
IF s%(4) = 1 THEN RETURN
COLOR mc1, 0: PRINT " �"; STRING$(76, "�"); "�"
'PRINT " �"; SPACE$(76); "�"
PRINT " �"; SPACE$(20); : COLOR mc2: PRINT "THE MARVELOUS WORLD OF EAMON DELUXE ";
COLOR mc1: PRINT SPACE$(20); "�": PRINT " �"; SPACE$(24); : COLOR mc2
PRINT "The Jim Jacobson Adventures"; : COLOR mc1: PRINT SPACE$(25); "�"
PRINT " �"; STRING$(76, "�"); "�": PRINT : COLOR 7: RETURN

'                       -----------
'                        Error Pit
'                       -----------
ErrHandler:
COLOR 15: PRINT : PRINT SPACE$(15);
PRINT "UH-OH, A PROGRAM ERROR JUST POPPED UP! (CODE"; STR$(ERR); ")": PRINT
k$ = "try and continue": IF ERR > 51 THEN k$ = "exit to Main Menu"
PRINT SPACE$(20); "Hit any key to "; k$; "...": k$ = INKEY$: k$ = ""
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
PRINT : IF ERR > 51 THEN CHDIR "..": RUN "EAMONDX"
PRINT "ATTEMPTING TO CONTINUE...": PRINT : L = 0: COLOR 7: RESUME NEXT

EDXMenuStrings:
DATA 3
DATA "  List visitors        "
DATA "  Visitor history      "
DATA "  Erase visitors file  "

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
'// Compact Eamon Deluxe Selection Menu 5.0           Revision 2: 20 JUN 2012
'     User locatable, two colors, single choice row with no error checking
'
'  Passed to SUB:
'     c - Vertical cursor position to begin at (0=CSRLIN)
'    c1 - Horizontal cursor position to begin at  (0=POS)
'    be - Location in em$ array to begin at
'    en - Location in em$ array to end
'   tc1 - Text foreground color of menu
'   tc2 - Text background color of menu
'  Modified by SUB:
'   ad/v/x/k/k$ - used as generic
'  Returned by SUB:
'     c - The number of the choice relative to the amount of choices
'    c1 - This holds the exit key code (Standard:1=Enter,2=Space,3=Escape)
'    en - The actual array number of em$() that was chosen
'  Required SHARED:
'   em0 - The maximum dimension of the em$() array
'em$(x) - em$(0) holds exit keys, em$(1-x) hold user defined choices
' s%(4) - If set to =1 then use numeric menu, use else standard menu

' VI Mode ------
EDXMenuMMXII:
IF s%(4) = 1 THEN
  FOR c = be TO en
     k = k * ABS(c > 1): k$ = SPACE$(ABS((c - be + 1 < 100) + (c - be + 1 < 10)))
     k$ = k$ + LTRIM$(STR$(c - be + 1)) + ": " + LTRIM$(RTRIM$(em$(c)))
     PRINT k$: k = k + 1
     IF k >= 22 AND c < en THEN
       PRINT : PRINT "Hit any key to continue..."; : k$ = INKEY$
          DO
            k$ = INKEY$
          LOOP UNTIL k$ <> ""
       CLS : k = 0
     END IF
  NEXT
  PRINT : PRINT "Enter choice number (0 to exit):"; : LINE INPUT ""; k$:                                 k$ = k$ + " "
  c = INT(VAL(k$)): k = ASC(k$): GOSUB EDXMenuMB
  c1 = c + (be - 1): IF c = 0 AND INSTR(k$, "0") > 0 THEN c1 = 3: EXIT SUB
  IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMMXII
  en = c1: c1 = 1: EXIT SUB
END IF

' EDXMenuM ------
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, tc2
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT
EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1: LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h:  COLOR tc1, tc2: PRINT em$(c);
IF LEN(k$) > 1 THEN
  IF k = 71 THEN c = be
  IF k = 79 THEN c = en
  IF k = 72 OR k = 75 THEN c = c - 1
  IF k = 80 OR k = 77 THEN c = c + 1
  GOTO EDXMenuM
END IF
GOSUB EDXMenuMB: IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB
'
' Make sure input is a valid ANSI code, else recode in ASCII (=SPOT)
EDXMenuMB:
IF k = 98 AND BORT = 0 THEN BORT = 2: SOUND 250, .5:
IF k = 111 AND BORT = 2 THEN BORT = 3: SOUND 350, .5:
IF k = 114 AND BORT = 3 THEN BORT = 4: SOUND 450, .5:
IF k <> 116 OR BORT <> 4 THEN RETURN:  ELSE BORT = 1: SOUND 550, .5: RETURN
END SUB

