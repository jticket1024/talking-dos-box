
'� (Originally converted to Eamon Deluxe v2 in MAY 1999)          �
'�                                                                �
'� The Zyphur Riverventure by Jim Jacobson                        �
'� Converted and expanded by Frank Black      Update: 11 MAR 2012 �
'�                                                                �
'����������� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED ���������ͼ
' modified with BASE ADVENTURE PROGRAM v4.1a and 5.0
'
'  d%(1) Benway & Prof. argue      d%(10) A.J.'s 'memory'
'  d%(2) Boat sinks                d%(11) A.J.'s new 'memory'
'  d%(3) New boat flag             d%(12) Dr. Benway's new 'memory'
'  d%(4) Give device to prof.      d%(13) Pris. #13's new 'memory'
'  d%(5) Take hobo's bag           d%(14) Prof. Axom's new 'memory'
'                                  d%(15) Gold War.'s 'memory'
'                                  d%(16) Black War.'s 'memory'

TYPE visitors
   pname AS STRING * 40: psex AS STRING * 3: v0 AS INTEGER: v1 AS INTEGER
   v2 AS INTEGER: v3 AS INTEGER: v4 AS INTEGER: v5 AS INTEGER: v6 AS INTEGER
   v7 AS INTEGER: v8 AS INTEGER: v9 AS INTEGER: v10 AS INTEGER: v11 AS INTEGER
   v12 AS INTEGER: v13 AS INTEGER: v14 AS INTEGER: v15 AS INTEGER
   v16 AS INTEGER: v17 AS INTEGER: v18 AS INTEGER: v19 AS INTEGER
   v20 AS INTEGER: v21 AS INTEGER: v22 AS INTEGER: v23 AS INTEGER
   v24 AS INTEGER: v25 AS INTEGER: v26 AS INTEGER
END TYPE
DIM vv AS visitors

'nr = 83: naf = 54: nm = 22: ne = 17: db = 33: na = naf + 6
'adj1 = 32: adj2 = 52: adj3 = 15: adj4 = 15


'Game returns here when finished
'-------------------------------

' Save adventure's "memories"
'-----------------------------
CLOSE:OPEN "visitors.dat" FOR RANDOM AS #1 LEN = 97
GET #1, 1, vv
r = 0: m = vv.v26: IF m = 0 THEN r = 1: m = 1: GOTO Visitor
FOR a = 1 TO m
   GET #1, a, vv: a$ = LCASE$(LTRIM$(RTRIM$(vv.pname)))
   IF a$ = LCASE$(m$(0)) THEN r = a: a = m
NEXT
IF r = 0 THEN m = m + 1: r = m
'
Visitor:
GET #1, r, vv
vv.pname = m$(0): vv.psex = "he": IF sex = 1 THEN vv.psex = "she"
vv.v2 = d%(11): vv.v3 = d%(12): vv.v4 = d%(13): vv.v5 = d%(14)
vv.v6 = d%(15): vv.v7 = d%(16): vv.v22 = vv.v22 + 1
PUT #1, r, vv
GET #1, 1, vv: vv.v26 = m: PUT #1, 1, vv


' SUB MAIN
'
'  Load 'memories'
'
OPEN "visitors.dat" FOR RANDOM AS #1 LEN = 97
GET #1, 1, vv
r = 0: m = vv.v26: IF m = 0 THEN GOTO Vl
FOR a = 1 TO m
   GET #1, a, vv: a$ = LCASE$(LTRIM$(RTRIM$(vv.pname)))
   IF a$ = LCASE$(m$(0)) THEN r = a: a = m
NEXT
IF r = 0 THEN GOTO Vl
GET #1, r, vv
d%(10) = vv.v0

Vl:
CLOSE

GOTO Move2


'
' Give room name and description (after room desc)
'
'Main3B:
'-------
IF ro = 16 THEN die = 1: GOTO Endgame


'
' List monsters in room
'
'Main4A:
'-------

       IF m%(m, 3) = 1 THEN
         PRINT m$(m); " is here.": GOSUB Lp4
         IF m = 12 THEN d%(15) = 3
         IF m = 22 THEN
           COLOR 2
           IF d%(10) = 0 THEN d%(11) = 3
           IF d%(10) > 0 AND d%(10) < 3 THEN r = 9: GOSUB Effect
           IF d%(10) = 1 THEN r = 10: GOSUB Effect: m%(m, 5) = 0
           IF d%(10) = 3 THEN r = 14: GOSUB Effect: d%(11) = 3
           COLOR 12
         END IF
         GOTO Main4B
       END IF


'
' List artifacts in room
'
'Main5A:
'-------
   IF ro < 17 THEN
     IF ro > 1 AND ro <> 10 AND ro <> 11 AND a <> 22 THEN
       a%(a, 4) = a%(a, 4) + 1: IF a%(a, 4) > 16 THEN a%(a, 4) = 0
     END IF
   END IF
   IF a%(a, 0) = 0 THEN
     IF m2 = 0 THEN GOSUB Lp1
     a%(a, 0) = 1: r = a: GOSUB DiskRead2: m2 = 1
     PRINT "You see "; a$(a); ".": GOSUB Lp2
     IF a%(a, 2) = 4 AND a%(a, 6) = 1 THEN GOSUB Inv4
     GOSUB Lp1: GOTO Main5B
   END IF
   IF a = 22 AND d%(3) = 1 THEN GOSUB Lp1: d%(3) = 2: r = 8: GOSUB Effect
   PRINT "You see "; a$(a); ".": GOSUB Lp2: m2 = 0


'Com0:
'-----
IF ro = 65 THEN
  IF m%(16, 5) = ro AND m%(17, 5) = ro AND d%(1) = 0 THEN
    d%(1) = 1: COLOR 2: r = 4: GOSUB Effect: COLOR 12
  END IF
END IF
GOSUB Lp1
IF m%(11, 5) = ro AND d%(2) = 0 THEN
  IF a%(22, 4) = ro THEN
    d%(2) = 1: r = 5: COLOR 2: GOSUB Effect: COLOR 12: a%(22, 4) = 0
  END IF
END IF


'Pfoe3:
'------
IF m%(22, 5) = ro AND d%(10) = 2 THEN
  IF a%(32, 4) = ro THEN
    a%(32, 4) = -1022: PRINT "A.J. puts on the trench coat.": GOSUB Lp4
  END IF
END IF



'MoveCheck:
'----------
IF r2 = -999 THEN
  IF m%(9, 0) = 0 THEN
    PRINT "You can't leave without finding "; m$(9); "!": GOTO Main0
  END IF
  PRINT "You successfully ride off into the sunset.": DI = 0: GOTO Endgame
END IF


'Move2:
'------
IF r%(r2, 11) > 1 THEN
  IF r%(r2, 11) = 2 AND a%(22, 4) <> ro THEN PRINT "You would drown.": GOTO Main0
  IF a%(22, 4) = ro THEN a%(22, 4) = r2
END IF
IF ls > 0 AND ls <= na THEN IF a%(ls, 4) <> -1 THEN ls = 0
IF r2 = 65 AND d%(12) = 0 THEN m%(11, 5) = 25: d%(12) = 3: d%(14) = 3
IF r2 = 74 AND d%(13) = 0 THEN d%(13) = 3
r3 = ro: ro = r2: GOSUB MoveMons: GOSUB EnemyCheck


'Drink:
'------
'
IF a > 7 AND a < 11 THEN
  k = a - 7: GOSUB WTRem: a%(a, 4) = 0
  PRINT "You have increased your ";
  PRINT MID$("hardiness.agility.  charisma.", (k - 1) * 10 + 1, 10): GOSUB Lp4
  COLOR 2: PRINT "The bottle vanishes!": COLOR 3
  IF k = 3 THEN ch = ch + 1: GOTO Lp12
  m%(0, k) = m%(0, k) + 1: GOTO Lp12
END IF
IF a = 26 THEN PRINT "Not <Hic!> bad.": GOTO Lp12



'Examine art.
IF a > naf THEN PRINT "This is your "; a$(a); ".": GOTO Lp12
IF a%(a, 2) = 12 THEN GOSUB ItemGet3: GOTO Main
IF a = 22 AND d%(3) > 0 THEN r = 8: GOSUB Effect: GOTO Pfoe


'ItemGet:
'--------
'???'IF a = 19 AND m%(15, 5) = ro THEN PRINT "The hobo won't let you.": GOTO Lp4
IF a = 19 AND m%(15, 5) = ro AND d%(5) = 0 THEN
  d%(5) = 1: m%(15, 11) = 1: GOSUB EnemyCheck: GOSUB Lp1
  COLOR 2: r = 16: GOSUB Effect: COLOR 3
END IF



'ItemOpen:
'---------
IF a = 19 AND m%(15, 5) = ro THEN PRINT "The hobo won't let you!": GOTO Lp12



'Use:
'----
IF a = 23 THEN PRINT "You can't seem to tune in anything.": GOTO Lp12
IF a = 29 THEN PRINT "You can't figure out how to use it.": GOTO Lp12


' Give money
'-----------
gold = gold - k: a$ = m$(m) + " takes the money...": GOSUB Lp10
IF m = 12 AND m%(m, 11) = 3 AND k >= 6000 AND d%(15) = 3 THEN
  d%(15) = 4: COLOR 2
  PRINT q$; "Thanks!  We'll begin when we return to the Main Hall!"; q$
  COLOR 3: GOSUB Lp4
END IF


' Give an item
'-------------
IF a = 29 AND m <> 9 AND m <> 16 AND m <> 17 THEN
  PRINT m$(m); " is confused.": GOTO Lp12
END IF
IF a = 29 AND ro <> 25 THEN PRINT "He doesn't notice you.": GOTO Lp12
a$ = "You give the " + a$(a) + " to " + m$(m) + "...": GOSUB Lp10
x = a%(a, 2): IF a = m%(0, 8) THEN m%(0, 8) = -1
IF (x = 6 OR x = 9) AND m%(m, 13) > 0 AND a%(a, 6) > 0 THEN GOTO GiveDrink
'
IF a = 29 AND d%(4) = 0 THEN
  d%(3) = 1: d%(4) = 1: r = 6: GOSUB Effect: a%(22, 4) = ro
END IF

'
Give2:
IF m = 22 AND a = 32 AND d%(10) = 2 THEN
  a%(a, 4) = -1022: PRINT q$; "Thanks!"; q$: GOSUB Lp4
END IF
GOSUB EnemyCheck: GOTO Pfoe


'Request:
'--------
IF m = 22 AND a = 32 AND d%(10) = 2 THEN
  PRINT q$; "What are you, some kind of pervert!?!"; q$: GOTO Lp12
END IF


'Smile1:
'-------
IF m = 9 AND m%(m, 11) = 3 THEN
  PRINT " strokes his beard in thought.": GOTO Lp2
END IF
IF m = 12 AND m%(m, 11) = 3 THEN PRINT " flexes a muscle.": GOTO Lp2
IF m = 16 AND m%(m, 11) = 3 THEN PRINT " grins insanely"; : GOTO Smile2
IF m = 22 AND m%(m, 11) = 3 THEN PRINT " grins"; : GOTO Smile2
Smile2:





' Hints
'------
'a = 12: m = 16



'EndGame1:
'---------
'(after sell loot)
IF m%(9, 5) = 0 THEN GOSUB Lp6: GOSUB Lp7: r = 15: GOSUB Effect: LOCATE 10
IF m%(9, 5) = ro THEN
  gold = gold + 2000: GOSUB Lp6: GOSUB Lp7: r = 2: GOSUB Effect: LOCATE 10
END IF
IF m%(9, 5) = 0 THEN GOSUB Lp6: GOSUB Lp7: r = 15: GOSUB Effect: LOCATE 10
IF m%(10, 5) = ro AND m%(10, 11) > 1 THEN
  GOSUB Lp6: GOSUB Lp7: r = 13: GOSUB Effect: LOCATE 10
END IF
IF m%(12, 5) = ro AND m%(12, 11) > 1 AND d%(15) = 4 THEN
  GOSUB Lp6: GOSUB Lp7: r = 17: GOSUB Effect: LOCATE 10
END IF
IF m%(16, 5) = ro AND m%(16, 11) > 1 THEN
  GOSUB Lp6: GOSUB Lp7: r = 11: GOSUB Effect: LOCATE 10
END IF
IF m%(11, 0) > 0 THEN d%(16) = 1
GOSUB Lp6



' Synonym checker
IF ro = 46 THEN
  sy$ = "wall": sy = 24: GOSUB Synonym1: sy$ = "shapes": : GOSUB Synonym1
  sy$ = "rectangular shape"
END IF
IF a%(7, 4) = ro THEN sy$ = "jewel": sy = 7
IF a%(29, 4) = ro OR a%(29, 4) = -1 THEN sy$ = "device": sy = 29

'Mstatus:
'--------
IF df = 14 THEN GOTO RockStatus
'
' Rockmon's health
'
RockStatus:
xm% = m%(df, 13) * 2 / m%(df, 1) + 1
ON xm% GOTO Rock1, Rock2, Rock3
PRINT "Mstatus err, "; xm%: GOTO Lp4
Rock1:
a$ = a$ + "lightly chipped.": GOTO Lp10
Rock2:
a$ = a$ + "cracking.": GOTO Lp10
Rock3:
a$ = a$ + "crumbling.": GOTO Lp10


'Mdead:
'------
IF df = 14 THEN a$ = a$ + "turned into rubble!"
'
IF df = 9 THEN d%(14) = 2: IF m%(of, 11) = 3 THEN d%(14) = 1
IF df = 10 THEN d%(13) = 2: IF m%(of, 11) = 3 THEN d%(13) = 1
IF df = 12 THEN d%(15) = 2: IF m%(of, 11) = 3 THEN d%(15) = 1
IF df = 16 THEN d%(12) = 2: IF m%(of, 11) = 3 THEN d%(12) = 1
IF df = 22 THEN d%(11) = 2: IF m%(of, 11) = 3 THEN d%(11) = 1
