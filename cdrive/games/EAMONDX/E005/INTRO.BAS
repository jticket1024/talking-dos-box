'浜様様様様様 THE MARVELOUS WORLD OF EAMON DELUXE v5.0 様様様様様融
'�                  by Frank Black Productions                    �
'�                                                                �
'�      Originally based upon the classic Eamon                   �
'�              computerized fantasy role-playing system          �
'�                                                                �
'�                MULTI-ADVENTURE INTRO PROGRAM 5.0               �
'�         DUMMY (SPACE HOLDER) ADVENTURE INTRO PROGRAM           �
'�                                                                �
'� The Thomas Zuchowski Adventures          Revision: 09 JUN 2012 �
'�                                            Update: 28 NOV 2012 �
'�                                                                �
'藩様様様様� ALL NON-COMMERCIAL DISTRIBUTION ENCOURAGED 様様様様様�

DECLARE SUB EDX.Select.Multi.Adventure (c, a$)
DECLARE SUB EDXMenuM (c, c1, be, en, tc1, tc2)
DIM SHARED Adventure$, author$, q$, sex
ON ERROR GOTO ErrHandler

TYPE RECORD
  CNAME AS STRING * 40: Status AS INTEGER: a1 AS INTEGER: a2 AS INTEGER
  A3 AS INTEGER: SP1 AS INTEGER: SP2 AS INTEGER: SP3 AS INTEGER
  SP4 AS INTEGER: WA1 AS INTEGER: WA2 AS INTEGER: WA3 AS INTEGER
  WA4 AS INTEGER: WA5 AS INTEGER: ae AS INTEGER: sex AS INTEGER: gold AS LONG
  bank AS LONG: ac AS INTEGER: WN1 AS STRING * 20: WN2 AS STRING * 20
  WN3 AS STRING * 20: WN4 AS STRING * 20: WCOMPLX1 AS INTEGER
  WCOMPLX2 AS INTEGER: WCOMPLX3 AS INTEGER: WCOMPLX4 AS INTEGER
  WTYPE1 AS INTEGER: WTYPE2 AS INTEGER: WTYPE3 AS INTEGER: WTYPE4 AS INTEGER
  WDICE1 AS INTEGER: WDICE2 AS INTEGER: WDICE3 AS INTEGER: WDICE4 AS INTEGER
  WSIDES1 AS INTEGER: WSIDES2 AS INTEGER: WSIDES3 AS INTEGER
  WSIDES4 AS INTEGER
END TYPE
DIM SHARED Z AS RECORD

' Load system settings
DIM SHARED dly#, CC1#, CC2#, CC3#, s%(10)
OPEN "SETTINGS.DAT" FOR INPUT AS #1
    INPUT #1, dly#, CC1#, CC2#, CC3#: FOR x = 1 TO 10: INPUT #1, s%(x): NEXT
CLOSE

RESTORE AdventureData: READ a
DIM SHARED a$(a), a1$(a), mp$(a), adv, adv1, adv2, ANAME$, dungeons
adv2 = a
OPEN "NAME.DAT" FOR INPUT AS #1
    INPUT #1, c, c, c, c, ANAME$, c, c, dungeons
    IF dungeons > 1 THEN
      DIM SHARED dn$(dungeons), dn%(dungeons, 9)
      FOR a = 1 TO dungeons
	 LINE INPUT #1, dn$(a): FOR m = 1 TO 9: INPUT #1, dn%(a, m): NEXT
      NEXT
    END IF
CLOSE

' Load multi-adventure names
'----------------------------
FOR a = 1 TO adv2: READ a$(a), a1$(a), mp$(a): a1$(a) = "by " + a1$(a): NEXT

' Init choices for EDXMenuM
DIM SHARED em0: RESTORE EDXMenuStrings: READ em0: DIM SHARED em$(em0)
FOR x = 1 TO em0: READ em$(x): NEXT: em$(0) = CHR$(13) + CHR$(32) + CHR$(27) '(Enter, Space, Escape)
'  Load useful character data
OPEN "NEW-MEAT" FOR RANDOM AS #1 LEN = 200: GET #1, 1, Z: CLOSE
name$ = LTRIM$(RTRIM$(Z.CNAME$)): sex = Z.sex: q$ = CHR$(34)
author1$ = "Converted to Eamon Deluxe by Frank Black": author$ = author1$
Adventure1$ = "The Thomas Zuchowski Adventures": Adventure$ = Adventure1$

' See if graphic mode is available
'----------------------------------
ON ERROR GOTO ScreenError: IF s%(4) <> 1 THEN SCREEN 7
SkreenTest:
ON ERROR GOTO 0: SCREEN 0: WIDTH 80: CLS : GOSUB Title

OPEN "GAMEDIR.DAT" FOR INPUT AS #1: INPUT #1, a: CLOSE
IF ASC(MID$(name$, 1)) = 0 THEN
  COLOR 15: PRINT "WARNING! This adventure has a corrupt file. Repairing the ";
  PRINT "file and returning to": PRINT "Main System Menu...": GOSUB KeyWait
  OPEN "GAMEDIR.DAT" FOR OUTPUT AS #1: PRINT #1, -1
      FOR m = 1 TO 5: PRINT #1, "(none)": NEXT
  CLOSE : CHDIR "..": RUN "EAMONDX"
END IF
IF a = -1 THEN
  OPEN "GAMEDIR.DAT" FOR OUTPUT AS #1
      a = -2: PRINT #1, a: FOR m = 1 TO 5: PRINT #1, "(none)": NEXT
  CLOSE
  COLOR 2: LOCATE 6: a$ = "This is Official Eamon Deluxe Adventure #5"
  GOSUB Center: LOCATE 20: GOSUB KeyWait: GOSUB Intro: GOTO Menu
END IF
IF a = -2 THEN GOTO Menu
OPEN "ADV.DAT" FOR INPUT AS #1: INPUT #1, adv1: CLOSE
adv = adv1: Adventure$ = a$(adv): mp$ = mp$(adv): author$ = a1$(adv)
PRINT "Welcome back to "; Adventure$; "!": PRINT
PRINT "Do you want to read the intro story again? (Y/N):"; : GOSUB KeyWait3
IF k$ <> "Y" THEN GOTO IntroDone
ON adv GOSUB Thror, Demo7
GOTO IntroDone

'              First-time intro
'             ------------------
Intro:
GOSUB Header: LOCATE 8: COLOR 2
PRINT "  Welcome to an unfinished copy of this adventure collection!": PRINT
PRINT "  Although the menu lists several adventures, only "; q$; "Thror's Ring"; q$; " and"
PRINT "  "; q$; "Eamon 7.0 Demo Adventure"; q$; " have been converted to Eamon Deluxe at this time."
LOCATE 20: GOSUB KeyWait
GOSUB Header: LOCATE 8: COLOR 2
PRINT "  Visit "; : COLOR 15: PRINT "www.EamonAG.org/pages/eamondx.htm"; : COLOR 2
PRINT " for the latest Eamon Deluxe updates.": PRINT : PRINT : PRINT
PRINT "  For information regarding the conversion status of these adventures or any"
PRINT "  other questions, comments, etc., please email:"; : COLOR 15
PRINT " eamondeluxe@gmail.com"
LOCATE 20: GOTO KeyWait

'CLS : LOCATE 3: COLOR 14: a$ = "THE THOMAS ZUCHOWSKI ADVENTURES": GOSUB Center
'COLOR 2: a$ = "by Tom Zuchowski": GOSUB Center: COLOR 12: PRINT
'PRINT : PRINT : PRINT
'a$ = "[Outdated contact info removed 12/17/11]": GOSUB Center
'a$ = "7625 Hawkhaven Dr.": GOSUB Center: a$ = "Clemmons, NC  27012"
'GOSUB Center: a$ = "email:  eamoncd@bellsouth.net": GOSUB Center
'PRINT : COLOR 2: a$ = "Correspondence welcome!": GOSUB Center
'COLOR 3: LOCATE 20: GOSUB KeyWait

Menu:
author$ = author1$: Adventure$ = Adventure1$: GOSUB Title
FOR a = 1 TO 3
   IF a < 3 THEN COLOR 2
   PRINT a$(a); : COLOR 8: LOCATE CSRLIN, 40: PRINT a$(a + 3)
NEXT
COLOR 7: PRINT
k = 11: c1 = 4
CALL EDXMenuM(k, c1, 1, 5, 3, 0)
IF k > 5 OR c1 = 3 THEN CHDIR "..": RUN "EAMONDX"
IF k = 5 THEN GOSUB Intro: GOTO Menu
IF k = 4 THEN adv = 0: GOTO Reviews

GOSUB Pick
ON k GOTO Stories, IntroDone0, Reviews

Stories:
GOSUB Title
ON adv GOSUB Thror, Demo7
GOTO Menu

Reviews:
IF adv > 0 THEN  ' AND adv <> 1
  COLOR 3: GOSUB Title1: PRINT : COLOR 2
  PRINT "Reviews have been written for both the Eamon Deluxe version of this adventure"
  PRINT "and the classic title from the original Apple II Eamon gaming system upon which"
  PRINT "it was based. Please select the version of the adventure that you wish to read"
  PRINT "the review for.": PRINT : PRINT
  c = 12: c1 = 4
  CALL EDXMenuM(c, c1, 7, 8, 3, 0)
  IF c1 = 3 THEN GOTO Menu
  IF c = 2 THEN GOTO DustyReviews
END IF
author$ = "Reviewed by Luke Hewitt"
'IF adv <> 2 THEN adventure$ = adventure$ + " Deluxe Edition"
Adventure$ = Adventure$ + " Deluxe Edition"
IF adv = 0 THEN Adventure$ = Adventure1$
a$ = "": IF adv < 9 THEN a$ = "0"
a1$ = "ADVENTURE #" + a$ + LTRIM$(RTRIM$(STR$(adv)))
a$ = "": IF adv + 1 < 9 THEN a$ = "0"
a2$ = "ADVENTURE #" + a$ + LTRIM$(RTRIM$(STR$(adv + 1)))
OPEN "EDX005RV.TXT" FOR INPUT AS #1
  DO
    LINE INPUT #1, a$
  LOOP UNTIL a$ = a1$
  COLOR 2: GOSUB Title1: COLOR 3
  DO
    LINE INPUT #1, a$
    IF a$ = "<paws>" THEN
      PRINT : COLOR 2: GOSUB KeyWait2: COLOR 2: GOSUB Title1: COLOR 3
      LOCATE CSRLIN - 1: a$ = ""
    END IF
    IF a$ <> a2$ THEN PRINT a$
  LOOP UNTIL a$ = a2$
  CLOSE : GOTO Menu
DustyReviews:
GOSUB Title
ON adv GOSUB Thror1, Demo71
GOTO Menu

IntroDone0:
GOSUB Title
ON adv GOSUB Thror, Demo7
IntroDone:
a$ = "(Waking up the monsters...)"
PRINT a$: FOR x# = 1 TO dly# * 2: NEXT
OPEN "ADV.DAT" FOR OUTPUT AS #1: PRINT #1, adv: CLOSE
'IF adv = 1 THEN
' GOSUB Title: LOCATE 6: COLOR 15
' PRINT "NOTE: Thror's Ring has been converted to Eamon Deluxe, however, the current"
' PRINT "conversion has many errors and is not included in this release. A new conversion"
' PRINT "which fixes the errors and uses the upgraded Eamon Deluxe 5.0 system is"
' PRINT "currently in development and will be released shortly."
' PRINT : PRINT "Visit "; : COLOR 2: PRINT "www.eamonag.org/pages/eamondx.htm";
' COLOR 15: PRINT " for the latest updates."
' LOCATE 20: GOSUB KeyWait: GOTO Menu
'END IF
RUN mp$

'                         Thror's Ring
'                        --------------
Thror:
GOSUB Title: LOCATE 6: COLOR 2
PRINT "One fine day, while lounging about the Main Hall, you noticed two newcomers who"
PRINT "were exceptional, even compared to the extraordinary types who hung about with"
PRINT "the Burly Irishman."
PRINT
PRINT "One was uncommonly fair to look upon, a high-elf by the look of him."
PRINT
PRINT "But the other- a small, nondescript man in dingy robes, but with eyes to pierce"
PRINT "steel; the look of one not to be trifled with."
PRINT
PRINT "Looking about the Hall, he spied you. "; q$; "This one"; q$; ", he said. They approached."
PRINT
PRINT q$; "Good sir"; q$; ", he said to you, "; q$; "I have a proposition in which you might have"
PRINT "interest. The good elf and I represent sponsors of a forthcoming expedition"
PRINT "into the abandoned Mines of Moria. There will be a worthy quest, fabulous loot,"
PRINT "and incredible danger. Dost thou have interest in such an adventure?"; q$
LOCATE 21: GOSUB KeyWait: LOCATE 6: COLOR 2
PRINT "(Who does he think he's talking to!?!)  You nod, and try to look nonchalant."
PRINT
PRINT q$; "This is good!"; q$; " he said, "; q$; "For you are my choice among the many worthies of this"
PRINT "Hall."
PRINT
PRINT q$; "Now, here is the meat of the quest. As all know, the dwarvish stronghold of"
PRINT "Khazad-Dum, that many know as Moria, was sacked by the orcs many hundreds of"
PRINT "years ago. It was then that the Ring of Power that controlled Moria was lost."
PRINT
PRINT q$; "We have reason to know that the ring is still there, and may be recovered by"
PRINT "one both cunning and strong."
PRINT
PRINT "Again, you nod. "; q$; "I am that one."; q$
LOCATE 21: GOSUB KeyWait: LOCATE 7: COLOR 2
'
' Note: This 'spell' turned on the 80-column hardware on the Apple II ver.
'
PRINT "The wizard stands stiff and tall and invokes, "; : COLOR 14
FOR a# = 1 TO dly# * 2: NEXT: a# = dly# / 2:
a$ = q$ + "Peiah!...Arrah!...Three!" + q$: GOSUB SlowPrint: COLOR 3
LOCATE 21: GOSUB KeyWait: LOCATE 6: COLOR 2
'
PRINT "Suddenly you find yourself in a lovely forest, surrounded by elves. Before you"
PRINT "is a high-elf lady of great power. Thus did she speak:"
PRINT
PRINT q$; "I see that the wizard has chosen well; a strong warrior of good heart. Know"
PRINT "you that the ring, should you find it, is the rightful property of the dwarf"
PRINT "hight Thror, who is of the Lonely Mountain far to the north. And we of the"
PRINT "forest Lothlorien would see the Ring of Power recovered, for we do not like the"
PRINT "threat of such falling into evil hands."
PRINT
PRINT q$; "I have chosen two stout companions for the quest. The first is the good dwarf"
PRINT "Gorim, nephew of Thror, who has come to Lothlorien for the quest."; q$; "  A dwarf bows"
PRINT "low, his beard touching the ground."
PRINT
PRINT q$; "Be not deceived by his short stature"; q$; ", the lady continued, "; q$; "the berserker comes"
PRINT "upon him in battle, and none may stand in his way."
LOCATE 21: GOSUB KeyWait: LOCATE 7: COLOR 2
PRINT q$; "The second companion will be the wood-elf Galahir."; q$; "  A tall elf with a long bow"
PRINT "slung across his shoulder bows to you. "; q$; "Know that he is a master bowman; there"
PRINT "is none better in Middle-Earth."
PRINT
PRINT q$; "I have parting gifts to you, that will aid you in your quest."; q$; "  She hands you a"
PRINT "small crystal bottle. "; q$; "This contains a magical substance called 'miruvor'. It"
PRINT "will heal the injuries that you take in battle."
PRINT
PRINT q$; "And know that I have enhanced your own POWER spell. It will not fail you for"
PRINT "five invocations, but then will be exhausted; use it wisely. It may be invoked"
PRINT "by any of the three companions. It will aid you beyond hope, never harm you,"
PRINT "and will never aid your enemies."
LOCATE 21: GOSUB KeyWait: LOCATE 10: COLOR 2
PRINT q$; "The quest is not a simple one; you may make many false starts. Prudent use of"
PRINT "the SAVE command will abate the many tedious journeys from the Main Hall."
PRINT
PRINT q$; "Go now, and have my blessing."; q$
LOCATE 21: GOTO KeyWait

' Note: The following was text was in the Apple version to see if an
'       80-column card was installed. It was clever enough to save in the
'       Eamon Deluxe conversion, but completely unnecessary. -Frank
'
'"First, we must ascertain if you have hidden qualities that would aid you in
'the quest.
'
'"We must determine: dost thou have the wider vision that some call
'80-column capability?"
'
'"This is good, for the use of such will enhance, I think, your ability to
'see all that is around you. "Wouldst thou use this wider vision? I must
'warn you, if thou hast lied about its possession, and it be invoked, thou
'will indeed go blind, and nought but the invocation of a RESET spell can
'return you to this world.
'
'"Wouldst thou use the wider vision?" ... "Good!" / "Alas. Then we must do
'without it."
'
'     (This referred to a bonus program on the Apple II disk)
'
'The wizard leans close...he whispers, "I have a final piece of advice for
'you- if thou find the quest too perilous, and wouldst divine the dunjon's
'secrets, do first peruse yon disk file hight 'Buil-der's Notes'."
'
'     (And here is the contents of that program:)
' Note: This all refers to the Apple II version and NOT the Eamon Deluxe
' version.
'
'Thror's Ring builder's notes
'
'1) There are two non-standard files: EAMON.ROOMS, and EAMON.ROOM NAMES,
'   which have record lengths of 32. To modify the Dungeon Edit pgm to work
'   on this adventure, you must change the "OPEN" commands in line 70 of the
'   edit pgm from "L64" to "L32". You must also load the artifact type file
'   named "PARAMETERS FILE". The "OPEN" cmds at lines 88 and 2060 of the
'   Dungeon List and Flex Dungeon List pgms must be changed to "L32" also.
'
'2) This disk does not contain a DOS image- it will not boot. You cannot use
'   FID to transfer the files to an empty disk that has been INIT'ed
'   normally- you will get a "DISK FULL" error. You must use COPYA, or any
'   "Fast-Copy" program that duplicates all 35 tracks, including tracks 0-2.
'
'3) I ran out of everything, including artifacts, forcing me to develop a
'   'doors' routine that did not utilize artifacts. The doors data:
'     30950: room on one side of door
'     30952: room on other side of door
'     30954: dir. of door from room in 30950  (123456=nsewud)
'     30956: dir. of door from room in 30952
'     30960: 0=open, 1=closed, 2=locked, 3=concealed
'    30960: 1=door, 5=gate
'  do%(x,0) thru do%(x,5) contain the door data in the same order as above.
'   dr%(6,2) is the temporary array that contains the door data for the
'   current room (ro). The first array term is the direction of the door.
'   The second term contains the following:
'     dr%(x,0): -1=no door; otherwise the same as do%(x,4)
'     dr%(x,1): room on other side of door
'     dr%(x,2): loc. of door info in do%(x,x) array.
'
'4) I have a Videx Videoterm- the 80-col. routines are designed for it. I
'   think I have the //e card covered, but do not have access to one for
'   testing. If your card uses different commands, and/or these commands
'   cause problems, please let me know so I can modify the routines to
'   accommodate any card.
'
'5) The MAIN PGM got pretty banged up- however, everything works in pretty
'   much the same fashion. The primary global variable change was to shorten
'   the variable name to conserve memory- eg: 'r1'='room', 'f'='fo',
'   'm%(x,x)'='md%(x,x)', 'a$(x)'='an$(x), etc.. This step alone saved about
'   1k of memory! The routines were rewritten for greater speed and
'   compactness.
'
'6) Artifact type 13 is part of another artifact. e.g. #35-'medallion' is
'   part of #34-'mithril shirt'. Note the location of #35 is 534. I will
'   endeavor to answer any correspondence concerning the internal workings
'   of this adventure. I plan to make a fully commented version available to
'   anyone who is willing to pay for the postage. Drop me a line...
Thror1:
s$ = "June 1986": GOSUB Review2: COLOR 7: LOCATE 7
PRINT "Reviewed by Bob Davis"
PRINT : PRINT "Playing Time: 15-30 hours": PRINT "Average rating: 9.0/1"
PRINT : COLOR 2
PRINT "Comment: The descriptions are fantastic! I felt I could actually see some of the"
PRINT "rooms. The puzzles are interesting with a few tricks thrown in. The hardest part"
PRINT "of the adventure is finding where to look for a needed object."
LOCATE 20: GOSUB KeyWait: LOCATE 7: COLOR 2
PRINT "This adventure makes good use of [standard Eamon features] hidden objects,"
PRINT "locked/unlocked doors, etc.) has some special effects (once in awhile the"
PRINT "characters converse) and can be very frustrating. I found a torch (which doesn't"
PRINT "last very long) but could not find anything to light with it to help me see in"
PRINT "the mines. This problem went on for several tries. Once solved, I became very"
PRINT "alert of the surroundings and questioned the existence of most everything I saw."
PRINT "This proved to be helpful. In the end, I realized that I have been taking most"
PRINT "Eamon adventures for granted (expecting them to work via my knowledge of"
PRINT "[programming them]) and this adventure got me to think again."
PRINT : COLOR 7: PRINT "Difficulty Rating: 10"
LOCATE 20: GOSUB KeyWait: RETURN
' !!! Temporarily(?) removed because I can't remember if I changed this...
'PRINT : PRINT : GOSUB KeyWait
'PRINT "One word of warning: The records room is described as having doors on both ends"
'PRINT "of the room and that they are strong enough to hold off an army. You could meet"
'PRINT "an army and therefore decide this is where the army needs to be trapped."
'PRINT
'PRINT "Although this was the original intent of the author, due to the size of the"
'PRINT "adventure program, this feature was deleted. DO NOT trap the army in the records"
'PRINT "room! If this is done, there will be no way out of the adventure once the"
'PRINT "mission is completed."
'LOCATE 20: GOTO KeyWait


'       Eamon 7.0 Demo Adventure
'      --------------------------
Demo7:
GOSUB Title: LOCATE 7: COLOR 15: PRINT "CONVERSION NOTES:": PRINT : COLOR 7
PRINT "The "; q$; "Eamon 7.0 Demo Adventure"; q$; " was written by Tom Zuchowski to test his Classic"
PRINT "Eamon 7.0 system for the Apple II series of computers. It is pointless as a"
PRINT "Eamon Deluxe adventure and was only converted for the sake of having the entire"
PRINT "collection of Classic Eamon adventures converted to Eamon Deluxe. (However, I do"
PRINT "apologize to Tom for previously placing it with the <grin> "; q$; "adult"; q$; " adventures"
PRINT "for so many years.)"
LOCATE 20: GOSUB KeyWait
LOCATE 6: COLOR 15: a$ = "WELCOME TO THE CLASSIC EAMON 7.0 DEMONSTRATION ADVENTURE!"
GOSUB Center: COLOR 2: PRINT
PRINT "This adventure is designed to show this version's features. There are 18 rooms,"
PRINT "141 artifacts, 2 effects, & 63 monsters."
PRINT
PRINT "Most of the artifacts and monsters are "; q$; "dummies"; q$; " that do not exist within the"
PRINT "adventure, but were added in order to demonstrate the speed of the search"
PRINT "routines."
PRINT
PRINT "The vast majority of the monsters are in the form of "; q$; "group-monsters"; q$; ", including"
PRINT "a 750-man army."
LOCATE 20: GOSUB KeyWait: LOCATE 6: COLOR 2
PRINT "The story is: You have been sent by the Burly Irishman to investigate a modern"
PRINT "Eamon that he has heard about."
PRINT
PRINT "You are told that there will be many subtle changes, and you promise to try many"
PRINT "things, and to be on the watch for new stuff."
PRINT
PRINT "Among other things, you suspect that the old spells may be completely new, that"
PRINT "the battles surely have been extensively reworked, and that hidden doors will be"
PRINT "found when you EXAMINE certain items."
PRINT
PRINT "You set out, eager to see what can be seen!"
LOCATE 20: GOTO KeyWait
Demo71:
s$ = "September 1998": GOSUB Review1: COLOR 7: LOCATE 6
PRINT "Reviewed by Tom Zuchowski": PRINT : PRINT "Playing Time: 10-15 min."
PRINT "Reviewer Rating: N/A": PRINT : COLOR 2
PRINT "Comment: When I wrote my first Eamon, I had a simply awful time figuring out how"
PRINT "stuff worked. The method I used to learn it all was to analyze John Nelson's"
PRINT q$; "Temple of the Trolls"; q$; " and thus learn from example. When I wrote the v7.0 "
PRINT "MAIN PGM, I wanted to save people the trouble that I had, so I wrote this demo,"
PRINT "which as far as I know demonstrates everything you can do with a standard v7.0"
PRINT "Eamon MAIN PGM and database. Most of the artifact and monster descriptions give"
PRINT "programming and database info, for easy reference."
LOCATE 20: GOTO KeyWait

'                  Review header
'                 ---------------
Review1:
GOSUB Title: a$ = "The Eamon Adventurer's Guild": COLOR 14: GOSUB Center
COLOR 2: a$ = "www.EamonAG.org": GOSUB Center: COLOR 3: PRINT : PRINT
PRINT "The following is from the "; s$; " issue of the Eamon Adventurer's Guild"
PRINT "newsletter. The EAG supports the Apple II version of Eamon and not Eamon"
PRINT "Deluxe. The review is for the old Apple II version."
Review1a:
PRINT
PRINT "The EAG rating system is from 1-10 (10 being the best), the average rating"
PRINT "is displayed with the number of people who have rated it. e.g. 4.6/3 is an"
PRINT "average rating of 4.6 by the ratings of three people."
PRINT : COLOR 2
PRINT "Remember, these reviews were written for the OLD versions of these adventures,"
PRINT "most Eamon Deluxe adventures have been enhanced considerably!"
GOTO KeyWait

'                *********************
'                  NEUC Review header
'                *********************
Review2:
GOSUB Title: a$ = "The Eamon Adventurer's Guild": COLOR 14: GOSUB Center
COLOR 2: a$ = "www.EamonAG.org": GOSUB Center: COLOR 3: PRINT : PRINT
PRINT "The following is from the "; s$; " issue of the "; : COLOR 7
PRINT "National Eamon User's Club": COLOR 3
PRINT "newsletter which preceded the Eamon Adventurer's Guild. The ratings are from"
PRINT "the EAG. The NEUC supported the Apple II version of Eamon and not Eamon Deluxe."
PRINT "The review is for the old Apple II version.": GOTO Review1a

'                  Select adventure
'                 ------------------
Pick:
adv = 0: a$ = ""
CALL EDX.Select.Multi.Adventure(adv, a$)
Adventure$ = a$(adv): mp$ = mp$(adv): author$ = a1$(adv): RETURN


KeyWait:
COLOR 3
KeyWait1:
PRINT : PRINT
KeyWait2:
PRINT "Press any key to continue...";
KeyWait3:
k$ = INKEY$: BS = POS(0): CC# = 0
DO
   IF s%(4) <> 1 THEN
     CC# = CC# + 1
     IF CC# = CC1# THEN PRINT "/": LOCATE CSRLIN - 1, BS
     IF CC# = CC2# THEN PRINT CHR$(179): LOCATE CSRLIN - 1, BS
     IF CC# = CC3# THEN : PRINT "\": LOCATE CSRLIN - 1, BS: CC# = 0
   END IF
   k$ = UCASE$(INKEY$)
LOOP UNTIL k$ <> ""

Title:
COLOR 3
Title1:
CLS : a$ = Adventure$: GOSUB Center: a$ = author$:
GOSUB Center: PRINT STRING$(80, 196): PRINT : RETURN

Center:
PRINT SPC(INT((80 - LEN(a$)) / 2)); a$: RETURN

'
'     Emulate Apple II SPEED command
'    --------------------------------
SlowPrint:
FOR x = 1 TO LEN(a$)
   PRINT MID$(a$, x, 1);
   FOR x1# = 1 TO a#: NEXT
NEXT
PRINT : RETURN

'              AD Menu Header 5.0
'             --------------------
Header:
mc1 = 1: mc2 = 15: mc3 = 7: COLOR 7: CLS
IF s%(4) = 1 THEN RETURN
COLOR mc1, 0: PRINT " �"; STRING$(76, "�"); "�"
'PRINT " �"; SPACE$(76); "�"
PRINT " �"; SPACE$(20); : COLOR mc2: PRINT "THE MARVELOUS WORLD OF EAMON DELUXE ";
COLOR mc1: PRINT SPACE$(20); "�": PRINT " �"; SPACE$(22); : COLOR mc2
PRINT "The Thomas Zuchowski Adventures"; : COLOR mc1: PRINT SPACE$(23); "�"
'PRINT " �"; SPACE$(76); "�"
PRINT " �"; STRING$(76, "�"); "�": PRINT : COLOR 7: RETURN

'                       -----------
'                        Error Pit
'                       -----------
ErrHandler:
COLOR 15: PRINT : PRINT SPACE$(15);
PRINT "UH-OH, A PROGRAM ERROR JUST POPPED UP! (CODE"; STR$(ERR); ")": PRINT
k$ = "try and continue": IF ERR > 51 THEN k$ = "exit to Main Menu"
PRINT SPACE$(20); "Hit any key to "; k$; "...": k$ = INKEY$: k$ = ""
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
PRINT : IF ERR > 51 THEN CHDIR "..": RUN "EAMONDX"
PRINT "ATTEMPTING TO CONTINUE...": PRINT : L = 0: COLOR 7: RESUME NEXT

ScreenError:
SCREEN 0: WIDTH 80: CLS : SkreenTest = 1
COLOR 0, 2: PRINT " Sorry, ": COLOR 3, 0: PRINT
PRINT "You won't be able to see the graphic intro screen for "; q$; "[GAME NAME]"; q$; ","
PRINT "because compatible graphics hardware is not available."
GOSUB KeyWait: RESUME SkreenTest

AdventureData:
'
' !!!! NOTE TEMPORARILY OUT OF CHRONOLOGICAL ORDER BECAUSE IT IS EASIER
'
DATA 6
DATA Thror's Ring,Tom Zuchowski,THROR
DATA Eamon 7.0 Demo Adventure,Tom Zuchowski,DEMO7
DATA Assault on Dolni Keep,Tom Zuchowski,DOLNI
DATA Journey to Jotunheim,Tom Zuchowski,JOTUN
DATA Walled City of Darkness,Tom Zuchowski,WALLCITY
DATA Hammer's Slammers,Tom Zuchowski,HAMMERS

EDXMenuStrings:
DATA 8
DATA "  Preview Adventure Intros          "
DATA "  START PLAYING AN ADVENTURE        "
DATA "  Read Adventure Reviews            "
DATA "  Read Review for This Collection   "
DATA "  Read Notes About This Collection  "
DATA "  Return to Eamon Deluxe Menu    "
DATA "  Review of the Eamon Deluxe Version   "
DATA "  Review of the Classic Eamon Version  "

SUB EDX.Select.Multi.Adventure (c, a$)
'/// Eamon Deluxe Multi-Adventure Selection
'
' (Updated for VI Mode support 06 JAN 2012)
'
'   Codes returned from SUB:
'      c - Number of adventure selected
'     a$ - Name of adventure selected

IF dungeons = 1 THEN c = 1: a$ = ANAME$: EXIT SUB

' VI Mode numeric menu
GetMAdvCVI:
IF s%(4) = 1 THEN
  CLS : L = 2: PRINT "Please select an adventure:"
  FOR c = 1 TO dungeons
     a$ = LTRIM$(RTRIM$(STR$(c))) + ": " + dn$(c)
     IF c < 10 THEN a$ = " " + a$
     IF c < 100 THEN a$ = " " + a$
     PRINT a$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       L = 0
     END IF
  NEXT
  PRINT : PRINT "Enter the number of your choice (0 to Exit):";
  LINE INPUT ""; a$: c = INT(VAL(a$))
  IF c < 0 OR c > dungeons THEN CLS : GOTO GetMAdvCVI
  'IF c = 0 THEN RUN 0
  a$ = dn$(c): EXIT SUB
END IF


' Standard Menu
'            Load adventure names
REDIM advn$(dungeons)
FOR x = 1 TO dungeons
   advn$(x) = LEFT$(LTRIM$(RTRIM$(dn$(x))), 38)
NEXT
numadv = dungeons

keyz$ = CHR$(13) + CHR$(13) + CHR$(73) + CHR$(81) + CHR$(71) + CHR$(79)
keyz$ = keyz$ + CHR$(72) + CHR$(80) + CHR$(75) + CHR$(77)
min = VAL(MID$(TIME$, 4, 2)) - 2 '<-- Force time display

mSelectADVa:
GOSUB mSelectADV0

mSelectADV:
IF c < be THEN c = en
IF c > en THEN c = be
x = ABS(c > be + 20): v = 3 + (c - be) - 21 * x: h = 2 + 39 * x
LOCATE v, h: COLOR 0, 3: PRINT advn$(c);

DO
  COLOR 7, 1
  IF VAL(MID$(TIME$, 4, 2)) <> min THEN
    hr = VAL(LEFT$(TIME$, 2)): min = VAL(MID$(TIME$, 4, 2))
    IF (hr = 0 OR hr = 24) AND min = 1 THEN GOSUB mSelectADV0
    k = hr - 12 * ABS(hr > 12) + 12 * ABS(hr = 0): x = 52 - ABS(k > 9)
    LOCATE 25, x: COLOR 7, 1: PRINT SPACE$(13); : LOCATE 25, x
    PRINT "  "; STR$(k);
    PRINT MID$(TIME$, 3); " "; MID$("a.m.p.m.", 4 * ABS(hr > 12) + 1, 4);
  END IF
  LOCATE 25, 60: PRINT RIGHT$(TIME$, 2); : k$ = INKEY$
  k = INSTR(keyz$, RIGHT$(k$, 1)): IF k > 2 THEN k = k * ABS(LEN(k$) > 1)
LOOP UNTIL k$ <> "" AND k > 0

LOCATE v, h: COLOR 3, 0: PRINT advn$(c);

'         Set arguments and exit
IF k < 3 THEN
  a$ = dn$(c): VIEW PRINT 1 TO 25: CLS : EXIT SUB
END IF

'          Pg up/down
IF k = 3 AND be > 1 THEN be = be - 42: GOTO mSelectADVa
IF k = 4 AND en < numadv THEN be = be + 42: GOTO mSelectADVa
'          Home/end
IF k = 5 THEN c = be
IF k = 6 THEN c = en
'         Up/down/left/right
IF k > 8 AND c < be + 21 AND c + 21 > en THEN k = k - 2
IF k = 7 THEN c = c - 1
IF k = 8 THEN c = c + 1
IF k = 9 THEN c = c - 21 + 42 * ABS(c < be + 21)
IF k = 10 THEN c = c + 21 - 42 * ABS(c > be + 21)
GOTO mSelectADV

'       Set up screen
mSelectADV0:
VIEW PRINT 1 TO 25: COLOR 7, 0: CLS : COLOR 7, 1
PRINT "    <Enter=Accept>                     <Page Up/Page Down=More Options>         "
LOCATE 25, 1:
PRINT " <Use Arrow keys, Home, or End to select>"; SPACE$(29);
PRINT MID$(DATE$, 4, 2); "-"; : k = VAL(LEFT$(DATE$, 2)) - 1
PRINT MID$("JanFebMarAprMayJunJulAugSepOctNovDec", k * 3 + 1, 3); "-";
PRINT RIGHT$(DATE$, 2); : PRINT SPACE$(81 - POS(0)); : COLOR 3, 0
'       Display choices
mSelectADV1:
IF be < 1 THEN be = 1
IF be > numadv THEN be = numadv
en = be + 41: IF en > numadv THEN en = numadv
VIEW PRINT 3 TO 23: CLS 2: v = 3: h = 2
FOR x = be TO en
   LOCATE v, h: PRINT advn$(x); : v = v + 1: IF v = 24 THEN v = 3: h = 41
NEXT
c = be: RETURN
END SUB

SUB EDXMenuM (c, c1, be, en, tc1, tc2)
' // Compact version of standard Eamon Deluxe selection menu
'      Allows only a single row of choices and has no error checking
'
' (Updated with alt. numeric menu for VI Mode support 06 JAN 2012)
'
'  Arguments passed to EDXMenuM:
'     c - Vertical cursor position to begin at (0=CSRLIN)
'    c1 - Horizontal cursor position to begin at  (0=POS)
'    be - Location in em$ array to begin at
'    en - Location in em$ array to end
'   tc1 - Text foreground color of menu
'   tc2 - Text background color of menu
'
' Values returned by EDXMenuM:
'     c - The number of the choice relative to the amount of choices
'    c1 - This holds the exit key code (Standard:1=Enter,2=Space,3=Escape)
'    en - The actual array number of em$() that was chosen
'
'  Required SHARED variables:
'    s%() - Eamon DX system settings (s%(4) = 1 is VI Mode Flag)
'   em$() - An array which holds all available choices. em$(0) must
'           contain exit keys
'    em0 - The maximum dimension of the em$() array.
'
'   Upon exit, the cursor will locate to the row just beneath the last
'   choice (or 25 if it exceeds screen), at column 1
'

' VI Mode numeric menu
EDXMenuMCVI:
IF s%(4) = 1 THEN
  L = 2
  FOR x = be TO en
     x$ = LTRIM$(STR$(x - be + 1)) + ": " + LTRIM$(RTRIM$(em$(x)))
     IF x < 100 THEN x$ = " " + x$: IF x < 10 THEN x$ = " " + x$
     PRINT x$: L = L + 1
     IF L = 20 THEN
       PRINT "Hit any key to continue...";
       DO
       LOOP UNTIL INKEY$ <> ""
       CLS : L = 0
     END IF
  NEXT
  PRINT : PRINT "Enter the number of your choice (0 to exit):";
  LINE INPUT ""; k$: c = INT(VAL(k$)): c1 = c + (be - 1)
  IF c = 0 THEN c1 = 3: EXIT SUB
  IF c1 < be OR c1 > en THEN CLS : GOTO EDXMenuMCVI
  en = c1: c1 = 1: EXIT SUB
END IF

' Standard Menu
IF c = 0 THEN c = CSRLIN
IF c1 = 0 THEN c1 = POS(0)
v = c: v1 = c: h = c1: c = be: c1 = 0: ad = en - (en - be) - 1: COLOR tc1, tc2
FOR x = be TO en: LOCATE v, h: PRINT em$(x); : v = v + 1: NEXT

EDXMenuM:
IF c < be THEN c = en
IF c > en THEN c = be
v = c - ad + v1 - 1
LOCATE v, h: COLOR tc2, tc1: PRINT em$(c);
DO
  k$ = INKEY$
LOOP UNTIL k$ <> ""
c1 = INSTR(em$(0), RIGHT$(k$, 1)): k = ASC(RIGHT$(k$, 1))
LOCATE v, h: COLOR tc1, tc2: PRINT em$(c);

IF LEN(k$) > 1 THEN
  IF k = 71 THEN c = be
  IF k = 79 THEN c = en
  IF k = 72 OR k = 75 THEN c = c - 1
  IF k = 80 OR k = 77 THEN c = c + 1
  GOTO EDXMenuM
END IF

IF c1 = 0 THEN GOTO EDXMenuM
LOCATE en - ad + v1 - 1: PRINT : en = c: c = c - be + 1: EXIT SUB
END SUB

