IN THE FOLLOWING WE LIST THE 50 BEST BASEBALL TEAMS OF ALL TIME AND
ALSO LIST THE CODES THAT CAN BE USED IN THE WORLD SERIES BASEBALL
GAME TO ACCESS EACH TEAM.

IF THE TEAM ITSELF IS NOT REPRESENTED IN THE GAME, THE CODE FOR
ANOTHER YEAR FOR THE SAME CLUB IS GIVEN AS A PROXY FOR THE TEAM.

AT THE END OF THE LIST, WE OFFER YOU A BRIEF REPORT ON EACH OF THE
TOP 10 TEAMS, IF YOU WANT TO READ THEM.

IN THE FOLLOWING WE SHOW FOR EACH TEAM, FIRST, ITS RANK, THEN ITS
NAME, THEN ITS CODE.

A * MEANS THAT THE INDICATED CODE IS FOR A TEAM WHICH IS A PROXY

1. 1927 NEW YORK YANKEES 27A
2. 1939 NEW YORK YANKEES 39A
3. 1995 CLEVELAND INDIANS 95A
4. 1929 PHILADELPHIA ATHLETICS 29A
5. 1906 CHICAGO CUBS 06N
6. 1902 PITTSBURGH PIRATES 02N
7. 1936 NEW YORK YANKEES 39A*
8. 1944 ST. LOUIS CARDINALS 44N
9. 1931 PHILADELPHIA ATHLETICS 29A*
10. 1943 ST. LOUIS CARDINALS 44N*



11. 1937 NEW YORK YANKEES 39A*
11. 1932 NEW YORK YANKEES 27A*
13. 1969 BALTIMORE ORIOLES 69A
14. 1912 NEW YORK GIANTS 12N
15. 1954 CLEVELAND INDIANS 54A
16. 1910 PHILADELPHIA ATHLETICS 10A
17. 1907 CHICAGO CUBS 06N*
17. 1905 NEW YORK GIANTS 05N
19. 1912 BOSTON RED SOX 12A
20. 1942 ST. LOUIS CARDINALS 44N*



21. 1953 BROOKLYN DODGERS 53N
22. 1911 PHILADELPHIA ATHLETICS 10A*
23. 1904 NEW YORK GIANTS 05N*
24. 1938 NEW YORK YANKEES 39A*
25. 1909 PITTSBURGH PIRATES 02N*
26. 1942 NEW YORK YANKEES 39A*
27. 1986 NEW YORK METS 86N
28. 1975 CINCINNATI REDS 75N
29. 1953 NEW YORK YANKEES 53A
30. 1903 BOSTON RED SOX 03A




31. 1948 CLEVELAND INDIANS 48A
31. 1941 NEW YORK YANKEES 39A*
33. 1970 BALTIMORE ORIOLES 69A*
34. 1961 NEW YORK YANKEES 61A
35. 1919 CINCINNATI REDS 19N
36. 1910 CHICAGO CUBS 06N*
37. 1913 NEW YORK GIANTS 12N*
38. 1901 PITTSBURGH PIRATES 02N*
39. 1923 NEW YORK YANKEES 27A*
40. 1984 DETROIT TIGERS 84A



41. 1955 BROOKLYN DODGERS 53N*
42. 1929 CHICAGO CUBS 29N
42. 1940 CINCINNATI REDS 40N
44. 1931 ST. LOUIS CARDINALS 34N*
45. 1930 PHILADELPHIA ATHLETICS 29A*
45. 1935 CHICAGO CUBS 38N*
47. 1911 NEW YORK GIANTS 12N*
48. 1958 NEW YORK YANKEES 61A*
48. 1957 NEW YORK YANKEES 53A*
50. 1956 NEW YORK YANKEES 53a*



1. 1927 NEW YORK YANKEES

What a team this was!  Of all the baseball teams that have played
during the modern era (starting in 1901), this Yankee team was
among the leaders in all but one of the 11 criteria we used to
select the best teams of all time.

Only four of the some 1,700 teams that have played since 1901 won
a higher percentage of their games.  Only six teams led the second
place team by more games.

They were number one in slugging.  Their team net slugging average
(their slugging average less the league's) was the highest ever -
.090 higher than the league's in 1927.

They were also first in net home runs.  They hit 103 MORE than the
average team in the league that year, at a time when the average
team was hitting only 55 in total!

The '27 Yanks had the second highest batting average in
history.They were seventh in net batting average.  They were third
in net runs scored and sixth in runs scored.

Even in pitching they were outstanding, fifth in net earned run
average.  And they were tied for first in World Series record,
sweeping their series with the 1927 Pirates.

Who were these supermen?

That year Babe Ruth hit 60 home runs, the all-time record until
1961 and still second best ever.  Ruth's slugging average that year
was .772, third best in history and exceeded only by Ruth himself
in two other years).

Lou Gehrig, the Yankee's great first baseman, batted .373, hit 47
home runs, and led the league in runs batted in with 175, the third
highest number in history.  Gehrig's slugging average was .765,
fourth highest in history.

Center fielder Earle Combs batted .356.  Second baseman Tony
Lazzeri batted .309.  Left fielder Bob Meusel hit .337.

Waite Hoyt led the league in wins and earned run average. his
record was 22-7.  Wilcy Moore was 19-7 and he had 13 saves which
led the league.  Herb Pennock was 19-8.  Urban Shocker was 18-6.

Following the 1927 World Series in which the Yanks swept the
Pirates 4-0, a writer for the New York Times wrote:

"The 1927 World Series has passed quickly into history with 10
records smashed and five others tied, chiefly through the batting
prowess of Babe Ruth.

"The Yankees are one of the best baseball teams ever gathered
together in this broad land, a baseball team which set a new
American League record with 110 victories and then climaxed a
glorious season by winning four straight World Series games.

"They may not be far wrong who assert that these Yankees are the
greatest team in the more than 50 years of baseball history."


2. 1939 NEW YORK YANKEES

Twelve years after they fielded the greatest team of all time, the
Yankees were back with the second best, led by center fielder Joe
DiMaggio.

Surprisingly, the team's greatest claim to fame was its pitching! 
It had the best net earned run average (the league ERA less theirs)
in history.  Their team ERA was 1.31 lower than the league's.  They
also tied for first for best record in a World Series, sweeping
their series 4-0.

The 1939 Bombers were seventh in runs scored.  They were eighth in
percentage of games won and ninth in net runs scored.

They were llth in games ahead of second and tenth in and net home
runs.

DiMaggio, the Yankee Clipper, led the league in hitting with a .381
average and had 30 home runs.  Charlie Keller in right field batted
.334.  Red Rolfe at third base batted .329.

George Selkirk in left field batted .306.  Bill Dickey, the
catcher, batted .302.

Pitcher Red Ruffing was 21 and 7.  Lefty Gomez had a bad year for
him with a record of 12 wins and 8 losses.  Johnny Murphy led the
league in saves with 19.

Lou Gehrig, who had played on that great 1927 team, opened the year
at first base, but was cut down by his terminal illness very early
in the season.

Many of the players on this team were also on some of the other
great Yankee teams: the 1932 team, 11th best; the 1936 team,
seventh best; the 1937 team, 11th best; the 1938 team, 24th best;
the 1941 team, 31st best; and the 1942 team, 26th best.  In fact,
there were even a couple who were on the 1927 team, best of all
time; or the 1923 team, 39th best.

During the 16 year period, 1927-1942, the Yankees fielded an
unbelievable eight teams that are among the 31 best of all time,
including the top two and three of the top seven.

Playing on these teams were Babe Ruth (two teams), Lou Gehrig
(six), Bill Dickey (seven - all but 1927), Joe DiMaggio (six),
Earle Combs (two),  Lefty Gomez (seven - all but 1927), Waite Hoyt
(one), Herb Pennock (two), and Red Ruffing (seven - all but 1927).

And there were other standouts, including Tony Lazzeri (four), Joe
Gordon (four), Red Rolfe (seven - all but 1927), Frank Crosetti
(seven - all but 1927), Charlie Keller (three), and George Pipgras
(two).

In all, the Yankees placed 14 teams in the Top 50, eight in the Top
30, and three in the Top 10.


3. 1995 CLEVELAND INDIANS

In the 144-game strike-shortened season of 1995, the Cleveland
Indians were an amazing team.  They finished 30 games ahead of
second place, the largest lead of all time.  They won .694 (100 of
144) of their games, the 10th best record of all time.

The Indians had excellent pitching and hitting, leading the
American League in both earned run average and batting average. 
They had the 11th best Net ERA ever.  They were 12th in Net Batting
Average with eight starters batting .300 or more.  They were ninth
in Net Slugging Average and 18th in Net Home Runs.

Outfielder Albert Belle hit 50 home runs and 52 doubles, to become
the first major leaguer to hit 50 homers and 50 doubles in the same
season.  He was among the league leaders in every major hitting
category: first in RBI, doubles, slugging average, home runs, extra
base hits and runs scored.  Kenny Lofton, their fleet center
fielder, led the league in stolen bases and was first in triples. 

Indian closer Jose Mesa was an incredible 46 for 48 in save
opportunities and led the league in saves.  In fact, Mesa alone had
more saves than any other major league team.

Starters Orel Hershiser, Charles Nagy, Dennis Martinez, Chad Ogea
and Ken Hill combined for a 56-21 record.  Martinez was third in
the league in ERA with 3.08 and posted a 12-5 record.  Hershiser
and Nagy led the team with 16 wins each.

The Indians played in 13 extra-inning games in the regular season
and won every one of them.  Including postseason play, the club won
29 games in its last at bat.    



4. 1929 PHILADELPHIA ATHLETICS

Between 1927 and 1932, two of Connie Mack's great Philadelphia
Athletics teams challenged the Yankees for the supremacy of
baseball.

In fact, the 1929 A's were the fourth best team of all time, and
the 1931 Athletics were the ninth best.

The '29 Athletics were ninth in games ahead of second and tied for
tenth in World Series record and 11th in percentage of games won.

Outfielder Al Simmons batted .365 in 1929 and hit 340 home runs. 
He also led the league with 157 runs batted in.  First baseman
Jimmie Foxx batted .354 and hit 33 homers.

Mule Haas in center field hit .313.  Mickey Cochrane, the catcher,
batted .331. 

Lefty Grove, who won 20 and lost 6, led the league in earned run
average with a 2.81.  George Earnshaw led the league in wins with
a record of 24 and 8.




5. 1906 CHICAGO CUBS

This team starred the famous double play combination of
Tinker to Evers to Chance.

The team holds the record for the highest percentage of games
ever won by a modern big league team, .763, which is more than
three of every four games.  And this team holds the fourth-best
record ever for games ahead of the second place team.

     In addition, the team had the second lowest earned run average
in history.  It also had the eighth lowest net earned run average
(the league's ERA less theirs).

First baseman Frank Chance batted .319.  Third baseman Harry
Steinfeldt batted .327 with a league-leading 83 runs batted in.
     The catcher, Johnny Kling, batted .312.
Three Finger Brown led the league in earned run average
with 1.04 and in wins with a record of 26 and 6.  Jack Pfiester was
20 and 8.


6. 1902 PITTSBURGH PIRATES

Only one team won a higher percentage of its games than the 1902
Pirates and only one team finished further ahead of the second
place team than they did - 27 1/2 games.

In addition, the '02 Pirates had the highest net batting average
(their average less the league average) in history.  And they were
also first in net runs, meaning that their runs less their league's
average per team was highest.  They scored 242 more runs in 1902
than the average team in the National League.

The Pirates were also sixth in net slugging average and had the
tenth lowest earned run average in history.

Ginger Beaumont, center fielder, led the league in hitting with a
batting average of .357.  The third baseman, Tommy Leach, led the
league in home runs with 6!

Honus Wagner, who played outfield, short stop, and first base
batted .329 and had a league-leading 91 runs batted in.

Jack Chesbro led the league in wins with a record of 28 and 6.


7. 1936 NEW YORK YANKEES

This edition of the Bronx Bombers holds the record for most runs
scored by a baseball team, 1,065.  (It should be pointed out that
in determining gross measures like runs, we 'normalized' the
numbers for teams that played longer schedules to a 154-game
schedule.  This was the number of games played before expansion.)

The 1936 Yankees were second in net home runs (their homers less
the league average per team).  And they were fourth in net slugging
average (their slugging average less the league's).

They were also sixth in games ahead of the second place team.

The '36 Yanks were also sixth in net runs scored and eighth in both
batting average and net earned run average.

Lou Gehrig led the league in home runs with 49 and he batted .354. 
Catcher Bill Dickey batted .362, still the highest average ever for
a catcher.

This was the great Joe DiMaggio's first year with the Yanks. He
played left field and batted .323.  Jake Powell in center field
batted .306.  Red Rolfe at third base hit .319.  George Selkirk in
right hit .308.

Red Ruffing won 20 and lost 12.



8. 1944 ST. LOUIS CARDINALS

This was a strong mound team.  They had the third best net earned
run average (their ERA less the league ERA) in history.  Thus, even
though they played during World War II, when some of the game's
best players were in uniform, they were far above the league
standard in 1944 in pitching.

The team was led by the great Stan Musial who batted .347. Third
baseman Whitey Kurowski led the team in home runs with 20. Johnny
Hopp in center batted .336.  Walker Cooper hit .317.

Mort Cooper, Walker's brother, won 22 and lost 7.  Max Lanier was
17 and 12.

Almost the same players made up the 1943 Cardinal team which is the
tenth best team of all time and the 1942 team which is the 20th
best.


9. 1931 PHILADELPHIA ATHLETICS

Only five teams won a higher percentage of their games than this
team did.  And only five teams had better net earned run averages.

Al Simmons in left field led the league in batting with a .390
average.  First baseman Jimmie Foxx hit 30 home runs and batted in
120 runs.

Mule Haas in center batted .323.  Catcher Mickey Cochrane batted
.349.

Lefty Grove led the league in wins and earned run average. He was
31 and 4.  Rube Walberg was 20 and 12.  George Earnshaw was 21 and
7.


10. 1943 ST. LOUIS CARDINALS

Only eight teams led the second place team by more games than did
the '43 Cards.  They were also eighth in net batting average.


Stan Musial led the league in batting with .357.  Catcher Walker
Cooper batted .318.

Mort Cooper led the league in wins.  His record was 21 and 8.


END OF PROGRAM

