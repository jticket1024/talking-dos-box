*WSBB News

August, 1993 Volume 3 Number 4
______________________________
Published by Harry H. Hollingsworth
692 S. Sheraton Dr., Akron, OH 44319
Telephone 216 644 2421
_____________________________________________________________

*More Improvements in WSBB On the Way
  Some significant improvements in the 'NEW TEAM' and 'THE
TEAMS' programs have been developed for us by Phil Vlasak of
Iselin, N.J.
  Phil has also made some improvements in the game itself.
All are now being beta tested and will be available in the
WSBB update following the end of the 1993 season.

*Total Baseball: A CD ROM Home Run
By Alan Clive
  'Total Baseball,' edited by John Thorn and Pete Palmer, set
out to combine stats with long, probing essays on nearly
every aspect of the game.  The book, published in 1991, ran
to 2608 oversized and closely-packed pages.  To this massive
combination of text and figures, the compact disk version
adds the multimedia flair of pictures and sounds.
  Let's do a sample search to demonstrate the scope of the
program.  I typed in the name of Hank Greenberg, Detroit's
one-time slugging first baseman.  I received the information
that he is mentioned 67 times in 67 places.  If I wanted to
limit the search with more descriptors, I could do so.  You
can copy text either to a printer or to a computer file.
  What's in the program?  As in the print version, one begins
with an excellent overview of the sport from its earliest
origins to 1991.  Histories of all current or past major
league and many minor league teams follow.  Hundreds of
players receive short but adequate biographical treatments.
Essays appear on just about any aspect of the game that might
be of interest - the fans, radio broadcasting, bubble gum
cards - to name just a few.  Team standings for each season
are provided, together with stats for season leaders in a
number of categories.  You get the box score for each World
Series game, other pertinent statistics, accompanied by a
short essay on each fall classic.
  For me, a great disappointment of the program is the
inability to review in one place all player stats by team for
each year.  This is important to devotees of WSBB, who want
to add their own teams with the 'NEW TEAM' program.
  However, it is possible to put a team together completely
independent of sighted help.
  If you are a baseball fan, and you have a CD ROM drive, you
should not be without Total Baseball.  I bought my copy for
$62.00 from CD ROM, Inc., (303) 231-9373.  The developer, CMC
Research, can be contacted at (503) 241-4351.
  (This is from an article that appeared in Tactic Magazine
by Dr. Clive, Civil Rights Program Manager for the Federal
Emergency Management Agency, Washington, D.C.  In the
article, he also tells of some of the problems of using the
CD.)

*1906 Cubs Win Clive's Superseries 9
  The 1906 Chicago Cubs, led by the famed Three-Finger Brown
and the immortal Tinker to Evers to Chance, won the World
Series in Superseries 9, Alan Clive's continuing competition.
They defeated the 1953 New York Yankees four games to two.
Three of the games went into extra innings.  It was Cubs'
pitching against Yankee hitting, and the Chicago team barely
prevailed.
  Coming into Game 1, the Yankees had a nine-game win streak
going, and they made it ten straight by exploding for four
runs in the top of the tenth to break a scoreless tie for a
4-0 win.  The win tied the Yankees with the illustrious 1927
Bronx Bombers for the most consecutive wins in a Superseries.
  The momentum shifted in Game 2, however, as the Cubs
defeated the Yankees 4-3.
  Game three was another extra-inning affair, won by the Cubs
with two runs in the bottom of the tenth, 6-4.
  The Cubs also won game 4 in extra innings, 3-2 in 15
innings.
  The Yankees rallied when the series shifted to the Windy
City for Game 5, winning 3-1, with each team getting only
five hits.
  The Cubs, however, were not to be denied.  They spotted the
Yankees a two-run lead in the sixth game, then rallied for
a 4-2 victory.
  EDITOR'S NOTE: Winners of Dr. Clive's earlier Super Series
were: 1. 1939 Yankees; 2. 1927 Yankees; 3. 1967 Red Sox; 4.
1912 Red Sox; 5. 1912 Red Sox; 6. 1929 Athletics; 7. 1919
White Sox; 8. 1961 Yankees.

*Pitchers Bat .138
  In 1992, the combined batting average of all National
League pitchers was .138, only slightly higher than the .125
average WSBB assigns to all pitchers.

*Having Problems With Your New Teams?
  Patricia Wise of Fostoria, Ohio, writes that the game does
not announce the names of fielders on outs with the new teams
she has created.  Why?
  It is necessary to have your CAPS LOCK on when you create a
new team.  The program is written completely in capital
letters and the way the computer finds out who the fielder is
is to compare the position with the position of each man on
the team in the field.  If you have the positions in lower
case, there is no match and the computer does not discover
the name of the fielder.
  In the 1993 update of the game, we will include
instructions to use your CAPS LOCK.

*Beware of Memory Resident Programs!
  In April, Scott Williams of Northridge, Calif., bought WSBB
and discovered that the baseball game would not run on his
new computer.
  About 10 transcontinental telephone calls later, we
discovered that the problem was that his computer's memory
was so clogged up with memory resident programs that there
was not room for the large WSBB game program.  By removing
one of the MR programs, everything was okay.
  Memory resident programs are activated by the AUTOEXEC.BAT
file when the computer is booted up.  The problem seems to
occur with DOS 5.0 or later versions of DOS.

*Gary Peterson Out Of Hospital
  Gary Peterson called to report that he is out of the
hospital and now recovering in his mother's home in San Jose,
Calif.
  He had been in the hospital for four months and one week
following the disastrous fire that gutted his apartment in
Santa Cruz.
  He has obtained a new computer to replace the one that was
destroyed in the fire and has gotten back to playing and
checking WSBB.

*Worst Team Loses 20 Straight
  Recently we reported on checking out the best team of all
time, the 1927 Yankees, by having them play all the other
pennant winners in WSBB.  They won 70 and lost 30 in beating
every other pennant winner at least once.
  Now, we have had the worst team of all time, the 1916
Athletics, play the 71 pennant winners.
  Their record: 8 wins, 71 losses, before losing once to
every team.  And, in doing so, they lost their last 20 games
in a row!
  Here are the scores of some of those 20 games: 1953 Dodgers
15, A's 0; 1954 Indians 13, A's 0 (in a no hitter); 1960
Pirates 9, A's 0; 1962 Giants 13, A's 2 - and these were
consecutive games!

*G. Stephenson Likes Canadian Teams
  'When I bought the baseball game a couple of years ago I
was thrilled with it but a little disappointed that there
were no Canadian teams,' Gerry Stephenson of Dundas, Ontario,
wrote.  'Now I am thrilled and very pleased that you have
added two Canadian teams,' he wrote.

*Aussie Likes WSBB But What Is a 'Bunt'
  Mark Curralejo telephoned from Melbourne, Australia, to
report that the WSBB game and information system had finally
reached him (free matter for the blind) and that he was
enjoying the game a lot.
  'My only problem is that I don't understand a lot of the
terminology, which is foreign to us.  For example, what is a
bunt?' he asked.
  We told him to put all the unfamiliar words on an audio
cassette and to send it to us and we would define each term
on a return cassette.

*How To Score Baseball
  Barbara Sheinbein of St. Louis would like to know if anyone
can advise her, as a blind person, how to score baseball
games.  Barbara, an active  member of the Society for
American Baseball Research (SABR), is really into baseball.
If you can give her any advice, write to her at 1033 Kinstern
Drive, St. Louis, Mo. 63131.

*Jack Every Tells of 'Most Memorable Game'
  Jack Every of Exeter, R.I., wrote recently, 'I thoroughly
enjoy the baseball game.'
  He described the most memorable game he has played.  It was
between the '69 Mets and '69 Orioles - Jim Palmer against Tom
Seaver.  Palmer pitched 11 perfect innings and the game was
scoreless until the 14th when pitcher Seaver hit a home run
in the top of the inning.  Frank Robinson hit a homer in the
bottom of the inning to retie the game.  But, in the top of
the 15th, the Mets scored two more runs to win the game.

*'I Still Enjoy Playing WSBB' - Bob McClain
  'Yes, I do have a family now and am involved in other
things, but I still enjoy playing the game as much as I did
the first time I played it - even more so now with all the
changes and additions - so I'll always find time to play
World Series Baseball,' wrote Robert McClain of Philadelphia
recently.

*Dennis Helms Likes Game
  'I have enjoyed playing the game very much,' Dennis Helms
of Roanoke, R.I., wrote in ordering the 1992 update to WSBB.

*Are Ironmen A Thing of the Past?
  Only one American Leaguer played in every game that his
team played in 1992: Cal Ripken, Jr. of Baltimore.
*END
 