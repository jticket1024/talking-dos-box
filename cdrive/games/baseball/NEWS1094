*WSBB NEWS
Volume 4 Number 5   October 1994
_______________________________                         
Published by Harry H. Hollingsworth                      692 S.
Sheraton Dr., Akron, OH 44319                     Telephone 216
644 2421
_______________________________________________________
WORLD SERIES EDITION
________________________________

*We Can Run Our Own World Series
 
 So, the owners and players have brought the grand old game to a
halt.    Forget them.  We can run our own World Series with
Version 9 of WSBB, the update that will be ready for mailing at
the end of October.
  As always, there will be some significant improvements in the
game, itself, and all the information programs will be updated.
  With the update will come the '94 Yankees and '94 Expos, who
were leading their leagues when everything stopped.  Also, the '94
allstar teams will be provided, as well as some other surprises.
  The game will feature a lot of new language, making it seem more
than ever that you are listening to a radio broadcast of the game
by one of the great announcers - Red Barber, Mel Allen, Rosie
Rosewell, Harry Caray, Lindsay Nelson, etc.
  As was the case last year, Phil Vlasak of Iselin, N.J. has
helped in providing many of the improvements, but they are based
upon suggestions of many of the users of WSBB
in 47 states and four foreign countries.
  As always, the cost of the update is only $5.  Price for new
users is $15.  Send your checks to Harry Hollingsworth at the
address shown on page 1.

*Every and Heatwole All-Star Teams To Clash
  Jack Every of Exeter, R.I., called to report, among other
things, that he and his friend, Galen Heatwole of Ellicott City,
Md., are preparing all-star teams of their two favorite
franchises, the Red Sox (Every) and the Orioles (Heatwole).  They
plan to get together for an all-out battle on WSBB. 
    Every added that he really enjoys playing WSBB.


*Two New All-Rookie Teams
  Robert McClain of Philadelphia likes to make up new teams to
play against the pennant-winning teams in WSBB.  Now he has come
up with two teams made up of the rookies of the year in the
National and  American leagues.  They are included with the 1994
update.

*Suggestions for WSBB Keep Coming In
  The WSBB game reflects suggestions from dozens of sight-impaired
baseball fans with computers.  And suggestions keep coming in.
  Recently, we received long letters from J. J. Meddaugh of
Saginaw, Mich., and Larry Vella of Allen Park, Mich., with many
excellent suggestions, some of which we have already incorporated.

*Hollingsworth's Book Is Out
  Harry Hollingsworth's new book, The Best and Worst Baseball
Teams of All Time - From the '16 A's to the '27 Yanks to the
Present (SPI Books, New York City) is now being distributed
throughout the U.S. by the ICD/Hearst and by General Publishing in
Canada.  A 211-page paperback, it sells for $5.50 in the U.S. and
$6.50 in Canada.
  Much of the math that went into the WSBB game was developed
while this book, a computer study of the best and worst teams, was
being researched.
  We are making the book available in disk form to WSBB users for
$3.

*Hey! So They Won't Play - We Can Simulate Them
  Those grasping owners and players may have brought on a strike
of our favorite sport, but so what?  With WSBB we can simulate the
rest of the season for our favorite team.
  I started with my favorites, the Indians, and here is the lead
of the story:
  'Led by Albert Belle's amazing exhibition of long range hitting,
the Cleveland Indians took two of three games from the Milwaukee
Brewers and then swept three from the Oakland Athletics
 In the first six scheduled games following the beginning
of the strike.' 
In the six games, Belle had 15 hits including six home runs, and
18 runs batted in.
  'In the second game against the Brewers, he went wild, with
three home runs one with the bases loaded, one with two men on and
one with one on, giving him nine runs batted in.  The Indians won
the game 14-2.
  It is interesting to note that many newspapers are running
computer-simulations of all the scheduled games each day.        

*One Million Five Hundred Thirty Seven Thousand Games in 19 Hours!
  We have just acquired a computer program that permits you to
play games with every major league team that has played since 1901
(through 1993).  There have been 1,754 teams.
  And you can have every team play every other team and determine
how many games is won by each team of the 1,753 teams it plays. 
That involves a total of  1,537,381 games.
  It took our 386-SX 20 hours running continually to do it.  (The
instructions said that a 486-33 could do it in three hours.)
  The games are run on team statistics and no individual players'
statistics are used.  
  Unfortunately, it is necessary to have a sighted person run the
program for you.
  Its final results are very interesting but not as accurate as a
game such as WSBB, using individual players, is.
  Here are the Top 10 teams and the percentage of all games won by
each team:
 1. 1939 Yankees .747
 2. 1936 Yankees .739
 3. 1948 Indians .725
 4. 1929 Athletics .723
 5. 1927 Yankees .712
 6. 1930 Giants .705
 7. 1937 Yankees .705
 8. 1938 Yankees .701
 9. 1930 Cardinals .697
10. 1930 Dodgers .697
  Here are the 10 worst teams:
 1. 1904 Senators .200
 2. 1906 Braves .211
 3. 1909 Braves .217
 4. 1908 Yankees .218
 5. 1910 Browns .219
 6. 1904 Braves .220
 7. 1909 Senators .224
 7. 1905 Braves .224
 9. 1905 Dodgers .225
10. 1906 Red Sox .229
  All of the worst teams played in the first decade of the
century.    The 1916 Athletics, which we have identified as the
worst team of all time, was 19th worst according to this
simulation.
  The program is available from  Zosafarm Publications, RR1 Box
250,  Levant ME 04456 for $25.    For the $25 you get two
programs, Baseball Simulator and Ultimate Baseball League plus the 
data for the 1,754 teams.  With them, you can also play any  major
league team that ever played.  

*Blind Baseball Announcer
  Phil Vlasak sent us a report on Donald Wardlo, a blind radio
announcer of games played by the New Britain (Conn.) Red Sox in
the AA Eastern League.  This report was from The Technical
Innovations Bulletin.
  Don's sighted partner, Jim Locus, handles the play-by-play
announcing while Don handles the color.  Every morning Jim gets
up-to-date statistics about the teams playing that day and reads
them into a tape recorder for Don.  Don then brailles them and
shows up for games with 30 pages of brailled notes.
  They sell local commercials for their broadcasts, charging $10
for an announcement, but only when the New Britain Red Sox win. 
In 1993, the Red Sox won only 52 games and lost 88 so the
advertisers got a real bargain.
  EDITOR'S NOTE: My son and I attend games of the Canton-Akron
Indians, which also play in the Eastern League.  

*Record Your Games
  A reminder.  It is possible to record every game you play, play
by play, in an ASCII file in your Baseball directory by activating
the game by typing, while in the DOS mode, 'BASEBALL>REPORT.  When
the game is over, you will find in your Baseball directory an
ASCII file entitled, 'REPORT', which will contain every word that
appeared on the screen during the game.  You can read the file
with the DOS TYPE command, or with a word processing program, or
with a special reading program such as HTYPE or READ.
  It would be particularly valuable to create these files if you
are competing in a WSBB game with someone who lives elsewhere, as
some users are doing.
  Whoever actually plays the game, following his opponent's
instructions for managing the opposing team, can create such a
report and send it to his opponent, who can then review the entire
game to see exactly what happened.

*Clyde Stout Gospel Singer Likes WSBB
  Clyde Stout of Anneheim, Calif. wrote to say that he is really
enjoying WSBB.  He learned about it from a bulletin board
  'Of course, you realize that it cannot be compared with playing
the real game out on the field, but for us who never will it's the
closest thing to the real thing that we will ever know.'
     Clyde sings professionally as a gospel singer.
*END




  

