WORLD SERIES BASEBALL SPECIAL EDITION
 VERSION ELEVEN AND TWELVE
PRODUCED BY
HARRY H. HOLLINGSWORTH,
AND
PCS Games
WRITTEN BY
HARRY HOLLINGSWORTH,  PHIL VLASAK AND CARL MICKLA
SOUNDS BY
PHIL VLASAK
COPY WRITE 1998

INTRODUCTION:
A COMPUTER BASEBALL GAME FOR VISUALLY IMPAIRED PEOPLE.
THIS GAME HAS REAL BASEBALL SOUNDS DESIGNED TO PLAY THROUGH EITHER
A SOUND CARD OR THE PC SPEAKER.

GETTING STARTED:
THIS GAME IS AN UPDATE TO WORLD SERIES BASEBALL VERSION ELEVEN AND
TWELVE.
YOU WILL NEED ABOUT SIX MEGABYTES OF SPACE TO INSTALL THIS
GAME ON YOUR HARD DISK C  IN THEe C:\BASEBALL FOLDER.
This game was written for the DOS included with Windows 95 and 98.
This is a DOS game and works best if you are in DOS using a DOS screen reader and hardware synthesizer.
To install from Windows:
1. From the CD which contains the game hit enter on 
INSTALL World series baseball special edition 98.BAT 
This will install your game in the 
"C:\World series baseball special edition 98 FOLDER
2. Exit to the MS-DOS prompt
3. type:
CD "c:\World series baseball special edition 98"
Then hit the enter key.
4. Type MENU 
Then hit the enter key.
This will start the game.

To install from DOS:
1. From the CD which contains the game hit enter on 
INSTALL World series baseball special edition 98.BAT
This will install your game in the 
"C:\World series baseball special edition 98 FOLDER
2. type: CD "C:\World series baseball special edition 98
then hit the enter key.
3. Type MENU
then hit the enter key.
This will start the game.

THE GAME WILL BEGIN WITH THE MAIN MENU SCREEN.
THE MAIN MENU LISTS ONE CHOICE AT A TIME.
YOU CAN USE THE UP AND DOWN ARROW KEYS TO CYCLE THROUGH THE
CHOICES, THEN HIT THE ENTER KEY TO PICK ONE.
OR YOU CAN HIT THE LETTER INDICATED NEXT TO EACH CHOICE TO QUICKLY
PICK THAT CHOICE.
FOR EXAMPLE, WHEN THE MAIN MENU APPEARS, THE G BASEBALL GAME CHOICE
IS ON THE SCREEN.  YOU CAN EITHER HIT THE ENTER KEY OR THE G KEY TO
PLAY THE GAME, BUT YOU CAN ALSO HIT I TO READ THE INSTRUCTIONS.
THE VERSION ELEVEN SPECIAL EDITION CAN BE RUN FROM THIS NEW MENU BY
USING THE UP AND DOWN ARROW KEYS UNTIL YOU READ:
VER. 11 WITH HIT SOUNDS.
IF YOU WANT TO PLAY THE TWO VERSION TWELVE GAMES:
USE THE UP AND DOWN ARROW KEYS UNTIL YOU READ:
VER. 12 WITH NEW SOUNDS OR
VER. 12 WITH OLD SOUNDS
YOU CAN ALSO USE THE LETTER HOT KEYS TO PLAY THESE GAMES.
HIT V FOR VER. 11 WITH HIT SOUNDS,
HIT O FOR VER. 12 WITH OLD SOUNDS AND
HIT U FOR VER. 12 WITH NEW SOUNDS.

THE G CHOICE SHOWS WHICH OF THE THREE VERSIONS OF THE BASEBALL GAME
YOU PLAYED LAST.

WHAT IS THE DIFFERENCE?
FOR THE FIRST ELEVEN YEARS, THE WORLD SERIES BASEBALL GAME HAS BEEN
USING SYNTHESIZED SOUNDS TO PLAY THE NATIONAL ANTHEM, TAKE ME OUT
TO THE BALL GAME AND CHARGE MUSIC.
THE SOUND COMES FROM THE PC SPEAKER THAT WAS DESIGNED TO 'BEEP'. 
WITH VERSION 12 WORLD SERIES BASEBALL HAS REMOVED THE SYNTHESIZED
SOUNDS AND HAS REPLACED THEM WITH REAL RECORDED DIGITAL SOUNDS.
THIS TAKES MORE MEMORY THAN THE OLD SYNTHESIZED SOUNDS, SO SOME
PEOPLE WITH LIMITED LOWER MEMORY IN THEIR COMPUTER MAY NOT BE ABLE
TO PLAY THEM.
THIS IS WHY THE OLD SOUND VERSION IS STILL IN VERSION TWELVE.
THE SPECIAL EDITION GOES ONE STEP FARTHER, IN TRYING TO RECREATE
ALL THE REAL SOUNDS OF A BALL GAME INCLUDING,
BAT HITTING BALL, BALL HITTING GLOVE, AND CROWD CHEERS.
THIS TAKES EVEN MORE MEMORY THAN EITHER VERSION TWELVE GAMES.
IT WOULD BE GREAT TO ONLY HAVE ONE VERSION OF THE GAME THAT WOULD
WORK ON EVERYONE'S COMPUTER BUT THAT IS NOT POSSIBLE.
IN HAVING SEVERAL VERSIONS, THERE SHOULD BE AT LEAST ONE THAT
EVERYONE CAN PLAY.
TO MAKE VERSION ELEVEN SPECIAL EDITION COMPATIBLE WITH VERSION
TWELVE WE RENAMED THE FOLLOWING FILES:
WSBBS.EXE IS THE SPECIAL EDITION GAME.
SOUND.EXE IS THE SPECIAL EDITION SCREEN AND SOUND ADJUSTMENT
PROGRAM.


SOUND SETTINGS
TO CHANGE THE CURRENT SETTING OF THE SOUND, TYPE Z FROM THE MAIN
MENU. THEN TYPE M FOR MUSIC.
YOU CAN CHANGE THE MUSIC AND SOUND IN ONE OF FIVE WAYS:
 (1) SOUND CARD.
 THE SOUND EFFECTS WILL BE PLAYED BY YOUR SOUND CARD ONLY.  
MANY OF TODAY'S COMPUTERS COME WITH A SOUND CARD THAT PLAYS THE
SOUNDS IN THIS GAME THE WAY THEY WERE DESIGNED TO BE HEARD. 
THIS SETTING GIVES YOU THE BEST PLAYBACK OF THE MUSIC IN THE GAME.
 (2) PC SPEAKER.
 THE SOUND EFFECTS WILL BE PLAYED ONLY BY THE PC SPEAKER.
WHICH COMES WITH EVERY COMPUTER.  THIS SETTING IS CAPABLE OF
PLAYING THE FIRST SIX SECONDS OF EACH SONG IN THE GAME.
 (3) PC OR SOUND CARD. 
THIS SETTING DETECTS IF YOU HAVE A SOUND CARD AND WILL PLAY THE
SOUND EFFECTS THROUGH IT IF IT EXISTS.  OTHERWISE IT WILL USE THE
PC SPEAKER.
THIS IS THE        CURRENT SETTING AFTER INSTILLATION.
 (4) PC OR SOUND CARD SMALL. 
THIS SETTING IS FOR THOSE OF YOU WHO HAVE COMPUTER MEMORY PROBLEMS
 (5) NO MUSIC OR SOUNDS
 THE SOUND EFFECTS WILL BE TURNED OFF.
RIGHT AFTER YOU CHOOSE A SOUND SETTING OTHER THAN FIVE, YOU WILL
HEAR A MAN'S VOICE  SAYING, TAKE OFF.
IF YOU DON'T HEAR THIS, TYPE N AND CHOOSE ANOTHER SOUND SETTING.

SOUND DELAY
YOU CAN ADJUST THE DELAY THAT COMES WITH THE SOUNDS BEING PLAYED. 
FROM THE MAIN MENU CHOOSE Z  THEN CHOOSE D
FOR DELAY.
YOU CAN CHOOSE A 0, 1, OR 2 SECOND DELAY ON THE PLAYING OF THE
SOUNDS.
SLOWER COMPUTERS MAY NEED THE 0 SETTING TO SPEED UP PLAYING THE
SOUNDS.
ONE SECOND IS THE DEFAULT THAT THE PROGRAM IS SET FOR.
SOME FAST COMPUTERS MAY HAVE TO SLOW DOWN THE PLAYING OF THE SOUNDS
WITH A TWO SECOND DELAY.

THE SPECIAL EDITION HAS THE FOLLOWING CHANGES:
IN ORDER TO MAKE ROOM FOR THE SOUNDS IN THE BASEBALL GAME, WE
REMOVED THE 40 CHARACTER WIDTH SCREEN SETTINGS IN THE SOUND
VERSION OF THE GAME.  THE 40 CHARACTER SETTINGS WILL STILL WORK ON
THE OTHER PROGRAMS.
CHANGED MANAGER'S OPTIONS.
WHEN YOU TYPE H OR Y FROM WITHIN THE GAME YOU WILL GET A SCREEN
LISTING ONE CHOICE AT A TIME.
YOU CAN USE THE UP AND DOWN ARROW KEYS TO CYCLE THROUGH THE
CHOICES, THEN HIT THE ENTER KEY TO PICK ONE.
BECAUSE THE SOUND PLAYING TAKES TOO MUCH ROOM IN THE PROGRAM, WE
COULD NOT ADD SOUNDS TO THE GAME WHICH YOU PLAY AGAINST THE
COMPUTER AND THE 27 YANKEES.

WORLD SERIES BASEBALL GAME AND INFORMATION SYSTEM INTERNET SITE
YOU CAN VISIT THE WSBB WEB SITE AT:
USERS.DELTANET.COM/~TDB/WSBB/
BELOW IS SOME INFORMATION FROM THE SITE.
WELCOME TO HOME PLATE!  PUT ON YOUR CLEATS AND BROWSE AROUND THE
ONLY OFFICIAL WEB SITE FOR THE GREAT TALKING AND REALISTIC SOUND
GAME OF WORLD SERIES BASEBALL.
     THE WORLD SERIES BASEBALL GAME AND INFORMATION SYSTEM IS BEING
USED BY SIGHT-IMPAIRED MEN AND WOMEN IN 48 STATES AND FOUR FOREIGN
COUNTRIES, MANY OF WHOSE SUGGESTIONS HELPED MAKE THE GAME WHAT IT
IS TODAY.
IT IS A BASEBALL GAME THAT SIMULATES THE REAL THING.  YOU MAKE THE
MANAGER'S DECISIONS: WHEN TO PINCH HIT, CHANGE THE PITCHER,
SACRIFICE, STEAL, PURPOSELY PASS THE BATTER, MOVE THE INFIELD IN,
ETC.  THE PROBABILITIES HAVE ALL BEEN CAREFULLY CALIBRATED TO MAKE
THEM MATCH REAL WORLD SERIES PLAY.

THE GAME COMES WITH OVER 130 TEAMS, INCLUDING MANY OF THE BEST OF
ALL TIME, TWO ALL-TIME ALL-STAR TEAMS, TWO CHAMPION NEGRO TEAMS AND
A
CHAMPION JAPANESE TEAM.  YOU CAN CREATE AS
MANY ADDITIONAL TEAMS AS YOU WOULD LIKE.
THE PLAY BY PLAY REPORT THROUGH YOUR SYNTHESIZER IS LIKE LISTENING
TO A WORLD SERIES GAME ON THE RADIO, WITH ALL THE ANNOUNCER'S
PATTER.
IN A SECOND VERSION OF THE GAME, YOU PLAY AGAINST THE GREATEST TEAM
OF ALL TIME, THE 1927 NEW YORK YANKEES, WITH THE COMPUTER MANAGING
THE YANKS.
THE SYSTEM HAS A MENU AND COMES WITH A NUMBER OF PROGRAMS TO
FACILITATE PLAYING THE GAME, INCLUDING A SPECIAL PROGRAM HELPING
YOU TO RUN DOUBLE-ELIMINATION TOURNAMENTS.
A WHOLE SERIES OF BASEBALL INFORMATION PROGRAMS ARE
PROVIDED.  THEY INCLUDE:
ALL THE MEMBERS OF THE BASEBALL HALL OF FAME.
THE IMPORTANT SEASON AND LIFETIME RECORDS.
THE BATTING AND PITCHING LEADERS EACH YEAR.
A DETAILED HISTORY OF BASEBALL.
A LIST OF THE TOP 50 TEAMS OF ALL TIME.
A MULTIPLE-CHOICE BASEBALL QUIZ.
ALL THE BACK ISSUES OF WSBB NEWS.

IN DECEMBER 1991, HARRY HOLLINGSWORTH, THE GAME'S CREATOR,
WAS PRESENTED WITH A CERTIFICATE OF MERIT AWARD BY JOHNS HOPKINS
UNIVERSITY FOR THE GAME.  IT HAS BEEN REVIEWED IN ARTICLES IN
DIALOGUE, NEWSBITS, NEWSREEL, TACTIC, COMPUTER FOLKS AND VIDPI
VIEWS MAGAZINES.  SUGGESTIONS INCORPORATED IN THE GAME HAVE BEEN
MADE BY DOZENS OF SIGHT-IMPAIRED PEOPLE.

AMONG CURRENT USERS OF THE GAME ARE PEOPLE RANGING IN AGE FROM 15
TO 80 AND IN OCCUPATIONS FROM STUDENT TO COLLEGE PROFESSOR, JUDGE,
LAWYER, MINISTER, PSYCHOLOGIST, AND COMPUTER PROGRAMMER.

HARRY HOLLINGSWORTH FIRST PROGRAMMED THE GAME AFTER TAKING
DISABILITY RETIREMENT FROM THE FIRESTONE TIRE & RUBBER CO., WHERE
HE HEADED UP SALES FORECASTING AND MANAGEMENT RESEARCH DURING A
32-YEAR CAREER.  AS A BOY HE SIMULATED MAJOR LEAGUE PLAY WITH
CHEWING GUM BASEBALL CARDS AND A HOMEMADE SPINNER.  AS HIS THREE
SONS GREW UP, HE INVENTED A BASEBALL GAME FOR THEM USING DICE TO
DETERMINE PROBABILITIES.  SOME OF THE SAME LOGIC WENT INTO HIS
COMPUTER BASEBALL GAME.  

The creater of WSBB, 
HARRY H. HOLLINGSWORTH, passed away December 4, 2002
His address was,
692 SOUTH SHERATON DRIVE, 
AKRON, OHIO 44319

Thank you for your time and support and if you
would like to give us some feedback on this or any other topic feel
free to contact us in any format.
Carl Mickla and Phil Vlasak

PCS Games
666 Orchard Street
Temperance, MI 48182
Phone: (734) 850-9502
email: phil@pcsgames.net
Web site:
www.pcsgames.net

