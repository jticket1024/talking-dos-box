
                      THE HISTORY OF BASEBALL
                      _______________________

NOTE: In this chronology, we attempt to pick up the significant
developments in the history of baseball as well as cover the
significant rules changes as they ocurred.

17th Century - People in England played "rounders", a forerunner
of baseball.  in rounders, the fielders threw the ball at the
runner and if it hit him when he was off base, he was out.

18th Century - American colonists in New England played rounders.

1830
1830-1850 - Practice of throwing the ball at the runner changed to
tagging him out.

1839 - Legend has it that Abner Doubleday invented baseball in
Cooperstown, N.Y.  Doubleday later became an U.S. Army  Civil War
general.  Historians now believe that Doubleday had little to do
with the invention of baseball.



1840
1845 - Alexander Cartwright, a sportsman in New York City, started
a club whose sole purpose was playing baseball.  He is now
regarded as the father of organized baseball.  It was called the
Knickerbocker Club of New York.  A set of baseball rules were
written.  They were modified in 1848 and 1854 and did much to make
baseball what it is today.  The 1845 rules set the distance
between bases at today's 90 feet and provided for nine players on
a side.
    
1845 - The rules were revised to provide for tagging first base to
put a runner out on a ground ball.

June 19, 1846 - The New York Knickerbocker Club met the New York
Nine in the first game involving two organized teams.  It was
played at the Elysian Field, Hoboken, N.J.  The New York Nine won
23-1.  The rules provided that the first team with 21 runs or more
at the end of an inning won the game.

1850
1854 - The rules were changed to provide for force outs.

1857 - The rules were changed to provide that the team with most
runs after nine innings won the game.

1860
1861-1865 - The Civil War helped to spread baseball to all parts
of the country.  Union soldiers often played it for recreation.
Other Union soldiers and Confederate prisoners watched them.

1864 - The rules were changed so that fair balls
caught after one bounce were no longer outs.

1865 - Following the Civil War, people in cities and on farms in
all parts of the country started to play baseball.

1868 - Rules were changed to provide for called strikes.  Until
then strikes were counted only when the batter swung and missed the
ball.

1869 - The Cincinnati Red Stockings decided to pay all their
players, thereby becoming the first professional team.  Many other
teams then turned professional.

1870
1876 - Eight professional teams formed the National League, the
first major league.

1877 - Rules changed to provide for bases on balls for the first
time.  Batters walked after nine balls.

1880
1880 - Base on balls on eight balls instead of nine.
Rule adopted making runner out if hit by batted ball.  Rule
adopted requiring catcher to catch the ball on the fly to retire
a batter on a strike out.

1881 - Distance from pitcher to home plate increased from 45 feet
to 50 feet.

1882 - Base on balls on seven balls.

1883 - The rules were changed so that foul balls caught after one
bounce were no longer outs.

1884 - Pitchers were permitted to throw overhand for the first
time.  (Until then all pitching had to be underhand.) Base on
balls on six balls.

1885 - Rule adopted that game counts if five innings are
completed.  Rule changed permitting one side of the bat to be flat.

1886 - Back to base on balls on seven balls.  Rule permitting
umpires to require players to look for a lost ball for 5 minutes
before play began again dropped.

1887 - Batter no longer allowed to call for high or low pitch.
Batter hit by pitched ball entitled to first base and not
charged with a time at bat.  Base on balls on five balls.  Base on
balls counted as a hit and batter is charged with a time at bat.
Batter gets four strikes if first third strike is a called strike.

1888 - Rule changed so that walk is not a hit and a time at bat.
Also, batter is out after three strikes even if called.

1889 - Present rule that batter walks after four balls was
adopted.  Sacrifice recognized but batter is charged with a time
at bat.

******************************************************************
Through these years the rules were constaNtly being changed as to
whether a pitcher should be charged with an error for a wild pitch,
base on balls, hit batsman, or balk and whether he should be given
an assist for a strikeout.  Also, the rules were changing as to
whether a catcher should be charged with an error for a passed
ball.
******************************************************************

1890
1891 - The rules were changed to provide that any player could be
substituted for but that he could not reenter the game if he had
been substituted for. Before this the rules permitted substituting
only as a result of an injury with the permission of the opposing
team.

1893 - Distance the pitcher stood from home plate was increased to
the present 60 feet 6 inches.  The rules were also changed to
require that bats be completely round.  Until then it was
permissible to use a bat that was flat on one side.

1894 - The rules changed to provide that if a batter is out while
advancing a runner with a bunt, he is not charged a time at bat. 
(This rule was not used uniformly until 1897.)

1894 - Rules changed to charge a batter with a strike for a foul
bunt.

1895 - The infield fly rule was adopted.  Also, the rules were
changed to charge a strike to a batter for a foul tip.

1897 - The concept of the "earned run" was introduced.

1898 - The stolen base was redefined to the present definition. 
(Prior to that runners were given stolen bases for advancing more
bases than justified by the hit on which they were advancing; e.g.
two bases on a single.)

1900
1900 - Eight teams organized the American League and it became the
second major league in 1901.

1900 - Beginning of the modern era of major league baseball.  Most
of the rules were about the same then as they are today.

1900 - Size of home plate changed from 12-inch square to a
five-sided figure 17 inches wide.

1901 - The National League revised its rules to provide for foul
balls being counted as strikes unless there were already two
strikes on the batter.  Until then, fouls were not counted.

1902 - St. Louis Browns replace Milwaukee Brewers in the American
League.  Chicago (NL) Orphans changes name to Cubs.

1903 - New York Highlanders replace Baltimore Orioles in the
American League.  Boston (AL) changes name from Somersets to
Pilgrims.

1903 - The American League began to count foul balls as strikes. 
New rule stated that pitchers' mound could be no more than 15
inches high.

1904 - Chicago (AL) changes name from White Stockings to White Sox.

1905 - Cleveland (AL) changes name from Blues to Naps.

1907 - Boston (AL) changes name from Pilgrims to Red Sox. Boston
(NL) Beaneaters renamed Doves.

1908 - Sacrifice fly rule adopted.

1909 - Rules changed to make a foul while bunting on the third
strike a strike out.

1910
1911 - Boston (NL) Doves become Rustlers.  Brooklyn (NL) changes
name from Superbas to Dodgers.

1912 - A new rule on earned runs adopted.  Earned runs did not
include runs scored with the aid of stolen bases.

1912 - Boston (NL) Rustlers renamed Braves.

1913 - New York (AL) Highlanders become Yankees.

1914 - Brooklyn (NL) changes name from Dodgers to Robins.

1915 - Cleveland (AL) changes name from Naps to Indians.

1917 - Earned run rule changed to count runs scored with the aid of
stolen bases.

1919 -The Cincinnati Reds beat the Chicago White Sox in the World
Series.  The next year eight of the White Sox players were accused
of throwing the series for gamblers and Baseball Commissioner
Kennesaw Mountain Landis banned all eight from baseball for life. 
This scandal was known as the "Black Sox
scandal."  Landis was the first commissioner, appointed in 1920.

1920
1920 - Babe Ruth joined the New York Yankees and a more lively
ball began to be used.  In the 1920s Ruth hit 50 or more home runs
four times, including a record 60 in 1927.

Before Ruth, no player had ever hit more than 24 in a season.
Ruth had a record 714 when he retired in 1935.  Ruth's success
helped to change baseball strategy.  Many more batters became full
swingers.  Before then most had been place hitters.

August 16, 1920 - Ray Chapman, short stop for the Cleveland
Indians, was hit
in the head by a pitch thrown by Yankee pitcher Carl Mays.  He died
the next day and is the only player ever killed during a major
league game.

1920 - The spit ball and other unorthodox deliveries were
abolished.  Each team was permitted to designate to "spit ball
pitchers" for 1920 and after 1920 no spit balls were to be allowed.

1920 - Rules changed to provide that if a batter hit a home run in
the last of the ninth or of an extra inning, all the men on base
score as well as the batter even if all the runs are not needed to
win the game.

1920 - The 'Run Batted In' was adopted as an official statistic.

1921 - Eight National and nine American League pitchers were named
as "spit ball pitchers" and allowed to use the spit ball for the
rest of their careers.

1921 - The first baseball game was broadcast over the radio on
August 5 on KDKA, Pittsburgh.  The Philadelphia Phillies were
playing the Pittsburgh Pirates.  The broadcaster was Harry Arland.

1923 - Slugging Average was adopted as an official statistic in the
National League.  (It was not adopted by the American League until
1946.)

1924 - Rogers Hornsby batted .424 for the St. Louis Cardinals,
still a modern record.

1920s - Many radio stations began to broadcast games.  Play-by-
play accounts reached millions of people.

1926 - Sacrifice fly rule adopted providing that the batter is not
charged with an at bat when a runner is advanced by his fly out.

1926 - Rules changed to state that a pitcher is not credited with
a strike out when the batter reaches first base on a strike out.

June 29, 1929 - Only one ball was used in a major league game for
the last time, in a game between the Cubs and Reds.

1930
1931 - A ball that bounces over the fence or through the fence is
a groundrule double.  Until now it was a home run.

1931 - Sacrifice fly rule discontinued.

1932 - Brooklyn (NL) Robins become Dodgers.

July 6, 1933 - The first all star game was played in Comiskey
Park, Chicago.

1934 - A new rule adopted requiring both leagues to use the same
ball.

June 24, 1935 - The first night game was played in major league
baseball at Crosley Field, Cincinnati, between the Reds and the
Philadelphia Phillies.

1936 - Boston (NL) changes name from Braves to Bees.

1939 - The Hall of Fame in Cooperstown was opened.

1939 - Rules changed still again reinstituting the sacrifice fly
rule but only when a mam scored from third on the fly out.

The rules for how many at bats a batter must have to qualify for
batting titles and how many innings a pitcher must pitch to qualify
for a pitching title as well as how many games a player had to be
in to qualify for a fielding title were constantly being changed
through these years.

1940
1941  - Joe DiMaggio hit in 56 consecutive games.  Ted Williams hit
.406, the last time any player hit over .400.

1941 - Boston (NL) Bees name changed to Braves.

1942-1945 - With many of the stars away in the armed services
during World War II, the 16 teams played with men who were too old
or too young or physically unable to serve in the services.

1944 - Philadelphia (NL) Phillies renamed Blue Jays.

1945 - Sacrifice fly rule again reinstituted for flies scoring
runners.

1946 - Post-war baseball attendance soared and many teams started
televising games.

1946 - Philadelphia (NL) Blue Jays renamed Phillies.

1947 - Jackie Robinson became the first black player in the major
leagues when he joined the Brooklyn Dodgers.

1948 - Satchel Paige, a star of the Negro leagues,  joined the
Cleveland Indians.  He became the oldest man ever to play in the
majors.

1949-1953 - The New York Yankees set a record by winning five
consecutive World Series under Manager Casey Stengel.

1950
1950 - The strike zone was redefined as "from the arm pits to the
top of the knee."

1951 - The first true encyclopedia of baseball was published.

1952 - Women were officially banned from organized baseball.

1953 - The Boston Braves moved to Milwaukee.  It was the first
National League franchise shift since 1900 and the first major
league shift since 1903.  Cincinnati (NL) Reds change name to
Redlegs.

1954 - The St. Louis Browns moved to Baltimore, the first American
League shift since 1903 when the New York Highlanders replaced
the Baltimore Orioles.

April 1954 - Baseball rules were changed to prohibit the previous
practice of leaving fielders' gloves on the field when a team came
up to bat. 

1955 - The Philadelphia Athletics moved to Kansas City.

1957 - Washington (AL) changes name from Nationals to Senators.

1958 - The Brooklyn Dodgers moved to Los Angeles and the New York
Giants moved to San Francisco.

1959 - Rules changed to provide for minimum distances down the left
field and right field lines of 325 feet in new ball parks and a
minimum of 400 feet to center.

1959 - Cincinnati (NL) Redlegs renamed Reds.

1960
1961 - The Washington Senators moved to Minneapolis-St. Paul and
became the Minnesota Twins.

1961 - The American League expanded to 10 teams, adding the Los
Angeles Angels And the new Washington Senators.  The number of
games played by each team was increased from 154 to 162.

1961 - Roger Maris, New York Yankee  outfielder, hit a record 61
home runs.  However, he did not best Babe Ruth's record of 60
until after playing 154 games.

1962 - The National League expanded to 10 teams adding the Houston
Colt 45s and the New York Mets.  The number of games was increased
from 154 to 162.

1963 Batter's strike zone redefined to "from the top of the
shoulder to the bottom of the knee."

1965 - Houston (NL) Colt 45s renamed the Astros.  Los Angeles
(AL) Angels become California Angels.

1965 - Sandy Koufax of the Los Angeles Dodgers became the first
pitcher to pitch four no hitters.

1965 - The first indoor baseball park, the Astrodome, opened in
Houston.  Artificial grass was introduced in 1966.

1966  - The California Angels moved to Anneheim.  The  Milwaukee
Braves moved to Atlanta.

1968 - The Kansas City Athletics moved to Oakland.

1968 - Major League batting averages averaged .237, the lowest
ever.

1969 - The strike zone was redefined again, this time "from the arm
pit to the top of the knee." Pitchers' mound lowered to 10 inches.

1969 - Both leagues expanded to 12 teams and division play
started.  The American League added the Kansas City Royals and the
Seattle Pilots.  The National League added the Montreal Expos and
the San Diego Padres.

1969 - The 'Save' became an official statistic.

1969 - The first complete set of major league records was published
by MacMillan as "The Baseball Encyclopedia."

1970
1970 - The Seattle Pilots moved to Milwaukee and became the
Milwaukee Brewers.

1972 - The Washington Senators moved to Dallas-Fort Worth and
became the Texas Rangers.

1972 - First baseball work stoppage - 13 days - 86 games lost -
first general player strike in baseball history.

1973 - A rule was adopted defining 'saves'.  It was modified in
1975.

1974 - Hank Aaron broke Babe Ruth's lifetime home run record of
714.

1974 - Frank Robinson became the first black manager in the major
leagues, at Cleveland.

1975 - The American League adopted the designated hitter rule as a
permanent rule.

1977 - The Seattle Mariners and Toronto Blue Jays were added to
the American League.

1978 - "Game winning RBI" added to official scoring.

1980
1980 - Baseball work stoppage lasts seven days - 92 exhibition
games lost as the result of a player strike cancelled at the end of
spring training..


1981 - Work stoppage of 50 days - 712 games lost - season split -
second half of season resumed with All-Star Game in Cleveland. 

1982 - Rickey Henderson broke stolenbase record by stealing 130
bases in a season.

Sept. 1, 1985 - Pete Rose broke career hits record with his
4,192nd.

1990
1990 - Work stoppage lasts 32 days - no game lost - Owners refused
to open spring training - Opening of season delayed by one week.

May 1, 1991 - Nolan Ryan set a new career record by pitching his
seventh no hit game at the age of 44.  On the same day, Rickey
Henderson set a new career stolen base mark, stealing his 939th.

1993 - Florida Marlins (in Miami) and Colorado Rockies (in denver)
become thirteenth and fourteenth teams in the National League.

1994 - Strike ends season on August 12.  Rest of season and World
Series called off on Sept. 14.

1995 - Season started on April 26 with only a 144-game schedule
after baseball play resumed following an NLRB ruling and a judge's
injunction.

September 6, 1995 - Cal Ripken, Jr. of the Baltimore Orioles plays
in his 2,131st consecutive game before a capacity crowd in Camden
Yard, breaking the record set by Lou Gehrig in 1939.
October 28, 1995 - Atlanta Braves win sixth game 1-0 over
Cleveland Indians, winning the World Series 4 games to 2, but 
attendance was off about 20% for the two leagues during the
year as a result of fan unhappiness over the strike.

1997 - Interleague play began.  Each major league team played 15 or
16 games with teams in the same division in the other league as
they were in.

November 6, 1997 - Realignment of the leagues approved.  A team,
the Milwaukee Brewers, moved from one league to the other for the
first time, moving to the National League.  Two expansion teams
will begin play in 1998, the Tampa Bay Devil Rays in the Eastern
Division of the American League and the Arizona Diamond Backs in
the Western Division of the National League.  The Detroit Tigers
will move from the Eastern Division of the American League to its
Central Division.  As a result, there will be 14 American League
teams: 5 in the East, 5 in the Central and 4 in the West.  There
will be 16 in the National League: 5 in the East, 6 in the Central
and 5 in the West.