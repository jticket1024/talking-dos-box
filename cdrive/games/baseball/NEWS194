*WSBB News

January 1994 Volume 4 Number 1
______________________________
Published by Harry H. Hollingsworth
692 S. Sheraton Dr., Akron, OH 44319
Telephone 216 644 2421
_____________________________________________________________
                   HOT STOVE LEAGUE EDITION
_____________________________________________________________

*Meet Phil Vlasak - Major Contributor to Version 8
  Who is this Phil Vlasak who spent the last eight months
improving our WSBB system?
  Phil, now 45 years old, lives in Iselin, N. J., about 30
miles from New York City.  He was working in New York City as
an architectural designer of department stores in 1982 when
he lost his sight as a result of diabetes.  He holds
bachelor's and master's degrees from Pratt Institute.
  In 1984 he bought his first computer, an Apple 2E with an
Echo synthesizer.  He learned how to program in BASIC and
in 1987 wrote a baseball game for use on the Apple.
  That same year he met Donna at a self-help group for blind
people.  She was an avid New York Mets fan.  They were
married in 1988.
  One of the first things they got after getting married was
the World Series Baseball Game and Info System which they
enjoyed together.
  Donna died in 1991 of a heart attack resulting from her
diabetes.
  Phil now works part time for an architect in New York City.
He has a seeing eye dog, a 90-pound chocolate Labrador named
Paco, and a 10-pound terrier named Punky.
   In playing WSBB, Phil came up with some ideas for making
it easier to use and contacted Harry in January 1993.  He
offered to revise the programs and Harry welcomed his help.
  Phil modified the THETEAMS and NEWTEAMS programs and made
some changes in the game itself.  He added the NEWS program
and helped modify the MENU and HOWTO programs.
  Next year Harry and Phil hope to modify the BBYANKS
program, to add many of the features that have been added to
the basic game, and to improve other programs.
  So, Phil has joined the many other blind baseball fans who
have helped to develop WSBB to where it is - Dr. Alan Clive,
Rich DeSteno, Gary Peterson, Robert McClain, Larry Vella, and
on and on.
                             ###
If you have not ordered your Update to WSBB (Version 8), with
143 teams and all of Phil Vlasak's improvements, send your
check for $5 to Harry Hollingsworth today.  The address is at
the bottom of page 1.

*Now You Can Find WSBB Teams With Your Favorite Player
  A last minute addition to the 1993-94 Update of WSBB by
Phil Vlasak will make it possible for you to find all of the
teams in the system on which any player played.
  This can be done either with THETEAMS program, listing all
the teams, or with a special program, PLAYERS, which is not
accessed through the MENU but can be called up directly by
typing PLAYERS and hitting ENTER.
  Using this program, we discovered that the most popular
name of players on the 143 teams in the system is Smith, with
30 appearances.  Second most popular name was Johnson, led by
the great Walter, with 20.  There are 19 Bells, 14 Jacksons,
12 Martinez, 11 Williams and nine each of White, Brown,
Thomas, Hill and Carter.  And on how many teams did the
greatest name in baseball, Ruth, appear?  Three, and it was
the Babe in each case, on the 1916 Red Sox, the 1927 Yankees
and the All-time American League Allstars.

*Why Bob McClain Added Teams In the 1993-94 Update of WSBB
There are a great many new teams, most of which were provided by Robert McClain
of Philadelphia.  Why these particular teams?  Bob had a reason for each.
Here are some of the reasons:
  1968 Cardinals.  It was this year in which the great Bob
Gibson of the Cards set the modern ERA record.
  1947 Dodgers.  This was the year Jackie Robinson broke the
color barrier, playing with the Dodgers.
  1949 Pirates.  Ralph Kiner, one of the great home run
hitters in baseball history, hit 54 for this team.
  1967 White Sox.  A "gutsy" team with lousy hitting but
excellent pitching.
  1978 Red Sox.  A very good team that lost the pennant to
the Yankees in a one-game playoff.
  1977 Cubs.  Because of Bruce Sutter, one of the top relief
pitchers back in the '70s.  Also, it has been a long time
since the Cubs won a pennant.
  1964 Phillies.  Had the pennant won before they collapsed
at the end of the season.
  1969 Twins.  Rod Carew, one of the premier hitters, played
on this team.
  "Playing these old teams brings back a lot of wonderful
memories," Robert said.

*A Good Christmas Gift?
Sherrill O'Brien of Tampa, Fla., likes playing WSBB so much
that she purchased a game for a friend in Philadelphia.  She
is a St. Louis Cardinal fan.

*Now It Is 44 States
  Robert Fitzpatrick of Haines, Alaska, is our first WSBB
user in Alaska.  That makes 44 states and four foreign
countries.

*Canadian Huber Was Glad To Avoid Braves' 'Annoying Chant'
  Kevin Huber of Guelph, Ont., Canada, writes that the WSBB
1993-94 Update "is even more exciting than I had
anticipated."
  He also expressed his thanks "to the gentleman in
Philadelphia for his contributions to the Update." He added,
"Please also thank him for the Blue Jays' victory over the
Phillies.  I must admit that I was pulling for the Phillies
in the NLCS because I did not want to go through another
World Series listening to that annoying chant that the
Braves fans do in Atlanta."

*We Respond to Please of Braves Fans - Add a 143rd Team!
  Because of the pleas of several died-in-the-wool Braves
fans - including Pshon Barrett of Jackson, Miss., and
Charles, Elsea of Nashville, Tenn., - we have added the 1993
Braves to the list of teams in the 1993-94 Update.  That
makes 143 teams in the Update!

*McClain's Simulation Has Braves World Series Champs
  In his simulation of the league championship series
playoffs and the World Series, Robert McClain of Philadelphia
came up with a different 1993 world champ: the Atlanta
Braves.
  The Braves won the National League championship series four
games to three.  (It was actually 4-2, Phils.)  The scores
were (Braves shown first) 5-1, 0-6, 3-6, 13-6, 5-11, 13-0,
and 6-5.
  In the American League CS, the Blue Jays topped the White
Sox four games to two.  The scores (Blue Jays first): 8-6, 4-
6, 5-2, 5-10, 3-2 and 6-4.
  The Braves swept the World Series four games to none. The
scores:  5-3, 5-4, 4-1 and 8-7.
  We hope this report makes some of ardent Braves fans,
including Scott Cruce and Pshon Barrett, happy.

*Once He Participated in Sports Now Mike Plays on Computer
  "I love baseball, football and most other sports but since
an injury left me with low vision, I became just a spectator.
With computer games I've become a competitor again," Mike
Galloway of Birmingham, Ala., wrote in ordering WSBB.
  Mike, now 43 years old, lost most of his sight when he was
26 and was accidentally shot in the face by a neighbor with a
shotgun.

*'Easy To Read News With Scanner'
  David Jollymore of Holly Hill, Fla., writes that WSBB News
is easy to read with his new Reading Edge optical scanner.
  "I have been using it for about a month and I love it," he
said about his Leading Edge.  "It's a whole new world.  It's
fantastic!"
  He added, "I like the baseball game.  The quiz is great.  I
do well on the quiz sometimes and sometimes I don't.  I like
the Hall of Fame program.  I like all of the statistics."
  David works for the Library for the Blind in Daytona Beach
as a braille proofreader.

*Another Philly Fan!
  Joseph Sikora of Bristol, Pa., called to say he was
enjoying his WSBB game and system, which he recently
purchased from Ann Morris Enterprises.
  Joe is an ardent Philly fan can is anxious to get the 1993-
94 Update with the '93 Phillies.  He works in Customer
Service for Recording for the Blind.

*Youngest WSBB User is 13
  The J. Meddaugh family of Saginaw, Mich., purchased WSBB
for Jason, their 13-year-old son as a Christmas gift.  That
makes the son our youngest user, although there are younger
boys and girls playing WSBB with their fathers.  Our users
run from 13 to over 80 with men and women in their forties
dominating.

*Barbara Sheinbein Using Arkenstone to Read WSBB News
  Barbara Sheinbein of St. Louis reports that she has a
new Arkenstone optical reader and can now read WSBB News as
well as 97% of her mail without help.  She also uses it to
read materials from SABR (Society for American Baseball
Research).

*Advantages of Buying From a Blind Vendor
  No matter how long you have used a computer - I've been
using a PC for 11 years and have been programming since 1957
- you still run into problems, especially if you have to use
screen readers and synthesizers.
  I have been very glad that I bought my present computer
from Mike Cozzolino of PC Place, Sacramento.  Mike is blind
himself and knows computers, ancillary hardware and even
software, as well as anyone I have run into.  And, he is
always willing to try to answer questions on the phone.

*Randy Horwitz Likes Info Programs
  Randy Horwitz of DeWitt, N.Y., called to say that he was
having trouble using his screen reader with the information
programs in WSBB.  He was pleased to learn that the Update
makes it possible to run all the programs in monochrome,
which solves the problem.
  "I really like those information programs," he said,
"especially the Hall of Fame and the Records."
  Randy is a student at Rochester Institute of Technology in
Rochester, NY.

*END
 
