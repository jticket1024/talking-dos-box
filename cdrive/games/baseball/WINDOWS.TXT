
BASEBALL GAME AND WINDOWS
by Phil Vlasak
Playing WSBB from a computer with Windows 3.1
If your computer starts up and goes right into Windows 3.1 follow
these steps.
Start up computer
When Windows starts up hit alt f4
Program manager should say 'Exit windows yes or no'
Just hit the enter key to choose yes.
This puts you into dos.
You need to run a dos screen reader now.
Go into the baseball sub directory by typing cd\baseball and hit
the enter key.
type menu and hit the enter key.
type g to play the game.

Playing WSBB from a computer with Windows 95
Start up computer
When Windows 95 comes up use the up and down arrow   keys  to
review your choices, and hit the enter key to pick one.
go to programs documents settings find help run... shutdown...
choose shutdown...
use the down arrow key to go to ms dos mode
choose restart ms dos mode
this will put you into dos.
run a dos speech program
go to the game directory by typing cd\baseball
type menu and hit the enter key.
Then type g to run the game.
