"THIRTY NEW YORK GIANTS"
"JOHN MCGRAW"
"THE POLO GROUNDS"
"WALLY ROETTGER","CENTER FIELD",.283,5
"TRAVIS JACKSON","SHORT STOP",.339,13
"BILL TERRY","FIRST BASE",.401,23
"MEL OTT","RIGHT FIELD",.349,25
"FREDDIE LINDSTROM","THIRD BASE",.379,22
"SHANTY HOGAN","CATCHER",.339,13
"FREDDIE LEACH","LEFT FIELD",.327,13
"HUGIE CRITZ","SECOND BASE",.265,4
"BOB O'FARRELL","CATCHER",.301,4
"ETHAN ALLEN","OUT FIELD",.307,7
"DAVE MARSHALL","SHORT STOP",.309,0
"ANDY REESE","CENTER FIELD",.273,4
"--","NONE",0,0
"CARL HUBBELL","17-12",3.76
"BILL WALKER","17-15",3.93
"FREDDIE FITZSIMMONS","19-7",4.25
"CLARENCE MITCHELL","10-3",3.98
"HUB PRUETT","5-4",4.78
"JOE HEVING","7-5",5.22
"JOE GENEWICH","2-5",5.61
"WELCOME TO THE POLO GROUNDS  ON COOGAN'S BLUFF IN MANHATTAN. THE"
"GIANTS PLAYED HERE IN    1891-1957.  SEATING CAPACITY IS  38,281."
"THE HIGH CONCRETE WALLS WERE AMONG THE MOST DIFFICULT TO PLAY."
"MANAGER JOHN MCGRAW HAD FLOWER BEDS IN RIGHT AND CENTER TORN OUT."
" "
" "
" "
