5 PRINT "The File Transfer Time Evaluater Version 1.00 written by Joseph Bahm."
10 PRINT "If you have any problems, questions, comments, or suggestions concerning this program,"
20 PRINT "please E-mail, via the internet, jbahm@cris.com"
30 input "Enter the baud rate you will be transfering this file at: ", baud
40 PRINT "If you know the size of the file in K-bytes rather than bytes, enter 0 as the file size."
50 input "Enter the size of the file in bytes: ", bytes
60 if bytes = 0 then gosub 10000 : rem get the file size in k-bytes and convert it to bytes
65 gosub 20000 : rem calculate cps, transfer time, and K-bytes
70 PRINT "File Size: ", bytes, "bytes"
80 PRINT "File Size: ", kbytes, "K-bytes"
90 PRINT "Characters Per Second:", cps
100 PRINT "Approximate Transfer Time: ", seconds, "seconds"
110 PRINT "Approximate Transfer Time: ", minutes, "minutes"
120 PRINT "r=repeat"
130 PRINT "n=new evaluation"
140 PRINT "q=quit"
150 PRINT "Please enter your selection"
160 a$=input$(1)
170 if a$ = "r" or a$ = "R" then goto 70
180 if a$ = "n" or a$ = "N" then goto 30
190 if a$ = "q" or a$ = "Q" then goto 220
200 PRINT "Invalid response."
210 goto 120
220 PRINT "Thank you for using this program."
230 PRINT "I hope that you find it useful, and that you will use it again in the future."
240 PRINT "Exiting."
250 END
310 rem end of main program
9990 rem get k-bytes and convert them to bytes
10000 input "Enter the size of the file in K-bytes: ", kbytes
10010 bytes = kbytes*1024
10020 return : rem to main program
19990 rem calculate cps, transfer time, and K-bytes
20000 cps = baud/10
20010 seconds = bytes/cps
20020 minutes = seconds/60
20030 kbytes = bytes/1024
20040 return : rem to main program
