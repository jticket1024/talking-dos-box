1 ' jet pilot -- flight simulation program
2 '
10 clear: p$="###.##":gosub 5000:gosub 8020
20 DEFSTR O:o(1)="throttle":o(2)="control":o(3)="flap":o(4)="spd brake":o(5)="lnd gear":o(6)="spoiler":o(7)="tail hook":o(8)="weapons":o(9)=" fuel system":o(10)="fire  warning "
30 o1(1)="very poor":o1(2)="poor":o1(3)="average":o1(4)="good":o1(5)="excellent":o2(1)="foxfire":o2(2)="hunter":o2(3)="avenger":o2(4)="swift-sword":o2(5)="gauntlet"
35 o3(1)="f-18":o3(2)="f-14":o3(3)="f-4":o3(4)="a-6":o3(5)="a-6"
40 ld=1:tf=0:k1=10:k2=2:k3=.5:k4=1.1:k5=7.5:k7=9.999999e-06:k8=.00005:k9=5:ka=.01:kb=2.5:ra=57.29578:w=350000!:ke=.2:kf=1:A0=0:vh=0:vv=0:t=0:tr=0:z4=3.5/ra:z5=.02:wl=INT(RND(1) *5)+5:x9=0:y9=0:dy=0:r1=0:r9=0
42 t7=30+(int(30*rnd(1))):ts=int(10*rnd(1))
60 dt=1:fl=2750*100
70 wz=1:if fl<82500! then print "insufficient  fuel":goto 60 else if fl>(200000!+75000!*dt) then print "excessive  fuel":goto 60
80 xt=int(50*rnd(1))-int(50*rnd(1)):yt=int(50*rnd(1))-int(50*rnd(1)):w1=int(360*rnd(1)):ws=int(400*rnd(1))+250:at=10000+int(30000*rnd(1))-int(20000*rnd(1)):if at<0 then goto 80
82 print"jet  pilot-- a flight simulation game":for delay=1 to 1000:next
84 input"do you need instructions";a$:if left$(a$,1)="y" then gosub 1860
90 input "carrier launch (y/n)";a$:if left$(a$,1)="n" or a$="" then 100 else d(0)=650:z4=1.5/ra:z5=0:ae=1:tf=0:cv=1:v1=(int(36*rnd(1))-1)*10:v2=((int(25*rnd(1))+10)*6080)/3600:hd=v1:h(tf)=v1:goto 120
100 o(7)="drag chute":ae=20:cx=1:for i=0 to 7:d(i)=3500+(int(40*rnd(1))*100):h(i)=(int(36*rnd(1))*10):next i:dx=d(0):hd=h(0)+int(10*rnd(1))-int(10*rnd(1)):for i=1 to 7:x(i)=int(120*rnd(1))-int(120*rnd(1)):y(i)=int(120*rnd(1))-int(120*rnd(1)):next i
105 x(0)=0:xp=0:y(0)=0:yp=0:if hd>=360 then hd=hd-360:if h(0)=360 then h(0)=0
110 input "display airports (y/n)";b$:if left$(b$,1)="n" then 120 else gosub 1840:tf=0:lo=0:print:input "new airports (y/n)";b$:if left$(b$,1)="n" then 120 else gosub 5000:goto 100
120 dr=1:mt=5:print  using "aircraft A0signed is an \  \ no. #####.";o3(int(4*rnd(1)));int(32000*rnd(1)):print "  it has had ";o1(mt);" maintenance.";
125 print:print:gosub 10100:'
130 gosub 510
140 if (es=0 and ld=0) or (tr<10 and tr>=0 and t1=0 and ld=0) or fl=0 then print "`flame out ";:rt=0:fu=0:es=0:st=0
150 tr=(tr+t1)*es:if t1>0 and tr>th then tr=th:t1=0 else if t1<0 and tr<th then tr=th:t1=0
160 if tr>100 then k6=20000:kc=7.5 else k6=12500:kc=2.5
170 t=k6*tr*es:fl=fl-abs(kc*tr):if fl<=0 then fl=0:t=0:tr=0
180 wt=w+fl+dt*5000+1000*wl:sp=sp+s5:if sp>s6 then sp=s6:s5=0
190 an=an+a2:if a2>0 and an>ad then an=ad:a2=0 else if a2<0 and an<ad then an=ad:a2=0
200 if ld=1 then 210 else 230
210 if A0<120 or an<0 then an=0: else if an>0 then ns=0
220 bk=bk+b1:if bk>bs then bk=bs:b1=0:goto 240 else 240
230 ns=0:bn=bn+b9:if b9>0 and bn>bd then bn=bd:b2=0 else if b9<0 and bn<bd then bn=bd:b2=0
240 a3=an/ra:a1=(6*an+1.5)/ra:fa=fa+f1:if fa>fs then fa=fs:f1=0
250 sb=sb+s1:if sb>s2 then sb=s2:s1=0
260 cl=k1*(sin(a1)-kb*(vv/(A0+1))):l=cl*(A0^2)+10*fa*A0:l=l-l*(sp/100):ss=1300-(al/100):if sx=1 then 280 else if A0>.95*ss then ss=.9000001*A0:sx=1:goto 270 else 280
270 if A0<ss then sx=0:goto 260: else sd=sqr((A0^2/ss^2)-1):goto 290
280 sd=(1/sqr(1-(A0^2/ss^2)))-.5
290 d=((k2*wt)/((A0/2)^2+10))+A0*(k3*(A0/2)+(75*(1-lg))+2*fa+2*sb +1000*ce+50*dt+.01*wl+.001*j)+2500*bk:d=d*sd:if al>15000 then z1=15000/al:l=l*z1:d=d*z1:if al>22500 then t=t*22500/al
300 hc=k7*(cos(a3)*t-k9*sin(a3)*abs(l)-k5*cos(a3)*d-sin(a3)*kf*wt):if cx=0 then hc=0
310 vc=k8*(cos(a3)*l+ke*sin(a3)*t-abs(sin(bn/ra)*l)-abs(sin(a3)*d)-cos(a3)*k4*wt):if (ld=1 and vc<0) or cx=0 then vc=0:al=0 else al=al+vv
320 vh=vh+hc:if ld=1 and vh<0 then vh=0
330 vv=vv+vc:gf=((hc+vc)/32)+(1/cos(bn/ra)):A0=sqr(vh^2+vv^2):hd=hd+(bn/22)*3+ns*(A0/80):if cv=1 and cx=0 then A0=v2:vh=v2:hd=v1: else if hd<0 then hd=hd+360 else if hd>=360 then hd=hd-360
340 if int((20*2^mt)*rnd(1))=mt*10 then gosub 1300 else if int(100*rnd(1))=50 then w1=int(360*rnd(1)):ws=int(500*rnd(1))+250:at=at-int(5000*rnd(1))+int(5000*rnd(1)):if at<=0 then gosub 1560
350 if wz=0 then td=0:lt=0:w1=0:ws=0:at=0:goto 360 else xt=xt+cos((w1-90)/ra)*(ws/6080):yt=yt+sin((w1-90)/ra)*(ws/6080)
360 if rk<>0 then for r2=1 to r1:if r(r2)=10 then print " fire warning";:r9=r9+1:if r9=15 then 1510 else if fu=0 then r9=0:r(r2)=0:r1=r1-1: else 370 else print o(r(r2));" malfunction";:next r2
370 z2=(hd-90)/ra:z3=A0/6080:xp=xp+cos(z2)*z3:yp=yp+sin(z2)*z3
374 dl=sqr((x(tf)-xp)^2+(y(tf)-yp)^2):gosub 1570:if cv=1 then x(tf)=x(tf)+v2*cos((v1-90)/ra)/6080:y(tf)=y(tf)+v2*sin((v1-90)/ra)/6080:dy=sqr((x(tf)-x9)^2+(y(tf)-y9)^2):x9=x(tf):y9=y(tf) else dy=0
380 if ld=0 then 400 else if al>0 then ld=0:dr=0:goto 400 else if h(te)=360 and hd<350 then h(0)=0 else if h(te)=0 and hd>350 then h(0)=360
390 if cx=1 and (dx<0 or (A0>100 and abs(hd-h(te))>2)) then 1500 else 470
400 if tf<>te then dx=d(tf):te=tf
410 if dl<=.02 and al>0 then dr=1
420 if A0<215 then 1550 else if al<=0 then 1480 else z0=1.2*sin(6*a3)-(vv/(A0+1))+sin(abs(bn/ra)):if z0>1.1 and sp=0 then 1470
430 if gf>10 or gf<-5 then 1520
440 if dr=1 then 470 else gs=al-abs(sin(z4)*(dl+z5)*6080):lc=sin((h(te)-(lo+180))/ra)*dl*6080:if wa=0 or wz=0 then 470 else td=sqr((xt-xp)^2+(yt-yp)^2+((at/6080)-(al/6080))^2):gosub 1650:if lt<0 then lt=lt+360 else if lt>=360 then lt=lt-360
450 if td*6080<300 then 1480 else if wl=0 then wa=0:goto 470
460 if wf=1 then wl=wl-1:wf=0:if td<5+int(2*rnd(1))-int(2*rnd(1)) and (hd<lt+1.5 and hd>lt-1.5) then gosub 1530:wa=0:wz=0 else w1=int(360*rnd(1)):ws=250+int(400*rnd(1)):at=at+int(5000*rnd(1))-int(5000*rnd(1)):if at<=0 then gosub 1560 else gosub 1540
470 if cx=0 then if dx=0 then dx=150:goto 500 else 500
480 if cv=1 then sf=1 else sf=2
490 if dr=1 then dx=dx+dy*6080-sf*vh: else if (te<>tf) or (cv=1 and cx=1) then dx=d(tf)
500 gosub 5100:a$=inkey$:if a$="" then 130 else 810
510 posw=0:gosub 45000:print  using "airspeed #### knots";A0*3600/6080;:if an>0 then z$=chr$(24) else if an<0 then z$=chr$(25) else z$=" "
520 print  using "attitude   ##.## !";abs(an);z$;:if bn<0 then z$="l" else if bn>0 then z$="r" else z$=" "
530 print "altitude ";:if al>18000 then print using "f/l ###";al/100;:print spc(79-pos(0)); else print using "#####.# feet";al;
540 if vv>0 then z1$=chr$(24) else if vv<0 then z1$=chr$(25) else z1$=" "
550 print  using " heading ###.#       bank ##.# !           climb ! #####.# feet per minute";hd;abs(bn);z$;z1$;abs(vv*60);
560 if dt=1 then d$="on " else d$="off"
570 print  using " thrust ###%         spoilers ###%         g. load +##.#";tr;sp;gf;
580 print "lndg. gear ";:if lg=0 then print chr$(25);: else print chr$(24);
590 print  using " fuel ###%    d.t. \ \";fl/2750;d$;
600 print "flap pos. ";:if fa>0 then print chr$(25); using " ###%";fa;: else print chr$(24);"      ";
610 ts=ts+3:if ts>=60 then ts=ts-60:tm=tm+1:if tm=60 then t9=t9+1
620 print  using "e.t. ##:##:##";t9;tm;ts;
630 print "speed brakes";:if sb>0 then print chr$(32);chr$(26);chr$(27);int(sb);spc(79-pos(0)); else print chr$(32);chr$(27);chr$(26);spc(79-pos(0));
640 if cv=1 then print "tail hook ";:if j=1 then print chr$(25);:goto 660 else print chr$(24);:goto 660
650 print "drag chute ";:if ce=0 and cf=0 then print "stowed"; else if cf=0 then print "deployed"; else print "releA0ed";
660 print  using " missles: #";wl;:if wa=1 then print  using "armed  target bearing ###.#: target range ###.#";lt;td;:if at-al>0 then print chr$(24); else print chr$(25);
670 if wa=1 then print  using "t.c. ###    t.s. ###";w1;ws;
680 if ld=0 then if wa=0 then print spc(79-pos(0)); else goto 700 else print "brakes";:if bk<>0 then print using " on ###%";bk; else print " off";spc(79-pos(0));
690 if cx=0 then print "cat. ready";
700 if wa=1 then 710 else if ld=1 or dl<6.5 then print  using "field remaining ####";dx;: else print "                     ";
710 print  using "runway heading ###runway length  ####";h(te);d(tf);
720 if nv=0 then gosub 5100:return
730 print  using "airport #:  bearing ###.#:   range ###.##";tf;lo;dl;
740 if ld=0 and dl<18 and abs(lc)<999.99 then print else gosub 5100:return
750 if cv=1 then l1=7.5:g1=3:g2=.01: else l1=15:g1=10:g2=-10
760 if lc<-l1 then print "right of";: else if lc>l1 then print "left of";: else print "on local                      ";:goto 780
770 print " local ";:print using p$;abs(lc);
780 print ;:if gs>g1 then print "above";: else if gs<g2 then print "below";: else print "on g/s";spc(79-pos(0));:goto 800
790 print using " g/s ####.#";abs(gs);:print spc(79-pos(0));
800 return 
810 print ;:if a$="t" then 830 else if a$="a" then 880 else if a$="d" then 910 else if a$="f" then 930 else if a$="l" then 980 else if a$="j" then 1020 
815 if a$="h" then 1040 else if a$="x" then 970 else if a$="c" then 1090 else if a$="w" then 1070 else if a$="s" then 1130 else if a$="z" then 1110
820 if a$="b" then 1180 else if a$="n" then 1160 else if a$="p" then 1220 else if a$="e" then 1430 else if a$="k" then 860 else 130
830 if r3=1 or es=0 then 130 else input "throttle setting (-10 to 120)";th
840 if th<-10 or th>120 then print "unable":goto 130
850 t1=(th-tr)/5:goto 130
860 if rk=1 then rk=0 else if rk=0 and r1<>0 then rk=1
870 goto 130
880 if r4=1 then 130 else ah=0:input "attitude (-180 to 180)";ad
890 if ad<-180 or ad>180 then print "unable":goto 130
900 a2=(ad-an)/3:goto 130
910 if r=9 then 130 else if dt=1 then dt=0:if fl>192500! then fl=192500!
920 goto 130
930 if r5=1 then 130 else input "flap setting (0 to 100)";fs
940 if fs>100 or fs<0 then print "unable":goto 130
950 if A0>464 then print "airspeed excessive":goto 130
960 f1=(fs-fa)/4:goto 130
970 if cx=0 then cx=1:hc=vh+220:gf=6.1:A0=hc:vh=hc:dr=0:ld=0:al=40+int(10*rnd(1))-int(10*rnd(1)):an=10:goto 130 else 130
980 if r7=1 then 130 else if ld=1 then print "on ground --- landing gear overridden":goto 130
990 if A0>385 then print "airspeed excessive":goto 130
1000 if lg=0 then lg=1 else lg=0
1010 goto 130
1020 if cv=0 or r9=1 then 130 else if j=0 then if A0<385 then j=1 else print "airspeed excessive";: else j=0
1030 goto 130
1040 if ld=1 or r4=1 then 130
1050 input "bank";bd:if bd<-90 or bd>90 then 1050
1060 b9=(bd-bn)/2:goto 130
1070 if r0=1 then 130 else input "arm/disarm/fire/jet";a$:if left$(a$,1)="a" then wa=1 else if left$(a$,1)="j" then wl=0:wa=0: else if left$(a$,1)="d" then wa=0 else if left$(a$,1)="f" and wz=1 and wl>0 then wf=1 else wf=0
1080 goto 130
1090 if r9=1 then 130 else if cf=1 then 130 else if ce=0 then ce=1:cf=0: else ce=0:cf=1
1100 goto 130
1110 if r8=1 then 130 else input "spoilers";s6:if s6<0 or s6>100 then 1110
1120 s5=(s6-sp)/4:goto 130
1130 if r6=1 then 130 else input "speed brake (0 to 100)";s2
1140 if s2>100 or s2<0 then print "unable":goto 130
1150 s1=(s2-sb)/4:goto 130
1160 input "nose wheel (-30 to 30)";ns:if ns>30 or ns<-30 then ns=0
1170 goto 130
1180 if ld=0 then 130
1190 input "brake setting";bs
1200 if bs>100 or bs<0 then 1190
1210 b1=(bs-bk)/3:goto 130
1220 input "nav-system (on, off, display, 0-7)";b$
1230 if left$(b$,1)="d" and cv=0 then l9=lo:tj=tf:gosub 1840:tf=tj:lo=l9:input "ready";a$:goto 130
1240 if b$="on" then nv=1:goto 1270
1250 if b$="off" then nv=0:goto 130
1260 if val(b$)<0 or val(b$)>7 then 1220 else tf=val(b$):goto 1280
1270 print ;:input "enter airport (0-7)";tf
1280 if tf<>0 and cv=1 then tf=0:goto 130
1290 if tf<0 or tf>7 then 1270 else 130
1300 r=int(10*rnd(1)):rk=1:r1=r1+1:r(r1)=r:on r goto 1310,1320,1330,1340,1350,1360,1370,1380,1390,1400
1310 r3=1:tr=120:goto 1410
1320 r4=1:an=int(50*rnd(1))-int(50*rnd(1)):ad=an:bn=int(30*rnd(1))-int(30*rnd(1)):bd=bn:goto 1410
1330 r5=1:fa=int(100*rnd(1)):fs=fa:goto 1410
1340 r6=1:sb=int(100*rnd(1)):s2=sb:goto 1410
1350 r7=1:lg=(int(2*rnd(1)))-1:goto 1410
1360 r8=1:sp=int(100*rnd(1)):s6=sp:goto 1410
1370 r9=1:if cv=1 then j=(int(2*rnd(1)))-1:goto 1410 else cf=int(rnd(1)+.5):goto 1410
1380 r0=1:wa=0:goto 1410
1390 fl=0:goto 1410
1400 rc=0
1410 if r1>1 then for i=1 to r1-1:if r=r(i) then r(r1)=0:r1=r1-1:r=0 else next i
1420 return 
1430 print "turbine or eject";:input a$
1440 if a$="t" then x9=1:if es=0 then x9=0:goto 1710 else 1710
1450 if a$="e" then if al>ae and abs(an)<45 and abs(bn)<25 and A0<850 then print  "safely ejected":print:print:system:gosub 7000:if left$(ans$,1) ="y" or left$(ans$,1) ="y" then 10  else print "cannot eject":goto 130
1460 goto 130
1470 print "stall";:l=0:an=-an:if vv>0 then vv=-vv:goto 440 else vv=2*vv:goto 440
1480 if cv=1 then 1490 else if wt<500000! and dr=1 and abs(lc)<75 and A0<380 and vv>-25 and an>0 and lg=0 then ld=1:goto 440: else 1500
1490 if wt<500000! and dr=1 and abs(lc)<30 and A0<380 and vv>-25 and an>0 and lg=0 and j=1 then ld=1:A0=v2:vh=v2:vv=0:ld=1:cx=0:goto 440 else 1500
1500 print  "crA0h ";:gosub 7000:if left$(ans$,1)="y" or left$(ans$,1)="y" then goto 10 else system
1510 print  "explosion";:gosub 7000:if left$(ans$,1)="y" or left$(ans$,1)="y" then goto 10 else system
1520 print  "aircraft broke up!";:gosub 7000:if left$(ans$,1)="y" or left$(ans$,1)="y" then goto 10 else system
1530 print "target destroyed":ma=0:mz=0:for z9=1 to 300:next:return 
1540 print "missle missed target":for z9=1 to 300:next:return 
1550 print "spin. -  flame out.";:tr=0:t=0:l=0:th=0:an=-(int(30*rnd(1))):hd=hd-int(50*rnd(1)):if al<=0 then 1480 else 440
1560 print "target crA0hed":wa=0:wz=0:for z9=1 to 300:next:return 
1570 xs=sgn(x(tf)-xp):ys=sgn(y(tf)-yp)
1580 if xs=0 and ys=1 then lo=0:goto 1640
1590 if xs=0 and ys=-1 then lo=180:goto 1640
1600 if tf=0 and xp=0 and yp=0 then 1640
1610 la=atn((y(tf)-yp)/(x(tf)-xp))*ra
1620 if xs=1 then lo=la+90:goto 1640
1630 if xs=-1 then lo=270+la
1640 return 
1650 x8=sgn(xt-xp):y8=sgn(yt-yp)
1660 if x8=0 and y8=1 then lt=0:return 
1670 if x8=0 and y8=-1 then lt=180:return 
1680 lb=atn((yt-yp)/(xt-xp))*ra
1690 if x8=1 then lt=lb+90:return 
1700 if x8=-1 then lt=270+lb:return 
1710 if fl=0 then 130 else print  "  engine ";:if es=0 then print "off";:if x8=1 then tr=0:x8=0:goto 130 else 1720: else print "on ";:if x9=1 then 1720 else tr=10:goto 130
1720 print  using "turbine rotation. ##### r p m";rt;:print " fuel control ";:if fu=0 then print "off";: else print "on ";
1730 if es=1 then rt=9000 else if fl=0 then fu=0
1740 rt=rt+st*100+750*fu:if rt>12000 and fu=0 then 1510 else if rt>9000 and fu=1 then es=1 else if rt<4500 and fu=1 then 1830 else if rt<0 then st=0:rt=0:x9=0:x8=1
1750 t5=t7+rt/40+380*fu:if t5>600 then t5=600
1760 t6=rt/73:if t6>65 then t6=65
1770 ts=ts+int(3*rnd(1)):print  using "temperature ### c.    oil pressure ## p s i";t5;t6;
1780	a$= inkey$:if a$<>"" then 1790 else 1710
1790 print ;:if a$="f" and fu=0 then print " fuel on      ":fu=1: else if a$="f" then print " fuel off     ":fu=0:t7=50+int(50*rnd(1))
1800 if a$="s" and es=0 then print "  engine spin     ":st=3:goto 1710
1810 if a$="s" and es=1 and fu=0 then print "  engine shutdown":st=-4:es=0:goto 1710
1820 goto 1720
1830 print  "engine  fire":system 
1840 print "airport"; tab(12);" runway lngth  ";"hdg","  direct","   dist"
1845 for tf=0 to 7:print  tab(2);tf; tab(14);d(tf); tab(22);h(tf); tab(33);:gosub 1570:print using "###";lo;:print tab(47); using "###.##";sqr((x(tf)-xp)^2+(y(tf)-yp)^2):next tf:return
1850 system 
1860 print chr$(34);"jet pilot";chr$(34);" simulates the flying characteristics of a modern high-performance":print"jet aircraft.  before attempting to take off, you must be familiar with the"
1870 print "instructions in the accompanying .documentation file.  your controls are:":print
1880 print" engine start sequence:  select  engine turbine (e), enter  engine startmode (t)"
1890 print"spin turbine (s), start  fuel flow (f) at 4500 r p m."
1900 print:print"your aircraft controls:
1910 print"[a] attitude (+-180)         [b] wheel brakes (0-100%)"
1920 print"[c] drag chute               [e] ejection seat"
1930 print"[f] flaps (0-100%)           [h] angle of bank (+-90 deg)"
1940 print"[j] tail hook                [k] cancel malfunction warnings"
1950 print"[l] landing gear             [n] nose wheel steering (+-30 deg)"
1960 print"[p] nav computer             [s] speed brakes (0-100%)"
1970 print"[t] throttle (0-120%)        [x] fire catapult"
1980 print"[z] spoilers (0-100)"
1990 print:gosub 10100
2000 return
5000 randomize timer:return
5100 posy=ypos:posx=pos(0):print spc(79-posx):for clr=posy+1 to 16:print spc(80);:next:return
7000 input"want to try again";ans$:return
8020 x=peek(&h417) and &h40:if x=0 then poke &h417,peek(&h417) or &h40
8030 return
10100 print tab(20) "hit [enter] to continue ";
10110 in$=inkey$:if in$="" then 10110 else return
45000 '  ***  evaluate expression for 
45010 posx=int(posw/64):posy=posw-(64*posx)
45020 posx=posx+1:posy=posy+1:return
45030 '