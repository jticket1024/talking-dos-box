5  'WW-VI.BAS BY TOM DEFELICE
10 RANDOMIZE ((VAL(RIGHT$(TIME$,2)))+VAL(MID$(TIME$,4,2)))
15 KEY(8) ON : ON KEY (8) GOSUB 2475
20 WIDTH 80 : COLOR 15,1,1
30 KEY OFF: CLS
40 LI=0: W=3: S$=" Shore"
50 DIM A(10,10), C(10,10), N$(10), D$(5), L$(5)
60 DATA North America, Iceland, South America, Russia, Phillipines
70 DATA Japan, England, Greenland, Africa, Australia
80 DATA Laser Canon, Planes, Tanks, Missles, Troops, North, South, East, West
90 FOR X=1 TO 10: READ N$(X): NEXT
100 FOR X=1 TO 5:READ D$(X): NEXT
110 FOR X=1 TO 4: READ L$(X): NEXT
120 FOR X=1 TO 10:C(X,5)=CINT((3000*RND)+5000)*10: NEXT
130 FOR X=1 TO 10: FOR J=1 TO 4: C(X,J)=CINT(RND*50)+25: NEXT J: NEXT X
140 FOR J=1 TO 10: FOR X=6 TO 10: C(J,X)=INT(4*RND)+1: NEXT X: NEXT J
150 FOR X=1 TO 5: FOR J=1 TO 10: A(X,J)=C(X,J): C(X,J)=0:NEXT J: NEXT X
160 PRINT TAB(35) "WORLD WAR VI": PRINT : PRINT " The world once again finds itself in a global war.": PRINT :PRINT "   Do you want instructions (y/n) "
170 IN$=INKEY$: IF IN$="" THEN GOTO 170
180 IF IN$<>"Y" AND IN$<>"y" THEN GOTO 500
190 CLS: PRINT : PRINT "This conflict is between the five powers of the Icelandic Alliance and the five": PRINT "powers of the Ultima Compact.": PRINT
200 PRINT " There have been great defeats and victories on both sides. The greatest": PRINT "victories for the Alliance have been the Battle of Reykjavik where the"
210 PRINT "Philippino army retook Iceland and the Sea Battle of Brisbane where the entire": PRINT "Compact's fleet was destroyed along with its infamous Commander Magnus Griel."
220 PRINT : PRINT :PRINT "The greatest defeat for the Alliance has been the loss of Greenland and the": PRINT "assassination of the Supreme Commander of the Icelandic Alliance along with"
230 PRINT "the disappearance of his science advisor. As his second in command, you now": PRINT "take over as the Supreme Commander of the Icelandic Alliance.": PRINT:GOSUB 2450: PRINT
240 PRINT "Your job is to send your forces against those of the Compacts.": PRINT "But time is essential for they must be destroyed before they rebuild their": PRINT "fleet and attack the Alliance."
250 PRINT : PRINT "At present the Alliance has two big advantages. The first is its battle fleet": PRINT "which can transport its forces to the enemy's shores.
260 PRINT :PRINT "The second is its planes. When the compact destroyed all of the Earth's":PRINT "satellites in order to disrupt communications it also destroyed the ionosphere"
270 PRINT "making long range flight impossible. The Allliances scientist, however, have": PRINT "come up with a shielding device which makes long range air travel possible."
280 PRINT "This will make it possible for you to retrofit your planes to make spy": PRINT "misions over the enemy's territory in order to get data on their defenses.": PRINT :GOSUB 2450:GOTO 310
290 FOR  X=1 TO 10: PRINT TAB(20);: IF X=10 THEN PRINT 0; ELSE PRINT X;
300 PRINT "- " N$(X): NEXT X: RETURN
310 PRINT :PRINT "There are 10 countries. Each is identified with its country number.": PRINT "        They are:":GOSUB 290
320 PRINT "The 5 defenses for each country are :": GOSUB 330: PRINT : GOSUB 2450: GOTO 340
330 FOR X=1 TO 5: PRINT TAB(22);: PRINT X "- " D$(X): NEXT: RETURN
340 PRINT " Each defense may reside on one shore or all may reside on any shore.": PRINT " The shores are:": PRINT TAB(20) "North shore": PRINT TAB(20) "South Shore": PRINT TAB(20) "East Shore": PRINT TAB(20) "West Shore"
350 PRINT : PRINT "You identify a shore by its first letter (ex: N = North Shore)": PRINT: GOSUB 370
360 GOTO 410
370 PRINT "There are 6 Commands they are:" :GOSUB 390: GOTO 410
380 WIDTH 80: GOSUB 2470
390 PRINT TAB(11) "1  - Show country status": PRINT TAB(11) "2  - Transport defenses to countries or shores": PRINT TAB(11) "3  - Show Troop Global Status"
400 PRINT TAB(11) "4  - Perform a spy mission": PRINT TAB(11) "5  - Begin an attack": PRINT TAB(11) "F8 - To surrender" : PRINT : RETURN
410 GOSUB 2450
420 PRINT TAB(38) "COMMANDS": PRINT " 1 - SHOW COUNTRY STATUS": PRINT "   Displays how many of each forces you have in a country and on what shore": PRINT "   they are located": PRINT : PRINT " 2 - TRANSPORT DEFENSES TO COUNTRIES OR SHORES"
430 PRINT "   Moves any one of your forces from one country to another or from one shore": PRINT "   of a country to another shore.": PRINT : PRINT  " 3 - SHOW TROOP GLOBAL STATUS": PRINT "   Shows how many Alliance and Compact troops are stationed in";
440 PRINT " each country.": PRINT : PRINT " 4 - PERFORM A SPY MISSION": PRINT "   Sends a group of your planes over a country to inform you of the amount of": PRINT "   enemy forces in that country and on what shore they are placed."
450 PRINT : PRINT " 5 - PERFORM AN ATTACK": PRINT "   A battle begins where both you and the enemy have forces in the same country": PRINT "   on the same shore."
460 PRINT "     note: if any of your forces are captured by the enemy they"
470 PRINT "           become enemy forces. And if you capture any enemy
480 PRINT "           forces they become your forces.
490 GOSUB 2450
500 GOSUB 2470
510 Y6=Y6+1:PRINT "  Your orders, Sir: ";
520 M$=INKEY$: IF M$="" THEN GOTO 520
530 IF M$="*" THEN WIDTH 80:CLS:GOSUB 380
540 IF ASC(M$)<48 OR ASC(M$)>57 THEN GOTO 510
550 M=VAL(M$): WIDTH 80
560 IF INT(RND*1000)<>500 THEN GOTO 610
570 WIDTH 80:BEEP:FOR COUNT=1 TO 25:BEEP:COLOR 15,0,0:CLS: COLOR 0,14,14: CLS: NEXT:BEEP
580 LOCATE 10,20: PRINT "You have been assinated!!!": PRINT "       A Compact Spy has gained entrance to the Alliance Headquarters": PRINT "   and killed you by detonating a neutron bomb."
590 PRINT :PRINT :PRINT "                   THE WORLD IS DOOMED"
600 GOTO 2440
610 IF M>5 THEN GOSUB 370:GOTO 510
620 IF M<1 THEN GOSUB 370: GOTO 510
630 ON M GOTO 640, 890, 1280, 1390, 1710
640 WIDTH 80:CLS:GOSUB 2470
650 PRINT "Country...";
660 R$=INKEY$: IF R$="" THEN GOTO 660
670 IF R$="*" THEN GOSUB 290
680 R=VAL(R$): PRINT R$:IF ASC(R$)<48 OR ASC(R$)>57 THEN GOTO 650
690 IF R=0 THEN R=10
700 COLOR 2,0,0
710 WIDTH 80:WIDTH 40:GOSUB 2470:COLOR 18,0,0:LOCATE 2,3: PRINT "** ";
720 COLOR 13,0,0
730 PRINT "STATUS REPORT " N$(R);
740 COLOR 18
750 PRINT " **"
760 COLOR 14
770 PRINT : PRINT TAB(2) "Defense" TAB(14);: COLOR 15
780 PRINT TAB(14) "Quantity" TAB(28);
790 COLOR 12
800 PRINT "Location": PRINT : FOR X=1 TO 5
810 COLOR 14
820 PRINT TAB(2) D$(X) TAB(14);
830 COLOR 15
840 PRINT USING "######,";A(R,X);: PRINT TAB(28);
850 COLOR 12
860 IF A(R,X)<>0 THEN PRINT L$(A(R,(X+5)))+S$ ELSE PRINT
870 PRINT
880 NEXT : GOTO 510
890 COLOR 15
900 WIDTH 80:COLOR 15,4,4: CLS: GOSUB 2470
910 PRINT " Enter from Country...";
920 F$=INKEY$: IF F$="" THEN GOTO 920
930 IF F$="*" THEN GOSUB 290: GOTO 910
940 IF ASC(F$)<48 OR ASC(F$)>57 THEN GOTO 920
950 F=VAL(F$): IF F=0 THEN F=10
960 IF F<1 OR F>10 THEN GOSUB 290: GOTO 900
970 PRINT N$(F);
980 PRINT "     to Country...";
990 T$=INKEY$: IF T$="" THEN GOTO 990
1000 IF T$="*" THEN GOSUB 290:GOTO 980
1010 T=VAL(T$): IF T=0 THEN T=10
1020 IF T<1 OR T>10 THEN GOSUB 290: GOTO 980
1030 PRINT N$(T): PRINT
1040 PRINT "    What defense...";
1050 D$=INKEY$: IF D$="" THEN GOTO 1050
1060 IF D$="*" THEN GOSUB 330: GOTO 1040
1070 D=VAL(D$): IF D<1 OR D>5 THEN GOTO 1040
1080 PRINT D$(D)
1090 PRINT :INPUT "             Quantity... (hit enter key after amount) ";Q:PRINT
1100 IF A(F,D)>=Q THEN GOTO 1120
1110 PRINT "     You only have " A(F,D) D$(D) " in " N$(F): GOTO 1090
1120 IF F<> T THEN GOTO 1150
1130 IF A(F,D)<>Q THEN PRINT "Sorry, Sir, but all " D$(D) " must be moved at the same time"; ELSE GOTO 1150
1140 PRINT " to insure their security.":PRINT " You have " A(F,D) " " D$(D) " in "N$(F): GOTO 1090
1150 A(T,D)=A(T,D)+Q: A(F,D)=A(F,D)-Q
1160 PRINT :PRINT "What shore you would like them redeployed to, Commander.": PRINT :PRINT "    Please enter N(orth), S(outh), E(ast) or W(est)."
1170 L$=INKEY$: IF L$="" THEN GOTO 1170
1180 IF L$="n" OR L$="N" THEN L$="N": L=1: GOTO 1230
1190 IF L$="s" OR L$="S" THEN L$="S": L=2: GOTO 1230
1200 IF L$="e" OR L$="E" THEN L$="E": L=3: GOTO 1230
1210 IF L$="w" OR L$="W" THEN L$="E": L=4: GOTO 1230
1220 PRINT :PRINT "I'm sorry, Sir, but you did not enter a Correct response": GOTO 1160
1230 PRINT :PRINT " The " L$(L) S$ ", yes, Sir."
1240 A(T,(D+5))=L
1250 IF INT(RND*100)>25 THEN 1270
1260 GOSUB 2290
1270 GOTO 510
1280 WIDTH 80: COLOR 3,8,8: GOSUB 2470: COLOR 30,8,8:PRINT TAB(25) " ++  ";:COLOR 14: PRINT "GLOBAL REPORT ";: COLOR 30: PRINT "++": COLOR 15
1290 PRINT TAB(9) "Country" TAB(26) "Alliance Troops" TAB(44) "Compact Troops" TAB(60) "Alliance Possession?"
1300 FOR X=1 TO 10: IF X=10 THEN PRINT 0; ELSE PRINT X;
1310 PRINT "- " N$(X) TAB(29);: PRINT USING "###,###";A(X,5);: PRINT TAB(49);: PRINT USING "###,###";C(X,5);: PRINT TAB(70);
1320 K2=0: FOR F2=1 TO 5: IF C(X,F2)<>0 THEN K2=K2+1
1330 NEXT
1340 IF K2=0 THEN GOTO 1360
1350 COLOR 12: PRINT "NO": GOTO 1370
1360 COLOR 10: PRINT "YES"
1370 PRINT :COLOR 15: NEXT X: GOTO 510
1380 STOP
1390 WIDTH 40:COLOR 3,6,6: GOSUB 2470
1400 LOCATE 2:PRINT TAB(14) " Spy Mission": PRINT
1410 PRINT "Sir, which country's planes do you wish": PRINT "to use?   ";
1420 F$=INKEY$: IF F$="" THEN GOTO 1420
1430 IF F$="*" THEN GOSUB 290: GOTO 1410
1440 IF ASC(F$)<48 OR ASC(F$)>57 THEN GOTO 1420
1450 F=VAL(F$)
1460 IF F=0 THEN F=10
1470 IF F<1 OR F>10 THEN GOSUB 290: GOTO 1440
1480 PRINT N$(F)
1490 IF A(F,2)<=9 THEN PRINT "I'm sorry, Sir. I know you are Supreme": PRINT "Commander, but we do not have": PRINT "enough planes based there to insure": PRINT "a successful mission." : GOTO 510
1500 PRINT :PRINT "To which country, Sir "
1510 T$=INKEY$: IF T$="" THEN GOTO 1510 ELSE T=VAL(T$)
1520 IF T$="*" THEN GOSUB 290: GOTO 1500
1530 IF T=0 THEN T=10
1540 IF T<1 OR T>10 THEN GOSUB 290: GOTO 1500
1550 GOSUB 2470: PRINT TAB(15) "Mission Report: " N$(T): PRINT
1560 IF (RND*100)>15 THEN GOTO 1590
1570 IF (RND*50)>45 THEN PRINT "Planes shot down over " N$(T): A(F,2)=A(F,2)-10:GOTO 510
1580 IF (RND*50)<8 THEN PRINT " All planes lost at sea " : A(F,2)=A(F,2)-10:GOTO 510
1590 PRINT : PRINT TAB(2) "Defense" TAB(14);: COLOR 15
1600 PRINT TAB(14) "Quantity" TAB(28);
1610 COLOR 0
1620 PRINT "Location": PRINT : FOR X=1 TO 5: IF RND*100<10 THEN PRINT "     - No Information- ": GOTO 1700
1630 COLOR 14
1640 PRINT TAB(2) D$(X) TAB(14);
1650 COLOR 15
1660 PRINT USING "######,";C(T,X);: PRINT TAB(28);
1670 COLOR 0
1680 IF C(T,X+5)<>0 THEN PRINT L$(C(T,(X+5)))+S$ ELSE PRINT
1690 PRINT
1700 NEXT : A(F,2)=A(F,2)-INT(RND*10)+1:GOTO 510
1710 K=0: FOR X=1 TO 10: FOR J=1 TO 5: IF A(X,J)<> 0 THEN GOTO 1790
1720 NEXT J: NEXT X: IF K>0 THEN GOTO 1710
1730 H=0: K=0: FOR X=1 TO 10: FOR J=1 TO 5
1740 IF C(X,J)<>0 THEN K=K+1
1750 IF A(X,J)<>0 THEN H=H+1
1760 NEXT J: NEXT X: IF K=0 THEN 2420
1770 IF H=0 THEN 2410
1780 GOTO 510
1790 C=1
1800 IF C(X,C)<>0 THEN 1830
1810 C=C+1: IF C=6 THEN GOTO 1720
1820 GOTO 1800
1830 IF A(X,(J+5))<>C(X,(C+5)) THEN GOTO 1810
1840 K=K+1: WIDTH 80: PRINT "       Battle in " N$(X) " on the " L$(A(X,(J+5)))+S$
1850 PRINT "  Enemy " D$(C) TAB(25) "Alliance " D$(J)
1860 O=C(X,C): O1=A(X,J): E=O: E1=O1
1870 IF (J=1 AND C<>1) THEN GOTO 2120
1880 IF (C=1 AND J<>1) THEN GOTO 2200
1890 SOUND 144,3
1900 PRINT TAB(4);: PRINT USING "######,";E;: PRINT TAB(25);: PRINT USING "######,";E1
1910 FOR XS=1 TO 500: NEXT
1920 GOSUB 2360
1930 IF E1=0 THEN E8=1: GOTO 1980
1940 IF E=0 THEN E8=0: GOTO 1980
1950 IF E1>E*1.8 THEN E8=0: GOTO 1980
1960 IF E>E1*1.8 THEN E8=1: GOTO 1980
1970 GOTO 1900
1980 FOR XS=1 TO 1000: NEXT :PRINT TAB(4);: PRINT USING "######,";E;: PRINT TAB(25);: PRINT USING "######,";E1: PRINT :PRINT TAB(9) " ** A VICTORY FOR THE ";
1990 IF E8=1 THEN PRINT "Enemy in ";: GOTO 2010
2000 PRINT "Allies in ";
2010 PRINT N$(X) " on the " L$(A(X,(J+5)))+S$
2020 PRINT :PRINT TAB(25) " - Total Casualties -"
2030 PRINT TAB(8) "Enemy " D$(C) " : ";: PRINT USING "######,"; O-E;
2040 PRINT TAB(38) "Allies " D$(J) " : ";: PRINT USING "######,"; O1-E1
2050 FOR XS=1 TO 1000: NEXT
2060 IF E8=1 THEN C(X,J)=C(X,J)+A(X,J): A(X,J)=0: GOTO 2100
2070 A(X,C)=A(X,C)+C(X,C): C(X,C)=0: A(X,(C+5))=C(X,(C+5))
2080 IF E>0 THEN PRINT TAB(22);: PRINT USING "######,";E;: PRINT " " D$(C) " were captured."
2090 FOR XS=1 TO 3000: NEXT :GOTO 1720
2100 IF E1>0 THEN PRINT TAB(22);: PRINT USING "######,";E1;: PRINT " " D$(J) " were captured."
2110 C(X,(J+5))=A(X,(J+5)): GOTO 1720
2120 SOUND 260,2
2130 FOR XS=1 TO 500: NEXT :PRINT USING "######,";E;: PRINT TAB(20);: PRINT USING "######,";E1
2140 GOSUB 2360
2150 IF E=0 THEN E8=0: GOTO 1980
2160 IF E1= 0 THEN E8=1: GOTO 1980
2170 IF E1/1000>E THEN E8=0: GOTO 1980
2180 IF E>E1*1000 THEN E8=1: GOTO 1980
2190 GOTO 2120
2200 SOUND 130,2
2210 PRINT USING "######,";E;: PRINT TAB(20);: PRINT USING "######,";E1
2220 FOR XS=1 TO 999: NEXT
2230 GOSUB 2360
2240 IF E=0 THEN E8=0: GOTO 1980
2250 IF E1=0 THEN E8=1: GOTO 1980
2260 IF E/1000>E1 THEN E8=1: GOTO 1980
2270 IF E1>E*1000 THEN E8= 1: GOTO 1980
2280 GOTO 2200
2290 R=INT(RND*100)+1:X2=INT(10*RND)+2:IF D=1 THEN X2=INT(100*RND)+2
2300 IF A(T,D)<=X2 THEN X2=A(T,D)
2310 A(T,D)=A(T,D)-X2
2320 RESTORE 2330: QUE=INT(RND*10)+1: FOR YUY=1 TO QUE: READ RE$: NEXT
2330 DATA "Heavy enemy attack!", "Sniper fire reported.", "Landing party hits heavy Enemy fire."," The transport ship has hit a underwater mine."," Violent seas sink a transport ship"
2340 DATA "Laser blasts destroys a ship.", "We are coming under heavy enemy attack"," Heavy resistance met on the beach. "," Under extreme artillery fire "," Ship hit by suicide attack"
2350 PRINT RE$:PRINT " We have lost " X2 D$(D) ". Total arrived in " N$(T) ":" A(T,D): RETURN
2360 E=E-INT((E/3)*RND)-2
2370 IF E<=0 THEN E=0
2380 E1=E1-INT((E1/3)*RND)-2
2390 IF E1<=0 THEN E1=0
2400 A(X,J)=E1: C(X,C)=E: RETURN
2410 PRINT "Sorry you blew it. All your defenses are gone": GOTO 2440
2420 PRINT "Congratulations, you conquered the enemy in " INT(Y6*.1) "years"
2430 PRINT "    and saved the world for democracy."
2440 GOTO 65000 ' End of program execution
2450 PRINT "Hit any key to continue"
2460 Q$=INKEY$: IF Q$="" THEN GOTO 2460 ELSE CLS: RETURN
2470 CLS:LOCATE 25: PRINT "Hit `*' for command list": LOCATE 1: PRINT "                                    ":LOCATE 1: RETURN
2475 CLS: SCREEN 0,0: COLOR 6:LOCATE 10,10
2476 PRINT "Take a few minutes to collect yourself, then come try again!!"
2477 GOTO 65000
2480 ' by Tom DeFelice, Leominster, ma 01453
65000 ' ****  Return to Magazette   ****
65001 COMMON ADDR.%,COLOR.MONITOR$ : LOCATE 25,1:PRINT SPACE$(79);:LOCATE 25,1:PRINT "  Press ESC key to continue ";:ANS$=INPUT$(1):IF ASC(ANS$)<>27 THEN 65001
65002 IF ADDR.%<>0 THEN CHAIN DRIVE$+":"+"START"
65005 END
