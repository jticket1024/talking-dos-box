5 CLS:REM Modified by Gern  07/10/83
10 PRINT "MADLIB Story-Writer by Dave Ahl based on a program"
15 PRINT "originally written by Henry Gallo, John Glenn HS.":PRINT 
20 DIM A$(11),B$(5),C$(10)
30 PRINT "O.K.  We're going to write some MAD-LIB stories"
40 PRINT "together.  You supply the words, I supply the plot.":PRINT 
50 PRINT:PRINT:PRINT:RANDOMIZE TIMER:CLS
60 PRINT "First I need 11 adjectives.  Adjectives modify"
70 PRINT "a noun, e.g., red, dirty, large, etc."
80 PRINT:FOR I=1 TO 11:PRINT I;:INPUT A$(I):NEXT I
90 CLS:PRINT :PRINT "Now I need 5 adverbs.  They modify verbs"
100 PRINT "and end in 'ly' (slowly, greedily, etc.)"
110 PRINT:FOR I=1 TO 5:PRINT I;:INPUT B$(I):NEXT I
120 CLS:PRINT "O.K.  Now 10 nouns, e.g., teletype, insect, coat, etc."
130 FOR I=1 TO 10:PRINT I;:INPUT C$(I):NEXT I
140 CLS:PRINT :PRINT "How about two first names of men"
150 INPUT "1 ";D$:INPUT "2 ";E$
160 PRINT:INPUT "The first name of a girl";F$
170 PRINT:INPUT "and someone's last name";G$
180 PRINT:INPUT "A geographical location";H$
190 PRINT:INPUT "A liquid";I$
200 PRINT:PRINT "And finally, an exclamatory word or two"
210 INPUT J$
220 CLS:PRINT:PRINT "Very good!  Would You like a newspaper ad (1),"
230 PRINT "a western (2), a story about the army (3), or"
240 INPUT "a waterbed (4).  Which one, 1, 2, 3, or 4";N:PRINT 
250 IF N>0 AND N<5 THEN 270
260 INPUT "Come on now -- 1, 2, 3, or 4";N:GOTO 250
270 CLS:FOR I=1 TO 6:ON N GOTO 300,400,500,600:NEXT I
300 PRINT "               Daily Grunt classifieds":PRINT 
310 PRINT "For sale: 1957 sedan.  This ";A$(1);" car is in a ";A$(2)
320 PRINT "condition. It was formerly owned by a ";A$(3);" school teacher"
330 PRINT "who always drove it ";B$(1);". There is a ";A$(4);" ";C$(1);" in"
340 PRINT "the back seat. It has a chrome ";C$(2);" on the hood, a ";A$(5)
350 PRINT "paint job, ";A$(6);" tires and the back opens into a ";A$(7)
360 PRINT C$(3);". Will consider taking a slightly used ";C$(4);" in trade."
370 PRINT :PRINT "Lost: In the vicinity of ";H$;", A ";A$(8);" french"
380 PRINT "poodle with ";A$(9);" hair and a ";A$(10);" tail. It answers"
385 PRINT "to the name of ";F$;". When last seen it was carrying a"
390 PRINT C$(5);" in its mouth. A ";A$(11);" reward is offered.":GOTO 700
400 PRINT "                 An adult western":PRINT 
410 PRINT "Tex ";G$;", the marshall of ";D$;" city rode into town. He"
420 PRINT "sat ";B$(2);" in the saddle, ready for trouble. He knew that"
430 PRINT "his ";A$(1);" enemy, ";E$;" the kid was in town. The kid was"
440 PRINT "in love with Tex's horse, ";F$;".  Suddenly the kid came out"
450 PRINT "of the ";A$(7);" Nugget saloon.":PRINT 
460 PRINT "'Draw Tex,' he yelled ";B$(3);".":PRINT 
470 PRINT "Tex reached for his ";C$(4);", but before he could get it"
475 PRINT "out of his ";C$(5);" the kid fired, hitting Tex in the ";C$(6)
480 PRINT "and the ";C$(7);".":PRINT 
485 PRINT "As Tex fell he pulled out his own ";C$(8);" and shot the kid"
490 PRINT INT(50*RND+3);"times in the ";C$(9);". The kid dropped in a pool"
492 PRINT "of ";I$;".":PRINT :PRINT "'";J$;",' Tex said, 'I hated to do it but"
495 PRINT "he was on the wrong side of the ";C$(10);".'":PRINT :GOTO 700
500 PRINT "If You plan on joining the ARMY, here are some ";A$(11)
510 PRINT "hints that will help you become a ";A$(10);" soldier.":PRINT 
520 PRINT "The ARMY is made up of officers, non-coms and ";C$(1);"s."
530 PRINT "You can recognize an officer by the ";C$(2);"s on his"
540 PRINT "shoulders and the funny-looking ";C$(3);"s on his cap."
550 PRINT "When You address an officer, always say ";C$(4);" and say it"
555 PRINT B$(5);".  If You get a ";A$(9);" haircut, keep your ";C$(5);"s"
560 PRINT "shined, and see that your ";C$(6);" is clean at all times,"
565 PRINT "You will be a credit to the slogan:":PRINT 
570 PRINT "            The army builds better ";C$(7);"s!":PRINT 
575 PRINT "At roll call, when the ";A$(8);" sergeant calls Your name,"
580 PRINT "shout '";J$;"' loud and clear.":PRINT 
585 PRINT "You will become familiar with weapons like the .30 calibre"
590 PRINT C$(8);" and the automatic ";C$(9);".":PRINT 
592 PRINT "Follow this advice and you may win the......"
595 PRINT "********** ";A$(7);" conduct ";C$(10);" **********":GOTO 700
600 PRINT "Bust-a-Button and Duck Dept. Store":PRINT "Dix Hills, New York"
610 PRINT :PRINT "Dear Sirs:":PRINT 
620 PRINT "Last week i purchased a ";A$(2);" contour water bed in your"
630 PRINT "store. I got it especially for my ";A$(4);" husband who sleeps"
640 PRINT "very ";B$(1);" and says that ";A$(6);" water beds that have"
650 PRINT C$(1);"s in them make his ";C$(3);" ache. When the bed"
655 PRINT "arrived my husband tested it ";B$(2);" and said the ";A$(8)
660 PRINT C$(5);" was bent and kept pressing into his ";C$(6);". He says"
665 PRINT "this could lead to a ";A$(10);" condition of the ";C$(8);".":PRINT 
670 PRINT "I would like to exchange this ";A$(9);" bed for one that"
675 PRINT "will allow my husband to sleep ";B$(4);" and won't make"
680 PRINT "his ";C$(10);" sore.":PRINT :PRINT TAB(30);"Yours ";B$(5);","
690 PRINT TAB(30);F$;" ";G$:PRINT 
700 PRINT:INPUT "Want another story (YES or NO)";Y$
710 IF Y$="NO" THEN 900
720 PRINT:INPUT "Want to use the same words (YES or NO)";Y$:PRINT
730 IF Y$="YES" THEN 220 ELSE IF Y$="NO" THEN 60
740 PRINT:INPUT"Come on now -- 'YES' or 'NO'";Y$:PRINT :GOTO 730
900 PRINT :PRINT "O.K.  See You again sometime!"
999 END 
