0 rem " Adventure 1.5 copyright(c)1980 by John O'Hare
1 lu=150:lf=2
2 lt=62:rem light object #
3 tv=15:t7=20:mx=tv*t7:mx=mx+50:rem tv=treasurevalue/t7=#of treasures/mx=max SCORE
4 ol=10:rem object limit
5 for i=0 to 5:READ d$(i):next 
6 data NORTH,south,east,west,up,down
7 tr=7:rem treasure storage room
8 rem n=nouns/v=verbs/r1=rooms/o=objects/m=messages/c=commands/a=actions
9 GOSUB 2200
10 READ n,v,r1,o,m,c,a,dr:dim n$(n),v$(v),rm$(r1),rm(r1,5),ob(o),ob$(o),ob%(o)
11 lb=57:rem limbo
12 dt=58:rem death room
13 RANDOMIZE TIMER
14 ad=3:ad$=" HAUNTED MANSION ":rem adventure number and name
15 dim ow$(o),m$(m),c$(c)
16 PRINT"Welcome to Adventure";ad
17 PRINT:PRINT""TAB(17-(LEN(ad$)/2));"*"ad$"*"
18 DEF FNr(x)=INT(RND(1)*x)+1
19 gn=4:so=50
20 GOSUB 2000
30 for i=0 to n:READ n$(i):next 
40 for i=0 to v:READ v$(i):next 
50 for i=0 to r1
60 READ rm$(i)
70 for j=0 to 5
80 read rm(i,j)
85 next j
90 next i 
100 for i=1 TO O
110 READ ob$(i),ow$(i),ob%(i),ob(i)
120 next 
130 for i=1 to m
140 READ m$(i)
150 next 
160 rm$(lb)=rm$(lb)+CHR$(13)+"A sign here says: LIMBO, find right exit "
161 rm$(lb)=rm$(lb)+" and live again."
170 for i=1 to c
171 READ c$(i):next 
175 r=1:rem (c)1980 john o'hare
176 READ w0:dim w1$(w0),w2$(w0):for wc=1 to w0:READ w1$(wc),w2$(wc):next :READ e4$:jp=1
181 for i=1 to 7:READ e$(i):next :for i=1 to 3:READ hs$(i):next 
182 READ e5$
186 PRINT"hit any key to begin adventure"
187 a$=input$(1)
189 INPUT"use saved game n";g$
190 if left$(g$,1)="n" then 198
193 OPEN "advent.sav" for input as 1
194 INPUT#1,r,lu,df,lf,so,gn,ga,gf
195 for sg=1 TO O
196 INPUT#1,ob%(sg):next 
197 CLOSE 1
198 CLS
199 PRINT""
200 df=0:if r>dr and ob%(lt)<>-1 and ob%(lt)<>r then df=1
201 if r=lb or r=dt then df=0
202 if df=0 then 209
203 PRINT"I CAN'T SEE IN THE DARK!"
204 PRINT:goto 330
209 if LEFT$(rm$(r),1)="*" then rm$=MID$(rm$(r),2):goto 220
210 PRINT"You're in ";:rm$=rm$(r)
220 PRINT Rm$"."
225 if r=dt then PRINT:so=so-12:qf=1:goto 1200
230 x=1:uf=0
240 PRINT:for vi=1 TO O:if ob%(vi)=r and x=1 then PRINT"VISIBLE ITEMS HERE:":x=0:uf=1
245 if ob%(vi)<>r then 260
250 PRINT ob$(vi)". "
260 next 
265 if uf then PRINT
270 x=1
280 PRINT:for oe=0 to 5
290 if rm(r,oe)<>0 and x=1 then PRINT"OBVIOUS EXITS:":x=0
300 if rm(r,oe)<>0 then PRINT D$(oe)" ";
310 next :PRINT
315 PRINT""
320 if ob%(lt)=-1 then lu=lu-1
321 GOSUB 400
322 ht=ht+1
325 GOSUB 4300
327 GOSUB 3900
328 if gk=1 then gk=0:so=so-5:goto 200
330 PRINT: PRINT"WHAT NEXT";
331 i1$="":i2$=""
340 INPUT ip$:i1$=ip$
341 CLS
342 if ip$="" then PRINT"":goto 330
345 PRINT
350 for sb=1 to LEN(ip$)
360 if MID$(ip$,sb,1)=" " then i1$=LEFT$(ip$,sb-1):i2$=RIGHT$(ip$,LEN(ip$)-sb):goto 380
370 next 
380 goto 500
390 rem lamp and attack checks
400 if lu<50 and lf=2 and ob%(lt)<>0 then PRINT"You're flashlight is getting dim!":lf=1
410 if lu<38 and lf=1 and ob%(lt)<>0 then PRINT"You're flashlight is getting dimmer!!":lf=0
420 if lu<25 and lu>0 and ob%(lt)<>0 then PRINT:PRINT"LIGHT RUNS OUT IN"lu"TURNS!"
430 if lu=0 and lf=0 then PRINT"LIGHT HAS RUN OUT!!!":lf=-1:ob%(lt)=0
440 rem 
450 RETURN
500 if i1$="go" then i1$=i2$:i2$=""
501 if i1$="say" then i1$=i2$:i2$=""
505 v1$=LEFT$(i1$,3):n1$=LEFT$(i2$,3):i1=0:i2=0
506 if v1$="fee" and n1$="oys" then n1$="big"
507 for w3=1 to w0:if v1$=w1$(w3) then v1$=w2$(w3)
508 next 
509 if n1$="hou" or n1$="tra" then n1$="doo"
510 for vl=1 to v
520 if v1$=v$(vl) then i1=vl:goto 540
530 next 
540 if i2$="" then 570
541 for nl=1 to n
550 if n1$=n$(nl) then i2=nl:goto 570
560 next 
570 if i1=0 then PRINT"YOU USE WORD(S) I DON'T KNOW":goto 330
580 if i1<7 and i1>0 then 1100
590 mf=0:k=0
595 jp=1:c7=53:if i1>17 then jp=54:c7=c
600 for y=jp to c7
610 if VAL(MID$(c$(y),1,2))<>i1 then 710
615 if VAL(MID$(c$(y),3,2))=0 then 630
620 if VAL(MID$(c$(y),3,2))<>i2 then 710
630 k=0:mf=1:for z=5 to 16 STEP 4
635 c1=VAL(MID$(c$(y),z,2)):c2=VAL(MID$(c$(y),z+2,2))
640 if c1=1 then if ob%(c2)<>-1 then 710
650 if c1=2 then if r<>c2 then 710
660 if c1=3 then if ob%(c2)<>0 then 710
680 if c1=5 then if ob%(c2)<>r then 710
695 k=k+1:if k=3 then an=y:goto 800
700 next 
710 next y
715 if mf=1 then PRINT"I CAN'T DO THAT NOW.":goto 730
720 PRINT"I DON'T UNDERSTAND?"
730 goto 320
800 af=0:dh=0:rf=0:rc=0
805 for g=17 to 34 STEP 6:a1=VAL(MID$(c$(an),g,2)):a2=VAL(MID$(c$(an),g+2,2))
807 a3=VAL(MID$(c$(an),g+4,2))
810 if a1=1 then PRINT m$(a2)".":goto 910
820 if a1=2 then ob=ob%(a2):ob%(a2)=ob%(a3):ob%(a3)=ob:goto 910
830 if a1=3 then ob%(a2)=0:goto 910
840 if a1=4 then ob%(a2)=r:goto 910
850 if a1=5 then r=a2:rc=1:goto 910
860 if a1=6 then dh=1:r=lb:so=so-5:goto 910
870 if a1=7 then GOSUB 920:goto 910
880 if a1=8 then GOSUB 1000:goto 910
885 if a1=17 then rm(r,a2)=a3:goto 910
890 if a1=9 then ob%(a2)=a3:goto 910
900 if a1=10 then GOSUB 1050:goto 910
901 if a1=77 then dh=1:goto 910
903 if a1=18 then GOSUB 4200:goto 910
904 if a1=11 then rc=1:goto 910
905 if a1=12 then GOSUB 1200:goto 910
906 if a1=13 then qf=1:goto 1200
907 if a1=14 then h=a2:af=1:GOSUB 950:goto 910
908 if a1=15 then e=a2:af=1:GOSUB 1030:goto 910
909 if a1=16 then GOSUB 2502:goto 910
910 next 
911 if dh=1 then PRINT:goto 200
912 if rc=1 then 199
913 goto 320
920 for h=1 TO O
930 if n1$=ow$(h) then 950
940 next:if i2$<>"" then PRINT"I see no "i2$" here!":RETURN
941 PRINT"be specific"
942 RETURN
950 if ob(h)=1 then PRINT"IT'S BEYOND MY POWER to DO THAT!":RETURN
952 if ob(h)>1 then PRINT"Don't be stupid!":RETURN
955 if ob%(h)=-1 then PRINT"I'm already carrying it.":RETURN
960 if ob%(h)<>r then PRINT"I DON'T SEE IT HERE!":RETURN
961 os%=0:for pq=1 TO O:if ob%(pq)=-1 then os%=os%+1
962 next 
970 if os%+1>ol then PRINT"I'VE TOO MUCH to CARRY":RETURN
980 PRINT"ok,":ob%(h)=-1:RETURN
990 rem 
1000 if n1$<>"tre" then 1008
1001 td%=0:for td=1 TO O
1002 if LEFT$(ob$(td),1)<>"*" then 1006
1003 if td=52 and ob%(td)=-1 and ob%(42)<>r then PRINT m$(24):ob%(52)=0:ob%(76)=r:goto 1006
1004 if td=52 and ob%(td)=-1 then PRINT M$(43):ob%(td)=r:goto 1006
1005 if ob%(td)=-1 then ob%(td)=r:PRINT"dropped: ";ob$(td):td%=1
1006 next td:if td%=0 then PRINT"You have no treasure to drop."
1007 RETURN
1008 if i2$="" then PRINT"be specific":RETURN
1009 for e=1 TO O
1010 if n1$=ow$(e) then 1030
1020 next :PRINT"You don't have it!":RETURN
1030 if ob%(e)=-1 then ob%(e)=r:PRINT"ok,":RETURN
1040 PRINT"I'M NOT CARRYING IT!":RETURN
1050 os=0:PRINT"I'M CARRYING:"
1070 for i7=1 TO O
1080 if ob%(i7)<>-1 then 1090
1081 os=1
1082 if LEN(ob$(i7))+POS(0)>36 then PRINT
1083 PRINT ob$(i7)". ";
1090 next 
1091 if os then PRINT:RETURN
1092 PRINT"nothing":RETURN
1100 if rm(r,i1-1)=0 and df=0 then PRINT"I CAN'T GO IN THAT DIRECTION.":goto 320
1101 if rm(r,i1-1)=0 and df=1 then PRINT"I FELLDOWN and BROKE MY NECK!!":goto 1130
1102 if ob%(54)=r and i1=1 then PRINT"The goblin won't let me by!":goto 320
1105 if ob%(55)=r and i1=3 then PRINT"The troll is blocking my way!":goto 320
1110 r=rm(r,i1-1):goto 199
1120 END
1130 r=lb:so=so-5:goto 200
1200 sc=0:for i=1 TO O
1210 if RIGHT$(ob$(i),1)="*" and ob%(i)=tr then sc=sc+tv
1220 next 
1222 if qf=1 then so=so-4
1223 if so<0 then so=0
1225 sc=sc+so
1230 if qf=1 then 1300
1240 PRINT"YOUR SCORE IS";sc"OUT OF A POSSIBLE"mx
1250 if sc-so=300 then PRINT:goto 1300
1255 RETURN
1300 PRINT"You SCORED"sc"points."
1310 PRINT"There were"mx"points possible."
1311 if sc<1 then PRINT"You must have nothing but space between your ears!":END
1312 if sc<51 then PRINT"You are obviously a rank amateur.":END
1313 if sc<75 then PRINT"You're SCORE qualifies you as a novice class adventurer.":END
1314 if sc<125 then PRINT"You've achieved the rating: Experienced Adventurer":END
1315 if sc<175 then PRINT"You have reached the 'junior master' status.":END
1320 if sc<225 then PRINT"Your SCORE puts you in MASTER ADVENTURER class -D-":END
1330 if sc<295 then PRINT"Your SCORE puts you in MASTER ADVENTURER class -C-":END
1335 if sc<345 then PRINT"Your SCORE puts you in MASTER ADVENTURER class -B-":END
1340 if sc<350 then PRINT"Your SCORE puts you in MASTER ADVENTURER class -A-":END
1350 if sc=mx then PRINT"All of adventuredom gives tribute"
1355 if sc=mx then PRINT" to you, ADVENTURERGR and MASTER!":END
2000 PRINT
2010 PRINT"This is adventure!!!"
2020 PRINT"I will be your eyes and hands as you"
2030 PRINT"search for lost treasure."
2040 PRINT
2050 PRINT"The object is to recover"t7"treasures"
2060 PRINT" and return them to the proper place."
2065 PRINT"Treasures have a '*' in there name":PRINT
2070 PRINT"Use two word commands consisting of"
2080 PRINT"a verb and a noun, example:"
2090 PRINT
2100 PRINT"go west, take gold, enter hole, shoot"
2110 PRINT
2120 PRINT"inventory, save game, drop treasure"
2121 PRINT"hit any key to continue"
2122 a$=input$(1)
2123 cls:PRINT" Words to Know "
2125 PRINT"READ - if you have something that can be READ you will READ it."
2126 PRINT"ENTER - will take you in to a hole, passage, building, etc..
2127 PRINT"INVENTORY - gives you list of what you are carrying."
2128 PRINT"LOOK - describes the room you are in."
2129 PRINT:PRINT"Use TAKE and DROP in most cases to manipulate objects."
2130 PRINT
2131 PRINT"There are many more words that will be discovered as you explore some may
2132 PRINT"even be magic, usually when you tell me things you should use two words."
2133 PRINT:PRINT"if you find one word doesn't work just try another."
2151 PRINT
2153 PRINT"Please wait while I load my data ..."
2160 RETURN
2165 PRINT"Treasures have a (*) in their name":PRINT
2200 PRINT"";
2400 PRINT" adventure "
2402 PRINT" by- "
2404 PRINT"john o'hare "
2450 PRINT" hit any key to begin adventure"
2455 a$=input$(1)
2465 RETURN
2502 OPEN"advent.sav" for output as 1
2503 PRINT#1,r:PRINT#1,lu:PRINT#1,df:PRINT#1,lf:PRINT#1,so:PRINT#1,gn:PRINT#1,ga:PRINT#1,gf
2505 for sg=1 TO O
2506 PRINT#1,ob%(sg):next 
2507 CLOSe 1
2508 RETURN
2999 rem n,v,r,o,m,c,a,dr
3900 if ob%(54)=r or ob%(55)=r or ob%(73)=r or ob%(74)=r then ob%(61)=0:RETURN
3999 if r<8 or r=lb or r=(dt) or ht<35 then RETURN
4000 if gn=0 then RETURN
4005 if ga=1 then ob%(61)=r:goto 4030
4010 if FNr(32)=7 then ga=1:goto 3999
4020 ob%(61)=0:RETURN
4030 rem 
4040 PRINT"There is a threatening little ghost in the room with you!"
4050 gf=(1-gf)
4060 if gf then RETURN
4070 PRINT"The ghost attacks!"
4080 if FNr(6)=2 then PRINT"It got you!":r=lb:ga=0:gk=1:RETURN
4090 PRINT"It missed!"
4100 RETURN
4200 if FNr(5)>2 then PRINT"You missed!":RETURN
4210 PRINT"You got him!":PRINT"The ghost disappears in a white mist!"
4220 gn=gn-1:ga=0:gf=0:ob%(61)=0
4230 RETURN
4300 if r=27 then PRINT"A clock shows the time: ";TIME$
4305 if ha=1 then PRINT"The spirit of Hubie Marsten appears and says:":PRINT hs$(FNr(3))"."
4306 if ha=1 then ob%(74)=r:ha=0
4310 if FNr(5)=4 and r>6 then PRINT e$(FNr(7))"."
4315 if r>3 and r<7 and FNr(4)=2 then PRINT"The trees seem to be alive!"
4316 if r>32 and r<36 and ob%(74)<>r then if FNr(3)=2 then ha=1
4317 if ob%(74)=r and FNr(3)=2 then PRINT"The spirit vanished??":ob%(74)=0:ha=0
4320 if ob%(54)=r and FNr(3)=2 then 4400
4330 if ob%(55)=r and FNr(3)=2 then 4500
4335 if ob%(73)=r and FNr(3)=2 then 4600
4340 RETURN
4400 PRINT"The goblin attack with an axe in hand!"
4410 if FNr(6)=4 then PRINT"CRUNCH!! He gets you!":r=lb:gk=1:RETURN
4420 PRINT"WHOOSH! He misses!":RETURN
4500 PRINT"The troll attacks!"
4510 if FNr(7)=6 then PRINT"He smashes your skull!":r=lb:gk=1:RETURN
4520 PRINT"He missed!":RETURN
4600 PRINT"Barlow attacks!"
4610 if ob%(38)=-1 then PRINT"He sees your cross and backs off!":RETURN
4620 if FNr(4)=3 then PRINT"He got you!!":r=lb:gk=1:RETURN
4630 PRINT"He missed!":RETURN
10269 data 35,31,58,78,59,91,0,35
10279 rem 
10289 rem 
10299 rem 
10309 data nnn,mai,big,spi,let,doo,mat,but,kni,bot,liq,des,oys,pap,red
10319 data blu,gre,boo,gol,clo,cof,pot,sta,lad,gun,cro,tru,rug,gob,gho,tro,sig
10329 data fla,win,gam,pas
10339 data vvv,nor,sou,eas,wes,up,dow,tak,dro,ent,inv,loo,sco,qui,hel,pus
10349 data rea,ope,mov,sho,run,kil,eat,cli,tur,on,off,dri,cbm,sav,thr,sta
10359 rem 
10369 rem 
10379 data "room #0",0,0,0,0,0,0
10389 data "*You're on an old worn path",6,0,1,2,0,0
10399 data "*You're walking along an old path",5,0,1,3,0,0
10409 data "front of a big spooky house",0,7,2,4,0,0
10419 data "the haunted forest",5,4,4,5,0,0
10429 data "the haunted forest",4,4,6,4,0,0
10439 data "the haunted forest",5,4,1,5,0,0
10449 data "*You're on the porch of the old house",3,0,0,0,0,0
10459 data "an old dusty living room",7,12,9,0,0,0
10469 data "the dining room",0,13,10,8,0,0
10479 data "the kitchen",0,0,11,9,0,0
10489 data "a musty old pantry",0,0,0,10,0,0
10499 data "the family room",8,0,13,0,00,0
10509 data "a small alcove",9,16,0,12,26,0
10519 data "the study",0,17,0,0,0,0
10529 data "*You're at the west end of a corridor ",0,18,16,0,0,0
10539 data "the middle of a corridor ",13,19,17,15,0,0
10549 data "*You're at the east end of a corridor ",14,20,0,16,0,0
10559 data "the Den",15,0,0,0,0,0
10569 data "the Library",16,0,0,0,0,0
10579 data "a Guest Room",17,0,0,21,0,0
10589 data "a dusty closet",0,0,20,0,0,0
10599 data "the trophy room",0,25,0,0,0,0
10609 data "an old dusty nursery",0,26,0,0,0,0
10619 data "a large bathroom",0,27,0,0,0,0
10629 data "*You're at the west end of a hallway",22,28,26,0,0,0
10639 data "a stairwell",23,31,27,25,0,13
10649 data "*You're at the east end of a hallway",24,32,0,26,0,0
10659 data "the master bedroom",25,0,0,29,0,0
10669 data "a dark, dusty closet. A ladder leads up here",0,0,28,0,30,0
10679 data "*You're on the ladder",0,0,0,0,0,29
10689 data "a yellow bedroom",26,0,0,0,0,0
10699 data "a white bedroom",27,0,0,0,0,0
10709 data "the west end of the attic",0,0,34,0,0,30
10719 data "the east end of the attic",0,35,0,33,0,0
10729 data "a dirty old storeroom",34,0,0,0,0,0
10739 data "the basement, there is a large open door to the south",0,40,0,37,10,0
10749 data "a laundry room",0,0,36,38,0,0
10759 data "a musty, old room in the basement",0,39,37,0,0,0
10769 data "an old wine cellar",38,0,0,0,0,0
10779 data "a chilly, damp room",36,0,0,0,0,0
10789 data "a secret underground cavern",0,42,0,0,40,0
10799 data "a narrow passage",41,0,43,0,0,0
10809 data "a drafty chamber",0,0,44,42,0,0
10819 data "a large circular chamber",45,0,46,43,0,0
10829 data "a splendid chamber 30 feet high",47,44,0,0,0,0
10839 data "a roughlycarved room, a pit leads down",0,52,0,44,0,53
10849 data "*You're at a junction of three passages",0,45,48,56,0,0
10859 data "a dusty passage",0,0,49,47,0,0
10869 data "an old broken passage",0,0,50,48,0,0
10879 data "*You're at a turn in the passage",0,51,0,49,0,0
10889 data "a ORTURE CHAMBER",50,0,0,0,0,0
10899 data "a cold damp room",46,0,0,0,0,0
10909 data "a pit",0,0,0,0,0,54
10919 data "a pit",0,0,0,0,0,55
10929 data "a pit. There are no exits",0,0,0,0,0,0
10939 data "a dead end passage",0,0,47,0,0,0
10949 data " a misty room with strange looking exits",1,58,58,58,5,58
10959 data " H E L L *",0,0,0,0,0,0
10969 rem 
10979 rem CLOSE
10989 rem 
10999 data "MAILBOX (name on it: Hubie Marsten)",mai,3,1
11009 data ring of keys,key,0,0
11019 data old letter,let,0,0
11029 data "LARGE PADLOCKED DOOR",doo,7,1
11039 data "OPEN DOOR ",doo,0,1
11049 data door mat,mat,7,0
11059 data "BUTTON BY door (doorbell?)",but,7,1
11069 data "SIGN - LEAVE TREASURE HERE SAY SCORE",sig,7,1
11079 data "FURNITURE",fur,8,1
11089 data "TABLE & CHAIRS",tab,9,1
11099 data *silver candlesticks*,can,9,0
11109 data *silver knife*,kni,10,0
11119 data pool of blood,blo,10,1
11129 data "BIGMAC",big,10,0
11139 data "vial of strange liquid",via,11,0
11149 data *wallet full of $100s*,wal,12,0
11159 data "DESK",des,14,1
11169 data slip of paper,pap,0,0
11179 data magic gun,gun,0,0
11189 data *gold watch*,wat,18,0
11199 data *rare painting*,pai,18,0
11209 data red book,red,19,0
11219 data blue book,blu,19,0
11229 data green book,gre,19,0
11239 data *gold book*,gol,0,0
11249 data "CLOSET",xxx,20,1
11259 data bed,bed,20,1
11269 data * oriental rug*,rug,20,0
11279 data cloths,clo,21,1
11289 data *mink*,min,21,0
11299 data *gold medal*,med,22,0
11309 data crib,cri,23,1
11319 data * $1000 savings bond *,bon,23,0
11329 data toilet, toi,24,1
11339 data sink,sin,24,1
11349 data a bubbling potion,pot,24,0
11359 data stairs going down,sta,26,1
11369 data wooden cross,cro,26,0
11379 data "CLOSET",xxx,28,1
11389 data *pearl necklace*,nec,28,0
11399 data bed,bed,28,1
11409 data *velvet pillow*,pil,28,0
11419 data ladder,lad,29,1
11429 data furniture,fur,31,1
11439 data "*Commodore PET*",pet,31,0
11449 data furniture,fur,32,1
11459 data cobwebs,cob,33,1
11469 data *rare family jewels*,jew,34,0
11479 data "A body hanging from a noose (Hubie?)",bod,35,1
11489 data old trunk,tru,35,1
11499 data *rare coins*,coi,0,0
11509 data *bottle of rare imported wine*,win,39,0
11519 data "An old wooden Coffin",cof,40,1
11529 data "A dangerous GOBLIN",gob,45,2
11539 data "A mean TROLL",tro,43,2
11549 data "a torture rack",rac,51,1
11559 data *gold death mask*,mas,51,0
11569 data "GIANT OYSTER",oys,52,1
11579 data *sack of diamonds*,dia,55,0
11589 data *large ruby*,rub,56,0
11599 data "AGHOST",gho,0,2
11609 data flashlight(on),fla,0,0
11619 data flashlight(off),fla,11,0
11629 data large rug,xxx,10,1
11639 data locked trapdoor ,tra,0,1
11649 data open trapdoor ,tra,0,1
11659 data locked trapdoor ,tra,30,1
11669 data open trapdoor ,tra,0,1
11679 data *giant pearl*,pea,0,0
11689 data stairs going up,sta,13,1
11699 data cobwebs,cob,8,1
11709 data "SIGN SAYS - YOU SHALL DIE...",sig,5,1
11719 data "-Barlow the Vampire-",vam,0,2
11729 data "The spirit of Hubie Marston",spi,0,1
11739 data empty vial,via,0,0
11749 data sweet smelling broken glass,gla,0,0
11759 data "PASSAGE",pas,0,1
11760 data empty bottle,bot,0,0
11769 data nothing happens
11779 data "how should I know what to do"
11789 data "I'M DEAD"
11799 data a couple things fell out
11809 data something fell out
11819 data the trapdoor creaks open
11829 data the door creaks open
11839 data "It says: those who enter may not leave heres a hint they may need"
11849 data "if something is TOO heavy to take, try to move it"
11859 data "OK, I moved it"
11869 data "OK, weird laughing comes from the house"
11879 data "the troll drinks the potion and turns in to a frog, the frog hops away"
11889 data "you got him. The goblin and disappears in a cloud of black smoke"
11899 data "Barlow turns to dust and blows away"
11909 data "The ghost vaporizes"
11919 data "It's poison I'm dead!"
11929 data "Everything changes and ...."
11939 data "BANGBANGBANG"
11949 data you got him
11959 data you missed
11969 data "it says - Hubie is harmless"
11979 data "It says:"
11989 data "This book is a treasure"
11999 data "The bottle of wine drops with a CRASH!"
12009 data "It's empty"
12019 data "some treasures may be weapons shoot goblin-troll potion!
12029 data "say CBM"
12039 data "Kill a Vampire with *silver* when trapped, take a drink"
12049 data indentify book by color 
12059 data "I don't have it"
12069 data "Where?!?"
12079 data "The BLUE book turns in to a *gold* book"
12089 data "thanks I was hungry"
12099 data the flashlight is on
12109 data the flashlight is off
12119 data "I can't move that!"
12129 data there was something under it
12139 data "OK,"
12149 data "It's locked"
12159 data "It's too heavy"
12169 data "I need a key"
12179 data "The oyster opens up to eat the BIG MAC and a pearl rolls out!"
12189 data "The bottle lands softly on the pillow"
12199 data "A voice booms out.....PLEASE LEAVE IT ALONE!"
12209 data "The lid of the Coffin rises and Barlow the vampire jumps out!!!"
12219 data "I don't see it here"
12229 data "HOW?!?"
12239 data "I have nothing to READ ","NOT WELCOME",closets aren't closed
12249 data "I have nothing to eat"
12259 data "I have nothing to drink"
12269 data "I don't have a gun"
12279 data "I don't have a knife"
12289 data "Barlow stops me!"
12299 data "There is nothing to attack here"
12309 data "The bullets bounce off the shell!"
12310 data "He only eats big macs and I don't have one"
12312 data "hic hic, burp. Boy was that good!"
12319 data 1000000000000000100000000000000000,1100000000000000110000000000000000
12329 data 1200000000000000120000000000000000,1300000000000000130000000000000000
12339 data 0712055800000000014000000000000000,0720055300000000014000000000000000
12349 data 0726055000000000014000000000000000,0717000000000000012900000000000000
12355 data 0731000000000000014400000000000000
12359 data 0727056400000000014000000000000000
12369 data 0732056300000000146300000000000000,0802055803690114014200046900031400
12370 data 0802055800000000015800000000000000
12379 data 0821055501360000011200035500033600,0832016300000000156300000000000000
12389 data 0817000000000000012900000000000000,0905050400000000013900000000000000
12399 data 0833015205420000014300155200000000,0833015200000000012400155200025276
12402 data 0700000000000000070000000000000000,0800000000000000080000000000000000
12409 data 0905050500000000050800000000000000,0905056600000000053600000000000000
12419 data 0905056800000000053300000000000000
12429 data 0919052600000000052100000000000000,0919053900000000052900000000000000
12439 data 0935057700000000054100000000000000
12445 data 1400000000000000010200000000000000
12449 data 1507050700000000011100000000000000
12459 data 1604010300000000010800010900000000
12469 data 1606050600000000012200014900000000
12479 data 1613011800000000012100000000000000
12489 data 1617000000000000012900000000000000
12499 data 1614012200000000012200012600000000
12509 data 1615012300000000012200012700000000
12519 data 1616012400000000012200012800000000
12529 data 1618012500000000012200012300000000
12539 data 1600000000000000014800000000000000
12549 data 1720037305530000012500014500047300
12555 data 1720055300000000012500000000000000
12559 data 1705050401020000010700020405170108
12569 data 1701050103020303010400040300040200
12579 data 1701050100000000012500000000000000
12589 data 1705056501020000010600026566170536
12599 data 1705056500000000014100000000000000
12609 data 1705050400000000014100000000000000
12619 data 1705056701020000010600026768170433
12629 data 1705056700000000014100000000000000
12639 data 1719000000000000015000000000000000
12649 data 1711051703180319010400041800041900
12659 data 1726055003510000010500045100000000
12669 data 1726055000000000012500000000000000
12679 data 1711051700000000012500000000000000
12689 data 1820057305530000015500000000000000
12699 data 1820037305530000014500015500047300
12709 data 1827056403650366011000013700046500
12719 data 1820055303770000011000013700047700
12729 data 1812000000000000013600000000000000
12739 data 1900056101190000011800180000000000
12749 data 1900057401190000011800010100000000
12759 data 1900055401190000011800011300035400
12769 data 1900055501190000011800010100000000
12779 data 1900055801190000015700000000000000
12789 data 1900057301190000011800010100000000
12799 data 1900011900000000015600000000000000
12809 data 1900000000000000015300000000000000
12819 data 2000000000000000013100000000000000
12829 data 2100000000000000014700000000000000
12839 data 2200011400000000013300031400000000
12849 data 2200000000000000015100000000000000
12859 data 2323054300000000053000000000000000
12869 data 2400016200000000013500026263770000
12879 data 2400016300000000013400026263770000
12889 data 2500016300000000013400026263770000
12899 data 2600016200000000013500026263770000
12909 data 2710011502550000011700021575050600
12919 data 2710011500000000011700021575050800
12925 data 2733015200000000015900025278000000
12929 data 2721013600000000011600033600060000
12939 data 2721000000000000013000000000000000
12940 data 2710000000000000013000000000000000
12941 data 2700000000000000015200000000000000
12949 data 2800012300000000013200022325000000
12959 data 2800000000000000010100000000000000
12969 data 2934000000000000160000000000000000
12979 data 3008057301120000011400097399041200
12989 data 3008011200000000010100041200000000
12999 data 3008000000000000015400000000000000
13000 data 3100057301120000011400097399000000
13001 data 3100011200000000010100000000000000
13002 data 3100000000000000015400000000000000
13009 data 13,n,nor,s,sou,e,eas,w,wes,u,up,d,dow,get,tak,use,sho,sea,ope,att,kil
13010 data fir,sho,fee,dro,giv,dro,end
13012 data "A black cat runs by","I heard a funny noise","I think I hear footsteps"
13014 data "Was that a scream I heard ?","I feel a chill","A spider runs by"
13016 data "I think I heard a door slam"
13019 data "Who dares to enter this house","You shall die..."
13020 data "Get out, get out before it's TOO late"
13029 data "the end"
