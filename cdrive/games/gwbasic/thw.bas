5 defint b-g,i-z:randomize timer:key off
10 h=70:x=22:y=22:print "Treasure Hunt 1.02!":gosub 1210
15 sword=1:gun=1:bullets=int(rnd(1)*3)+4:accuracy=.5
20 dim grid(x+1,y+1)
30 for i=0 to x
31 for j=0 to y
32 grid (i, j)=int(rnd(1)*5)+1
33 next j,i
34 winx=int(rnd(1)*x)+1:winy=int(rnd(1)*y)+1:grid(winx,winy)=6 'treasure for look
40 px=int(rnd(1)*x)+1:py=int(rnd(1)*y)+1:if px=winx or py=winy then 40
42 gosub 44:goto 51
44 if grid(px,py)=6 then print "treasure is here.":goto 51
45 print "treasure is to the ";:if winy>py then print "south";
46 if winy<py then print "north";
47 if winx<px then print "west."
48 if winx>px then print "east."
49 print
50 return 'two possibilities: either in stats or gamestart which is why we use gosub
51 print "sector ("px","py")":goto 170
70 if h<=0 then print "sorry, you died":end
73 print "health=";h:print "move??"
74 p$=inkey$:if p$="" then 74
75 if look=1 then 1215
76 if p$="l" then look=1:goto 74
80 if len(p$)=2 and asc(right$(p$,1))=72 then py=py+1:goto 156
90 if len(p$)=2 and asc(right$(p$,1))=80 then py=py-1:goto 156
100 if len(p$)=2 and asc(right$(p$,1))=77 then px=px+1:goto 156
110 if len(p$)=2 and asc(right$(p$,1))=75 then px=px-1:goto 156
115 cls:if p$="s" then print "sector: ("px","py")":goto 1100
116 print "invalid keypress.":goto 74
156 cls:if px<0 then px=x
157 if px>x then px=0
158 if py<0 then py=y
159 if py>y then py=0
160 print "("px","py")"
165 if px=winx and py=winy and ch>=5 then print "you win!":end
167 'what is here?
170 w=grid(px,py):if w=1 then print "there is nothing here":goto 70
180 if w=2 then print "there is a snake here":goto 1000
190 if w=3 then 1020 'sword resides
200 if w=4 then print "a challenger lurks here":goto 1040
210 if w=5 then print "you pick up a bullet":bullets=bullets+1:goto 70
220 if w=6 then print "the treasure is here, but your goal has not been met.":goto 70
999 'snake is here
1000 if sword>0 then print "you kill it with one of your swords; the sword shatters because of the blow.":sword=sword-1:grid(px,py)=1:goto 70
1005 snake=int(rnd(1)*2)+1:if snake=1 then print "it does nothing":goto 70
1010 if snake=2 then print "it bites you!":h=h-5:goto 70
1019 'sword is here
1020 print "you pick up a sword":grid(px,py)=1
1025 s=int(rnd(1)*2)+1:if s=1 then print "it turns into 5 points of health":h=h+5:else sword=sword+1
1030 goto 70
1039 'challenger
1040 a=int(rnd(1)*30)+20
1050 print "your health="h", opponents health ="a
1051 if sword>=1 and gun=1 and bullets>0 then 1058
1052 if gun=1 and bullets>0 then fire=int(rnd(1)*5)+10:print "you shoot your gun, causing";fire;"damage to the opponent!":d=1 else 1054
1053 if d=1 then a=a-fire:bullets=bullets-1:d=0:goto 1063
1054 if sword>=1 then hit=rnd(1):else 1065
1055 if hit>accuracy then print "The sword missed the challenger!!":goto 1065 
1056 accuracy=accuracy+((int(rnd(1)*10)+11)/1000):damage=int(rnd(1)*10)+11
1057 print "you take"damage"damage off the enemy with your sword!":a=a-damage:goto 1063
1058 print "use sword or gun, press 's' or 'g'"
1059 i$=input$(1)
1060 cls:if i$="g" then 1052
1061 if i$="s" then 1054
1062 print "invalid response.":goto 1059
1063 if a<1 then grid(px,py)=1:print "You win the fight!":ch=ch+1:goto 70
1065 p=int(rnd(1)*10)+1:h=h-p:print "it hits for "p:if h<1 then 70 else 1050
1099 'stats
1100 if sword>=1 then print "you hold "sword" swords"
1102 print "you hold a gun with "bullets" bullet(s)"
1103 print "you have killed "ch" challengers"
1105 gosub 44:goto 70 'location
1200 'data
1201 data"nothing","a snake","a sword","a challenger","a bullet","the treasure!"
1209 'init
1210 dim stuff$(6)
1211 for i=1 to 6
1212 read stuff$(i):next i:return 'continue game
1215 'look dir
1216 lx=px:ly=py:look=0 'init sector; otherwise will assume 0 causing false position
1217 if len(p$)=2 and asc(right$(p$,1))=72 then ly=ly+1:goto 1223
1218 if len(p$)=2 and asc(right$(p$,1))=80 then ly=ly-1:goto 1223
1219 if len(p$)=2 and asc(right$(p$,1))=77 then lx=lx+1:goto 1223
1220 if len(p$)=2 and asc(right$(p$,1))=75 then lx=lx-1:goto 1223
1221 print "invalid keypress.":goto 74
1223 if lx<0 then lx=x
1224 if lx>x then lx=0
1225 if ly<0 then ly=y
1226 if ly>y then ly=0
1227 print stuff$(grid(lx,ly)):lx=0:ly=0:goto 70
