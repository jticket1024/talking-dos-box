10 DIM R$(7),R(7)
20 DATA ensign,lieutenant,commander,captain,commodore,admiral,"fleet admiral"
30 FOR I=1 TO 7:READ R$(I):NEXT
40 RANDOMIZE TIMER
50 PRINT"Please enter your current rank."
60 PRINT"1 IS Ensign. 2 IS Lieutenant. 3 IS Commander. 4 IS Captain. 5 is Commodore."
70 SM$=INKEY$:IF SM$="" THEN 70
80 R=VAL(SM$):IF R>5 OR R<1 THEN 50
90 R$=R$(R)
100 INPUT "what is your first name?";N$
110 TN=0:ML=0:PRINT"greetings, "R$" "N$"!"
120 INPUT"ship name?";S$
130 PRINT"speed value?"
140 SM$=INKEY$:IF SM$=""THEN 140
150 S1=VAL(SM$):IF S1<1 OR S1>8 THEN 130
160 PRINT"fire power value?"
170 SM$=INKEY$:IF SM$="" THEN 170
180 S2=VAL(SM$):IF S2<1 OR S2>8 THEN 160
190 PRINT"shield value?"
200 SM$=INKEY$:IF SM$="" THEN 200
210 S3=VAL(SM$):IF S3<1 OR S3>8 THEN 190
220 IF S1+S2+S3<>10 THEN PRINT"ship characteristics must add up to 10.":GOTO 130
230 PRINT"are you satisfied with your ship?"
240 Q1$=INKEY$:IF Q1$="" THEN 240
250 IF Q1$="n" THEN 120
260 S4=10:GOTO 270
270 REM
280 SH=INT(5*RND(1)+3)
290 PRINT"your ship, the "S$" has"SH"shuttle craft available."
300 C=INT(25*RND(1)+10)
310 PRINT"you have just been handed the secret plans,":PRINT" you must deliver them to the earth outpost which is";
320 D=INT(75*RND(1)+50)
330 T=1:V=0:P=1:M=100
340 PRINT D" parsecs away. "
350 PRINT"WILL YOU CARRY OUT A PRE LAUNCH INSPECTION ON THE "S$"?"
360 Q3$=INKEY$:IF Q3$="" THEN 360
370 IF Q3$="n" THEN 430
380 M=M-5
390 IF INT(3*RND(1)+1)<3 THEN 410
400 PRINT"You have found a minor malfunction, but luckily manage to fix it before launch.":V=50:GOTO 420
410 PRINT"You find nothing unusual.":V=15
420 T=T+1
430 PRINT R$" "N$", it is now day "T". the "S$" and your crew of "C" are ready for launch."
440 PRINT"5. 4. 3. 2. 1.  Launch."
450 IF INT(100*RND(1))<((30*(SQR(R)/2))-V) THEN 470
460 GOTO 700
470 REM
480 REM
490 PRINT"The number 3 ion drive IS overheating. should we shut it down"
500 Q4$=INKEY$:IF Q4$=""THEN 500
510 IF Q4$="y" THEN 630
520 IF Q4$="n" THEN 540
530 GOTO 490
540 IF INT(100*RND(1))<75 THEN 560
550 PRINT"luckily, the ion drive cooled down, and the launch continued successfully.":GOTO 700
560 IF INT(4*RND(1)+1)=4 THEN 580
570 GOTO 590
580 PRINT"sorry, but the drive exploded.":GOTO 5420
590 PRINT"we need immediate shut down. shut it down now?"
600 Q5$=INKEY$:IF Q5$="" THEN 600
610 IF Q5$<>"y" THEN 580
620 PRINT"we managed to shut down the drive, but suffered 1 point of speed damage.":S1=S1-1:S4=S4-1:GOTO 700
630 IF INT(3*RND(1))=2 THEN 650
640 PRINT"the drive has been shut down, and the launch is now continuing as planned.":GOTO 700
650 PRINT"drive will not shut down. should we attempt manual override?"
660 Q7$=INKEY$:IF Q7$="" THEN 660
670 IF Q7$="n" THEN 540
680 IF INT(4*RND(1)+1)=4 THEN 580
690 GOTO 640
700 PRINT"the "S$" is now in earth orbit, "
710 T=T+1:D=ABS(D-S1):M=M-1
720 INPUT"PRESS RETURN";PAUSE$:cls
730 PRINT R$" "N$", it is now day "T" of your mission, and the "S$" is "D;" parsecs from the outpost."
740 PRINT SH"shuttle craft remain."
750 S4=S1+S2+S3
760 PRINT"speed is "S1" fire power is "S2" shield is "S3" energy is "S4" crew is "C" moroul is "M"%."
770 GOSUB 790
780 GOTO 900
790 IF INT(100*RND(1))>M AND C>0 AND D>8 THEN 5090
800 REM
810 IF C<=0 THEN 4650
820 IF S4<=0 THEN PRINT "there is no energy left, therefore the ship has haulted.":GOTO 5420
830 IF S1<=0 THEN 4480
840 IF S2<=0 THEN 4620
850 IF S3<=0 THEN 4630
860 IF SH<=0 THEN 4640
870 IF D<=8 THEN 4760
880 IF P=0 THEN 5290
890 RETURN
900 REM
910 IF INT(100*RND(1))<(10*(SQR(R)/2)) THEN 1010
920 IF INT(100*RND(1))<(10*(SQR(R)/2)) THEN 2080
930 IF INT(100*RND(1))<(10*(SQR(R)/2)) THEN 2470
940 IF INT(100*RND(1))<(10*(SQR(R)/2)) THEN 2520
950 IF INT(100*RND(1))<(10*(SQR(R)/2)) THEN 2990
960 IF INT(100*RND(1))<(15*(SQR(R)/2)) THEN 3970
970 IF INT(100*RND(1))<(15*(SQR(R)/2)) THEN 4020
980 IF INT(100*RND(1))<(15*(SQR(R)/2)) THEN 4310
990 PRINT"nothing of interest occurred today."
1000 GOTO 710
1010 PRINT"you have encountered an alien ship."
1020 A=INT(4*RND(1)+1)
1030 ON A GOTO 1040,1080,1120,1160
1040 REM
1050 A1=5:A3=INT(4*RND(1)+1):A2=5-A3:A4=10
1060 H=0:PR=0
1070 GOTO 1200
1080 REM
1090 A3=5:A1=INT(4*RND(1)+1):A2=5-A1:A4=10
1100 H=0:PR=0
1110 GOTO 1200
1120 REM
1130 A2=5:A3=INT(4*RND(1)+1):A1=5-A3:A4=10
1140 H=1:PR=0
1150 GOTO 1200
1160 REM
1170 A1=5:A2=INT(4*RND(1)+1):A3=5-A2:A4=10
1180 H=0:PR=1
1190 GOTO 1200
1200 PRINT"what do you wish to do? 1 attack, 2 wait, 3 communicate, 4escape,"
1210 PRINT"5 scan the alien ship."
1220 SM$=INKEY$:IF SM$="" THEN 1220
1230 B=VAL(SM$):IF SD0=1 THEN S3=SD1:SD0=0
1240 ON B GOTO 1260,1580,1630,1970,2030
1250 GOTO 1200
1260 IF S2<=0 THEN PRINT"no fire power. reselect.":GOTO 1200
1270 IF INT(100*RND(1))>(20*(SQR(R)/2)) THEN 1440
1280 PRINT"your attack on the aliens failed,":M=M-2
1290 IF INT(100*RND(1))<(20*(SQR(R)/2)) AND A2>0 THEN GOSUB 1310:GOTO 1390
1300 PRINT"the alien attack had no effect on the "S$".":M=M+3:GOTO 1200
1310 DA=INT(2*RND(1)+1)+A2-S3:IF DA<0 THEN DA=0
1320 DP=INT(3*RND(1)+1):ON DP GOTO 1330,1350,1370
1330 D$="speed":S1=S1-DA:IF S1<0 THEN S1=0
1340 D1=S1:RETURN
1350 D$="fire power":S2=S2-DA:IF S2<0 THEN S2=0
1360 D1=S2:RETURN
1370 D$="shield":S3=S3-DA:IF S3<0 THEN S3=0
1380 D1=S3:RETURN
1390 S4=S1+S2+S3:IF S4<=0 THEN PRINT "the aliens have destroyed you!":GOTO 5420
1400 PRINT"the alien attack did "DA" points damage to your":M=M-DA
1410 PRINT D$" generator ("D$" now="D1" ."
1420 IF RND(1)<.25 AND SH<>0 THEN PRINT"the attack also destroyed a shuttle ("SH-1"remain.":SH=SH-1
1430 GOTO 1200
1440 DS=INT(3*RND(1)+1)+S2-A3:IF DS<0 THEN DS=0
1450 DP2=INT(3*RND(1)+1):ON DP2 GOTO 1460,1480,1500
1460 A1=A1-DS:IF A1<0 THEN A1=0
1470 GOTO 1510
1480 A2=A2-DS:IF A2<0 THEN A2=0
1490 GOTO 1510
1500 A3=A3-DS:IF A3<0 THEN A3=0
1510 A4=A1+A2+A3
1520 PRINT"your attack did "DS" points damage to the aliens.":M=M+DS
1530 IF A4<=0 THEN PRINT"aliens destroyed.":M=M+5:GOTO 1550
1540 GOTO 1290
1550 IF INT(3*RND(1)+1)<=2 THEN 710
1560 S3=S3-INT(3*RND(1)+1):IF S3<0 THEN S3=0
1570 S4=S1+S2+S3:PRINT"the explosion of the alien ship damaged your shield generator it is now "S3".":GOTO 710
1580 PRINT"waiting."
1590 REM
1600 IF H=1 THEN PRINT"the aliens attacked the "S$".":GOTO 1290
1610 IF INT(3*RND(1)+1)=3 THEN 1720
1620 PRINT"the aliens waited, too.":GOTO 1200
1630 IF H=0 THEN 1720
1640 IF INT(3*RND(1)+1)=3 THEN 1650
1650 IF P=0 THEN 1660 ELSE PRINT"the aliens say that they will let you be if":GOTO 1680
1660 IF P=0 THEN PRINT "finding that you do not have the secret plans they wanted, they attack":GOTO 1290
1670 PRINT"the aliens will not talk to you, and attack.":GOTO 1290
1680 PRINT"you give them the secret plans. will you?"
1690 Q11$=INKEY$:IF Q11$="" THEN 1690
1700 IF Q11$="n" THEN 1670
1710 PRINT"the aliens are given the plans and leave.":M=M-20:P=0:GOTO 710
1720 PRINT"the aliens wish to communicate with you."
1730 IF INT(3*RND(1)+1)=3 THEN PRINT"they simply want you to let them go their own way.":GOTO 710
1740 PRINT"they want to come on board to talk. your response?"
1750 Q12$=INKEY$:IF Q12$="" THEN 1750
1760 IF Q12$="y" THEN 1790
1770 IF PR=1 THEN PRINT"Insulted, these pirates attack.":GOTO 1290
1780 GOTO 1730
1790 PRINT"the aliens come aboard and chat for a while."
1800 IF PR=0 THEN 1890
1810 IF PR=1 AND P=0 THEN 1660
1820 PRINT"suddenly, one of them grabs the plans and runs off with the others. should we"
1830 PRINT "try to get them back?"
1840 Q12$=INKEY$:IF Q12$="" THEN 1840
1850 IF Q12$="n" THEN P=0:M=M-20:GOTO 710
1860 CK=INT(5*RND(1))+1:IF CK>C THEN 1860
1870 IF INT(3*RND(1)+1)=3 THEN PRINT"you succeeded, but "CK" of your crew were killed.":C=C-CK:P=1:GOTO 710
1880 PRINT"you failed, and "CK" of your crew were killed.":C=C-CK:P=0:M=M-20:GOTO 710
1890 IF P=0 THEN PRINT"the aliens return the plans you had lost and":GOTO 1910
1900 GOTO 1920
1910 PRINT"which they found.":P=1:M=M+20
1920 IF S1<=1 THEN PRINT"the aliens also repaired your speed generator.":S1=S1+INT(4*RND(1)+1)
1930 IF S2<=0 THEN PRINT"the aliens also repaired your fire power generator.":S2=S2+INT(4*RND(1)+1)
1940 IF S3<=0 THEN PRINT"the aliens also repaired your shield generator.":S3=S3+INT(4*RND(1)+1)
1950 CG=INT(10*RND(1)+1):PRINT"before leaving, the aliens give you a gift of "CG
1960 PRINT"crew to help you.":C=C+CG:M=M+10:GOTO 710
1970 IF S1>=A1 THEN PRINT"you escape successfully.":GOTO 710
1980 PRINT"the alien ship is faster than yours. should we try to escape anyway?"
1990 Q1$=INKEY$:IF Q1$="" THEN 1990
2000 IF Q1$="n" THEN 1200
2010 IF INT(100*RND(1))<(50*(SQR(R)/2)) THEN PRINT"the attempt fails, and the angered aliens attack.":GOTO 1290
2020 PRINT"your escape succeeded.":GOTO 710
2030 PRINT"the alien ship has the following characteristics":SD1=S3:S3=0:SD0=1
2040 PRINT"speed is"A1"fire power is"A2"shield is"A3"energy is"A4
2050 IF H=1 THEN 2070
2060 PRINT"the aliens did not react to your scan.":GOTO 1200
2070 PRINT"while you were scanning, the aliens attacked.":GOTO 1290
2080 PRINT"we have encountered an asteroid field. shall we"
2090 PRINT"1, navigate through it, 2, try to go around it, or"
2100 PRINT"3, clear a path with our lasers."
2110 SM$=INKEY$:IF SM$="" THEN 2110
2120 BS=VAL(SM$):ON BS GOTO 2130,2290,2390:GOTO 2110
2130 SR=((40+S1)*(2/SQR(R))):PRINT"success rate calculated at "SR"%"
2140 REM
2150 IF (100*RND(1))<SR THEN 2280
2160 PRINT"we have been hit by a small asteroid"
2170 AD=INT(S1*RND(1)+1):ON INT(3*RND(1)+1) GOTO 2180,2200,2220
2180 D$="speed":S1=S1-AD:IF S1<0 THEN S1=0
2190 D2=S1:GOTO 2240
2200 D$="fire power":S2=S2-AD:IF S2<0 THEN S2=0
2210 D2=S2:GOTO 2240
2220 D$="shield":S3=S3-AD:IF S3<0 THEN S3=0
2230 D2=S3
2240 S4=S1+S2+S3:PRINT"we suffered "AD" points damage to the "D$
2250 PRINT"generator "D$" is now "D2
2260 PRINT
2270 GOTO 710
2280 PRINT"navigation successful.":GOTO 710
2290 IF INT(3*RND(1)+1)=3 THEN PRINT"there is no way to go around the asteroid field,":GOTO 2310
2300 GOTO 2350
2310 PRINT"shall we 1, navigate, or 2, use lasers."
2320 SM$=INKEY$:IF SM$="" THEN 2320
2330 AA=VAL(SM$):ON AA GOTO 2130,2390
2340 GOTO 2310
2350 PRINT"we have found an alternate path."
2360 REM
2370 IF INT(3*RND(1)+1)=3 THEN PRINT"we have been hit by a stray asteroid.":GOTO 2170
2380 GOTO 2280
2390 IF S2<=0 THEN PRINT"no fire power, reselect.":GOTO 2090
2400 PRINT
2410 REM
2420 IF INT(S2*RND(1)+1)<=2 THEN PRINT"we have been hit by fragments of a partially":GOTO 2440
2430 GOTO 2460
2440 PRINT"disintegrated asteroid.":IF S3>=S2 THEN PRINT"luckily our shield absorbed them.":GOTO 710
2450 GOTO 2170
2460 PRINT"we successfully blasted through the asteroid field.":GOTO 710
2470 PRINT"we have been pulled in by a black hole."
2480 IF INT((2/SQR(R))*RND(1)+S4)<=5 THEN PRINT"you never emerged from the black hole.":GOTO 5420
2490 BH=INT(30*RND(1)-10):D=D-BH:IF D<0 THEN D=ABS(D)
2500 PRINT"we emerged from the black hole. we are now "D
2510 PRINT"parsecs from the earth outpost.":GOTO 710
2520 PRINT"You have discovered a new planet. should we"
2530 PRINT" 1, land to investigate, 2, use the planet's gravity to"
2540 PRINT"accelerate the "S$", or 3, take scientific measurements."
2550 SM$=INKEY$:IF SM$="" THEN 2550
2560 PL=VAL(SM$):ON PL GOTO 2580,2920,2980
2570 GOTO 2550
2580 IF C<=0 THEN PRINT"sorry, , but there are no more crew":GOTO 2600
2590 GOTO 2610
2600 PRINT"left for an expedition. re select.":GOTO 2550
2610 IF SH<=0 THEN PRINT"sorry, but no shuttles remain. re select.":GOTO 2550
2620 PE=INT(5*RND(1)+1):IF C-PE<0 THEN 2620
2630 PRINT"ok. "PE" of your crew are in the shuttle and are ready for the landing."
2640 PRINT
2650 PRINT"shuttle entering atmosphere."
2660 IF INT(5*RND(1)+1)=5 THEN PRINT"shield malfunction. the shuttle burned up in":GOTO 2680
2670 GOTO 2690
2680 PRINT"the atmosphere, and all "PE" crew were killed.":C=C-PE:SH=SH-1:M=M-20:GOTO 710
2690 PRINT"Landing successful."
2700 PRINT"the crew is now going out to investigate."
2710 REM
2720 PR=INT(4*RND(1)+1):ON PR GOTO 2730,2780,2860,2900
2730 PRINT"they have found some strange leaves. should they take them on board?"
2740 Q15$=INKEY$:IF Q15$="" THEN 2740
2750 IF Q15$="n" THEN 2910
2760 IF INT(3*RND(1)+1)=3 THEN PRINT"the leaves were poisonous, and the crew died.":C=C-PE:SH=SH-1:M=M-10:GOTO 710
2770 PRINT"they seem to have mysterious healing properties.":M=M+10:ML=1:GOTO 2910
2780 H=INT(RND(1)*3)+1:IF H=3 THEN H=1 ELSE H=0
2790 PRINT"you have encountered strange alien beings on the planet. will you 1, be friendly, or 2, attack."
2800 SM$=INKEY$:IF SM$=""THEN 2800
2810 PA=VAL(SM$):ON PA GOTO 2820,2870:GOTO 2790
2820 IF H=0 THEN PRINT"the aliens gave you 5 crew and quyzlapvox crystals used for warp travel.":C=C+5:S1=S1+5:M=M+15
2830 IF H=0 AND P=1 THEN 2910
2840 IF H=0 AND P=0 THEN PRINT "they also returned the secret plans.":P=1:M=M+20:GOTO 2910
2850 PRINT "the aliens attacked and killed the entire crew.":SH=SH-1:C=C-PE:M=M-10:GOTO 710
2860 GOTO 2790
2870 IF H=0 AND P=0 THEN PRINT"you killed the aliens and found the secret plans.":P=1:M=M+20:GOTO 2910
2880 IF H=0 AND P=1 THEN PRINT"you killed the aliens,but they had nothing of value.":GOTO 2910
2890 PRINT"you killed the aliens, and you find a mysterious black box.":S2=S2+3:S3=S3+2:M=M+10:GOTO 2910
2900 PRINT"your crew found nothing unusual, ":M=M+10:GOTO 2910
2910 PRINT"your crew has returned successfully to the ship.":GOTO 710
2920 IF INT(S1*RND(1)+1)=1 THEN PRINT"you approached too close to the planet, were":GOTO 2940
2930 GOTO 2950
2940 PRINT"caught by its gravity, and crashed.":GOTO 5420
2950 PRINT"the planet's gravity sent you shooting through"
2960 D=D-5:S1=S1+1
2970 PRINT"space at "S1" units per turn.":GOTO 710
2980 PRINT"you made several new scientific discoveries, and your crew's spirits were lifted.":M=M+20:GOTO 710
2990 PRINT"scanners have detected a pulsating object."
3000 SO=INT(3*RND(1)+1)
3010 PRINT"should we 1, destroy it, 2, move in closer, 3, try to avoid it, or"
3020 REM
3030 PRINT"4, send out a shuttle to attempt identification."
3040 SM$=INKEY$:IF SM$=""THEN 3040
3050 SM=VAL(SM$):ON SM GOTO 3060,3210,3710,3820:GOTO 3040
3060 REM
3070 IF S2<=0 THEN PRINT"no fire power.":GOTO 3010
3080 ON SO GOTO 3090,3100,3110
3090 PRINT"a space mine exploded harmlessly before you.":M=M+2:GOTO 710
3100 PRINT"the object was destroyed and Pieces of an alien ship are all that remain.":GOTO 710
3110 PRINT"the object was an asteroid and broke into pieces."
3120 IF INT(3*RND(1)+1)=1 THEN PRINT"luckily nothing happenned to your ship.":GOTO 710
3130 PRINT"we were hit by asteroid fragments!"
3140 IF S3=>S2 THEN PRINT"our shield absorbed them and no damage occurred.":GOTO 710
3150 A2=6:GOSUB 1310
3160 S4=S1+S2+S3:IF S4<=0 THEN 5420
3170 PRINT"the asteroid did"DA"points damage to the"
3180 PRINT D$" generator power is now"D1
3190 IF RND(1)<.2 AND SH<>0 THEN PRINT"a shuttle was also destroyed"SH-1"remain":SH=SH-1
3200 GOTO 710
3210 REM
3220 PRINT"ok, "R$" moving in."
3230 ON SO GOTO 3240,3300,3540
3240 PRINT"the object was a space mine, and it exploded!":A2=10:GOSUB 1310
3250 S4=S1+S2+S3:IF S4<=0 THEN 5420
3260 PRINT"the explosion did"DA"points damage to the"
3270 PRINT D$" generator power is"D1
3280 IF RND(1)<.5 AND SH<>0 THEN PRINT SH-1"shuttles remain, one was destroyed.":SH=SH-1
3290 GOTO 710
3300 PRINT"you have received s o s signals from the object, a derelict. will your shuttle men go aboard to help?"
3310 Q17$=INKEY$:IF Q17$="" THEN 3310
3320 IF Q17$<>"y"THEN PRINT"ok, "R$". you avoid the derelict and leave.":GOTO 710
3330 IF C<5 OR SH=0 THEN PRINT"sorry, but you can't.":GOTO 710
3340 CL=INT(3*RND(1)+2)
3350 PRINT CL;"men have left in a shuttle and arrive safely in the derelict ship. they are now investigating." 
3360 IF INT(3*RND(1)+1)=3 THEN 3450
3370 PRINT"your crew rescued stranded aliens. your reward":M=M+10
3380 IF RND(1)<.5 THEN PRINT"quyzlapvox crystals":S1=S1+5:M=M+5
3390 IF RND(1)<.5 THEN PRINT"a black box":S2=S2+3:S3=S3+2:M=M+5
3400 IF P=0 THEN PRINT"the secret plans.":P=1:M=M+20
3410 IF RND(1)<.5 THEN PRINT"a shuttle craft":SH=SH+1:M=M+5
3420 CG=INT(3*RND(1)+3):PRINT""CG"crew":C=C+CG:M=M+(CG*3)
3430 PRINT"your crew returns and you leave."
3440 GOTO 710
3450 PRINT"the derelict appears deserted. continue search?"
3460 Q18$=INKEY$:IF Q18$="" THEN 3460
3470 IF Q18$<>"y" THEN PRINT"ok. your crew return and you leave.":GOTO 710
3480 IF INT(3*RND(1)+1)<>3 THEN 3360
3490 PRINT"your crew accidentally damage one of the ship's"
3500 PRINT"generators, and the entire vessel explodes."
3510 M=M-(2*CL):A2=8:SH=SH-1:C=C-CL
3520 GOSUB 1310
3530 GOTO 3250
3540 PRINT"the object, an asteroid, seems a likely place for"
3550 PRINT"muyning. should we shuttle down a crew to try"
3560 Q19$=INKEY$:IF Q19$="" THEN 3560
3570 IF Q19$<>"y" THEN PRINT"ok. we are leaving, "R$:GOTO 710
3580 IF C<10 OR SH=0 THEN PRINT"not possible. "R$:GOTO 710
3590 CL=INT(4*RND(1)+5):DM=INT(3*RND(1)+1)
3600 PRINT CL" crew have landed a shuttle on the asteroid,"
3610 PRINT"and spent"DM"days muyning.":T=T+DM
3620 IF INT(3*RND(1)+1)=3 THEN PRINT"they find nothing, return, and you leave.":GOTO 710
3630 PRINT"they found a rich metal deposit! are now muyning."
3640 IF INT(4*RND(1)+1)<>4 THEN 3670
3650 PRINT"unfortunately, the metal was volatile, and":SH=SH-1:C=C-CL:M=M-2
3660 PRINT"exploded, shattering the asteroid.":GOTO 3130
3670 PRINT"the metal was rare krylaz, and can be used in the"
3680 PRINT S$"'s generators! the crew then returned."
3690 M=M+10:S1=S1+INT(3*RND(1)+1):S2=S2+INT(3*RND(1)+1):S3=S3+INT(3*RND(1)+1)
3700 S4=S1+S2+S3:GOTO 710
3710 REM
3720 IF INT(S1*RND(1)+1)<=1 THEN 3740
3730 PRINT"the object was successfully avoided, "R$:GOTO 710
3740 PRINT"while attempting this, you strayed closer."
3750 ON SO GOTO 3760,3790,3790
3760 PRINT"unfortunately, the object was a mine and exploded!"
3770 M=M-3:A2=10:GOSUB 1310
3780 GOTO 3250
3790 IF INT(RND(1)*3)+1=1 THEN PRINT"your ship was hit by the object and you were destroyed.":GOTO 5420
3800 IF SO=3 THEN PRINT"you managed to identify the object as an asteroid.":GOTO 3540
3810 PRINT"the object was then identified as a derelict ship.":GOTO 3300
3820 REM
3830 IF C<5 OR SH=0 THEN PRINT"sorry, but you can't, "R$:GOTO 3010
3840 CL=INT(3*RND(1)+2)
3850 PRINT"ok."CL"crew have gone out on a mission."
3860 ON SO GOTO 3870,3890,3900
3870 PRINT"as the shuttle approached, a mine exploded!"
3880 M=M-(2*CL):SH=SH-1:C=C-CL:GOTO 710
3890 PRINT"your crew identified the object as a derelict ship.":GOTO 3910
3900 PRINT"your crew identified the object as an asteroid.":GOTO 3910
3910 PRINT"you may now 1, destroy the object,"
3920 PRINT"2, move closer, or"
3930 PRINT"3, try to avoid it."
3940 SM$=INKEY$:IF SM$="" THEN 3940
3950 SM=VAL(SM$)
3960 ON SM GOTO 3060,3210,3710:GOTO 3940
3970 PRINT"a mysterious alien disease has swept the"
3980 IF ML=1 THEN PRINT"ship, but the healing leaves you found cured it.":GOTO 710
3990 DP=INT(C/3):C=C-DP:M=M-DP
4000 PRINT"ship, and"DP" of your crew were killed "C
4010 PRINT"remain.":GOTO 710
4020 REM
4030 PRINT"we suffered a computer failure!"
4040 IF CW<>1 THEN 4150
4050 IF C<5 THEN PRINT"the computer can no longer replace your crew. you":GOTO 4740
4060 M=M-5:PRINT"we must shut down a generator. which one?"
4070 PRINT"1, speed, 2, fire power, or 3, shield,"
4080 SM$=INKEY$:IF SM$="" THEN 4080
4090 CF1=VAL(SM$)
4100 ON CF1 GOTO 4110,4120,4130:GOTO 4080
4110 S1=0:GOTO 4140
4120 S2=0:GOTO 4140
4130 S3=0:GOTO 4140
4140 PRINT"ok. shutting down.":CW=0:S4=S1+S2+S3:GOTO 710
4150 M=M-3:CF0=INT(3*RND(1)+1)
4160 ON CF0 GOTO 4170,4200,4230
4170 CF$="speed"
4180 S1=S1-INT(3*RND(1)+1):IF S1<0 THEN S1=0
4190 CF2=S1:GOTO 4260
4200 CF$="fire power"
4210 S2=S2-INT(3*RND(1)+1):IF S2<0 THEN S2=0
4220 CF2=S2:GOTO 4260
4230 CF$="shield"
4240 S3=S3-INT(3*RND(1)+1):IF S3<0 THEN S3=0
4250 CF2=S3:GOTO 4260
4260 PRINT"power to the "CF$" generator was automatically reduced "CF$" is now"CF2
4270 REM
4280 S4=S1+S2+S3
4290 IF S4<=0 THEN 5420
4300 GOTO 710
4310 PRINT"sensors report we are entering an intense radiation field! it has affected the
4320 PRINT"ship and crew in the following ways."
4330 IF INT(2*RND(1)+1)=2 THEN S1=S1-1:GOTO 4350
4340 S1=S1+1:GOTO 4350
4350 IF INT(2*RND(1)+1)=2 THEN S2=S2-2:GOTO 4370
4360 S2=S2+2:GOTO 4370
4370 IF INT(2*RND(1)+1)=2 THEN S3=S3-3:GOTO 4390
4380 S3=S3+3:GOTO 4390
4390 IF S1<0 THEN S1=0
4400 IF S2<0 THEN S2=0
4410 IF S3<0 THEN S3=0
4420 S4=S1+S2+S3
4430 RF=INT(SQR(C)):C=C-RF:M=M-RF
4440 REM
4450 PRINT"speed is now "S1" fire power is now "S2" shield is now "S3" energy is now"S4" crew killed "RF" crew remaining "C
4460 IF S4=0 THEN 5420
4470 GOTO 710
4480 IF TN<>0 THEN 4520
4490 PRINT"our speed generators are not functioning, but we have hooked up
4500 PRINT"a temporary navigating system. we only have enough fuel to use this"
4510 PRINT"manual system for five days after that, the "S$" will drift aimlessly.":TN=1:GOTO 840
4520 IF TN=5 THEN PRINT"we can no longer use the manual navigation system.":GOTO 4550
4530 TN=TN+1
4540 GOTO 840
4550 PRINT"the "S$" can no longer move on course, and it":S1=0
4560 IF INT(100*RND(1))<(50*(SQR(R)/2)) THEN 4590
4570 PRINT"is drifting through space. luckily a space tug found you
4580 PRINT"and towed you back to earth. you have been demoted to ensign for deriliction of duty.":R=1:R$="ensign":GOTO 5510
4590 PRINT"is drifting through space. your crew abandons ship in a rescue vessel, but you remain. even"
4600 PRINT"though you are destined to monotony for the remainder of your life, take heart:if your ship"
4610 PRINT"becomes a new satellite, at least it might be named "N$" in your memory!":GOTO 5420
4620 PRINT"your ship has no fire power,":S2=0:GOTO 850
4630 PRINT"there is no shield energy.":S3=0:GOTO 860
4640 PRINT"no shuttle craft remain,":SH=0:GOTO 870
4650 PRINT R$" "N$", none of your crew remain. you":C=0:IF CW=1 THEN 4670
4660 IF INT(100*RND(1))<(30*(SQR(R))) THEN 4740
4670 PRINT"were able to hook up a computer to do their work":IF CW=1 THEN 4720
4680 PRINT"with an energy drain":CW=1:S1=S1-1:IF S1<0 THEN S1=0
4690 S2=S2-1:IF S2<0 THEN S2=0
4700 S3=S3-1:IF S3<0 THEN S3=0:S4=S1+S2+S3
4710 IF S4=0 THEN 5420
4720 REM
4730 GOTO 820
4740 PRINT"can no longer manage the "S$", and drift aimlessly through space for the remainder of your"
4750 PRINT"life.":GOTO 5420
4760 PRINT"the "S$" is within "D" parsecs of the earth outpost." 
4770 PRINT"SHALL WE SPEND A DAY Checking equipment before docking?"
4780 Q8$=INKEY$:IF Q8$="" THEN 4780
4790 IF Q8$="y" THEN 4820
4800 PRINT"ok, "R$", docking will occur in "(D/S1+1)
4810 PRINT"hours.":V=0:GOTO 4850
4820 IF INT(4*RND(1)+1)=4 THEN 4840
4830 PRINT"you find nothing unusual, "R$:V=15:GOTO 4850
4840 PRINT"there was a minor equipment failure, but it was detected in the check.":V=30
4850 PRINT"every thing is ok for "
4860 PRINT"docking. 5. 4. 3. 2. 1."
4870 IF INT(100*RND(1))<((30*(SQR(R)/2))-V) THEN 4890
4880 GOTO 4990
4890 PRINT"docking malfunction. . explosion imminent. abandon ship?"
4900 Q9$=INKEY$:IF Q9$="" THEN 4900
4910 IF INT(3*RND(1)+1)<3 THEN 4970
4920 IF Q9$="y" THEN 4950
4930 PRINT"sorry, "R$" "N$", but both the "S$
4940 PRINT"and the outpost were destroyed.":GOTO 5420
4950 PRINT"you and your crew managed to abandon ship, but both the "S$" and the outpost were destroyed"
4960 PRINT"in a huge explosion.":GOTO 5420
4970 IF Q9$="y" THEN PRINT"you abandoned ship, but nothing happenned, coward.":GOTO 5000
4980 PRINT"you briefly stayed with the ship, but luckily nothing happenned.":GOTO 5000
4990 PRINT".docking successful."
5000 REM
5010 PRINT"space bulletin. the "S$", under the command of "R$" "N$", has arrived
5020 PRINT"at the earth outpost after a "T" day mission."
5030 IF P=0 THEN 5060
5040 PRINT R$" "N$" safely delivered the secret plans, and you and your crew of "C" are to be commended for this success."
5050 BF=((C+S4+SH)/T):PRINT"battle factor"BF:GOTO 5300
5060 PRINT"unfortunately, the "R$" did not deliver the secret plans,
5070 PRINT"and must undergo questioning by authorities. in the meantime, you"
5080 PRINT"are to be releeved as commander of the "S$".":GOTO 5510
5090 PRINT"the crew have turned against you,"
5100 C1=INT(INT(C/2)*RND(1))+1
5110 PRINT"and "C1" of them have decided to abandon ship":IF SH>0 THEN 5120 else 5140
5120 PRINT"in a shuttle craft.":SH=SH-1
5130 REM
5140 C=C-C1
5150 IF INT(RND(1)*100)>M THEN 5170
5160 GOTO 5270
5170 PRINT"while leaving, one of your mutinous crew"
5180 C2=INT(3*RND(1)+1)
5190 ON C2 GOTO 5200,5220,5240
5200 C2$="speed":S4=S4-S1
5210 S1=0:GOTO 5260
5220 C2$="fire power":S4=S4-S2
5230 S2=0:GOTO 5260
5240 C2$="shield":S4=S4-S3
5250 S3=0:GOTO 5260
5260 PRINT"destroyed the "S$"'s "C2$" generator."
5270 REM
5280 GOTO 800
5290 PRINT"you don't have the secret plans!":GOTO 890
5300 IF BF>=1.5 THEN 5330
5310 IF BF<.5 THEN 5380
5320 PRINT"you have remained at your present rank.":GOTO 5510
5330 SB=INT(BF/1.5)
5340 R=R+SB
5350 IF R>=8 THEN 5360 ELSE R$=R$(R):GOTO 5370
5360 PRINT"you have fulfilled your duty to the earth fleet, and can now retire with fame and fortune.":SYSTEM
5370 PRINT"congratulations! you have been promoted to "R$"!":GOTO 5510
5380 IF R<=1 THEN PRINT"you have been discharged from the earth fleet.":IF R>1 THEN 5400
5390 PRINT"if you reenter the fleet it must be as an ensign.":GOTO 5510
5400 R=R-1:R$=R$(R)
5410 PRINT"you have been demoted to "R$:GOTO 5510
5420 PRINT"space bulletin. we have just been informed that the star cruiser"
5430 PRINT S$" under the command of "R$
5440 PRINT N$", has been destroyed "T" days into its mission. the "R$" and all "C" remaining crew are presumed dead."
5450 PRINT"the "S$" came within "D" parsecs of the earth outpost, where it was to deliver secret plans."
5460 REM
5470 IF R<=1 THEN PRINT"you will be discharged from the earth fleet. if you reenter the fleet,":PRINT:PRINT "it must be as an ensign.":GOTO 5510
5480 PRINT"if you return to the fleet it must be as an ensign.":GOTO 5510
5490 R=R-1:R$=R$(R)
5500 PRINT"will be demoted to "R$:GOTO 5510
5510 PRINT"another mission?"
5520 I$=INKEY$:IF I$="" THEN 5520
5530 IF I$="n"THEN PRINT"please await further orders.":SYSTEM
5540 IF I$="y"THEN 110
5550 PRINT "please answer y or n.":GOTO 5510
