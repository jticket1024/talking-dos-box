2 key off:randomize timer
6 'new character entries will cause sections 10, 270, 6930, 4730,
7 '4340, 7930, 8870, 9080, and 8790 to be modified.
8 'this section defines amount of characters, charNames, and control types
10 sel$="select"
12 mn$="Menu: ":ed$="Prompt: "
16 for x=1 to 8:read per$(x):next
17 gosub 9230 'subroutine to define all arrays
19 'error evaluation section
20 on error goto 35:goto 60
35 IF AZ=2 THEN close:print "Bad file name, please re-enter.":GOTO 9080
40 IF AZ=1 THEN close:print "File not found, please re-enter.":GOTO 8870
50 print "Program malfunction, error code: "err". line:"erl
51 print "Press S to restart, any other key to exit the game.":clear:k$=input$(1)
52 if k$="s" then print "RESTARTING":goto 6
53 end
60 s.lives=5:LIVES=s.lives:print "Tournament V3.0 Beta;";
70 HEA=10000:print "Original written by Munawar Bijani; first release 02/06/02.":print "Revised by BPCPrograms, Inc."
80 A=HEA:print mn$"Game Menu"
82 x=0:choice$=""
84 choice$=input$(1)
86 if choice$="f" then cls:x=x+1
88 if choice$="b" then cls:x=x-1
90 if x>4 then x=1
92 if x<1 then x=4
 94 if choice$="e"  then cls:goto 100
96 print gmenu$(x):goto 84
100 if x=1 then 230
102 if x=2 then 8870
104 if x=3 then gosub 122:goto 80
106 if x=4 then print "EXITING PROGRAM":end
122 print "Since version 2.0.6, the instructions were removed. For documentation,"
124 print "please refer to 'readme.txt.' This file should have come with this  package."
126 print "If this is not the case, contact BPCPrograms to obtain this file.":return
230:print mn$"Select your character"
250 'this section is the fighter select screen
270 x=0:choice$=""
280 choice$=input$(1)
290 if choice$="f" then cls:x=x+1
292 if choice$="b" then cls:x=x-1
294 if x<1 then x=8
296 if x>8 then x=1
298 if choice$="e" then cls:goto 310
299 print per$(x):goto 280 
310 char$=per$(x)
320 'destiny
880:print mn$"CHOOSE YOUR DESTINY!"
885 x=0:choice$=""
895 choice$=input$(1)
900 if choice$="f" then cls:x=x+1
905 if choice$="b" then cls:x=x-1
910 if x<1 then x=3
915 if x>3 then x=1
920 if choice$="e" then cls:goto 930
923 print dst$(x):goto 895
930 dest=x:goto 4340
1040 'character attributes start here.
1050 'snake
1060 x=0:choice$="":print mn$""sel$
1111 choice$=input$(1)
1112 if choice$="f" then cls:x=x+1
1114 if choice$="b" then cls:x=x-1
1116 if x<1 then x=6
1118 if x>6 then x=1
1120 if choice$="e" then cls:goto 1130
1121 print snake$(x)"="snakeNew(x)+s.ef:goto 1111
1130 if x=6 then 9552
1320:print snake$(x)"!":a=a-(snakenew(x)+s.ef)
1340 gosub 23001:gosub 22005:goto 6905
1480 GETOVER=0:print mn$""sel$
1515 choice$="":x=0
1517 choice$=input$(1)
1518 if choice$="f" then cls:x=x+1
 1520 if choice$="b" then cls:x=x-1
1522 if x<1 then x=7
1524 if x>7 then x=1
1526 if choice$="e" then cls:goto 1534
1528 print scorpion$(x)"="scorNew(x)+s.ef:goto 1517
1534 if x=2 then 1870
1539 if x=7 then 9552
1790:print scorpion$(x)"!":a=a-(scornew(x)+s.ef)
1840 gosub 23001:gosub 22005:goto 6905
1860 'get over here attributes
1870 GETOVER=GETOVER+1
1880 IF GETOVER=2 THEN GETOVER=0:print "Already captured, releasing":GOTO 4730
1900 GETOVERMISS=int(rnd(1)*2)+1
1910 IF GETOVERMISS=1 THEN print "MISSED!":GOTO 4730
1920 'goes to challenger's attributes
1930 IF GETOVERMISS=2 THEN print "GET OVER HERE!":A=A-(scornew(x)+s.ef):GOTO 1515
2100 'This ends the attributes for Scorpion
2110 'Sub-zero
2120 DEEPFREEZE=0:FREEZETIME=0:print mn$""sel$
2130 choice$="":x=0
2134 choice$=input$(1)
2136 if choice$="f" then cls:x=x+1
2138 if choice$="b" then cls:x=x-1
2140 if x<1 then x=6
2142 if x>6 then x=1
2143 if choice$="e" then cls:goto 2147
2144 print subzero$(x)"="subnew(x)+s.ef:goto 2134
2146 goto 2134
2147 nn=subnew(x)+s.ef
2148 if x=1 then 2390
2152 if x=3 then 2500
2156 if x=5 then 2640
2158 if x=6 then 9552
2360:print subzero$(x)"!":a=a-(subnew(x)+s.ef)
2375 gosub 23001:gosub 22005:goto 6905
2380 'Puddle
2390 print "Puddle!"
2400 PUDDLE=int(rnd(1)*2)+1
2410 IF PUDDLE=1 THEN print "THE CHALLENGER WAS NOT CLOSE ENOUGH!":GOTO 4730
2440 gosub 23001:gosub 22005:goto 6905
2490 'Freeze
2500 print "FREEZE!"
2510 FREEZETIME=FREEZETIME+1
2530 IF FREEZETIME=2 THEN FREEZETIME=0:print "Already frozen, melting!":GOTO 4730
2540 FREEZE=int(rnd(1)*2)+1
2550 IF FREEZE=1 THEN print "MISSED!":GOTO 4730
2570 A=A-(subnew(x)+s.ef):GOTO 2130
2630 'Deep Freeze
2640 print "Deep Freeze!"
2650 DEEPFREEZE=DEEPFREEZE+1
2670 IF DEEPFREEZE=2 THEN print "Already frozen, melting!":DEEPFREEZE=0:GOTO 4730
2680 DEEPFREEZETYPE=int(rnd(1)*2)+1
2690 IF DEEPFREEZETYPE=1 THEN print "MISSED!":GOTO 4730
2700 IF DEEPFREEZETYPE=2 THEN A=A-(subnew(x)+s.ef):GOTO 2130
2710 'This ends Sub-zero's move attributes
2720 'Stryker
2730:print mn$""sel$
2732 choice$="":x=0
2735 choice$=input$(1)
2736 if choice$="f" then cls:x=x+1
2738 if choice$="b" then cls:x=x-1
2740 if x<1 then x=5
2742 if x>5 then x=1
2744 if choice$="e" then 2760
2746 print stryker$(x)"="strnew(x)+s.ef:goto 2735
2760 if x=5 then 9552
2960:print stryker$(x)"!":a=a-(strnew(x)+s.ef)
2970 gosub 23001:gosub 22005:goto 6905
3110 'This ends move attributes for Stryker
3120 'LiuKang
3130:print mn$""sel$
3132 choice$="":x=0
3136 choice$=input$(1)
3138 if choice$="f" then cls:x=x+1
3140 if choice$="b" then cls:x=x-1
3142 if x<1 then x=5
3144 if x>5 then x=1
3146 if choice$="e" then cls:goto 3162
3148 print liuk$(x)"="liunew(x)+s.ef:goto 3136
3162 if x=5 then 9552
3350:print liuk$(x)"!":a=a-(liunew(x)+s.ef)
3380 gosub 23001:gosub 22005:goto 6905
3510 'This ends attributes for LiuKang
3520 'Jax
3530:print mn$""sel$
3532 choice$="":x=0
3535 choice$=input$(1)
3536 if choice$="f" then cls:x=x+1
3538 if choice$="b" then cls:x=x-1
3540 if x<1 then x=6
3542 if x>6 then x=1
3544 if choice$="e" then cls:goto 3562
3546 print jax$(x)"="jaxnew(x)+s.ef:goto 3535
3562 if x=6 then 9552
3790:print jax$(x)"!":a=a-(jaxnew(x)+s.ef)
3820 gosub 23001:gosub 22005:goto 6905
3990 'This ends the attributes for Jax
4000 'Ken
4010:print mn$""sel$
4012 choice$="":x=0
4015 choice$=input$(1)
4016 if choice$="f" then cls:x=x+1
4018 if choice$="b" then cls:x=x-1
4020 if x<1 then x=4
4022 if x>4 then x=1
4024 if choice$="e" then cls:goto 4038
4026 print ken$(x)"="kennew(x)+s.ef:goto 4015
4038 if x=4 then 9552
4180:print ken$(x)"!":a=a-(kennew(x)+s.ef)
4210 gosub 23001:gosub 22005:goto 6905
4300 'This section defines your challenger
4310 cls:print "Save game? y/n"
4320 sa$=input$(1):gosub 9040
4340 CHALL=int(rnd(1)*8)+1
4350 if cl(chall)=1 then 4340
4360 charchall$=per$(chall)
4430 'end of character definition section
4440 'This section defines the health and live attributes
4450 H=10000+s.health:print "Your challenger is "CHARCHALL$"!"
4460 A=HEA
4470 print "Your energy is currently "H"."
4480:print CHARCHALL$"'s energy is "A"."
4500 print "You currently have "LIVES" lives left.":goto 4575
4510 'The above section is a bouncing type of code,
4520 'saying that if you have already fought the randomly selected challenger, the
4530 'random selection will regenerate.
4570 'This section defines round attributes
4575 if round+roundchall>1 and abs(round-roundchall)>0 then 4650
4580 print "ROUND "str$(round+roundchall+1)", FIGHT!":H=10000+s.health:A=HEA:GOTO 6930
4620 'The next section is activated only if none of the above
4630 'statements are true
4640 'Basically, it determines who won the match what should be done next.
4650 IF ROUND>roundchall then :print "You have defeated "CHARCHALL$".":H=10000+s.health:GOTO 8790
4660 IF ROUNDCHALL>ROUNd then print "You have failed to beat "CHARCHALL$".":GOTO 8510
4690 'This section is not a whole rewrite of the character attribute
4700 'section above, instead most was cut and pasted and variables were modified accordingly.
4710 'We will start by first writing the statements to activate the
4720 'appropriate challenger.
4730 IF CHARCHALL$="Snake" THEN 4830
4740 IF CHARCHALL$="Scorpion" THEN 5070
4750 IF CHARCHALL$="Sub-zero" THEN 5470
4760 IF CHARCHALL$="Stryker" THEN 5910
4770 IF CHARCHALL$="LiuKang" THEN 6150
4780 IF CHARCHALL$="Jax" THEN 6400
4790 IF CHARCHALL$="Ken" THEN 6680
4792 IF CHARCHALL$="Humorse" THEN 8060
4796 if charchall$="Munawar" then 9842
4810 'we now need to define the attributes for each
4820 'first, the rnd function for snake's moves
4830 SNAKE=int(rnd(1)*5)+1
4900:print snake$(snake)"!":h=h-(snakenew(snake)*dest)
4940 goto 6885
5070 GETOVER=0
5080 SCORPION=int(rnd(1)*6)+1
5100 IF SCORPION=2 THEN 5240
5160:print scorpion$(scorpion)"!":h=h-(scornew(scorpion)*dest)
5210 goto 6885
5230 'get over here attributes
5240 GETOVER=GETOVER+1
5250 IF GETOVER=2 THEN print "Already captured, releasing":GOTO 6930
5270 GETOVERMISS=int(rnd(1)*2)+1
5280 IF GETOVERMISS=1 THEN print "MISSED!":GOTO 6930
5300 H=H-40*DEST:GOTO 5080
5470 FREEZETIME=0:DEEPFREEZE=0
5480 SUBZERO=int(rnd(1)*5)+1
5490 IF SUBZERO=1 THEN 5580
5510 IF SUBZERO=3 THEN 5690
5530 IF SUBZERO=5 THEN 5820
5560:print subzero$(subzero)"!":h=h-(subnew(subzero)*dest)
5564 goto 6885
5570 'Puddle
5580 print "Puddle!"
5590 PUDDLE=int(rnd(1)*2)+1
5600 IF PUDDLE=1 THEN print "THE CHALLENGER WAS NOT CLOSE ENOUGH!":GOTO 6930
5610 H=H-800*DEST
5630 goto 6885
5680 'Freeze
5690 print "FREEZE!"
5700 FREEZETIME=FREEZETIME+1
5720 IF FREEZETIME=2 THEN print "Already frozen, melting!":FREEZETIME=0:GOTO 6930
5730 FREEZE=int(rnd(1)*2)+1
5740 IF FREEZE=1 THEN print "MISSED!":GOTO 6930
5760 H=H-80*DEST:GOTO 5480
5810 'Deep Freeze
5820 print "Deep Freeze!"
5830 DEEPFREEZE=DEEPFREEZE+1
5850 IF DEEPFREEZE=2 THEN print "Already frozen, melting!":DEEPFREEZE=0:GOTO 6930
5860 DEEPFREEZETYPE=int(rnd(1)*2)+1
5870 IF DEEPFREEZETYPE=1 THEN print "MISSED!":GOTO 6930
5880 H=H-100*DEST:GOTO 5480
5890 'This ends Sub-zero's move attributes
5900 'now, we start attributes for stryker
5910 STRYKER=int(rnd(1)*4)+1
5980:print stryker$(stryker)"!":h=h-(strnew(stryker)*dest)
5984 goto 6885
6130 'This ends move attributes for Stryker
6140 'Now, we start the attributes for LiuKang
6150 LIUKANG=int(rnd(1)*4)+1
6220:print liuk$(liukang)"!":h=h-(liunew(liukang)*dest)
6250 goto 6885
6380 'This ends attributes for LiuKang
6390 'now, we start attributes for Jax
6400 JAX=int(rnd(1)*5)+1
6460:print jax$(jax)"!":h=h-(jaxnew(jax)*dest)
6490 goto 6885
6660 'This ends the attributes for Jax
6670 'now, we start move attributes for ken
6680 KEN=int(rnd(1)*3)+1
6730:print ken$(ken)"!":h=h-(kennew(ken)*dest)
6760 goto 6885
6850 '(character deffinitions continue on line 9842)
6860 'now, we will write health attributes
6870 'this is what the challenger attributes will
6880 'use to display health
6885IF h<1 then print charchall$", wins!":ROUNDCHALL=ROUNDCHALL+1:goto 4575
6890 print char$" "str$(H)", "CHARCHALL$" "str$(A):GOTO 6930
 6900 'this is what your attributes will use to display health
6905 if a<1 then round=round+1:print:print char$", wins!":goto 4575
6910 print:M=M+1:print char$" "str$(H)", "CHARCHALL$" "str$(A):GOTO 4730
6920 'this section determines your correct fighter in a match
6930 IF CHAR$="Snake" THEN 1060
6940 IF CHAR$="Scorpion" THEN 1480
6950 IF CHAR$="Sub-zero" THEN 2120
6960 IF CHAR$="Stryker" THEN 2730
6970 IF CHAR$="LiuKang" THEN 3130
6980 IF CHAR$="Jax" THEN 3530
6990 IF CHAR$="Ken" THEN 4010
6992 if char$="Munawar" then 9802
7010 'this is the money section
7020 A=HEA:MONEY=MONEY+INT(50000/(M+2+ROUNDCHALL)/(INT(RND(1)*5)+1)*DEST)
7030 M=0
7040 'end money section
7050 ROUND=0:print "You now have $"money"."
7060 ROUNDCHALL=0:print "Do you want to enter the store? Keep in mind the store closes"
7070 print "at 10:00 p.m., and opens at 8:00 a.m."
7072 print "It is now "left$(time$,5)
7076 if val(left$(time$,2))<13 then print "which is "left$(time$,5)" A.M. standard time."
7077 if val(left$(time$,2))>12 then print "which is "str$(val(left$(time$,2))-12)""mid$(time$,3,3)" P.M. standard time."
7078 print mn$
7080 print "yes":enter$=input$(1)
7090 IF ENTER$="e"  THEN 7190
7100 IF enter$="f" or enter$="b" THEN 7130
7120 GOTO 7080
7130 print "no":enter$=input$(1)
7140 IF ENTER$="e"  THEN 7930
7150 IF enter$="f" or enter$="b" THEN 7080
7170 GOTO 7130
7180 'these are the store attributes
7190 IF val(left$(time$,2))>21 OR val(left$(time$,2))<8 then print "Sorry, the store is closed.":GOTO 7930
7210 print "Hello "CHAR$"!"
7220 print "You have $"money"."
7230:print mn$"What would you like to buy? "
7232 choice$="":x=0
7236 choice$=input$(1)
7238 if choice$="f" then cls:x=x+1
7240 if choice$="b" then cls:x=x-1
7242 if x<1 then x=8
7244 if x>8 then x=1
7246 if choice$="e" then cls:goto 7252
7248 print store$(x):goto 7236
7252 if x=1 then 7400
7254 if x=2 then 7620
7256 if x=3 then 7740
7258 if x=4 then 9502
7260 if x=5 then 9531
7262 if x=6 then 9537
7264 if x=7 then 9543
7266 if x=8 then 7930
7390 'this is the more lives option code
7400 print "At the moment, you have "LIVES" lives."
7410:print ed$"Lives are priced at $500.00 per life. How many lives do you want? "
7420 INPUT"Type a -1 for maximum.", LVAMO
7430 if lvamo=-1 then lvamo=int(money/500):print "Max amount="lvamo".":goto 7440
7435 IF LVAMO<>INT(LVAMO) OR ABS(LVAMO)>LVAMO and lvamo<>-1 THEN print "Must be a positive integer!":GOTO 7420
7440 LVBUY=LVAMO*500
7450 IF LvBUY>MONEY THEN print "You can't afford this much. Remember you only have $"money" left.":GOTO 7420
7460 IF MONEY>=LVBUY THEN print "You have chosen to buy "LVAMO" lives.":GOTO 7470
7470 print "Total price="LVBUY".":MONEY=MONEY-LVBUY
7480 s.lives=LVAMO
7490 LIVES=LIVES+s.lives:print "You now have "LIVES" lives."
7500 if wear=0 then gosub 24001:wear=1
7501:print mn$"Anything else? "
7510 print "Yes":anything$=input$(1)
7520 IF ANYTHING$="e"  THEN 7220
7530 IF anything$="f" or anything$="b" THEN 7560
7550 GOTO 7510
7560 print "No":anything$=input$(1)
7570 IF ANYTHING$="e"  THEN 7930
7580 IF anything$="f" or anything$="b" THEN 7550
7600 GOTO 7560
7610 'This is the effect power code
7620 print "Your current add on effect power is "S.ef"."
7630 print "Effect power is priced at $15.00 per unit."
7640:print ed$"How many units do you want? ":INPUT"Type a -1 for maximum.", EPAMO
7645 if epamo=-1 then epamo=int(money/15):print "Max amount="epamo"."
7650 EPBUY=EPAMO*15
7660 IF EPAMO<>INT(EPAMO) OR ABS(EPAMO)>EPAMO and epamo<>-1 THEN print "Must be a positive integer!":GOTO 7640
7670 IF EPBUY>MONEY THEN print "You can't afford this much. Remember you only have $"money" left.":GOTO 7640
7680 IF MONEY>=EPBUY THEN print "You have chosen to buy "EPAMO" units.":GOTO 7690
7690 print "Total price="EPBUY"."
7700 MONEY=MONEY-EPBUY
7710 S.ef=S.ef+EPAMO
7720 print "You now have "S.ef" units of effect power.":GOTO 7500
7730 'this is the more health option code
7740 print "Your health is currently at "H"."
7750 print "Health is priced at $15.00 per unit."
7760:print ed$"How many units do you want? ":INPUT"Type -1 for maximum.", HAMO
7765 if hamo=-1 then hamo=int(money/15):print "Maximum amount="hamo"."
7770 IF HAMO<>INT(HAMO) OR ABS(HAMO)>HAMO and hamo<>-1 THEN print "Must be a positive integer!":GOTO 7760
7780 HBUY=HAMO*15
7790 IF HBUY>MONEY THEN print "You can't afford this much. Remember you only have $"money" left.":GOTO 7760
7800 IF MONEY>=HBUY THEN print "You have chosen to buy "HAMO" units."
7810 MONEY=MONEY-HBUY
7820 s.health=s.health+HAMO
7830 H=10000+s.health:print "You now have "H" units of health.":GOTO 7500
7910 'this is the section which determines whether
7920 'you are fighting the end challenge or not.
7930 CL(CHALL)=1
7940 TOURNAMENT=TOURNAMENT+1
7950 IF TOURNAMENT>=8 THEN 7980
7970 GOTO 4310
7975 'this is the section which allows you to fight humorse
7980 print "Congratulations! You have defeated all the mortals!"
7990 print "Before we confirm you winner, you will be put through one more match!"
8000 HEA=20000
8010 A=HEA:print "With a total health of "A","
8020 print "You have a strong power, your chances are weak of defeating this"
8030 print "Warrior."
8040 CHARCHALL$="Humorse"
8050 goto 4575
8055 'humorse
8060 HUMORSE=int(rnd(1)*7)+1
8160:print hum$(humorse)"!":h=h-(humnew(humorse)*dest)
8190 goto 6885

8490 'this section defines the code for life subtraction
8500 'and rematch concept.
8510 H=10000+s.health
8520 A=HEA
8530 LIVES=LIVES-1
8540 IF LIVES<1 then print "Sorry, you don't have any lives left! Better luck next time!":END
8560 print "You have "LIVES" lives remaining."
8570 ROUND=0:ROUNDCHALL=0:print mn$"Do you want to attempt to defeat "CHARCHALL$" again":print
8580 print "Yes":again$=input$(1)
8590 IF again$="e"  then 4575
8600 IF again$="f" or again$="b" THEN 8630
8620 GOTO 8580
8630 print "No":again$=input$(1)
8640 IF again$="e"  THEN print "GIVING UP??print ":END
8650 IF again$="f" or again$="b" THEN 8580
8670 GOTO 8630
8680 'ok, that settles that.
8690 'now, we write the winning code. this is the code which is activated
8700 'when you beat the end challenger.
8710 print "You have proven that you are imortal!"
8712 print "All eight mortals, and the *former* imortal, Humorse, have been defeated!"
8714 print "You have beat version 3 of Tournament, CONGRATULATIONS!"
8780 END
8790 IF TOURNAMENT>=8 then 8710
8810 GOTO 7020
8820 'save/load feature
8860 'load game
8870 AZ=1:print ed$"Enter name of file where your data is stored. Do not include the .tng extension":INPUT l$:l$=l$+".tng"
8880 print "loading: "l$:OPEN l$ for input as 1
8890 input#1,CHAR$
8900 input#1,CHARCHALL$
8910 input#1,DEST
8920 FOR I=1 TO 8:input#1,CL(I):NEXT
8930 input#1,TOURNAMENT
8940 input#1,MONEY
8950 input#1,LIVES
8960 input#1,S.ef
8970 input#1,H
8980 input#1,A
8990 input#1,HEA
9000 input#1,s.health
9005 input#1,bottle
9008 input#1,bot
9009 input#1,boot,glove,ring1
9010 close 1:kill l$
9020 AZ=0:print "Restored.":GOTO 4340
9030 'save feature starts here
9040 IF sa$="y" THEN gosub 9080:return 'to challSelect
9050 IF sa$="n" THEN return 'to challSelect
9060 goto 4310
9070 'save feature starts here
9080 AZ=2:print ed$"Enter file name. Do not include a file extension.":INPUT FILE$:file$=file$+".tng"
9090 print "SAVING":open file$ for output as 2
9100 print#2,CHAR$
9110 print#2,CHARCHALL$
9120 print#2,DEST
9130 FOR I=1 TO 8:print#2,CL(I):NEXT
9140 print#2,TOURNAMENT
9150 print#2,MONEY
9160 print#2,LIVES
9170 print#2,S.ef
9180 print#2,H
9190 print#2,A
9200 print#2,HEA
9210 print#2,s.health:print#2,bottle:print#2,bot
9211 print#2,boot,chr$(13),glove,chr$(13),ring1
9215 print "File name="file$".":CLOSE 2:az=0:return 'to 9040
9220 'this section pre-defines all arrays used in this program
9230 'snake

9234 for x=1 to 6:read snake$(x):next
9236 for y=1 to 6:read snakenew(y):next
9240 'scorpion
9244 for x=1 to 7:read scorpion$(x):next
9246 for y=1 to 7:read scornew(y):next
9256 'sub-zero
9260 for x=1 to 6:read subzero$(x):next
9262 for y=1 to 6:read subnew(y):next
9267 'stryker
9270 for x=1 to 5:read stryker$(x):next
9272 for y=1 to 5:read strnew(y):next
9277 'liukang
9280 for x=1 to 5:read liuk$(x):next
9282 for y=1 to 5:read liunew(y):next
9287 'jax
9290 for x=1 to 6:read jax$(x):next
9292 for y=1 to 6:read jaxnew(y):next
9297 'ken
9300 for x=1 to 4:read ken$(x):next
9302 for y=1 to 4:read kennew(y):next
9305 'store
9307 for x=1 to 8:read store$(x):next
9311 'munawar
9314 for x=1 to 7:read mb$(x):next
9316 for y=1 to 7:read mbnew(y):next
9324 'dest
9327 for x=1 to 3:read dst$(x):next
9332 for x=1 to 4:read gmenu$(x):next
9334 'humorse
9337 for x=1 to 7:read hum$(x):next
9338 for y=1 to 7:read humnew(y):next
9340 return
9500 'this is the store option where you may buy the instant death potion
9502 if bot>0 then 9516
9503 bot=1:print "'You have chosen to buy the instant death potion!'"
9504 print "'Before I hand you this bottle, I must warn you!'"
9506 print "'This potion isn't always successful!'"
9510 print "'If you use it, be warned that it may back fire,":print "killing both you and your challenger!'"
9512 print "'Or, it may not reverse and instead explode in your challenger's range.'"
9514 print "'With that said, I need $5000.'"
9516 if money>=5000 then print "You hand him $5000.":goto 9520
9518 print "You don't have $5000.":goto 7220
9520 print "'Here you go, "char$". Have fun!'"
9522 print "The store clerk hands you a gleaming bottle, with a thick, yellowish liquid in its contents."
9524 print "You may now use the Kill Instantly! option in your move list!"
9526 money=money-5000:bottle=bottle+1:goto 7500
9530 'hard boots
9531 if boot=1 then print "You fool! Why buy them if you have them already?":goto 7500
9533 if money<2000 then print "You can't afford Hard Boots for $2000.":goto 7500
9535 print "You buy a pair of Hard Boots for $2000.":boot=1:money=money-2000:wear=0:goto 7500
9536 'Punching gloves
9537 if glove=1 then print "You fool! Why buy them if you have them already? ":goto 7500
9539 if money<2000 then print "You can't afford Punching Gloves for $2000.":goto 7500
9541 print "You buy Punching gloves for $2000.":glove=1:money=money-2000:wear=0:goto 7500
9542 'death ring
9543 if ring1=1 then print "You have one with you already, old one.":goto 7500
9544 if money<10000 then print "Old one, this ring is beyond your income.":goto 7500
9545 print "The ring has a precious stone on it with '"char$"' engraved on it."
9546 print "After placing the ring around your right-hand ring finger, you feel the presence of unknown magic.";
9547 print "In your head, a voice says, 'May the magic be with you, old one.'":money=money-10000:ring1=1:wear=0:goto 7500
9550 'this is the code which runs the instant death potion
9552 if bottle<1 then print "You don't have one with you!":goto 6930
9553 if charchall$="Humorse" then print "This potion may not be used when fighting "charchall$".":goto 6930
9554 bottle=bottle-1:print "You get one bottle from your inventory, labeled 'INSTANT DEATH'."
9556 throw=int(rnd(1)*2)+1
9558 if throw=1 then print "The bottle explodes in a blinding yellow light! Both you and your opponent face serious doom!":goto 8510
9560 print "You loosen the cap on the bottle, and throw it!"
9562 a=a-10000
9564 if a<1 then print "It destructs "charchall$" instantly!":round=round+2:goto 4575
9800 'This is move code for Munawar
9802 choice$="":x=0
9804:print mn$""sel$
9808 choice$=input$(1)
9810 if choice$="f" then cls:x=x+1
9812 if choice$="b" then cls:x=x-1
9814 if x<1 then x=7
9816 if x>7 then x=1
9818 if choice$="e" then cls:goto 9824
9820 print mb$(x)"="mbnew(x)+s.ef:goto 9808
9824 if x=7 then 9552
9826:print mb$(x)"!":a=a-(mbnew(x)+s.ef)
9830 gosub 22005:gosub 23001:goto 6905
9840 'Move code for Munawar challenger
9842 mb=int(rnd(1)*6)+1
9844 print mb$(mb)"!":h=h-(mbnew(mb)*dest)
9850 goto 6885
9860 'this section has been added in 2.0.6; these are the names of chars, moves,
9861 'and menu items used throughout the runtime of this program
9862 'moveNames will be accompanied by their default effect power in the respective order
9867 'charnames
9870 data"Snake","Scorpion","Sub-zero","Stryker","LiuKang","Jax","Ken","Munawar"
9875 'snake
9876 data"Spit","Poison bite","Tongue swing","coil","Clamp","Kill Instantly!"
9878 'effect
9879 data 100,150,500,1000,900,0
9884 'scorpion
9885 data"Hand grab","Get over here","Air throw","Combo","Fire breath","Back throw","Kill instantly!"
9887 'effect
9888 data 300,40,1000,2000,500,800,0
9893 'sub-zero
9894 data"Puddle","Ice throw","Freeze","Combo","Deep freeze","Kill instantly!"
9896 'effect
9897 data 800,500,80,2000,100,0
9901 'stryker
9902 data"Guns","Bomb","Combo","Double bomb","Kill instantly!"
9904 'effect
9905 data 2000,1000,1000,2000,0
9910 'LiuKang
9911 data"Flying kick","Combo","Bicycle kick","Fire ball","Kill instantly!"
9913 'effect
9914 data 500,1000,4000,800,0
9919 'jax
9920 data"Stomp","Combo","Got ya","Multiple slam","Metal punch","Kill Instantly!"
9922 'effect
9923 data 900,1700,2500,2000,500,0
9928 'ken
9929 data"Dragon punch","Fire ball","Combo","Kill instantly!"
9931 'effect
9932 data 1000,2000,450,0
9937 'constants for store options
9938 data"Increase lives","Increase effect power","Increase health","Purchase instant death potion"
9939 data"Buy Hard Boots","Buy Punching Gloves","Buy Death Ring","Exit store"
9946 'munawar
9947 data"Fire blast","Whirl wind","Club swing","Slam","Head butt","Munawar punch","Kill instantly!"
9949 'effect
9950 data 1000,2500,1500,1500,900,2500,0
9955 'destiney
9956 data"Novice","Intermediate","Expert"
9957 'Game menu
9958 data"Start new game","Load game","Instructions","Exit game"
9963 'humorse
9964 data"Grab punch","Crush","Toss","Rip","Head smash","STUNNER","Zap"
9966 'effect
9967 data 500,3000,2000,1500,2000,4500,3000
22000 'this next section is the follow up actions
22005 fa=int(rnd(1)*4)+1:if fa=1 or a<1 then return
22007 d=0:print "You follow up your attack with a ";:s.fa=int(rnd(1)*50)+1
22010 if fa=2 then print "powerful kick to the balls.";:a=a-(s.fa+150):if boot<>1 then return
22013 if boot=1 and fa=2 then print "You inflict more damage using your boots.":a=a-(s.fa+300):d=int(rnd(1)*5)+1
22014 if d=3 or d=4 and boot=1 then print "They crack, rendering them unusable!":boot=0:return
22015 if fa=3 then print " deadly punch to the head.";:a=a-(s.fa+180):if glove<>1 then return
22016 if glove=1 and fa=3 then print "You inflict more damage using your gloves.":a=a-(s.fa+400):d=int(rnd(1)*5)+1
22017 if d=3 or d=4 and glove=1 then print "They rip, rendering them unusable!":glove=0:return
22020 if fa=4 then print "cheesy slap across the face.":a=a-5:return
22021 return
23000 'all magical jewelry is generated here
23001 if ring1=1 and charchall$="Humorse" then print "Your Death Ring glows and allows your challenger one more move only!"
23002 if ring1=1 and charchall$="Humorse" then round=4:a=0:return
23003 return
24000 'what you are wearing is told here
24001 print "You are wearing:"
24003 if boot+glove+ring1=0 then print "nothing":return
24005 if boot=1 then print "A pair of Hard Boots"
24007 if glove=1 then print "A pair of Punching Gloves"
24009 if ring1=1 then print "A ring with '"char$"' engraved on a stone on top"
24010 return
