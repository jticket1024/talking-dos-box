10 INPUT "Subtraction , Addition, Multiplication or Division?";M$
20 IF M$="A" THEN M1$=" + " ELSE IF M$="S" THEN M1$=" - " ELSE if m$="M" THEN M1$="*" ELSE IF M$="D" THEN M1$="/" ELSE PRINT "ENTER EITHER 'S' 'A', 'M' OR 'D'":RUN
50 INPUT "ENTER DIFFICULTY LEVEL (1-6)";D
70 IF M$="D" AND D>1 THEN N1=D-1 ELSE N1=D
100 INPUT "NUMBER OF PROBLEMS";N
150 RANDOMIZE TIMER
160 FOR I=1 TO D:SP$=SP$+"#":NEXT
200 FOR I=1 TO N
220 N9$="                  "
250 A=INT(RND(1)*(10^D))+1:IF LEN(STR$(A))-1<D THEN 250
300 B=INT(RND(1)*(10^N1))+1
305 IF A<B THEN SWAP A,B
310 IF M$="D" AND A/B<>INT(A/B) THEN A=INT(A/B)*B
455 IF M$="A" THEN AN=A+B ELSE IF M$="S" THEN AN=A-B ELSE IF M$="M" THEN AN=A*B ELSE AN=A/B
458 PRINT A;M1$;B:INPUT N9
555 IF N9=AN THEN C=C+1:PRINT CHR$(7):?:?"CORRECT!!":GOTO 750
560 ?:?
600 W=W+1:IF W=2 THEN PRINT "WRONG AGAIN....THE ANSWER IS";AN:W=0:GOTO 750"
650 ?"WRONG....TRY AGAIN"
660 ?
700 GOTO 458
750 C=C-W
755 W=0
760 ?
800 NEXT
850 ? "YOU GOT";C;"PROBLEMS CORRECT OUT OF"N"PROBLEMS"
860 ?"YOUR AVERAGE FOR LEVEL"D"IS"INT((C/N)*100)"PERCENT"
870 DATA "F","D","C","B","A","A+"
880 A=INT((C/N)*10):IF A<5 THEN A=5
890 FOR I=5 TO A:READ G$:NEXT I
900 PRINT "YOU GET ";:IF A=5 OR A>8 THEN PRINT "AN '"; ELSE ?"A '";
910 PRINT G$;"'"
