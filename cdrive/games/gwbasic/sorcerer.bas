2 sr$="you have no sword to do it with. oh my! the sorcerer has just"
3 gb$="a gold bar falls out!"
4 d1$="okay. gulp gulp gulk. something seems different now..."
5 di$="you're not holding it hence it is impossible to drink it."
6 co$=chr$(44)
7 x=rnd(-ti):x=0
8 nw$(1)="nice weather we've been having lately."
9 qt$=chr$(34)
10 vt$="the movement of the painting reveals a vault in the wall. the vault is"
11 print " sorcerer's castle adventure"
12 vt$=vt$+" locked."
13 print :gt$(1)="wheeee!":gt$(2)="are you enjoying yourself?"
14 sr$=sr$+" killed you!":print " by greg hassett
15 print :sk$="the sorcerer died and then vanished. his sept or remains."
16 print " converted to pet by john o'hare"
17 db$="you must supply a direct object."
18 def fn r(q)=int(rnd(1)*q)+1
19 cp=49:s1$="i don't see it here.":s2$="don't be ridiculous.":goto 1700
20 goto 1500
21 RANDOMIZE TIMER
30 if fnr(85)=20 then 1170
40 if zz>2 and fnr(100)=50 then 1240
50 if fnr(27)=8 then 1330
60 t=t+1:gosub 1430:if vb$="ala" then if cp=32 then cp=17:goto 20
65 if vb$="ala" then print "nothing happened.":goto 60
70 if vb=-1 and (no>21 and no<30) then vb=1
80 if no$="jes" then print "i see no jester here.":goto 60
90 if vb$="cro" then if (cp=52 or cp=53) then if cp=53 then cp=52:goto 20
95 if vb$="cro" then if (cp=52 or cp=53) then 20
100 if vb$="tur" then print "you have used"t"turns so far.":goto 60
110 if vb<>30 and (vb>10 or vb=20 or vb=6) and no$="" then print db$:goto 60
120 if vb$="hi" then print nw$(fnr(2)):goto 60
130 if no$="gun" then no=1
140 if vb=30 then 1070
150 if no$="bag" then no=5
160 if vb=-1 and no<>0 and (no<22 or no>29) then print "you must supply a verb.":goto 60
170 if vb<1 and no=0 then print "i don't know how to "qt$n1$qt$" anything.":goto 60
180 if vb=6 and (no$="eve" or no$="all") then 200
190 if no=0 and vb>10 then print "i don't know what a "qt$n1$qt$" is.":goto 60
200 on vb goto 210,280,20,350,380,400,460,470,480,490,510,520,550,640,660,740,770
205 on vb-17 goto 830,840,840,550,920,920,940,950,980,1010,1040,1050,1070
210 if (no<22 or no>29) and no$<>"" then print "i don't know how to do that.":goto 60
220 if no$="" then print "where?":goto 60
230 if no>25 then no=no-4
240 no=no-22:if p(cp,no)=0 then print "there is no way to go in that direction.":goto 60
250 if cp=1 and no=1 and df=0 then print "the castle doorz is locked.":goto 60
260 if cp=17 and no=1 and cf=0 then print "the crack is too small for you to follow":goto 60
270 cp=p(cp,no):goto 20
280 if ob(no,0)=-1 then print "you've already got it!":goto 60
290 if no=0 then print "whats a "n1$"?":goto 60
300 if ob(no,0)<>cp then print s1$:goto 60
310 if no=17 or no=21 or no=20 or no=16 then print s2$:goto 60
320 if zz+1>7 then print "you are unable to carry any more.":goto 60
330 if no=9 and pf=0 then print vt$:pf=1:ob(16,0)=cp:ob(9,0)=-1:zz=zz+1:goto 60
340 print "okay.":zz=zz+1:ob(no,0)=-1:goto 60
350 if zz=0 then print "you are empty-handed.":goto 60
360 print "you are holding the following:":for i=1 to lo:if ob(i,0)=-1 then print ob$(i)
370 next i:goto 60
380 j=0:for i=1 to lo:if ob(i,0)=1 then j=j+ob(i,1)
385 next i
390 print "out of a maximum of 225 points, you have";j;"points.":if j<225 then 60
395 if j=225 then 3000
400 if no<>0 and ob(no,0)<>-1 then print "you're not carrying it!":goto 60
410 if no$<>"eve" and no$<>"all" then 440
420 if zz=0 then print "you're not carrying anything!":goto 60
425 for i=1 to lo:if ob(i,0)=-1 then ob(i,0)=cp
430 next i:zz=0:print "ok.":goto 60
440 if no=0 then print "i've never heard of a "n1$".":goto 60
450 ob(no,0)=cp:print "dropped.":zz=zz-1:goto 60
460 print "s or ry, this is your adventure, no mine!":goto 60
470 print "":input"press <return> when ready to ***save*** ";a$
473 open "sorcerer.sav" for output as 1
475 print "saving.":for i=1 to lo:print #1,ob(i,0):next i:print #1,cp:print #1,t
476 print #1,df;co$;vf;co$;cf;co$;pf;co$;sf;co$;wf;co$;zz
478 close 1:goto 20
480 print "":input"press <return> when ready to ***load*** ";a$
482 open "sorcerer.sav" for input as 1
485 print "loading":for i=1 to lo:input#1,ob(i,0):next i
486 input#1,cp,t,df,vf,cf,pf,sf,wf,zz
487 close 1:goto 20
490 input"do you really want to quit now";qn$:if left$(qn$,1)<>"y" then 60
495 input"are you sure";q2$:if left$(q2$,1)<>"y" then print "i didn't think so.":goto 60
500 goto 1370
510 print "alright...";n1$:goto 60
520 if ob(no,0)<>-1 then print di$:goto 60
530 if no<>7 then print s2$:goto 60
540 print d1$:zz=zz-1:ob(7,0)=0:cf=1:goto 60
550 if no<>31 and no<>16 and no<>30 then print "i don't know how to open such a thing.":goto 60
560 if no=16 and ob(16,0)<>cp then print "what vault?":goto 60
570 if no=16 and ob(2,0)<>-1 then print "you don't have the key.":goto 60
580 if no=16 then print "the vault is open":vf=1:if ob(15,0)=0 then print gb$:ob(15,0)=cp
590 if no=16 then 60
600 if no=31 then 1140
610 if cp<>1 then print "what doorz ?":goto 60
620 if ob(2,0)<>-1 then print "you don't seem to have the key.":goto 60
630 print "the doorz is open.":df=1:goto 60
640 if ob(no,0)<>-1 then print "you are not carrying it.":goto 60
650 print "i've known strange people, but attacking"ob$(no);"?":goto 60
660 if no=20 then 710
670 if no=17 then print "what knight ?":goto 60
680 if no=21 then print "i see no pirate here.":goto 60
690 if ob(i,0)<>-1 then print "you are not currently holding that.":goto 60
700 print "it isn't alive!":goto 60
710 if cp<>32 then print "what sorcerer?":goto 60
720 if ob(8,0)<>-1 then print sr$:for i=1 to 3000:next i:goto 1370
730 print sk$:ob(14,0)=cp:ob(20,0)=0:goto 60
740 if ob(no,0)<>-1 then print "you don't have that right now.":goto 60
750 if no<>3 then print "how do you expect to read ";ob$(no);"?":goto 60
760 print "it says: magic w or d: alakazam.":goto 60
770 if ob(no,0)<>-1 and ob(no,0)<>cp then print "i don't see that here.":goto 60
780 if ob(1,0)<>-1 then print "you don't have a gun!":goto 60
790 if no=17 then print "the bullet dissolves as it hits the arm or .":goto 60
800 print "the golden bullet from the golden pistolvap or izes that."
810 if ob(no,0)=-1 then zz=zz-1
820 ob(no,0)=0:goto 60
830 if ob(no,0)=-1 then print s2$
835 print "you're not holding it.":goto 60
840 if no=16 or no=30 or no=31 then 870
850 if ob(no,0)<>-1 then print "you're not holding that to my knowledge.":goto 60
860 print "i don't know how to close such a thing.":goto 60
870 if no=16 and ob(16,0)<>cp then print "what wault?":goto 60
880 if no=16 then print "the vault is closed and locked.":vf=0:goto 60
890 if no=31 then 1110
900 if cp<>1 then print "what doorz ?":goto 60
910 print "the doorz is closed and locked.":df=0:goto 60
920 if ob(no,0)<>-1 then print "you're not holding it.":goto 60
930 print "no, these items have to be used on the next adventure, to o.":goto 60
940 print "the value of that is"ob(no,1)"points.":goto 60
950 if ob(no,0)<>-1 then print "you don't seem to have it.":goto 60
960 if no<>8 then print "wow is this fun!":goto 60
970 print "whoosh!":goto 60
980 if ob(no,0)<>-1 then print "you're not currently carrying it.":goto 60
990 if no<>8 then print s2$:goto 60
1000 print "it is already raz or sharp.":goto 60
1010 if ob(no,0)<>-1 then print "tu ne le p or tes pas(you're not carrying it.":goto 60
1020 if no<>19 then print "how?":goto 60
1030 print "very good! now you can go to the second grade!":goto 60
1040 print "try drop.":goto 60
1050 print "i can only tell you what is in the room with you, s or ry."
1060 print "i cannot find anything.":goto 60
1070 if cp<>44 then print gt$(fnr(2))
1080 if cp<>44 then 60
1090 if wf=0 then print "the window is only slightly ajar.":goto 60
1100 print "okay...you fall safely in the pile of leaves.":cp=48
1101 print "you seem to be lost in a forest of spruce trees.":goto 60
1110 if cp<>44 then print "what window? i don't see a window!":goto 60
1120 if wf=0 then print "it is already closed.":goto 60
1130 print "having opened it earlier, you find it impossible to close.":goto 60
1140 if cp<>44 then print "waht window?":goto 60
1150 if wf=1 then print "it is already open.":goto 60
1160 print "with great effort, you open the"
1161 print "window, there is a pile of leaves outside the window.":wf=1:goto 60
1170 print "holy moly! a black knight walked in to kill you!":gosub 1430
1180 if vb<>17 or no<>17 then 1210
1190 if ob(1,0)<>-1 then print "you have no gun!":goto 1210
1200 print "the knight disintigrated.":goto 60
1210 print "the knight is running to wards you.......";
1220 if fnr(5)=2 then print "it kills you!":for i=1 to 3000:next i:goto 1370
1230 print "it misses you
1232 print " and crashes in to a wall...wait! he has disappeared!":goto 60
1240 print "a pirate just appeared and chartled,"
1250 print "well shiver me timbers! not another one!";
1255 print "har har har, i'll just snatch all this"
1260 print "booty and hide it deep in the maze."
1270 rem
1280 print "with that he takes all of your treasure!"
1290 for i=1 to lo:if ob(i,0)<>-1 then 1320
1300 if ob(i,1)=0 then 1320
1310 ob(i,0)=fnr(6)+34:zz=zz-1
1320 next i:goto 60
1330 print "jingle jingle jingle!":print "chester the jester pops up and chants,-"
1340 print c$(fnr(8)-1):print " tee hee hee!"
1350 print " and then vanishes in a cloud of pink smoke."
1360 goto 60
1370 print "":j=0:for i=1 to lo:if ob(i,0)=1 then j=j+ob(i,1)
1380 next i
1390 print "out of a maximum of 225 points, you have scored"j"points."
1400 print "hope you had fun!":end
1410 print "whazzat?"
1420 stop
1430 input"**what do you want to do ";cm$
1435 if cm$=" " then 1430
1440 n1$="":v1$="":no=0:vb=0:no$="":vb$=""
1450 cm=len(cm$):for i=1 to cm:if mid$(cm$,i,1)<>" " then v1$=v1$+mid$(cm$,i,1):next i
1460 vb$=left$(v1$,3):for i=1 to nv:if vb$(i)=vb$ then vb=i:goto 1480
1465 next i
1470 vb=-1:n1$=v1$:goto 1490
1480 if len(v1$)+1>len(cm$) then no=0:return
1485 n1$=right$(cm$,len(cm$)-1-len(v1$))
1490 no$=left$(n1$,3):for i=1 to nn:if no$(i)=no$ then no=i:return
1493 next i
1495 return
1500 print "":print "you're ";p$(cp):for i=1 to lo
1510 if ob(i,0)=cp then print "*there is ";ob$(i)" here."
1520 next i
1530 if cp=1 and df=0 then print "the doorz is locked."
1540 if cp=18 and vf=0 and ob(16,0)=18 then print "the vault is locked."
1550 if cp=17 and cf=0 then print "a small crack leads southward."
1560 if cp=1 and df=1 then print "the door is open."
1570 if cp=18 and vf=1 and ob(16,0)=18 then print "the vault is open."
1580 if cp=44 and wf=0 then print "the window is slightly ajar."
1590 if cp=17 and cf=1 then print "a to wering hole leads southward."
1600 if cf=0 then p(17,1)=0
1610 if cp=44 and wf=1 then print "the window is wide open. there is a"
1615 if cp=44 and wf=1 then print "rather large pile of leaves outside the window."
1620 k=0:print "you can go ";:for i=0 to 3:if p(cp,i)=0 then 1650
1630 if k=1 then print ", ";
1640 print d$(i);:k=1
1650 next i:if k=0 then print "nowhere!"
1660 if k=1 then print 
1670 p(17,1)=18
1680 goto 30
1690 o=0:return
1700 np=53:lo=35:nn=31:nv=30:dim p(np,3),p$(np),vb$(nv),no$(nn),ob(lo,1),ob$(lo)
1710 p$(1)="outside a medieval castle, the pavement has an inscription:"
1711 p$(1)=p$(1)+chr$(13)+"leave all treasure here say 'score'."
1715 data 5,8,3,2,4,7,1,0,6,7,0,1
1720 p$=" side of the castle.":p$(2)="at the west"+p$:p$(3)="at the east"+p$
1730 p$(4)="in a vast forest stretching out of sight.":data 4,2,4,4,53,0,6,1
1740 p$(5)=p$(4):p$(6)=p$(4):data 6,6,6,5
1750 p$(7)="at the south"+p$+" there is a to wer with a small window in"
1755 p$(7)=p$(7)+" the far, far distance.":data 2,0,0,0
1760 p$(8)="in a splendid chamber 30 feet high.":data 1,11,0,10
1770 p$(9)="in a damp, musty library.":data 0,10,0,0
1780 p$(10)="in the master bedroom.":data 9,0,8,0
1790 p$="in a vast corridor stretching out of sight to the ":p$(11)=p$+"south."
1795 data 8,12,14,0
1800 p$(12)=p$+"northand south.":data 11,13,15,0
1810 p$(13)=p$+"north.":data 12,0,16,17
1820 p$="in a bedroom with a ":p$(14)=p$+"stone floor.":p$(15)=p$+"wooden floor
1825 p$(16)=p$+"dirt floor.":data 0,0,0,11,0,0,0,12,0,33,0,13
1830 p$(17)="in a dusty pantry. markings in the dust read , go south to dungeon.
1835 data 0,18,13,0
1840 p$(18)="in a primitive art gallery.":data 17,19,0,19
1850 p$(19)="in a maze of twisty little passages."
1860 p$(20)="in a twisty maze of little passages."
1870 p$(21)="in a little maze of twisty passages."
1880 p$(22)="in a twisting maze of little passages."
1890 p$(23)="in a little twisty maze of passages."
1900 data 0,20,18,0,20,22,20,19,21,21,22,23,21,22,20,22,21,24,23,23
1910 p$(24)="in the kitchen.":data 23,27,25,0,0,27,26,24,29,28,30,25
1920 p$(25)="in the dining room.":p$(26)="in the alcove to the study."
1925 p$(27)="in the office of the sorcerer (?)":data 24,0,28,0
1930 p$(28)="in the drawing room.":data 0,0,0,27,0,26,0,0,0,31,0,26
1940 p$(29)="in the parl or .":p$(30)="in the study.":p$(31)="in the sitting room."
1950 p$(32)="in the dungeon!":data 0,0,0,0,0,0,0,0
1960 for i=1 to np:for j=0 to 3:read p(i,j):next j:next i:p(31,fnr(4)-1)=32
1970 p$(33)="in a primeval conference room.":data 16,0,35,0
1980 p$(34)="in the to wer. it overlooks a huge"
1981 p$(34)=p$(34)+"kingdom down a monsterous mountain, the grass is greener "
1982 p$(34)=p$(34)+"than green itself.":data 0,0,36,0
1990 p$(35)="in a maze of twisty little passages, all alike."
1995 for i=36 to 40:p$(i)=p$(35):next i:data 36,35,35,33,38,34,37,35,36,0,39,0,40,39
1996 data 39,36,38,37,39,38,41,38,0,0,42,40,0,0
2000 p$(41)="at the brink of a huge pit."
2001 p$(43)="in a barren cubical, with passages leading off to the"
2002 p$(43)=p$(43)+" northand south.":p$(42)="in the pit. a good passage"
2003 p$(42)=p$(42)+" exits to the north."
2010 p$(44)="at the end of the castle. you cansee a forest out a small window."
2015 data 43,0,41,0,44,42,0,0,0,0,0,0
2020 p$(45)="lost in a dark forest made up chiefly of spruce trees."
2025 for i=46 to 48:p$(i)=p$(45):next i:data 45,46,45,49,45,47,46,50,45,51,47,48,50
2026 data 48,47,48
2030 p$(49)="on an old path made by horses in mideval times."
2035 p$(51)="in the middle of a clearing, to the south is a bridge."
2037 p$(52)="on the north side of the bridge."
2038 p$(53)="on the south side of the bridge."
2039 data 0,50,45,0,49,48,46,0,0,52,47,0,51,53
2040 data 0,0,52,5,0,0,pis,key,boo,rug,coi,sap,liq,swo,pai,opa,sil,pen,pil,sep
2045 data bar,vau,kni,cup,cro,s or ,pir,n or ,sou,eas,wes,n,s,e,w,doo,win
2050 data go,get,loo,inv,sco,dro,hel,sav,loa,qui,say,dri,ope,hit,kil,rea,sho,eat
2055 data clo,loc,unl,des,bre,val,swi,sha,wea,thr,fin,jum
2060 for i=1 to nn:read no$(i):next i:for i=1 to nv:read vb$(i):next i
2070 data a gold pistol,4,10,a set of keys,2,0,a book on sorcery,9,0
2072 data a persian rug,10,15,a bag of coins,3,15,a priceless saphire,14,10
2074 data a flask of liquid,15,0,a golden sword,42,10,a priceless painting,18,15
2080 data a giant opal,22,15,a set of silverware,24,15,a platinum pen,27,5
2082 data a velvet pillow,43,20,the sorcerer's septor,32,50,a gold bar,0,20
2084 data a vault in the wall,0,0,a black knight,0,0,a pewter cup,25,15
2086 data a kings crown,30,10,xxx
2090 data 32,0,an evil pirate,0,0
2100 for i=1 to 21:read ob$(i),ob(i,0),ob(i,1):next i
2110 data north,south,east,west
2120 for i=0 to 3:read d$(i):next i
2130 p$(19)=p$(19)+chr$(13)+"a note on the wall read s: this is not the maze"
2135 p$(19)=p$(19)+" where the pirate hides his treasure. --gh"
2140 ob$(20)="an evil high sorcerer just waiting to cast a spell on you."
2150 p$(50)="at the end of the path, with forest surrounding you in all"
2155 p$(50)=p$(50)+" directions except north, where you were before."
2160 p(50,3)=48:p$(47)=p$(47)+chr$(13)+" to the south there seems to be light."
2170 for i=52 to 53:p$(i)=p$(i)+chr$(13)+"across the bridge is more forest.":next
2180 p(36,2)=37
2190 c$(0)="pay attention in the maze, to the wording of the phrase! "
2195 c$(0)=c$(0)+"if you do you'll be un-mazed."
2200 c$(1)="gulp gulp gulp, drink it down, i'm not such a stupid clown!"
2235 c$(4)=c$(4)+" confront youface to face!"
2240 c$(5)="hocus pocus, alakazam! don't read the book or you'll be sorry!"
2250 c$(6)="passages that all seem alike can seem quite different, just hope"
2255 c$(6)=c$(6)+" you're right!"
2260 c$(7)="not all treasure lies under a roof, some might be under a tree!"
2270 goto 20
3000 print " congratulations! you killed the"
3010 print "sorcerer and gathered all the treasures!"
3020 print "you are a terrific adventurer!"
3030 print "do you want to play again? (y/n)"
3040 a$=inkey$:if a$="" then 3040
3050 if a$="y" then run
3060 if a$="n" then system
3070 goto 3040
