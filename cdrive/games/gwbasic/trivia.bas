100 print "hi and welcome to a trivia game."
101 print "this game has 10 questions"
102 print "and you have to answer them."
103 print "press y to start, n to exit game."
104 I$=INKEY$:IF I$="" THEN 104
105 if i$="y" or i$="Y" OR I$="Y" then 109
106 if i$="n" or i$="N" then 253
107 goto 103
109 cls:print "question 1."
110 print "what is my home page address?"
111 input n$
112 if right$(n$,22)="geocities.com/mun00092" or right$(n$,22)="GEOCITIES.COM/MUN00092" THEN 117
115 goto 250
117 print "correct!"
118 print "press y to go to the next question, n to quit!"
119 i$=inkey$:if i$="" then 119
120 if i$="y" or i$="Y" then 124
121 if i$="n" or i$="N" then 253
124 cls:print "question 2."
125 print "in e-mail, the e stands for what?"
127 input n$
128 if n$="electronic" then 131
129 goto 250
131 print "correct!"
132 print "press y for next question, n to quit."
133 i$=inkey$:if i$="" then 133
134 if i$="y" or i$="Y" then 138
135 if i$="n" or i$="N" then 250
136 goto 132
138 cls:print "question 3."
139 print "what file type is this game in?"
140 input n$
141 if left$(n$,3)="bas" or left$(n$,3)="BAS" then 146
142 if right$(n$,3)="bas" or right$(n$,3)="BAS" then 146
144 goto 250
146 print "correct!"
148 print "press y for next question, n to quit."
149 i$=inkey$:if i$="" then 149
150 if i$="y" or i$="Y" then 155
152 if i$="n" or i$="N" then 253
153 goto 148
155 cls:print "question 4."
156 print "what is my first name?"
157 input n$
158 if left$(n$,4)="muna" or left$(n$,4)="MUNA" then 162
160 goto 250
162 print "correct!"
163 print "press y for next question, n to quit."
164 i$=inkey$:if i$="" then 164
165 if i$="y" or i$="Y" then 170
166 if i$="n" or i$="N" then 253
168 goto 163
170 cls:print "question 5."
171 print "what screen reader uses scripts? win eyes or jaws for windows?"
172 input n$
173 if n$="jaws for windows" or n$="JAWS FOR WINDOWS" THEN 177
175 goto 250
177 print "correct!"
178 print "press y for next question, n to exit."
179 i$=inkey$:if i$="" then 179
180 if i$="y" or i$="Y" then 184
181 if i$="n" or i$="N" then 253
182 goto 178
184 cls:print "question 6."
185 print "how many seconds in an hour?"
186 print "(do not use commas.)"
187 input n$
188 if n$="3600" then 191
189 goto 250
191 print "correct!"
192 print "press y for next question, n to quit."
193 i$=inkey$:if i$="" then 193
194 if i$="y" or i$="Y" then 198
195 if i$="n" or i$="N" then 253
196 goto 192
198 cls:print "question 7."
199 print "what is the file name of this game?"
200 input n$
201 if n$="trivia.bas" or n$="TRIVIA.BAS" then 203
202 goto 250
203 print "correct!"
204 print "press y for next question, n to quit."
205 i$=inkey$:if i$="" then 205
206 if i$="y" or i$="Y" then 210
207 if i$="n" or i$="N" then 253
208 goto 204
210 cls:print "question 8."
211 print "how do you get into the status menu on a blazie/freedom scientific notetaker?"
212 input n$
213 if n$="s t chord" or n$="S T CHORD" then 217
213 if n$="s t chord" or n$="S T CHORD" THEN 217
214 if n$="st chord" or n$="ST CHORD" then 217
215 goto 250
217 print "correct!"
218 print "press y for next question, n to quit."
219 i$=inkey$:if i$="" then 219
220 if i$="y" or i$="Y" then 224
221 if i$="n" or i$="N" then 253
222 goto 218
224 cls:print "question 9."
225 print "in jfw, to toggle the vertual pc cursor on or off,"
226 print "you press:"
227 input n$
228 if n$="insert+z" or n$="INSERT+Z" then 231
229 goto 250
231 print "correct!"
232 print "press y for next question, n to quit."
233 i$=inkey$:if i$="" then 233
234 if i$="y" or i$="Y" then 238
235 if i$="n" or i$="N" then 253
236 goto 232
238 cls:print "question 10."
239 print "what is the capital of australia?"
240 input n$
241 if n$="canberra" or n$= "CANBERRA" then 244
242 goto 250
244 print "correct!"
245 print "congratulations!"
246 print "you beat the trivia challenge!"
247 print "be sure to check my website for updates!"
248 end
250 print "sorry, you lose. better luck next time."
251 end
253 print "thanks for playing trivia challenge."
254 print "program written by munawar bijani"
255 print "v 1.0."
256 end