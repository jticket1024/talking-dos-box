10 CLS:L=9:DIM I$(33),O$(33),O(33),L$(37),D$(3,37),D(3,37)
100 SCREEN 0:WIDTH 40:KEY OFF:RANDOMIZE VAL(RIGHT$(TIME$,2))
110 LOCATE 11,11,0:PRINT"STONEVILLE MANOR"
120 LOCATE 20,4:PRINT"Translated by Allan & Andrew Lee."
400 FOR X=1 TO 33:READ I$(X),O$(X),O(X):NEXT X
430 FOR X=1 TO 37:READ L$(X):NEXT X
460 FOR Y=1 TO 37:FOR X=1 TO 3:READ D$(X,Y),D(X,Y):NEXT X:NEXT Y
490 FOR X=1 TO 3:READ P$(X):NEXT X
520 FOR X=1 TO 8:READ VE(X):NEXT X
550 FOR X=1 TO 3:Z=INT(89*RND(1))+11:N(X)=Z:NEXT X
590 FOR X=1 TO 3
600 Z=INT(3*RND(1))+1
610 IF SQ(Z)=Z THEN 600 ELSE S(Z)=N(Z):SQ(Z)=Z:NEXT X
660 LOCATE 23,8:INPUT"Do you want instructions";C$:IF LEFT$(C$,1)="y" OR LEFT$(C$,1)="Y" THEN GOSUB 7500
700 CLS:LOCATE 12,2:INPUT"Do you want to continue an old game";C$:IF LEFT$(C$,1)="y" OR LEFT$(C$,1)="Y" THEN GOSUB 8300
1000 RANDOMIZE VAL(TIME$):CLS
1010 PRINT:COLOR 0,7:PRINT"Location:":COLOR 7,0:PRINT L$(L)
1020 PRINT:COLOR 0,7:PRINT"Directions:":COLOR 7,0
1030 FOR X=1 TO 3:GOSUB 5010:NEXT X
1060 PRINT:COLOR 0,7:PRINT"Visible objects:":COLOR 7,0
1070 IF L=30 AND O(13)<>0 THEN 1190
1075 IF L=31 AND O(13)<>0 THEN 1190
1085 FOR X=1 TO 33:IF O(X)=L THEN PRINT O$(X)
1087 NEXT X
1100 GOSUB 5210
1190 PRINT:INPUT">",C$
1220 IF LEFT$(C$,3)="get" THEN 2030
1225 IF C$="drop snorkel"THEN 3640
1230 IF LEFT$(C$,4)="drop"THEN 2190
1240 IF LEFT$(C$,8)="take inv"THEN 2350
1260 IF C$="enter vent"THEN 2395
1270 IF C$="enter balloon"THEN 2540
1280 IF C$="enter lake"THEN 2600
1290 IF C$="enter door"THEN 2650
1300 IF C$="enter store"THEN 2680
1320 IF LEFT$(C$,5)="enter"THEN 2750
1340 IF LEFT$(C$,8)="examine"THEN 2840
1350 IF LEFT$(C$,5)="look "THEN 2850
1360 IF LEFT$(C$,6)="go jog"THEN 3000
1370 IF LEFT$(C$,4)="go e"AND L=32 THEN 3950
1380 IF LEFT$(C$,4)="go o"THEN 3030
1390 IF LEFT$(C$,2)="go"THEN 3080
1400 IF C$="pet serval"THEN 3130
1410 IF C$="feed serval"THEN 3150
1420 IF C$="give trout"THEN 3150
1430 IF LEFT$(C$,8)="cut tree"THEN 3190
1435 IF LEFT$(C$,10)="climb tree"THEN 3800
1440 IF LEFT$(C$,9)="chop tree"THEN 3190
1450 IF LEFT$(C$,4)="dive"THEN 3210
1460 IF C$="end game" THEN end
1470 IF LEFT$(C$,3)="buy"AND L=10 THEN 2060
1475 IF C$="remove cover"THEN 3250
1480 IF LEFT$(C$,9)="open vent" THEN 3250
1485 IF C$="open book"THEN 2850
1490 IF C$="open door"THEN 3295
1495 IF C$="open credenza"THEN 2850
1500 IF C$="open bag"THEN 2850
1510 IF C$="open safe"THEN 7000
1520 IF C$="unlock door"THEN 3295
1540 IF C$="inflate raft"THEN 3350
1550 IF C$="inflate balloon"THEN 3380
1560 IF C$="build balloon"THEN 3380
1570 IF C$="fly balloon"THEN 3460
1580 IF C$="sail balloon"THEN 3460
1590 IF C$="read will"AND F=1 THEN 7200
1600 IF C$="read book"THEN 2850
1605 IF C$="read sign"THEN 3900
1610 IF C$="save game"THEN 8400
1620 IF C$="clear screen"THEN 1000
1990 PRINT"I don't understand.":GOTO 1190
2030 IF C$="get trout"AND L=29 AND O(10)<>0 THEN PRINT"It slipped out of your hands.":GOTO 1190
2035 IF C$="get picture"AND L=16 THEN PRINT"Too valuable.":GOTO 1190
2040 IF L=10 THEN PRINT"Can only buy from the store.":GOTO 1190
2045 IF C$="get table"AND L=37 THEN PRINT"It's nailed down.":GOTO 1190
2050 IF S=1 THEN 6000
2055 IF I=4 THEN PRINT"Inventory too heavy.":GOTO 1190
2060 IF C$="get mask"THEN 6150
2065 IF C$="get snorkel"THEN 6100
2070 FOR X=1 TO 19
2080 G=LEN(I$(X))
2090 IF MID$(C$,5,G)=I$(X) AND O(X)=0 THEN PRINT"Already have object.":X=19:HERP=1:GOTO 2110
2100 IF MID$(C$,5,G)=I$(X) AND O(X)=L THEN O(X)=0:I=I+1:X=19:HERP=2
2110 NEXT X:IF HERP=1 THEN HERP=0:GOTO 1190 ELSE IF HERP=2 THEN HERP=0:GOTO 1000
2120 IF C$="get serval"AND O(30)=L THEN 6210
2130 IF C$="get credenza"AND L=14 THEN PRINT"Can't lift it.":GOTO 1190
2140 IF C$="get case"AND O(26)=L THEN PRINT"Not thirsty.":GOTO 1190
2150 IF C$="get safe"AND O(25)=L THEN PRINT"Safe is secured to wall.":GOTO 1190
2170 GOTO 1990
2190 FOR X=1 TO 19
2200 G=LEN(I$(X))
2210 IF MID$(C$,6,G)=I$(X)AND O(X)=0 THEN P=X:X=19:HERP=1
2220 NEXT X:IF HERP=1 THEN HERP=0:X=P:GOTO 2240
2230 GOTO 1990
2240 IF X=8 AND L=28 THEN O(8)=5:I=I-1:COLOR 23,0:PRINT"Raft drifts away!":FOR X=1 TO 3000:NEXT X:COLOR 7,0:GOTO 1000
2245 IF X=8 AND L=29 THEN O(8)=5:I=I-1:COLOR 23,0:PRINT"Raft drifts away!":FOR X=1 TO 3000:NEXT X:COLOR 7,0:GOTO 1000
2270 I=I-1
2280 IF L=28 THEN O(X)=30:GOTO 1000
2290 IF L=29 THEN O(X)=31:GOTO 1000
2300 O(X)=L:GOTO 1000
2350 FOR X=1 TO 19
2360 IF O(X)=0 THEN PRINT O$(X)
2370 NEXT X
2380 GOTO 1190
2395 FOR X=1 TO 8
2400 IF VE(X)=L THEN HERP=1:X=8
2405 NEXT X:IF HERP=1 THEN HERP=0:GOTO 2415
2410 GOTO 1990
2415 IF O(8)=0 AND R=1 THEN PRINT P$(2):GOTO 1190
2420 FOR X=1 TO 4
2425 IF O(X)=0 THEN PRINT P$(2):HERP=1:X=4
2430 NEXT X:IF HERP=1 THEN HERP=0:GOTO 1190
2450 IF L=13 AND C1=0 THEN PRINT P$(1):GOTO 1190
2460 IF L=14 AND C2=0 THEN PRINT P$(1):GOTO 1190
2470 IF L=17 AND C3=0 THEN PRINT P$(1):GOTO 1190
2480 IF L=18 AND C4=0 THEN PRINT P$(1):GOTO 1190
2485 IF W=0 THEN PRINT"You weigh too much.":GOTO 1190
2490 IF L=13 AND C1=1 THEN L=21:GOTO 1000
2500 IF L=14 AND C2=1 THEN L=24:GOTO 1000
2510 IF L=17 AND C3=1 THEN L=26:GOTO 1000
2520 IF L=18 AND C4=1 THEN L=27:GOTO 1000
2530 GOTO 1990
2540 IF H=0 THEN PRINT"Not ready yet.":GOTO 1190
2550 IF L=8 THEN L=34:GOTO 1000
2560 IF L=36 THEN L=5:GOTO 1000
2570 PRINT"Can't find.":GOTO 1190
2600 IF L<>5 THEN 1990
2610 IF O(8)<>0 THEN PRINT"Need something to float on.":GOTO 1190
2630 IF R=0 THEN PRINT"Raft is too flat.":GOTO 1190
2640 L=28:GOTO 1000
2650 IF L=16 AND K=0 THEN PRINT"Door is locked.":GOTO 1190
2655 IF L=20 THEN L=16:K=1:GOTO 1000
2660 IF L=16 THEN L=20:GOTO 1000
2670 GOTO 1990
2680 IF L<>9 THEN 1990
2690 FOR X=1 TO 19
2700 IF O(X)=0 THEN PRINT"Can't enter store with inventory.":HERP=1:X=19
2710 NEXT X:IF HERP=1 THEN HERP=0:GOTO 1190
2720 L=10:GOTO 1000
2750 IF C$="enter manor"AND L=9 THEN L=12:GOTO 1000
2760 IF C$="enter manor"AND L=1 THEN L=17:GOTO 1000
2770 IF C$="enter hospital"AND L=9 THEN L=11:GOTO 1000
2780 IF C$="enter tunnel"AND L=31 AND O(13)=0 THEN L=32:GOTO 1000
2790 IF C$="enter creek"AND L=4 THEN COLOR 23,1:PRINT"You slipped and fell.":FOR X=1 TO 3000:NEXT X:COLOR 7,0:S=1:L=11:GOTO 1000
2800 IF C$="enter gorge"AND L=8 THEN PRINT"Too steep.":GOTO 1190
2810 IF C$="enter shack"AND L=36 THEN L=37:GOTO 1000
2820 GOTO 1990
2840 G=LEN(C$)-8:Q$=MID$(C$,9,G):GOTO 2860
2850 G=LEN(C$)-5:Q$=MID$(C$,6,G)
2860 FOR X=1 TO 33
2870 IF Q$=I$(X) AND O(X)=L THEN HERP=1:X=33:GOTO 2880
2875 IF Q$=I$(X) AND O(X)=0 THEN HERP=1:X=33
2880 NEXT X:IF HERP=1 THEN HERP=0:GOTO 2900
2890 GOTO 1990
2900 IF Q$="bottle"THEN PRINT P$(3);N(1):GOTO 1190
2910 IF Q$="goblet"THEN PRINT P$(3);N(2):GOTO 1190
2920 IF Q$="table"THEN PRINT"On top is a note with the number ";N(3):GOTO 1190
2930 IF Q$="case"THEN PRINT"One bottle is missing.":GOTO 1190
2940 IF Q$="book"THEN 6550
2950 IF Q$="credenza"AND O(13)=40 THEN PRINT"Inside is a swim mask.":GOTO 1190
2960 IF Q$="bag"AND O(19)=40 THEN PRINT"Inside is a snorkel.":GOTO 1190
2970 IF Q$="picture"THEN PRINT"Behind picture is a safe.":E=1:GOTO 1190
2980 PRINT"Nothing unusual.":GOTO 1190
3000 IF O(11)<>0 THEN PRINT"Need shoes.":GOTO 1190
3010 IF L>9 THEN PRINT"Can't jog here.":GOTO 1190
3015 W=1:PRINT"Whew!...done.":GOTO 1190
3020 IF L=28 THEN L=5 GOTO 1000
3025 GOTO 1990
3030 IF S=1 THEN PRINT"Not well.":GOTO 1190
3040 IF L=21 AND C1=0 THEN PRINT P$(1):GOTO 1190
3050 IF L=24 AND C2=0 THEN PRINT P$(1):GOTO 1190
3060 IF L=26 AND C3=0 THEN PRINT P$(1):GOTO 1190
3070 IF L=27 AND C4=0 THEN PRINT P$(1):GOTO 1190
3080 IF LEFT$(C$,4)="go e"AND L=18 THEN 6300
3090 FOR X=1 TO 3
3100 IF MID$(C$,4,1)=D$(X,L)THEN L=D(X,L):HERP=1:X=3
3110 NEXT X:IF HERP=1 THEN HERP=0:GOTO 1000
3120 PRINT"Direction not clear.":GOTO 1190
3130 IF V=0 AND L=18 THEN 6200
3140 GOTO 1990
3150 IF V=1 THEN 1990
3160 IF L<>18 THEN 1990
3170 IF O(14)<>0 OR L<>18 THEN PRINT"Need food.":GOTO 1190
3180 COLOR 23,0:PRINT"Serval took trout and escaped!":IF O(14)=0 THEN I=I-1
3185 V=1:O(14)=40:O(30)=40:FOR X=1 TO 3000:NEXT X:COLOR 7,0:GOTO 1000
3190 IF L=2 AND O(12)=0 OR O(12)=L THEN O(4)=2:GOTO 1000
3200 GOTO 1990
3210 IF L=8 AND O(8)=0 AND O(19)=0 THEN O(8)=5:I=I-1:L=30:COLOR 23,0:PRINT"Raft drifts away.":FOR X=1 TO 3000:NEXT X:COLOR 7,0:GOTO 1000
3215 IF L=29 AND O(8)=0 AND O(19)=0 THEN O(8)=5:I=I-1:L=31:COLOR 23,0:PRINT"Raft drifts away.":FOR X=1 TO 3000:NEXT X:COLOR 7,0:GOTO 1000
3220 IF L=28 AND O(19)=0 THEN L=30:GOTO 1000
3225 IF L=29 AND O(19)=0 THEN L=31:GOTO 1000
3230 IF L=28 OR L=29 THEN PRINT"Need snorkel.":GOTO 1190
3240 GOTO 1990
3250 IF L=13 THEN C1=1:GOTO 1000
3255 IF L=21 THEN C1=1:GOTO 1000
3260 IF L=14 THEN C2=1:GOTO 1000
3265 IF L=24 THEN C2=1:GOTO 1000
3270 IF L=17 THEN C3=1:GOTO 1000
3275 IF L=26 THEN C3=1:GOTO 1000
3280 IF L=18 THEN C4=1:GOTO 1000
3285 IF L=27 THEN C4=1:GOTO 1000
3290 GOTO 1990
3295 IF L=16 OR L=20 THEN 3305
3300 GOTO 1990
3305 IF L=16 AND K=0 THEN PRINT"Can't, door is locked from other side.":GOTO 1190
3310 PRINT"O.K.":GOTO 1190
3350 IF L<>5 THEN PRINT"Not here.":GOTO 1190
3360 IF R=1 THEN PRINT"Already inflated.":GOTO 1190
3370 PRINT"O.K.":R=1:GOTO 1190
3380 IF L<>8 THEN PRINT"Not here.":GOTO 1190
3390 FOR X=1 TO 6
3395 IF O(X)=0 OR O(X)=8 THEN HB=HB+1
3400 NEXT X
3405 IF HB=6 THEN 3420
3410 PRINT"Not ready.":HB=0:GOTO 1190
3420 FOR X=1 TO 6
3425 IF O(X)=0 THEN I=I-1
3430 O(X)=40
3440 NEXT X
3450 H=1:GOTO 1000
3460 IF H=0 THEN PRINT"Not ready.":GOTO 1190
3470 IF L=8 OR L=36 THEN PRINT"Need to get in first.":GOTO 1190
3480 IF L=34 THEN 3500
3485 IF L=35 THEN 3570
3490 GOTO 1990
3500 CLS:Y=0:FOR H=6 TO -6 STEP -1:Z=ABS(H):Y=Y+2:GOSUB 6420:NEXT H
3550 CLS
3560 L=35:GOTO 1000
3570 CLS:Y=29:FOR H=6 TO -6 STEP -1:Z=ABS(H):Y=Y-2:GOSUB 6420:NEXT H
3620 CLS
3630 L=34:GOTO 1000
3640 IF O(19)<>0 THEN PRINT"Don't have.":GOTO 1190
3650 IF L>27 AND L<32 THEN PRINT"You quickly grab it back.":GOTO 1190
3660 O(19)=L:I=I-1:GOTO 1000
3800 IF L<>2 THEN 1990
3810 COLOR 23,0:PRINT"You fell off.":FOR X=1 TO 3000:NEXT X:COLOR 7,0:S=1:L=11:GOTO 1000
3900 IF O(9)=0 OR O(9)=L THEN PRINT"Sign says:An appropriate place.":GOTO 1190
3910 PRINT"Can't find.":GOTO 1190
3950 IF O(19)=0 THEN 3080
3960 PRINT"Need snorkel.":GOTO 1190
5010 IF D$(X,L)="-"THEN RETURN
5020 IF D$(X,L)="o"THEN PRINT"Out":RETURN
5030 IF D$(X,L)="n"THEN PRINT"North":RETURN
5040 IF D$(X,L)="e"THEN PRINT"East":RETURN
5050 IF D$(X,L)="s"THEN PRINT"South":RETURN
5060 IF D$(X,L)="w"THEN PRINT"West":RETURN
5070 IF D$(X,L)="u"THEN PRINT"Up":RETURN
5080 IF D$(X,L)="d"THEN PRINT"Down":RETURN
5210 IF O(13)=0 AND L=31 THEN PRINT"an underwater tunnel":RETURN
5220 IF L=13 OR L=14 OR L=17 OR L=18 THEN PRINT"vent"
5230 IF L=13 AND C1=1 THEN PRINT"vent cover":RETURN
5240 IF L=14 AND C2=1 THEN PRINT"vent cover":RETURN
5250 IF L=17 AND C3=1 THEN PRINT"vent cover":RETURN
5260 IF L=18 AND C4=1 THEN PRINT"vent cover":RETURN
5270 IF H=1 AND L=8 OR L=36 THEN PRINT"hot air balloon":RETURN
5280 Z=INT(10*RND(1))+1
5290 IF L=6 AND Z=1 THEN PRINT"the butler with two sticks of dynamite":RETURN
5300 IF L=3 AND Z=3 THEN PRINT"the maid with a pack of bloodhounds":RETURN
5310 IF L=7 AND Z=5 THEN PRINT"the gardener with a bulldozer":RETURN
5320 IF L=33 AND Z<5 THEN PRINT"A bat passes close by.":RETURN
5330 IF L=27 AND Z<3 THEN PRINT"You have cobwebs in your hair.":RETURN
5340 IF L=25 AND Z<3 THEN PRINT"A rodent brushes your leg.":RETURN
5350 IF L=4 AND Z=7 THEN PRINT"A toad jumps across the creek.":RETURN
5360 IF L=8 AND O(14)=0 AND Z<5 THEN PRINT"A hungry gull circles overhead.":RETURN
5370 IF L=2 AND Z=6 THEN PRINT"A primate watches from above.":RETURN
5390 RETURN
6000 IF C$="get well"THEN S=0:PRINT"Recovered.":GOTO 1190
6020 GOTO 1990
6100 IF O(19)=0 THEN PRINT"Already have object.":GOTO 1190
6115 IF O(19)=40 AND O(7)=0 THEN O(19)=0:I=I+1:GOTO 1000
6120 IF O(19)=40 AND O(7)=L THEN O(19)=0:I=I+1:GOTO 1000
6130 IF O(19)=L THEN O(19)=0:I=I+1:GOTO 1000
6140 GOTO 1990
6150 IF O(13)=0 THEN PRINT"Already have.":GOTO 1190
6160 IF O(13)=40 AND L=14 THEN O(13)=0:I=I+1:GOTO 1000
6170 IF O(13)=L THEN O(13)=0:I=I+1:GOTO 1000
6180 GOTO 1990
6210 COLOR 23,1:PRINT"You have just enough strength to get away.":FOR X=1 TO 3000:NEXT X:COLOR 7,0:S=1:L=11:GOTO 1000
6300 IF V=0 THEN PRINT"Serval won't let you.":GOTO 1190
6310 L=19:GOTO 1000
6420 LOCATE Z+1,Y:PRINT SPC(10):LOCATE Z+2,Y:PRINT"   ���Ŀ   "
6425 LOCATE Z+3,Y-1:PRINT"   /     \   "
6430 LOCATE Z+4,Y-1:PRINT"   �������   "
6440 LOCATE Z+5,Y-1:PRINT"   \     /   "
6445 LOCATE Z+6,Y:PRINT"   \   /   "
6450 LOCATE Z+7,Y+1:PRINT"   �Ĵ   "
6455 LOCATE Z+8,Y+1:PRINT"     "
6460 LOCATE Z+9,Y+1:PRINT"   �ķ   "
6470 LOCATE Z+10,Y+1:PRINT"   ***   "
6480 LOCATE Z+11,Y+1:PRINT"   �Ľ   ":LOCATE Z+12,Y-1:PRINT"            "
6490 RETURN
6500 FOR X=1 TO 20:PLAY"o6d64":FOR Y=1 TO 50:NEXT Y:NEXT X
6510 RETURN
6550 CLS
6560 PRINT:PRINT TAB(5)"How to build a hot air balloon"
6570 PRINT:PRINT TAB(8)"#1 Balloon"
6575 PRINT TAB(8)"#2 Heat source"
6580 PRINT TAB(8)"#3 Fuel"
6585 PRINT TAB(8)"#4 Gondola or container"
6590 PRINT TAB(8)"#5 Cable or twine"
6595 PRINT TAB(8)"#6 Matches or lighter"
6600 PRINT "Build at an appropriate place."
6605 LOCATE 25,8:PRINT"Press return to continue";:C$=INPUT$(1)
6620 GOTO 1000
7000 IF E=0 THEN PRINT"Can't find.":GOTO 1190
7010 IF E=0 THEN PRINT"Can't find.":GOTO 1190
7030 IF L<>16 THEN PRINT"Not here.":GOTO 1190
7040 PRINT"Combination lock"
7050 INPUT"Enter first number--",F(1)
7055 GOSUB 6500
7060 IF F(1)<>S(1)THEN PRINT"Not correct":GOTO 1190
7070 INPUT"Enter second number--",F(2)
7075 GOSUB 6500
7080 IF F(2)<>S(2)THEN PRINT"Not correct":GOTO 1190
7090 INPUT"Enter last number--",F(3)
7095 GOSUB 6500
7100 IF F(3)<>S(3)THEN PRINT"Not correct":GOTO 1190
7105 F=1
7110 PRINT"Click!.................Inside is a will":GOTO 1190
7200 CLS
7210 LOCATE 6,1
7225 PRINT SPC(37);
7230 PRINT"                WILL"
7235 PRINT SPC(37);
7240 PRINT" I,Mr. Stone,leave all my worldly"
7245 PRINT"possesions to whomever opens this"
7250 PRINT"safe.
7255 PRINT SPC(37)
7265 PRINT"         <<<Congratulations>>>":KEY ON:end
7500 CLS:PRINT
7510 PRINT"Welcome to Stoneville. You have recently"
7520 PRINT"learned that wealthy Mr. Stone died and"
7530 PRINT"rumor has it that this eccentric miser"
7540 PRINT"has left his entire estate to whomever"
7550 PRINT"finds and opens his safe.":PRINT
7570 PRINT"To play,you must manipulate objects and"
7580 PRINT"explore your surroundings by using two"
7590 PRINT"word commands.For example,'get basket'"
7600 PRINT"or 'go south'.To speed up directional"
7605 PRINT"movement, 'go' commands may be shortened"
7610 PRINT"to include one letter such as 'go s'.":PRINT
7615 PRINT"The command'save game' will preserve"
7620 PRINT"your progress for play at a later time"
7625 PRINT"or if you prefer to just end the game"
7630 PRINT"then enter 'end game'.And,if needed"
7640 PRINT"'clear screen'will reset your location."
7670 LOCATE 25,8:PRINT"Press any key to continue";:C$=INPUT$(1):RETURN
8000 DATA balloon,fallen weather balloon,3,stove,small wood burning stove,1,basket,large wicker basket,12
8010 DATA logs,logs,40,twine,roll of twine,17,matches,book of matches,15
8020 DATA bag,burlap bag,18,raft,inflatable raft,1,sign,sign,8
8030 DATA net,fish net,7,shoes,jogging shoes,10,axe,axe,10
8040 DATA mask,swim mask,40,trout,trout,29,goblet,crystal goblet,19
8050 DATA bottle,empty bottle of chablis,33,book,book,14,picture,picture of Mr.  Stone,16
8060 DATA snorkel,snorkel,40,manor,Stoneville Manor,9,manor,Stoneville Manor,1
8070 DATA sack,old abandoned shack,36,table,wooden table,37,credenza,wooden credenza,14
8080 DATA safe,safe,40,case,case of chablis,18,trees,trees,2
8090 DATA door,door,20,door,door,16,serval,an imported serval,18
8100 DATA store,general store,9,stairway,stairway,19,hospital,hospital,9
8110 DATA in the courtyard,in a wooded area,in a meadow,along a slippery creek,on the bank of a lake,in a barren field,on a rocky trail
8120 DATA at the edge of a gorge,on main street,inside the general store,inside the hospital,in the foyer,in the parlour
8130 DATA in the study,in an oriel,in the gallery,in the atrium,in the west wing of the wine cellar,in the east wing of the wine cellar
8140 DATA at top of an air way,at an outlet in the duct,at a turn in the duct,at a fork in the duct,at an outlet in the duct,at a turn in the duct
8150 DATA at an outlet in the duct,at an outlet in the duct,on the lake,in the southern bay,under the surface of the lake,under the surface of the lake
8160 DATA along an underground river,inside a cavern,in a hot air balloon,in a hot air balloon,on top of a plateau,inside the shack
8170 DATA w,2,s,4,"-",0,e,1,s,3,n,9,n,2,e,4,"-",0,w,3,e,5,n,1,w,4,"-",0,"-",0,s,9,e,7,"-",0,w,6,e,8,"-",0
8180 DATA w,7,"-",0,"-",0,s,2,n,6,"-",0,o,9,"-",0,"-",0,o,9,"-",0,"-",0,o,9,s,13,"-",0,n,12,e,14,s,17,w,13,e,15,s,16
8190 DATA w,14,"-",0,"-",0,n,14,w,17,"-",0,o,1,n,13,e,16,e,19,"-",0,"-",0,w,18,u,20,"-",0,d,19,"-",0,"-",0,o,13,s,22,"-",0,n,21,e,23,"-",0
8200 DATA w,22,n,24,s,25,o,14,s,23,"-",0,n,23,w,26,"-",0,o,17,d,27,e,25,o,18,u,26,"-",0,o,5,s,29,"-",0,n,28,"-",0,"-",0
8210 DATA u,28,s,31,"-",0,u,29,n,30,"-",0,e,31,w,33,"-",0,e,32,"-",0,"-",0,o,8,"-",0,"-",0,o,36,"-",0,"-",0,"-",0,"-",0,"-",0,o,36,"-",0,"-",0
8220 DATA Vent is covered.,Something is too big.,Inside is a note with the number.
8230 DATA 13,14,17,18,21,24,26,27
8300 OPEN"i",1,"game"
8340 INPUT#1,L,W,S,I,F,H,R,K,E,V,C1,C2,C3,C4,N$(1),N$(2),N$(3),S$(1),S$(2),S$(3)
8370 FOR X=1 TO 33:INPUT#1,O(X):NEXT X
8395 CLOSE#1:RETURN
8400 OPEN"o",1,"game"
8435 WRITE#1,L,W,S,I,F,H,R,K,E,V,C1,C2,C3,C4,N$(1),N$(2),N$(3),S$(1),S$(2),S$(3)
8470 FOR X=1 TO 33:WRITE#1,O(X):NEXT X
8495 CLOSE#1:CLS:KEY ON:end
