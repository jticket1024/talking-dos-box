0 REM 02-02-85 <-- last backup
10 PRINT"Eric Gans"
20 PRINT"French Department UCLA"
30 PRINT"Los Angeles, CA 90024"
40 PRINT"02/02/85"
50 REM creates doubly linked list (alphabetical)
60 PRINT TAB(35)"LINK"
70 PRINT:PRINT"     This program creates a doubly linked list that will alphabetize"
80 PRINT" a list of words and then print them both backwards and forwards.":PRINT
90 LINE INPUT " Max # of words (>= 20 <CR> quits)";NW
100 IF NW>20 THEN NW=20
110 IF NW<1 THEN 90
120 PRINT
130 DIM A$(20),P(20),Q(20)
140 P(0)=1:Q(0)=1
150 P(K)=0:Q(K)=0
160 Z=0:ZZ=0
170 FOR K=1 TO NW
180 PRINT "Word"K;:INPUT;A$(K):PRINT
190 IF A$(K)="" THEN K=K-1:GOTO 350
200 J=0:JJ=0
210 FOR I=1 TO K
220 IF A$(K)<A$(P(J)) THEN P(K)=P(J):P(J)=K:GOTO 270
230 J=P(J)
240 IF P(J)=0 THEN Z=J
250 NEXT
260 P(K)=0:P(Z)=K
270 FOR I=1 TO K
280 IF A$(K)>A$(Q(JJ)) THEN Q(K)=Q(JJ):Q(JJ)=K:GOTO 330
290 JJ=Q(JJ)
300 IF Q(JJ)=0 THEN ZZ=JJ
310 NEXT
320 Q(K)=0:Q(ZZ)=K
330 NEXT K
340 PRINT
350 X=0
360 FOR I=1 TO K
370 PRINT A$(P(X));" ";
380 X=P(X)
390 NEXT I
400 PRINT
410 PRINT
420 X=0
430 FOR I=1 TO K
440 PRINT A$(Q(X));" ";
450 X=Q(X)
460 NEXT I
 ";
380 X=P(X)
390 NEXT I
400 PRINT
410 PRINT
420 X=0
430 FOR I=1 TO K
440 PRINT A$(Q(X));" ";
450 X=Q(X)
460 NE