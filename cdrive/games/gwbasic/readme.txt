Hi, and welcome to super simon written by Graham Pearce. After unzipping simon.zip, you should have the following files:
readme.txt: You're reading it!
Simon.exe: The file needed to run the simon game.
Simon.bas: The basic source code for simon.
simon.src: The source code for the fs notetaker version of super simon. It will only run on a braille lite 18, bns, tns or type lite. It has not been fully tested yet, so f you do try this version, tell me how it goes!


Version History

1.3: Fixed some bugs about the number of mistakes allowed.
Super simon will now work properly if it is playing when midnight strikes.
A number of little bugs corrected.

1.2: actually gave the game a proper title. grin.
modified the introduction to the game, so you can modify the speed, pause ratio, etcetera, without having to answer the other questions.
added the ability to save and restore configuration files.
added the acceleration rate, which determines how quickly, if at all, the game speeds up.
fixed a bug which occurred when you didn't complete any tones and the simon.rec file had already been created.
1.1: Added the pause ratio, which determines how long the game should pause between rounds.
1.0.1: Fixed major bug involving the simon.rec file! Thanks kwork for pointing this out!
1.0: First release of program.

You may freely distribute the simon game as long as any charge for it is only to cover the cost of the media it is produced on. Ask me first if you want to distribute this as part of a commercial package. The only other thing I ask of you is that if you make any modifications to the source, send your modified source code , so I can incorporate any of your ideas into future versions of simon. Also, if you can successfully port simon to any other platform (especially to the braille companion), please let me know.

Any comments, suggestions, bug reports and so forth should be sent to:
lpp@vianet.net.au

Enjoy!