50 RANDOMIZE TIMER
100 DIM P(4,2)   
110 PRINT "THIS IS THE GAME OF HIDE AND SEEK."   
120 PRINT
130 PRINT "THE OBJECT OF THE GAME IS TO FIND THE FOUR PLAYERS "  
140 PRINT "WHO ARE HIDDEN ON A 10 BY 10 GRID."   
150 PRINT
160 PRINT "HOMEBASE WILL BE THE POSITION AT (0,0) AND ANY GUESS "
170 PRINT "YOU MAKE SHOULD CONTAIN TWO NUMBERS.  THE FIRST GIVES"
180 PRINT "THE UNIT DISTANCE RIGHT OF THE HOMEBASE AND THE SECOND"   
190 PRINT "IS THE UNIT DISTANCE ABOVE HOMEBASE."
200 PRINT   
210 PRINT "YOU WILL HAVE TEN ATTEMPTS TO LOCATE THESE PLAYERS"  
220 PRINT "AND WILL BE TOLD HOW CLOSE YOUR GUESS IS"
230 PRINT "TO EACH PLAYER." 
240 PRINT   
250 PRINT "IF AFTER 10 TRIES YOU ARE UNABLE TO CARRY OUT THIS TASK" 
260 PRINT "YOU MAY CONTINUE TO BE 'IT', BUT THE PLAYERS WILL"   
270 PRINT "BE PERMITTED TO MOVE TO NEW LOCATIONS."  
280 PRINT   
290 PRINT   
300 PRINT "ARE YOU READY TO BEGIN (YES OR NO)"  
310 INPUT A1$
320 IF A1$="NO" GOTO 710 
330 GOSUB 730  
340 T=0 
350 T=T+1   
360 PRINT   
370 PRINT   
380 PRINT "TURN NUMBER";T;", WHAT IS YOUR GUESS?"   
390 INPUT M,N   
400 FOR I=1 TO 4
410 IF P(I,1)=-1 GOTO 490   
420 IF P(I,1)<>M GOTO 470   
430 IF P(I,2)<>N GOTO 470  
440 P(I,1)=-1   
450 PRINT "YOU HAVE FOUND PLAYER";I 
460 GOTO 490
470 D=SQR((P(I,1)-M)^2+(P(I,2)-N)^2)
480 PRINT "YOUR DISTANCE FROM PLAYER";I;"IS";INT(D*100)/100;"UNIT(S)."  
490 NEXT I  
500 FOR J=1 TO 4
510 IF P(J,1)<>-1 GOTO 560  
520 NEXT J  
530 PRINT   
540 PRINT "YOU HAVE FOUND ALL THE PLAYERS IN ";T;" TURNS!"  
550 GOTO 670
560 IF T<10 GOTO 350   
570 PRINT   
580 PRINT "YOU DIDN'T FIND ALL OF THE PLAYERS IN TEN TRIES."
590 PRINT " DO YOU WANT TO KNOW WHERE THE PLAYERS YOU DID NOT"  
600 PRINT "FIND WERE HIDDEN (YES OR NO)"
610 INPUT B1$
620 IF B1$="NO" GOTO 670 
630 FOR I=1 TO 4
640 IF P(I,1)=-1 GOTO 660   
650 PRINT "PLAYER";I;"HID AT (";P(I,1);",";P(I,2);")."  
660 NEXT I  
670 PRINT   
680 PRINT "DO YOU WANT TO PLAY AGAIN (YES OR NO)"   
690 INPUT C1$
700 IF C1$="YES" GOTO 330
710 PRINT "THEN PLEASE LOGOUT." 
720 GOTO 790   
730 FOR J=1 TO 2   
740 FOR I=1 TO 4   
750 P(I,J)=INT(RND(1)*10)  
760 NEXT I 
770 NEXT J 
780 RETURN 
790 END
