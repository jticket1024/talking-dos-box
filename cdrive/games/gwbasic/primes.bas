100 CLS: KEY OFF: DEFINT A-Z
110 INPUT "Primes up to: ",N
115 CLS: DIM M(N): MASK$=STRING$(LEN(STR$(N)),"#")
120 T1$=TIME$    'start timing
130 FOR C=2 TO N
140    IF M(C) THEN 190
150    PRINT USING MASK$; C;
160    FOR L=C*2 TO N STEP C
170        M(L)=1 'tag array location
180    NEXT L
190 NEXT C
200 T2$=TIME$    'stop timing
205 PRINT: PRINT
210 T1#=3600*VAL(MID$(T1$,1,2))+60*VAL(MID$(T1$,4,2))+VAL(MID$(T1$,7,2))
220 T2#=3600*VAL(MID$(T2$,1,2))+60*VAL(MID$(T2$,4,2))+VAL(MID$(T2$,7,2))
230 PRINT "Calculations took ";T2#-T1#;" seconds."
240 END
                                                                    