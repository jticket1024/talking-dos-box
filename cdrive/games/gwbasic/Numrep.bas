0 REM 10-25-84 <-- last backup
10 cls
20 PRINT"Eric Gans"
30 PRINT"French Department UCLA
40 PRINT"Los Angeles, CA 90024"
50 PRINT
60 REM deciphers BASIC number representation
70 PRINT TAB(30)"NUMREP":PRINT
80 PRINT"This program displays MBasic's representation of numbers in integer,"
90 PRINT"single or double precision.  The memory bytes are displayed in binary;"
100 PRINT"the decimal number displayed below is calculated from the memory, and"
110 PRINT"should be (approximately) identical to the first.":PRINT
120 PRINT"Integer,Single or Double, Q to quit (i/s/d/q)?"
130 E$=INPUT$(1)
140 IF E$="q" THEN END
150 IF E$="s" THEN 280 ELSE IF E$="d" THEN 420 ELSE IF E$<>"i" THEN 130
160 REM integer
170 DEFINT A
180 INPUT "Enter an integer (<CR> quits) >>",A
190 IF A=0 GOTO 120
200 FOR I=1 TO 2
210 N(I)=PEEK(VARPTR(A)+I-1)
220 N=N(I):GOSUB 590
230 NEXT
240 PRINT
250 NB=N(1)+((N(2) AND 127)-(N(2) AND 128))*2^8
260 PRINT NB
270 GOTO 180
280 REM single precision
290 DEFSNG A
300 INPUT "Enter a number (<CR> quits) >>",A
310 IF A=0 GOTO 120
320 FOR I=1 TO 4
330 N(I)=PEEK(VARPTR(A)+I-1)
340 N=N(I):GOSUB 590
350 NEXT
360 PRINT
370 EX=N(4)-129
380 NB=2^EX*(1+2^(-7)*(N(3) AND 127)+2^(-15)*N(2)+2^(-31)*N(1))
390 IF (N(3) AND 128) THEN NB=-NB
400 PRINT NB
410 GOTO 300
420 REM double precision
430 DEFDBL A,D
440 INPUT "Enter a number (<CR> quits) >>",A
450 IF A=0 GOTO 120
460 FOR I=1 TO 8
470 N(I)=PEEK(VARPTR(A)+I-1)
480 N=N(I):GOSUB 590
490 NEXT
500 PRINT
510 EX=N(8)-129
520 DNB=2^EX*(1+(N(7) AND 127)*2^(-7))
530 FOR I=6 TO 1 STEP-1
540 DNB=DNB+N(I)*(2^(EX-(8*(8-I)-1)))
550 NEXT
560 IF (N(7) AND 128) THEN DNB=-DNB
570 PRINT DNB
580 GOTO 440
590 REM binary
600 B=128
610 WHILE B>=1
620 IF (N AND B) THEN PRINT"1"; ELSE PRINT"0";
630 B=B/2
640 WEND
650 PRINT" ";
660 RETURN
ry
600 B=128
610 WHILE B>=1
620 IF (N AND B) THEN PRINT"1