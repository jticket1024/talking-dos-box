100 print "hi and welcome to trivia 1.1."
102 print "program written by munawar bijani."
105 print "this challenge consists of ten questions, just like v1.0, except"
106 print "that the questions are different."
107 print "you still have to fill out the answers."
108 print "press y to continue, n to exit."
109 i$=inkey$:if i$="" then 109
110 if i$="y" or i$="Y" then goto 114
111 if i$="n" or i$="N" then goto 260
112 goto 108
114 cls:print "question 11."
115 print "the person who sings oops i did it again is:"
116 input n$
117 if n$="britney spears" or n$="BRITENY SPEARS" then goto 120
118 goto 269
120print "correct"
121 print "press y for next question, n to exit."
122 i$=inkey$:if i$="" then 122
123 if i$="y" or i$="Y" then goto 127
124 if i$="n" or i$="N" then goto 260
125 goto 121
127 cls:print "question 12."
128 print "the accessible game, accessible star fight can be downloaded"
129 print "from what website?"
130 input n$
131 if right$(n$,24)="www.gamesfortheblind.com" or right$(n$,24)="WWW.GAMESFORTHEBLIND.COM" THEN 136
133 if n$="games for the blind" or n$="GAMES FOR THE BLIND"  then goto 136
134 goto 269
136print "correct"
137 print "press y for next question, n to quit."
138 i$=inkey$:if i$="" then 138
139 if i$="y" or i$="Y" then goto 143
140 if i$="n" or i$="N" then goto 260
141 goto 137
143 cls:print "question 13."
144 print "in the screen reader name, jaws for windows,"
145 print "jaws stands for:"
146 input n$
147 if n$="job access with speech" or n$="JOB ACCESS WITH SPEECH" then 150
148 goto 269
150print "correct"
151 print "press y for next question, n to quit."
152 i$=inkey$:if i$="" then 152
153 if i$="y" or i$="Y" then goto 157
154 if i$="n" or i$="N" then goto 260
155 goto 151
157 cls:print "question 14."
158 print "in jaws, to read the title bar,"
159 print "what keys do you press? use plus signs where necessary."
160 input n$
161 if n$="insert+t" or n$="INSERT+T" then goto 164
162 goto 269
164print "correct"
165 print "press y for next question, n to quit."
166 i$=inkey$:if i$="" then 166
167 if i$="y" or i$="Y" then goto 171
168 if i$="n" or i$="N" then goto 260
169 goto 165
171 cls:print "question 15."
172 print "in jaws to go to the verbosity dialog in an app, you press:"
173 input n$
174 if n$="insert+v" or n$="INSERT+V" then goto 177
175 goto 269
177print "correct"
178 print "press y for next question, n to quit."
179 i$=inkey$:if i$="" then 179
180 if i$="y" or i$="Y" then goto 184
181 if i$="n" or i$="N" then goto 260
182 goto 178
184 cls:print "question 16."
185 print "what is the keystroke in jaws that"
186 print "moves to the address bar in IE?"
187 input n$
188 if n$="insert+a"or n$="INSERT+A" then goto 191
189 goto 269
191print "correct"
192 print "press y for next question, n to quit."
193 i$=inkey$:if i$="" then 193
194 if i$="y" or i$="Y" then goto 198
195 if i$="n" or i$="N" then goto 260
196 goto 192
198 cls:print "question 17."
199 print "to get into the parameters menu in a blazie/freedom scientific notetaker,"
200 print "you press:"
201 input n$
202 if n$="p chord" or n$="P CHORD"then goto 207
203 if n$="p-chord"  or n$="P-CHORD" then goto 207
204 if n$="chord p" or n$="CHORD P" then goto 207
205 goto 269
207print "correct"
208 print "press y for next question, n to quit."
209 i$=inkey$:if i$="" then 209
210 if i$="y" or i$="Y" then goto 214
211 if i$="n" or i$="N" then goto 260
212 goto 208
214 cls:print "question 18."
215 print "to get into the jaws window from any app,"
216 print "what key do you press?"
217 input n$
218 if n$="insert+j"  or n$="INSERT+J" then goto 221
219 goto 269
221print "correct"
222 print "press y for next question, n to quit."
223 i$=inkey$:if i$="" then 223
224 if i$="y" or i$="Y" then goto 228
225 if i$="n" or i$="N" then goto 260
226 goto 222
228 cls:print "question 19."
229 print "what is the file name of this game after you unzip it?"
230 input n$
231 if n$="trivia11.bas" or n$="TRIVIA11.BAS" then goto 234
232 goto 269
234print "correct"
235 print "press y for last question, n to quit."
236 i$=inkey$:if i$="" then 236
237 if i$="y" or i$="Y" then goto 241
238 if i$="n" or i$="N" then goto 260
239 goto 235
241 cls:print "final question."
242 print "when henter-joyce was developed,"
243 print "one of the people who started it was ted henter."
244 print "what was the name of"
245 print "ted's partner?"
246 print "clue: the company was named henter-joyce,"
247 print "because ted henter was one of the people who made it."
248 print "his partner's last name was joyce, what was ted's partner's"
249 print "name?"
250 input n$
251 if n$="bill joyce" or n$="BILL JOYCE" then goto 255
252 goto 269
255 print "congratulations, you've beat round 2 of my trivia challenge,"
256 print "(v1.1.)"
257 print "check my site for round 3, (v1.2.)"
258 goto 260
260 print "thanks for playing round 2 of trivia challenge."
261 print "if you have any comments' or questions"
262 print "contact me."
263 print "e-mail: mun0009@cfl.rr.com"
265 print "website: http://www.geocities.com/mun00092"
266 end
269 print "sorry, you lose."
270 print "thanks for trying."
271 end