100 print "hi and welcome to my unending adventure program."
101 print "program written by munawar bijani"
102 print "on friday, may 25, 2001."
103 print "press y to continue, n to exit."
104 i$=inkey$:if i$="" then  104
105 if i$ = "y" then cls:goto  108
106 if i$ = "n" then cls:goto  201
108 print "you wake up to find yourself in a cement, cold,"
109 print "dark room."
110 print "you start walking, and you find yourself in a hallway."
111 print "there is a door to your left, and a door to your right. the hallway has ended."
112 print "which door do you take, press r for right, or l for left."
113 i$=inkey$:if i$="" then  113
114 if i$ = "r" then cls:goto  118
115 if i$ = "l" then cls:goto  127
118 print "you open the door to the right, and a cold, hard, foul smelling wind blows out!"
119 print "you fall back against the left door, which does not open."
120 print "then, finally the wind stops, and you can see again."
121 print "what do you do next? press w to walk into the right doorway, or l to open the left door."
122 i$=inkey$:if i$="" then  122
123 if i$ = "w" then cls:goto  136
124 if i$ = "l" then cls:goto  127
127 print "you open the left door."
128 print "but you find yourself falling... falling... falling..."
129 print "finally, you hit the bottom."
130 print "you are now in a dark pit."
131 print "you find a trap door on the floor. do you open it? press y to open, or n to wait."
132 i$=inkey$:if i$="" then  132
133 if i$ = "y" then cls:goto  151
134 if i$ = "n" then cls:goto  193
136 print "you walk in the right door, and..."
137 print "you get attacked!!"
138 print "he punches you and kicks you horribly in your stomach, chest and head."
139 print:print
140 print "when you wake up, your bleeding and aching."
141 print "you can't move, and your losing blood fast!"
142 print "now your feeling cold,  because of all the lost blood."
143 print "then, you open your eyes to a bright, sunny day! you look at your clock, 12:40 a.m."
144 print "all a dream!"
145 print "play again? press y or n"
146 i$=inkey$:if i$="" then  146
147 if i$ = "y" then cls:goto  100
148 if i$ = "n" then cls:goto  201
151 print "you open the trapdoor."
152 print "the whole floor comes up!"
153 print "then, someone jumps out at you, and starts yelling!"
154 print "what are you doing here?"
155 print "the man takes out a knife, and stabs you in your heart!"
156 print "you kick him, but your losing energy fast!"
157 print "then cls:goto  you find a bottle on the ground, do you open it? press y to open, or n to keep fighting."
158 i$=inkey$:if i$="" then  158
159 if i$ = "y" then cls:goto  163
160 if i$ = "n" then cls:goto  170
163 print "you open the bottle, and he disappears!"
164 print "your healed now, and you find yourself in a nice, comfy bed, it 12:40 p.m."
165 print "its over! press y to play again, or n to quit"
166 i$=inkey$:if i$="" then  166
167 if i$ = "y" then cls:goto  100
168 if i$ = "n" then cls:goto  201
170 print "you get hit!"
171 print "the bottle falls out of your hand, and disappears."
172 print "you fight him. you do a bicycle kick!"
174 print "you do your kick."
177 print "bicycle kick"
179 print "his energy is down to 10 out of 20."
180 print "body slam, kick, brutal attack."
182 print "your energy is down to 10 out of 30."
183 print "round house kick, combined with air attack."
185 print "his energy is down to 0 out of 20."
186 print "you win!"
187 print "you find yourself awake in bed, sweating. it is 12:40 p.m."
188 print "its over, press y to play again, or n to quit."
189 i$=inkey$:if i$="" then  189
190 if i$ = "y" then cls:goto  100
191 if i$ = "n" then cls:goto  201
193 print "you wait, wait, and wait."
194 print "nothing happens."
195 print "you find yourself in bed. its 12:40 p.m."
196 print "its over. play again? press y or n"
197 i$=inkey$:if i$="" then  197
198 if i$ = "y" then cls:goto  100
199 if i$ = "n" then cls:goto  201
201 print "thanks for playing!"
202 print "if you have any comments, suggestions, or questions"
203 print "feel free to contact me."
204 print "e-mail: mun0009@cfl.rr.com"