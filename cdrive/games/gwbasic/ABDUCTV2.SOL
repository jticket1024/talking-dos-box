This walk-through is designed to lay out the whole game like a 
story 

Optional commands that are not required to win but help flesh out 
the narrative
are enclosed in square brackets [] 

N N N 
[X BOOKS]
W
[X CLOTHES X BED MAKE BED]
GET ALL
[X PHOTO X PICTURES]
E S 
[TALK TO MAYBEL]
S S S SE Z Z Z Z
[Z]
SAY "YELLOW" SAY "RED" SAY "BLUE" SAY "SPHERE" SAY "PYRAMID"
SAY "CUBE" Z UNRANDOM PUSH GREEN BUTTON OUT N NW 
GET ROPE GET BUCKET, HOE SE N N
[TALK TO DAD ASK DAD ABOUT MOM W X CYLINDER E]
ASK DAD ABOUT CYLINDER
[ASK DAD ABOUT MOM ASK DAD ABOUT POP]
[ASK DAD ABOUT     ]
[SEARCH FOR CLICK] (The 10 points you get for this are above and 
beyond the
regular points   You will win with more than the max )
TOUCH HEAD
[PULL WIRE]
W 
TYPE XXXX ON PAD (where XXXX is the code that dad gave you)
LOOK THROUGH WINDOW E N GET IN CAR DRIVE CAR GET OUT
[X POPS]
S GIVE HOE TO POP
[ASK POP ABOUT DUCK ASK POP ABOUT DAD]
[ASK POP ABOUT     ]
N
[X LIBRARY N]
GET IN CAR DRIVE CAR GET OUT S ASK DAD ABOUT LIBRARY S S S S SW
[X CONTRAPTION X CYLINDER X DUCK X WINDOW X AXE]
GET DUCK NE
[X CONTRAPTION X CYLINDER X AXE]
POUR WATER IN CYLINDER GET DUCK NE N N N N N GET IN CAR DRIVE CAR
GET OUT S GIVE DUCK TO POP N N X DESK X DESK X DESK GET ARTICLES
[READ ARTICLES]
S GET IN CAR DRIVE CAR GET OUT SW
[X GARDEN]
HOE GARDEN GET CLOD NE S GIVE ARTICLES TO DAD 
TELL DAD ABOUT CYLINDER W E S S S Z Z
S SW PUT CLOD IN CYLINDER PRY AXE WITH HOE GET AXE NE N
GIVE AXE TO DAD Z ATTACK ALIEN WITH HOE 
Z [OR L]
Z
[X ME]
PULL WIRE
[X ME]
GET RECENT PICTURES

For which puzzle would you like a hint?  (Type 0 to exit)
 1   What should I do in the opening scene?
 2   General Information
 3   Help for Beginners
 4   Message for Advanced Players
 5   About this Game
#  0

>unscript