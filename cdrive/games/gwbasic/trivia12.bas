100 print "welcome to trivia round 3!"
101 print "this is a multiple choice trivia, unlike the other"
102 print "two rounds."
103 print "program written by munawar"
104 print "bijani on 06/23/01."
105 print "press y to continue, n to exit."
106 I$=INKEY$:IF I$="" THEN 106
107 if i$="y" or i$="Y" then 111
108 if i$="n" or i$="N" then 344
109 goto 100
111 cls:print "question 21."
112 print "to GET to the desktop on a pc with jaws,"
113 print "you press:"
114 print "a: insert+d."
115 print "b: insert+f5."
116 print "c: insert+f6."
117 print "d: insert+f7."
118 print "press letter of choice, or n to repeat question."
119I$=INKEY$:IF I$="" THEN 119
122 if i$="c" or i$="C" then 127
124 if i$="n" or i$="N" then 111 else 343
127 print "correct"
128 print "press y for next question, n to quit"
129 I$=INKEY$:IF I$="" THEN 129
130 if i$="y" or i$="Y" then 134
131 if i$="n" or i$="N" then 344
132 goto 128
134 cls:print "question 22."
135 print "a braille lite 2000 can hold up to how many files in ram?"
136 print "a: 99."
137 print "b: 127."
138 print "c: 125."
139 print "d: 126."
140 print "press the letter of choice or n to repeat."
141 I$=INKEY$:IF I$=""THEN 141
142 if i$="n" or i$="N" THEN 134
143 if i$="b" or i$="B" then 148 else goto 343
148 print "correct"
149 print "press y for next question n to exit."
150 I$=INKEY$:IF I$="" THEN 150
151 if i$="y" or i$="Y" then 155
152 if i$="n" or i$="N" then 344
153 goto 149
155 cls:print "question 23."
156 print "to GET into the jaws window in jfw, you press:"
157 print "a: insert+f."
158 print "b: insert+j."
159 print "c: insert+w."
160 print "d: insert+s."
161 print "press letter of choice or n to repeat."
162 I$=INKEY$:IF I$="" THEN 162
164 if i$="b" or i$="B" then 169
165 if i$="n" or i$="N" then 155 else 343
169 print "correct"
171 print "press y for next question, n to exit."
172 I$=INKEY$:IF I$="" THEN 172
173 if i$="y" or i$="Y" then 177
174 if i$="n" or i$="N" then 344
175 goto 171
177 cls:print "question 24."
178 print "to GET to the start menu on a pc, you press:"
179 print "a: control+escape."
180 print "b: alt+escape."
181 print "c: control+f1."
182 print "d: alt+f1."
183 print "press letter of answer, or n to repeat question."
184 I$=INKEY$:IF I$="" THEN 184
185 if i$="a" or i$="A" then 192
186 if i$="n" or i$="N" then 177 else 343
192 print "correct"
193 print "press y for next question, n to exit"
194 I$=INKEY$:IF I$="" THEN 194
195 if i$="y" or i$="Y" then 199
196 if i$="n" or i$="N" then 344
197 goto 193
199 cls:print "question 25."
200 print "what is the green pigment inside all plants?"
201 print "a: jell-o."
202 print "b: nucleus."
203 print "c: chlorophyll."
204 print "d: membrane."
205 print "press letter of answer or n to repeat."
206 I$=INKEY$:IF I$="" THEN 206
209 if i$="c" or i$="C" then 213
211 if i$="n" or i$="N" then 199:goto 343
213 print "correct" 343
214 print "press y for next question, n to exit"
215 I$=INKEY$:IF I$="" THEN 215
216 if i$="y" or i$="Y" then 220
217 if i$="n" or i$="N" then 344
218 goto 214
220 cls:print "question 26."
221 print "the  person who sings irresistible, is:"
222 print "a: britney spears."
223 print "b: jessica simson."
224 print "c: destiny's child."
225 print "d: christina aguelera."
226 print "press letter of answer, or n to repeat."
227 I$=INKEY$:IF I$="" THEN 227
229 if i$="b" or i$="B" then 241
238 if i$="n" or i$="N" then 220
239 goto 343
241 print "correct"
242 print "press y for next question, n to exit"
243 I$=INKEY$:IF I$="" THEN 243
244 if i$="y" or i$="Y" then 248
245 if i$="n" or i$="N" then 344
246 goto 242
248 cls:print "question 27:"
249 print "in writing scripts for jaws for windows,"
250 print "to tell jaws to INPUT the current word,"
251 print "you type the statement:"
252 print "a: findword () ."
253 print "b: INPUTcurrentword () ."
254 print "c: INPUTword () ."
255 print "d: readword () ."
256 print "press letter of answer or n to repeat."
257 I$=INKEY$:IF I$="" THEN 257
259 if i$="b" or i$="B" then 343
260 if i$="c" or i$="C" then 265
262 if i$="n" or i$="N" then 248
263 goto 343
265 print "correct"
266 print "press y for next question, n to exit"
267 I$=INKEY$:IF I$="" THEN 267
268 if i$="y" or i$="Y" then 272
269 if i$="n" or i$="N" then 344
270 goto 266
272 cls:print "question 28."
273 print "mortal kombat 4 was the best game of what year?"
274 print "a: 1987."
275 print "b: 1997."
276 print "c: 1992."
277 print "d: 1998."
278 print "press letter of correct answer, or n to repeat question."
279 I$=INKEY$:IF I$="" THEN 279
280 if i$="b" or i$="B" then 284
281 if i$="n" or i$="N" then 272
282 goto 343
284 print "correct"
285 print "press y for next question, n to exit"
286 I$=INKEY$:IF I$="" THEN 286
287 if i$="y" or i$="Y" then 291
288 if i$="n" or i$="N" then 344
289 goto 285
291 cls:print "question 29."
292 print "the jfw voice is also known as:"
293 print "a: elequents."
294 print "b: jaws for windows."
295 print "c: job access with speech."
296 print "d: eleques."
297 print "press letter of correct answer, n to repeat question."
298 I$=INKEY$:IF I$="" THEN 298
299 if i$="a" or i$="A" then 303
300 if i$="n" or i$="N" then 291
301 goto 343
303 print "correct"
304 print "press y for round 3, final question, n to exit."
305 I$=INKEY$:IF I$="" THEN 305
306 if i$="y" or i$="Y" then 310
308 goto 304
310 cls:print "round 3, final question."
311 print:print
312 print:print
313 print "in the file extension, .vbs, vbs stands for what?"
314 print "a: visual basic script."
315 print "b: virus basic script."
316 print "c: visual based script."
317 print "d: visual basic scriptcompiler."
318 print "press letter of answer, or n to repeat."
319 I$=INKEY$:IF I$="" THEN 319
320 if i$="a" or i$="A" then 327
321 if i$="n" or i$="N" then 313
322 goto 343
327 print "CONGRATULATIONS!"
328 print "YOU HAVE WON ROUND 3."
329 print "PLEASE EMAIL ME AT:"
330 print "mun0009@cfl.rr.com, and i will put your name on my site."
331 print:print
332 print "when you do email me, put the following."
333 print "in the subject, type i won."
334 print "in the body type your name, and age. you do not have to put last name."
335 print "also in the body, type the following code:"
336 print:print
337 print "wonquensgeorndtrv786110//trivia13.bascjbasassjbdgeocities.com/mun00092"
339 print "this code will help me know that you really won. in about 2 days you will see your name on my site."
340 end
343 print "sorry, you lose better luck next time."
344 print "thanks for playing trivia round 3.
345 print "comments/questions/suggestions,"
346 print " mun0009@cfl.rr.com.":end
