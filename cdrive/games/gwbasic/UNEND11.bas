100 print "hi and welcome to unending adventures"
101 print "v1.1."
103 print "written by munawar bijani."
104 print "http://www.geocities.com/mun00092."
105 print "press y to continue, n to exit."
106 i$=inkey$:if i$="" then 106
107 if i$ = "y" then cls:goto 111
108 if i$ = "n" then cls:goto 352
109 goto 105
111 print "you are walking to school the next day,"
112 print "and you find your self in an alley type area."
113 print "you keep walking, and you see a door to your left."
114 print "do you open it? press y to open, or n to keep walking."
115 i$=inkey$:if i$="" then 115
116 if i$ = "y" then cls:goto 120
117 if i$ = "n" then cls:goto 142
118 goto 114
120 print "you open the door, and walk in."
121 print "then, you here a noise, and you trip over something."
122 print "however it is all dark."
123 print "what do you do next? press e to examine the object, w to keep walking,"
124 print "or n to quit."
125 i$=inkey$:if i$="" then 125
126 if i$ = "e" then cls:goto 130
127 if i$ = "w" then cls:goto 134
128 if i$ = "n" then cls:goto 352
129 goto 123
130 print "you examine the object, and you find out its a dead body!"
131 print "you faint and hit your head on a wall, and die!"
132 goto 352
134 print "you keep walking, and you find yourself in a bright,"
135 print "sunny sky. you think to yourself,"
136 print "what the hale happened here?"
137 print "then, you get jumped by a gang!"
138 print "you fight back, and you shake them off easily!"
139 print "then you get run over by a car!"
140 goto 352
142 print "you keep walking, and you turn a corner."
143 print "on the other side there's a door."
144 print "its open."
145 print "do you go into the door?"
146 print "press y to go in, n to keep walking."
147 i$=inkey$:if i$="" then 147
148 if i$ = "y" then cls:goto 152
149 if i$ = "n" then cls:goto 286
150 goto 146
152 print "you go in."
153 print "there's a steep hill going down."
154 print "you go down the hill, and you slip!"
155 print "you fall, and start sliding down the hill!"
156 print "finally, you get to the bottom."
157 print "there is a path going 2 way."
158 print "which way do you take?"
159 print "the left way is dimly lit, but is still hard to see in."
160 print "the right way is totally dark."
161 print "press l for the left way, or r for the right way."
162 i$=inkey$:if i$="" then 162
163 if i$ = "l" then cls:goto 167
164 if i$ = "r" then cls:goto 296
165 goto 161
167 sound 144,1
168 print:print
169 print "you go through the left path."
170 print "it is dimly lit, but not enough for you"
171 print "to see what is ahead of you."
172 print "you keep walking, and,"
173 print "you find a bottle on the ground."
174 print "do you open it, or pocket it."
175 print "press o to open, p to pocket."
176 i$=inkey$:if i$="" then 176
177 if i$ = "o" then cls:goto 181
178 if i$ = "p" then cls:goto 357
179 goto 175
181 print "you open the bottle, and a mist comes out."
182 print "the mist turns dark green."
183 print "it suddenly starts circling you"
184 print "at a speed of 75 miles per hour."
185 print "then then out of the bottle,"
186 print "comes christeena agueleria."
187 print "and she starts singing, i'm a genie"
188 print "in a bottle."
189 print "after the song is done,"
190 print "she gives you 3 wishes."
191 print "do you want to use wishes, or take out shot"
192 print "gun and blow her head off?"
193 print "press w to get wishes, or s to shoot."
194 i$=inkey$:if i$="" then 194
195 if i$ = "w" then cls:goto 211
196 if i$ = "s" then cls:goto 199
197 goto 193
199 beep:print "you shoot cristeena, and she turns"
200 print "around with a big hole in her head, and"
201 print "blood gushing out."
202 print "you turn and run away."
203 print "and then you see her in front of you."
204 print "she has a sword in her hand."
205 print "you turn and run the other way."
206 print "suddenly your head is on the floor."
207 print "yes, your dead. is it really the end????"
208 print "check website for unending adventure 1.2."
209 goto 352
211 print "she asks you what are your wishes?"
212 print "you tell her your wishes, she disappears in a swirling mist."
213 print "then you start walking again."
214 print "you find a sword, and you pick it up."
215 print "just then, an unidentified creature jumps out."
216 print "you say fuck, and jump backward."
217 print "the creature comes closer, and..."
218 print "what do you do next?"
219 print "press y to slice his head with the sword, or n to fight him by hand."
220 i$=inkey$:if i$="" then 220
221 if i$ = "y" then cls:goto 225
222 if i$ = "n" then cls:goto 360
223 goto 219
225 beep:print "you swing your sword, and you cut his head off."
226 print "there is blood gushing out of his socket."
227 print "he falls to the ground, and"
228 print "the blood is still gushing out of the hole."
229 print "you turn and run away."
230 print "then, you find the creature standing in front of you."
231 print "you take out your sword, and you make an attempt"
232 print "to cut him."
233 print "however, the sword goes right through him."
234 print "he picks up a brick from the ground, and hits you with it."
235 print:print
236 print "when you wake up, your sore and bruised."
237 print "obviously, he spared you."
238 print "you sit up, and look around, still the same place you were before."
239 print "you find a door to your left."
240 print "you try to open it, but it is locked."
241 print "then, you find a room in front of you."
247 print "you walk in, and there she is again. cristeena agueleria."
248 print "she is not the same as before."
249 print "now she has the same sword you do, and she's getting ready to stab you."
250 print "do you stab her? y or n"
251 i$=inkey$:if i$="" then 251
252 if i$ = "y" then cls:goto 256
253 if i$ = "n" then cls:goto 272
254 goto 250
256 print "you stab her, and her head comes off."
257 print "you turn and run."
258 print "then, you find yourself in a pit of snakes."
259 print "you picked up a shot gun on your way here, and now you pull the trigger."
260 print "you shoot most of the snakes, but not all."
261 print "you get bitten by a very poisonous snake."
262 print "you are poisoned."
263 sound 144,30:sound 300,40:beep:beep
264 print:print
265 print "you here a man say finish him."
266 print "the creature comes up from nowhere,"
267 print "and slams your body in half!"
268 print "blood is gushing, and you are screaming."
269 print "its over, you died."
270 goto 352
272 print "you stand there, and she takes back her knife."
273 print "she says, i thought were the gangster"
274 print "who was trying to kill me."
275 print "then, you turn around, and there he is."
276 print "the gangster."
277 print "you take out your shot gun, and shoot him."
278 beep:print "he falls back."
279 beep:print "he's bleeding in the chest."
280 beep:print" cristeena screams, and you ask her what's wrong"
281 print "but never here the answer."
282 print "you fall forward, and you feel a stabbing pain going through your body."
283 print "yes, you guessed it. your dead."
284 goto 352
286 print "you keep walking, and it gets very dusty."
287 print "its getting even dustier."
288 print "you find a rifle on the ground."
289 print "you pick it up, and..."
290 print "it blows up in your hand."
291 print "now you have only one hand, your right one."
292 print "you are in pain, and your arm socket is gushing blood."
293 print "you are now very cold because of blood loss."
294 print "your dead."
295 end
296 beep:print "you take the right path."
297 print "it is all dark."
298 print "then, you see a square opening in the left wall."
299 print "do you go in? y or n."
300 i$=inkey$:if i$="" then 300
301 if i$ = "y" then cls:goto 305
302 if i$ = "n" then cls:goto 335
303 goto 299
305 print "you crouch down and go into the opening."
306 print:print
307 print "it is all dark, and you can only see up to your middle finger."
308 print "you walk slowly, and take small steps, so you won't fall into any holes."
309 print:print
310 print "you find another door to your left. open it? y or n."
311 i$=inkey$:if i$="" then 311
312 if i$ = "y" then cls:goto 316
313 if i$ = "n" then cls:goto 346
314 goto 310
316 print "you open the door, and a hard gust of wind blows out."
317 print "you fall back against the other wall, and get knocked out."
318 print:print
319 print:print
320 print "when you wake up you find your self in a small, dark room."
321 print:print
322 print "then, you get attacked."
323 print:print
324 print "the creature rips off your arms, then, after that, rips off your legs."
325 print "you are now lying on the ground, paralyzed."
326 print "you are losing blood. very fast."
327 print:print
328 print "you are getting cold, because of your blood loss."
329 print:print
330 print "colder... colder... colder... colder..."
331 beep:beep:beep:beep:beep:beep
332 print "your dead."
333 goto 352
335 print "you keep walking straight."
336 print "and, suddenly, you find yourself falling."
337 print "you are now in a dimly lit room."
338 print "then, you feel a stab of pain go through your heart."
339 print:print
340 print "you sit up to see what happened, but it is too dark."
341 print "you can now feel liquid pouring out of your chest. its blood."
342 print "yu now start to feel cold."
343 print "that's it. your dead."
344 goto 352
346 print "you keep walking, and...."
347 print "you find yourself in bed. all a dream, right?"
348 print "no, you just found the right path, congratulations."
349 print:print
350 goto 366
352 print "thanks for playing."
353 print "if you have any comments, contact me."
354 print "e-mail: mun0009@cfl.rr.com."
355 print "website: http://www.geocities.com/mun00092."
356 end
357 beep:print "it blows up in your pocket because of the heat."
358 print "your dead."
359 goto 352
360 print "bad idea."
361 print:print
362 print "he rips off your arms, then legs."
363 print "you bleed to death."
364 goto 352
366 print "please note that when unending adventure"
367 print "1.2, or any later version comes out, you will be asked for a code."
368 print "when you start playing 1.2, it will ask you for a code to access the game."
372 print "this code will now be written to a file. if you delete this file, you will be unable to access unending adventure 1.2.3."
374 print "please wait while this code is being written."
375 print:print
376 open "ugcode.txt" for output as 1
377 print#1,"ud1-1-0084mrp10228":close
379 print "the file where the code is stored is named ugcode.txt."
380 print:print
381 print "this code will be used to access the up coming unending adventure."
382 print:print
383 goto 352