10 RANDOMIZE TIMER
20 DIM A$(20),B$(20),C$(20),D$(20),E$(20),F$(20),G$(20),H$(20),I$(20),J$(20)
30 REM UPDATED ON THE 12TH OF JANUARY 1997.
60 PRINT"Story. adapted by Gary Pearse."
70 PRINT"DO YOU WANT INSTRUCTIONS?"
80 I$ = INKEY$:IF I$ = ""THEN 80
90 IF I$ = "y" OR I$="Y"THEN PRINT"yes.":GOTO 1610
100 IF I$ = "n" OR I$="N"THEN PRINT"no.":GOTO 120
110 GOTO 70
120 INPUT"What is your first name?";NA$:PRINT"Thanks "NA$
130 PRINT"Do you want to get a word list from disk?"
140 Q$ = INKEY$:IF Q$=""THEN 140
150 IF Q$ = "n"OR Q$="N"THEN PRINT"No.":GOTO 180
160 IF Q$="y" OR Q$="Y"THEN PRINT"yes.":dq=1:GOTO 1460
170 GOTO 130
180 REM
190 PRINT"OK. We're going to make up some stories"
200 PRINT"together. You supply the words, I supply the plot."
210 REM
220 PRINT"First I need 15 adjectives. Adjectives modify"
230 PRINT"a noun, like: Red, Dirty, Large, etc."
240 FOR X = 1 TO 15
250 PRINT"Adjective #";X;" ";
260 INPUT"";A$(X)
270 NEXT X
280 PRINT"Thanks"
290 PRINT"Now I need 10 adverbs. They modify verbs and end"
300 PRINT"in l y, like: Slowly, Greedily."
310 FOR X = 1 TO 10
320 PRINT"Adverb "X"
330 INPUT"";B$(X)
340 NEXT X
350 PRINT"Thank you, ";NA$
360 PRINT"Next I'll need 15 nouns! These are names of things, such"
370 PRINT"as: Insect, Box, Tree, Banana"
380 FOR X = 1 TO 15
390 PRINT"Noun #";X;" ";
400 INPUT"";C$(X)
410 NEXT X
420 PRINT"You're doing great, ";NA$;", there's not much more."
430 PRINT"How about 5 first names of men."
440 FOR X = 1 TO 5:PRINT"Name "X:INPUT"";D$(X): NEXT X
450 PRINT"now 5 first names of women."
460 FOR X = 1 TO 5:PRINT"Name "X:INPUT"";E$(X):NEXT X
470 PRINT"and now "NA$" five surnames."
480 FOR X = 1 TO 5:PRINT"Name "X:INPUT"";F$(X):NEXT X
490 PRINT"now "NA$" 5 geographical locations."
500 FOR X = 1 TO 5:PRINT"Place name "X:INPUT"";G$(X):NEXT X
510 PRINT"now 6 differant kinds of likwids."
520 FOR X = 1 TO 6
530 PRINT"liquid "X:INPUT"";I$(X)
540 NEXT X
550 PRINT"And finally, 6 exclamatory expressions"
560 FOR X = 1 TO 6
570 PRINT"expression "X:INPUT"";J$(X)
580 NEXT X
590 PRINT"Well done, ";NA$;", now,"
600 REM
610 PRINT"Would you like a STORY ABOUT"
620 PRINT"1: a newspaper ad,"
630 PRINT"2: a western,"
640 PRINT"3: A story about the army,"
650 PRINT"4: a water bed story."
660 QQ$ = INKEY$:IF QQ$=""THEN 660
670 IF QQ$<"1"OR QQ$>"4"THEN 660
680 N=VAL(QQ$)
690 REM
700 ON N GOSUB 810,940,1110,1290
710 PRINT"Want another story ";NA$;"?"
720 QQ$=INKEY$:IF QQ$=""THEN 720
730 IF QQ$="n"OR QQ$="N"THEN PRINT"no.":GOTO 1490
740 IF QQ$="y"OR QQ$="Y"THEN PRINT"yes.":GOTO 760
750 GOTO 710
760 PRINT"Want to use the same words?"
770 QQ$=INKEY$:IF QQ$=""THEN 770
780 IF QQ$="y"OR QQ$="Y"THEN PRINT"yes.":GOTO 600
790 IF QQ$="n"OR QQ$="N"THEN PRINT"no.":GOTO 1560
800 GOTO 770
810 REM
820 PRINT"DAILY GRUNT CLASSIFIED adds"
830 PRINT"FOR SALE: 1957 holden. This "A$(INT(RND(1)*15)+1)" car is in a "A$(INT(RND(1)*15)+1)
840 PRINT"condition. It was formerly owned by a "A$(INT(RND(1)*15)+1)" school teacher"
850 PRINT"who always drove it "B$(INT(RND(1)*10)+1)". There is a "A$(INT(RND(1)*15)+1)" "C$(INT(RND(1)*15)+1)" in"
860 PRINT"the back seat. It has a chrome "C$(INT(RND(1)*15)+1)" on the bonnet, a "A$(INT(RND(1)*15)+1)
870 PRINT"paint job, "A$(INT(RND(1)*15)+1)" tires and the back opens into a "A$(INT(RND(1)*15)+1)
880 PRINT C$(INT(RND(1)*15)+1)". Will consider taking a slightly used "C$(INT(RND(1)*15)+1)" in trade. ."
890 PRINT"LOST: In the vicinity of "G$(INT(RND(1)*5)+1)", a "A$(INT(RND(1)*15)+1)" french poodle with"
900 PRINT A$(INT(RND(1)*15)+1)" hair and a "A$(INT(RND(1)*15)+1)" tail. It answers to the name of ";
910 PRINT E$(INT(RND(1)*5)+1);". When last seen it was holding a "C$(INT(RND(1)*15)+1)" in its mouth."
920 PRINT"A "A$(INT(RND(1)*15)+1)" reward is offered."
930 RETURN
940 REM
950 PRINT"AN ADULT WESTERN."
960 PRINT"Tex "F$(INT(RND(1)*5)+1);", the Marshle of "G$(INT(RND(1)*5)+1)" City rode into town. He sat"
970 PRINT B$(INT(RND(1)*10)+1);" in the saddle, ready for trouble. He knew that his "A$(INT(RND(1)*15)+1)
980 PRINT"enemy, "D$(INT(RND(1)*5)+1)" the Kid was in town. The Kid was in love with Teckses"
990 PRINT"horse, "E$(INT(RND(1)*5)+1);". Suddenly, the Kid came out of the "A$(INT(RND(1)*15)+1)" Nugget"
1000 PRINT"Saloon. "
1010 PRINT"DRAW TEX, he yelled "B$(INT(RND(1)*10)+1)
1020 PRINT"Tex reached for his "C$(INT(RND(1)*15)+1)", but before he could get it out of"
1030 PRINT"his "C$(INT(RND(1)*15)+1);" the Kid fired, hitting Tex in the "C$(INT(RND(1)*15)+1)" and"
1040 PRINT"the ";C$(INT(RND(1)*15)+1);"."
1050 PRINT"As Tex fell he pulled out his own "C$(INT(RND(1)*15)+1)" and shot the Kid ";
1060 PRINT INT(RND(1)*100)+6
1070 PRINT"times in the "C$(INT(RND(1)*15)+1)". The Kid dropped in a pool of "I$(INT(RND(1)*6)+1)
1080 PRINT"'";J$(INT(RND(1)*6)+1)"', Tex said. 'I hated to do it, but he was on the wrong"
1090 PRINT"side of the ";C$(INT(RND(1)*15)+1)"."
1100 RETURN
1110 REM
1120 PRINT"If you plan on joining the army, here are some "A$(INT(RND(1)*15)+1)" hints"
1130 PRINT"that will help you become a "A$(INT(RND(1)*15)+1)" soldier."
1140 PRINT"The army is made up of Officers, Noncoms, and "C$(INT(RND(1)*15)+1)"s."
1150 PRINT"You can recognize an Officer by the "C$(INT(RND(1)*15)+1)"s on his shoulders"
1160 PRINT"and the funny-looking "C$(INT(RND(1)*15)+1);"s on his cap. When you address"
1170 PRINT"an Officer, always say "C$(INT(RND(1)*15)+1);" and say it ";B$(INT(RND(1)*10)+1)". If you get"
1180 PRINT"a "A$(INT(RND(1)*15)+1)" haircut, keep your ";C$(INT(RND(1)*15)+1);"s shined, and see that"
1190 PRINT"your ";C$(INT(RND(1)*15)+1);" is clean at all times. you will be a credit to the "
1200 PRINT"slogan:"
1210 PRINT" The Army Builds Better ";C$(INT(RND(1)*15)+1);"s!"
1220 PRINT"At roll call, when the ";A$(INT(RND(1)*15)+1)" sergeant calls your name, shout"
1230 PRINT"'";J$(INT(RND(1)*6)+1);"' loud and clear."
1240 PRINT"You will become familiar with weapons like the .30 calibre "C$(INT(RND(1)*15)+1)
1250 PRINT"and the automatic "C$(INT(RND(1)*15)+1);"."
1260 PRINT"Follow this advice and you may win the"
1270 PRINT A$(INT(RND(1)*15)+1)" Conduct "C$(INT(RND(1)*15)+1)"."
1280 RETURN
1290 REM
1300 PRINT"Bust a Button and Duck Department Store"
1310 PRINT"Dix Hills, New York."
1320 PRINT"Dear Sirs,"
1330 PRINT" Last week I purchased a "A$(INT(RND(1)*15)+1)" king sized water bed in your"
1340 PRINT"store. I got it especially for my "A$(INT(RND(1)*15)+1)" husband who sleeps"
1350 PRINT"very ";B$(INT(RND(1)*10)+1)" and says that ";A$(INT(RND(1)*15)+1)" water beds that have"
1360 PRINT C$(INT(RND(1)*15)+1);"s in them make his ";C$(INT(RND(1)*15)+1)" ache. When the bed arrived "
1370 PRINT"my husband tested it "B$(INT(RND(1)*10)+1)" and said the "A$(INT(RND(1)*15)+1)" ";C$(INT(RND(1)*15)+1)
1380 PRINT"was bent and kept pressing into his "C$(INT(RND(1)*15)+1)". He says this"
1390 PRINT"could lead to a ";A$(INT(RND(1)*15)+1)" condition of the ";C$(INT(RND(1)*15)+1);"."
1400 PRINT" I would like to exchange this "A$(INT(RND(1)*15)+1)" bed for one that"
1410 PRINT"will allow my husband to sleep ";B$(INT(RND(1)*10)+1)" and won't make his"
1420 PRINT C$(INT(RND(1)*15)+1)" sore."
1430 PRINT"Yours "B$(INT(RND(1)*10)+1);","
1440 PRINT E$(INT(RND(1)*5)+1)" "F$(INT(RND(1)*5)+1)
1450 RETURN
1460 INPUT"Enter file name.";FL$:FL$=FL$+".sty"
1470 OPEN FL$ FOR INPUT AS#1
1480 FOR X = 1 TO 15:INPUT#1,A$(X),B$(X),C$(X),D$(X),E$(X),F$(X),G$(X),I$(X),J$(X):NEXT X:CLOSE:GOTO 600
1490 IF DQ=1 THEN PRINT "Thanks for the game "na$:system else PRINT"Do you want to save your words on disk, yes, or no?"
1500 QQ$=INKEY$:IF QQ$=""THEN 1500
1510 IF QQ$="n"OR QQ$="N"THEN PRINT"No. goodbye "NA$".":SYSTEM
1520 IF QQ$="y"OR QQ$="Y"THEN PRINT"yes.":GOTO 1540
1530 GOTO 1490
1540 INPUT"Enter file name.";FL$:FL$=FL$+".sty":OPEN FL$ FOR OUTPUT AS#1
1550 FOR X = 1 TO 15:WRITE#1,A$(X),B$(X),C$(X),D$(X),E$(X),F$(X),G$(X),I$(X),J$(X):NEXT X:CLOSE:SYSTEM
1560 PRINT"Do you want to get a word list from disk, yes, or no?"
1570 QQ$=INKEY$:IF QQ$=""THEN 1570
1580 IF QQ$="y"OR QQ$="Y"THEN PRINT"yes.":GOTO 1460
1590 IF QQ$="n"OR QQ$="N"THEN PRINT"no.":GOTO 210
1600 GOTO 1560
1610 PRINT"When you first run story, it will ask you do you want to get a "
1620 PRINT"word list from disk."
1630 PRINT"The first time you run the program, you should answer no to this "
1640 PRINT"prompt, that is unless you have a word list on your disk"
1650 PRINT" supplied by someone else."
1660 PRINT"all disk files saved by this program will have the extension "
1670 PRINT"s t y added to them automatically,"
1680 PRINT" so don't put extensions on your saved or loaded word lists."
1690 PRINT"Play the game for the first time answering all the questions"
1700 PRINT"asked by the program and you will quickly see how it works."
1710 PRINT"Later you will be able to load word lists from your disk thus "
1720 PRINT"saving you the bother of answering all of the questions"
1730 PRINT" every time you play."
1740 PRINT"Remember, the more outrageous the word list the funnier the story."
1750 PRINT"If you have any questions about the programme, call me on home "
1760 PRINT"phone +61 2 9829 1909 or FAX +61 2 9829 3355"
1770 PRINT"PO BOX 88 Macquarie Fields N.S.W. 2564 Australia."
1780 PRINT"Do you want the instructions repeted?"
1790 I$ = INKEY$:IF I$="" THEN 1790
1800 IF I$="y"OR I$="Y"THEN PRINT"yes.":GOTO 1610
1810 IF I$="n"OR I$="N"THEN PRINT"no.":GOTO 120
1820 GOTO 1780
