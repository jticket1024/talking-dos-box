THE GREAT ESCAPE

Ported to gwbasic by Jason Smith
Modified by Graham Pearce

You are lost in a giant maze of a hundred rooms.  The goal of the
game is to find your way out.  And oh yeah, to collect as many gold
coins as you can along the way.
Sound easy?  Yeah right!
Along the way you will probably come in contact with a villain who
steels all of your gold coins, you will be trying to catch the
leprechaun, you may meet up with a mad gambler, an evil merchant,
and
lots of other creatures of all kinds.
To play the game, you need to either have a Gwbasic interpreter
(Basica would probably also do it) or you need to compile the game
E.G. with Quickbasic PDS, Powerbasic, etc.
Once the game is running, you are told to put your caps lock down. 
This is crucial!  If you don't, none of your input will be
recognized!  This is a holdover from the days when this game ran on
the Apple computer family and I just have not bothered to convert
it to use lowercase.  Anybody know of an easy way to do this?  Let
me know!
OK, so once the caps lock is down, hit a key and you are asked if
you want to try a hard game.  Answering Y will, maybe among other
things, not give you a map so you don't know what room you start
out in.  I usually just hit N.
Once the game is afoot, you travel by entering N, S, E, W or X.  N,
S, E and W are for north, south, east and west.  X is to use a
secret passageway.  This is useful if you get stuck in a room with
no exits (it does happen!) or with exits you don't want to take. 
A secret passageway will count as ten moves.
How to quit
To quit the game and return to the basic prompt, press q.
To quit the game and either return to your basic interpreter's prompt , or back to the dos prompt if you are using a compiled version, press control break. 
Typing the word system at this time returns you to DOS or whatever
and wipes out your game.
Typing the word run will restart the game from the beginning, also
wiping the old game.
Type in the word cont as in continue and the game will proceed from
where it left off.
To save press "A" at the your move prompt, and to restore press 'l' at the 'your move' prompt.
How to cheat
If you are a totally honest person and wouldn't even think of
cheating on any kind of game then please skip this section.  If you
either want to cheat, or just want to know how the darned thing
works, read on!
I once wrote a routine into an Apple version of this game that
would, at startup, ask the player various questions E.G. what room
they wanted themselves and the villain to be in, if they wanted
certain objects, etc.  This routine never made it over to the IBM
version and the parameters could not be changed once the game was
going.
But there is a way to do that.  This will not work in a compiled
version unless you have modified the source to add in secret cheat
commands.
But in a Gwbasic/Basica game you can Ctrl-break to break out of the
game.  Then, you can look at or change various variables to effect
the game.  A few of note are:
The variable p is where the player is, as an integer from 1 to 100
indicating the room.  The v variable is where, you guessed it, the
villain is.  The variable tr E.G. treasure, is how many gold coins
the player has.
Look at the source to find other variables and what values they are
assigned.
Once you've peeked around or changed something, type in the "cont"
command to continue the game.  It will continue from where it left
off, never knowing that you've modified something.
Note:
The intentional (I think!) misspellings that were in the original
Apple version E.G. "villian" for "villain" have been left in this
version.
