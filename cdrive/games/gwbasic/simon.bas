10 ON ERROR GOTO 1680
20 RANDOMIZE TIMER
30 KEY OFF
40 DIM sn(8),s$(8),t$(4),NU(200),O(200)
50 DATA 233,293,348,466,588,696,932,1172
60 FOR I=1 TO 8:READ SN(I):NEXT
70 DATA "diminished","minor","major","augmented"
80 FOR I=1 TO 4:READ T$(I):NEXT
90 CLS:PRINT "welcome to super simon 1.3, programmed by Graham Pearce, inspired by a"chr$(13)"version of simon by Gary Pearce, with ideas borrowed from other versions."
100 PRINT "do you want instructions?":IN$=INPUT$(1)
110 IF IN$="y" THEN 1200
120 IF IN$="n" THEN 140
130 PRINT "invalid response.":GOTO 100
140 SP=1:MI=3:T=3:LE=2:AR=1.3:NOTES=4:LR=1:m=3
150 IF LR=1 THEN S$(1)="s":S$(2)="d":S$(3)="f":S$(4)="k"
160 IF LR=2 THEN S$(1)="s":S$(2)="j":S$(3)="k":S$(4)="l"
170 IF NOTES=6 THEN S$(1)="s":S$(2)="d":S$(3)="f":S$(4)="j":S$(5)="k":S$(6)="l"
180 IF NOTES=8 THEN S$(1)="a":S$(2)="s":S$(3)="d":S$(4)="f":S$(5)="j":S$(6)="k":S$(7)="l":S$(8)=";"
190 IF T=1 THEN SN(2)=277:SN(3)=330:SN(5)=554:SN(6)=660:SN(8)=1108
200 IF T=2 THEN SN(2)=277:SN(5)=554:SN(8)=1108
210 IF T=4 THEN SN(3)=370:SN(6)=740
220 CLS
230 PRINT "press 'n' to change the number of notes and their positions."chr$(13)"you can currently choose from";NOTES;" notes ";:IF LR=1 THEN PRINT "mostly in the left hand":GOTO 240 ELSE IF LR=2 THEN PRINT "mostly in the right hand":GOTO 240
240 PRINT "press 'm' to change the number of mistakes allowed during the game."chr$(13)"currently";MI;"mistakes will be allowed."
250 PRINT "press 's' to change the speed. at the moment, the speed is";SP
260 PRINT "press 'a' to change the acceleration rate. at the moment, the acceleration rate is";AR
270 PRINT "press 'p' to change the pause ratio. the pause ratio is currently";LE
280 PRINT "press 't' to change the tonality. the tonality is currently ";T$(T)
290 PRINT "press 'v' to save a configuration file."
300 PRINT "press 'l' to load a configuration file."
310 PRINT "press 'r' to go into practice mode."
320 PRINT "press 'b' to begin the game."
330 I$=INPUT$(1)
340 IF I$="n" THEN 450
350 IF I$="m" THEN 530
360 IF I$="s" THEN 560
370 IF I$="a" THEN 590
380 IF I$="p" THEN 620
390 IF I$="t" THEN 660
400 IF I$="r" THEN 680
410 IF I$="v" THEN 1590
420 IF I$="l" THEN 1630
430 IF I$="b" THEN 790
440 PRINT "invalid response.":GOTO 330
450 CLS
460 PRINT "enter 1 if you want to have a choice of four notes, 2 if you want to have a":PRINT"choice of 6, and 3 if you want to have a choice of 8.":INPUT "",CH
470 CH=ABS(fix(CH)):IF CH<1 OR CH>3 THEN PRINT "must be between 1 and 3.":GOTO 460
480 NOTES=CH*2+2
490 IF NOTES >4 THEN LR=0:GOTO 170
500 CLS
510 PRINT "press 1 if you want the notes mostly on your left hand, 2 if you want them":PRINT"mostly on your right.":INPUT "",LR
520 IF LR<1 OR LR>2 THEN PRINT "please type 1 or 2":GOTO 510 ELSE 150
530 CLS
540 PRINT "how many mistakes will I allow before the game is over? 0, 1, 2 or 3?":INPUT "",MI:M=MI
550 IF M<0 OR M>3 THEN PRINT "only 0, 1, 2 or 3 mistakes will be allowed!":GOTO 540 ELSE 220
560 CLS
570 PRINT "enter the starting speed of the notes.":INPUT "",SP
580 SP=ABS(SP):IF SP<.5 OR SP>20 THEN PRINT "you've got to be joking!":GOTO 570 ELSE 220
590 CLS
600 PRINT "enter the acceleration rate between 1 and 2. 1 gives you no change in speed"chr$(13)"during the game and 2 will give you the most change in speed.":INPUT "",AR
610 AR=ABS(AR):IF AR<1 OR AR>2 THEN PRINT "you've got to be joking!":GOTO 600 ELSE 220
620 CLS
630 PRINT "enter the pause ratio between 1.5 and 4."chr$(13)"1.5 gives the most pause between rounds and 4 gives the least.":INPUT "",LE  
640 LE=ABS(LE):IF LE<1.5 OR LE>4 THEN PRINT "are you serious?":GOTO 630 ELSE 220
650 CLS
660 PRINT "press 1 if you would like the notes in a diminished tonality, 2 if you would"chr$(13)"like them in a minor tonality, 3 if you would like them in a major tonality or 4":PRINT"for an augmented tonality.":INPUT "",T
670 t=abs(fix(t)):IF T<1 OR T>4 THEN PRINT "please enter 1, 2, 3, or 4.":GOTO 660 ELSE 190
680 CLS
690 PRINT "practice mode active. to exit press escape."
700 I$=INKEY$:IF I$="" THEN 700
710 FOR I=1 TO NOTES:IF I$=S$(I) THEN SOUND SN(I),6/SP:Z=I:GOTO 720
720 NEXT
730 IF I$=CHR$(27) THEN 750
740 IF Z=0 THEN PRINT "not a symbol used in this game.":GOTO 700 ELSE Z=0:GOTO 700
750 CLS:PRINT "are you happy with the settings?":I$=INPUT$(1)
760 IF I$="y" THEN 790
770 IF I$="n" THEN 220
780 PRINT "invalid response.":GOTO 760
790 OP=SP:PRINT "press any key to start the game."
800 I$=INKEY$:IF I$="" THEN 800
810 CLS:TZ=TIMER
820 Z=0:TR=0:SO=fix(RND(1)*NOTES)+1:NU(S+1)=SN(SO):S=S+1:O(S)=SO
830 IF S>0 THEN SOUND 0,5/SP/LE:FOR I=1 TO S:SOUND 0,.1:SOUND NU(I),6/SP:NEXT:TM=TIMER
835 tn=timer:if tm>tn then tn=tn+86400
840 IF tn-TM >=3 then S=S-1:GOTO 920 ELSE 850
850 I$=INKEY$:IF I$="" THEN 835
860 FOR I=1 TO NOTES:IF I$=S$(I) THEN SOUND SN(I),6/SP:Z=I:GOTO 870
870 NEXT
880 IF Z=0 THEN PRINT "not a symbol used in this game.":TM=TIMER:GOTO 840
890 IF O(TR+1)=Z THEN TR=TR+1 ELSE GOTO 920
900 FOR I=1 TO S/2:IF TR=S AND S>3 AND 2^I =S THEN SP=SP*AR:GOTO 910 ELSE NEXT 
910 IF TR=S THEN Z=0:GOTO 820 ELSE TM=TIMER:Z=0:GOTO 840
920 M=M-1:IF M=-1 THEN 980
930 PRINT "you can make only"M+1"mistakes now before the game is over."
940 PRINT "press space to resume game.":
950 I$=INKEY$:IF I$="" THEN 950
960 IF I$=" " THEN 970 ELSE 950
970 TR=0:I$="":GOTO 830
980 S=S-1:FOR I=130 TO 100 STEP -3:SOUND I,1:NEXT:SOUND 90,15
990 PRINT "YOU BLEW IT!"
995 if tm-tz<0 then tm=tm+86400
1000 IF S=0 THEN TM=0:TZ=0
1010 PRINT "YOU COMPLETED";S"TONES CORRECTLY IN ";:PRINT USING "###.##";TM-TZ;:PRINT "SECONDS, WITH AN AVERAGE TIME": PRINT "PER TONE OF ";:IF S=0 THEN PRINT 0;"seconds" ELSE PRINT USING "#.##";(TM-TZ)/S;:PRINT " SECONDS."
1020 OPEN "simon.rec" FOR INPUT AS 1
1030 INPUT#1,OT,OS,OA
1040 IF S>OT THEN 1080
1050 IF S=0 THEN 1070
1060 IF S=OT AND OT/OA<S/((TM-TZ)/S) THEN 1080
1070 PRINT "the current record is";OT;"tones completed in";OS;"seconds with an average time":PRINT"per tone of";OA;"seconds.":GOTO 1110
1080 CLOSE:PRINT "which sets a new record!"
1090 OPEN "simon.rec" FOR OUTPUT AS 1: PRINT#1,S,:PRINT#1, USING "###.##";TM-TZ:PRINT#1, USING "#.##";(TM-TZ)/S:CLOSE
1100 PRINT "the old record was when";OT;"tones were completed in";OS;"seconds with an average time per tone of";OA;"seconds."
1110 PRINT "do you want to play again?"
1120 I$=INKEY$:IF I$="" THEN 1120
1130 IF I$<>"n" AND I$<>"y" THEN PRINT "INVALID RESPONSE.":GOTO 1110
1140 IF I$="y" THEN FOR I=1 TO S:NU(I)=0:O(I)=0:NEXT:S=0:M=MI:SP=OP:CLOSE:GOTO 1160
1150 IF I$="n" THEN PRINT "hope you had fun!":SYSTEM
1160 PRINT "do you want to change any settings for the next game?":I$=INPUT$(1)
1170 IF I$="y" THEN 150
1180 IF I$="n" THEN 790
1190 PRINT "invalid response.":GOTO 1160
1200 CLS
1210 PRINT "I will play one note, then you must copy it."
1220 PRINT "I will play that note , then another one, and you have to copy"chr$(13)"those two, and so on."
1230 PRINT "the objective of the game is to correctly copy as many tones as"chr$(13)"you can."
1240 PRINT "after the instructions which you are reading now, the game will present a menu"chr$(13)"of choices."chr$(13)"the first choice will be the number of notes you want to choose from."
1250 PRINT "if you choose to have six, the keys to remember are s, d, f, j, k and l."
1260 PRINT "if you choose to have eight tones, the keys to remember are a, s, d, f, j, k, l and semi-colon."
1270 PRINT "if you don't choose four tones, the game will take you back to the menu of"chr$(13)"choices." 
1280 PRINT "if you choose to have four tones, the game will ask you if you want most of the tones on the left hand, or most of them on your right hand."
1290 PRINT "if you choose to have most of the tones on your left hand, the keys to press to activate the associated tones are s, d, f and k."
1300 PRINT "if you choose the right hand option, the keys to remember are s, j, k and l."
1310 PRINT "press any key to continue.":I$=INPUT$(1)
1320 CLS:PRINT "the next choice in the menu refers to  the"chr$(13)"number of mistakes to be allowed before the game is over."
1330 PRINT "this prompt is pretty self-explanatory, but it is worth noting that hanging"chr$(13)"around while the game is in progress for more than three seconds is counted as a mistake."
1340 PRINT "the next choice in the menu is the speed at which the notes are played."
1350 PRINT "choose numbers between 0.5 and 20 here."
1360 PRINT "a speed of 0.5 is very slow, a speed of 1 is normal and a speed of 4 or 5 is"chr$(13)"very fast."
1370 PRINT "how fast the speed increases is determined by the acceleration rate, see below."
1380 PRINT "press any key to continue.":I$=INPUT$(1)
1390 CLS:PRINT "the next choice in the menu is the acceleration rate."
1400 PRINT "this refers to how fast the speed will increase."
1410 PRINT "a value of 1 means that the speed never increases during the game, and a value"chr$(13)"of 2 means it increases very quickly."
1420 PRINT "the default value is 1.4."
1430 PRINT "next, the game will ask you for a pause ratio."
1440 PRINT "this determines how long the game pauses after you have"chr$(13)"successfully typed in all the notes the computer has asked for."
1450 PRINT "if you are just starting with these kind of games, you should pick a value"chr$(13)"between 1 and 2."
1460 PRINT "if you are an experienced simon player, you should probably pick a value between 3 and 4."
1470 PRINT "press any key to continue.":I$=INPUT$(1)
1480 CLS:PRINT "The next menu choice is  the tonality you want the tones in, either diminished, minor, major or augmented."
1490 PRINT "for those who don't know or have forgotten, diminished is when you have a stack of minor 3rds on top of each other, giving a rather spooky sound."
1500 PRINT "a minor chord is when you have a minor 3rd then a major 3rd, making it sound"chr$(13)"rather sad."
1510 PRINT "a major chord is when you have a major 3rd then a minor one, making it sound"chr$(13)"quite bright."
1520 PRINT "and an augmented chord is a stack of major 3rds, making it sound pretty weird."
1530 PRINT "have a go at each one to see which one you prefer."
1540 PRINT "the rest of the game is self-explanatory, and besides, you need some surprises, grin."
1550 PRINT "do you want the instructions repeated?":I$=INPUT$(1)
1560 IF I$="y" THEN 1200
1570 IF I$="n" THEN 460
1580 PRINT "invalid response.":GOTO 1550
1590 AZ=1:PRINT "enter file name to store configuration file in. do not include a .cfg extention.":INPUT "",F$
1600 F$=F$+".cfg":OPEN F$ FOR OUTPUT AS 1
1610 PRINT#1,NOTES,LR,MI,SP,AR,LE,T
1620 AZ=0:CLOSE:GOTO 220
1630 AZ=2:PRINT "enter the name of the configuration file to load. do not include the .cfg"chr$(13)"extention":INPUT "",F$
1640 F$=F$+".cfg"
1650 OPEN F$ FOR INPUT AS #1
1660 INPUT#1,NOTES,LR,MI,SP,AR,LE,T
1670 m=mi:CLOSE:AZ=0:GOTO 150
1680 IF ERR=53 AND S=0 AND AZ=0 THEN 1070
1690 IF ERR=53 AND AZ>0 THEN PRINT "file not found. please re-enter":GOTO 1630
1700 IF ERR=52 THEN PRINT "bad file name. please re-enter":IF AZ=1 THEN 1590 ELSE IF AZ=2 THEN 1630
1710 IF ERR=53 THEN 1080 ELSE PRINT "an error code";ERR;"has occurred in line";ERL;". terminating ...":END
