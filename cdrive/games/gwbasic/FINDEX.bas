0 REM 02-02-85 <-- last backup
10 cls
20 PRINT"Eric Gans"
30 PRINT"French Department UCLA"
40 PRINT"Los Angeles, CA 90024"
50 PRINT"02/02/85"
60 PRINT:PRINT TAB(35)"FINDEX":PRINT
70 REM file indexer by E. Gans 10/17/84
80 REM 10/20 added ( to array variables, #!$% at end, . allowed within
90 DEFINT A-Z
110 PRINT TAB(30)"PROGRAM INDEXER":PRINT
120 PRINT"This program produces an index of the variables and reserved words used"
130 PRINT"in an MBASIC program file.  The file to be indexed MUST be saved in the"
140 PRINT"'A' (=ASCII) mode.  The file RESWD.DAT must be on disk."
150 PRINT:PRINT"To gain space and speed, multiple mentions on the same line are not shown."
160 PRINT
170 DIM RES$(133)
180 GOSUB 820
190 DIM WD$(1100),R$(1100),WDSUP$(600),RSUP$(600)
200 N1=1:N=1:SI=1
210 INPUT "Program to index";FILE$
215 input "file to print index to?",f$
220 FOR I=1 TO LEN(FILE$):IF ASC(MID$(FILE$,I,1))>96 THEN MID$(FILE$,I,1)=CHR$(ASC(MID$(FILE$,I,1))AND NOT 32)
230 NEXT
240 INPUT "Index reserved words (y/n)";Y$
250 IF Y$="n" THEN NORES=-1 ELSE IF Y$<>"y" GOTO 240
260 OPEN "I",1,FILE$
270 LINE INPUT#1,L$:if len(l$)=0 then 520
280 I=1
290 WHILE MID$(L$,I,1)<>" "
300 IF ASC(MID$(L$,I,1))<48 OR ASC(MID$(L$,I,1))>57 THEN print "error in file.":end
310 I=I+1
320 WEND
330 LIN$=LEFT$(L$,I)
340 I=I+1:IF I>LEN(L$) THEN 460
350 IF MID$(L$,I,1)=CHR$(34) THEN I=I+1:WHILE MID$(L$,I,1)<>CHR$(34):I=I+1:WEND
360 k=i
370 IF ASC(MID$(L$,I,1))<65 OR ASC(MID$(L$,I,1))>90 THEN 340
380 WHILE (ASC(MID$(L$,I,1))>64 AND ASC(MID$(L$,I,1))<91) OR MID$(L$,I,1)="." OR (ASC(MID$(L$,I,1))>47 AND ASC(MID$(L$,I,1))<58)
390 I=I+1:IF I>LEN(L$) THEN 440
400 WEND
410 IF I=K THEN 340
420 IF MID$(L$,I,1)="!" OR ASC(MID$(L$,I,1))>34 AND ASC(MID$(L$,I,1))<38 THEN I=I+1
430 IF MID$(L$,I,1)="(" THEN I=I+1
440 WD$(N)=MID$(L$,K,I-K):GOSUB 880:N=N+1:I=I-1:IF MID$(L$,K,I+1-K)="REM" or MID$(L$,K,I+1-K)="'" or MID$(L$,K,I+1-K)="data" THEN 460
450 GOTO 340
460 J=N1
470 FOR I=N1 TO N-1
480 IF ASC(WD$(I))=1 THEN WD$(I)="":GOTO 500
490 WD$(J)=WD$(I):R$(J)=R$(I):J=J+1
500 NEXT
510 N1=J:N=J
520 PRINT LIN$;:IF NOT EOF(1) GOTO 270
530 PRINT:PRINT"Sorting ";
540 FOR I=1 TO N-1
550 FOR J=1 TO I-1
560 IF WD$(I)<WD$(J) THEN SWAP WD$(I),WD$(J):SWAP R$(I),R$(J):GOTO 560
570 NEXT J
580 PRINT"*";
590 NEXT I
600 PRINT
610 open f$ for output as 3
630 PRINT"Index of program "FILE$
640 print#3,"Index of program "FILE$
650 PRINT:print#3,
660 IF NORES THEN PRINT"Variables:" ELSE PRINT"Reserved words:"
670 IF NORES THEN print#3,"Variables:" ELSE print#3,"Reserved words:"
680 ZAP=NORES
690 FOR I=1 TO N-1
700 IF NOT ZAP THEN IF ASC(WD$(I))>0 THEN PRINT:print#3,:PRINT"Variables:":print#3,"Variables:":ZAP=-1
710 LN=LN+1:IF LN=12 THEN LN=0:PRINT"[More]"CHR$(13);:E$=INPUT$(1):PRINT SPC(6)CHR$(13);
720 PRINT WD$(I)" "R$(I)
730 print#3, WD$(I)" "R$(I)
740 NEXT
750 IF SI>1 THEN PRINT:print#3,:PRINT"Supplementary list:":print#3, "Supplementary list:" ELSE GOTO 800
760 FOR I=1 TO SI-1
770 PRINT WDSUP$(I)" "RSUP$(I)
780 print#3, WDSUP$(I)" "RSUP$(I)
790 NEXT
800 WIDTH 80
810 END
820 REM res wds
830 OPEN "I",2,"RESWD.DAT"
840 FOR I=1 TO 123
850 INPUT#2,RES$(I)
860 NEXT
870 RETURN
880 REM see if res wd
890 IF ASC(WD$(N))=72 THEN IF MID$(L$,K-1,1)="&" THEN WD$(N)="":N=N-1:RETURN
900 FOR J=N1 TO N-1
910 IF WD$(N)=WD$(J) OR WD$(J)=CHR$(1)+WD$(N) OR WD$(J)=CHR$(0)+WD$(N) THEN WD$(N)="":N=N-1:RETURN
920 NEXT
930 FOR J=1 TO N1-1
940 IF NORES THEN IF WD$(N)=WD$(J) THEN R$(J)=R$(J)+LIN$:WD$(N)=CHR$(1)+WD$(N):GOTO 1070 ELSE GOTO 960
950 IF WD$(N)=WD$(J) OR WD$(J)=CHR$(0)+WD$(N) THEN R$(J)=R$(J)+LIN$:IF ASC(WD$(N))=0 THEN MID$(WD$(N),1,1)=CHR$(1):GOTO 1070 ELSE WD$(N)=CHR$(1)+WD$(N):GOTO 1070
960 NEXT
970 IF RIGHT$(WD$(N),1)="#" OR RIGHT$(WD$(N),1)="(" THEN WT$=WD$(N):WD$(N)=LEFT$(WD$(N),LEN(WD$(N))-1)
980 FOR U=1 TO 133
990 IF WD$(N)=RES$(U) THEN 1030
1000 NEXT
1010 IF WT$<>"" THEN WD$(N)=WT$:WT$=""
1020 R$(N)=LIN$:RETURN
1030 IF NORES THEN WD$(N)="":N=N-1 ELSE WD$(N)=CHR$(0)+WD$(N):R$(N)=LIN$
1040 IF RIGHT$(WT$,1)="#" THEN WD$(N)=WD$(N)+"#"
1050 IF WT$<>"" THEN WT$=""
1060 RETURN
1070 IF LEN(R$(J))>249 THEN WDSUP$(SI)=WD$(N):RSUP$(SI)=R$(J):SI=SI+1:R$(J)="[See supplementary listing] ":RETURN
1080 RETURN
