10 ON ERROR goto 1438
50 RANDOMIZE TIMER
100 PRINT "hi and welcome to unending adventure 1.2.3!"
102 PRINT "written by munawar bijani on 09/02/01."
103 print "now trying to access code which you should have gotten in unending adventure":print "v1.1."
104 PRINT "if you downloaded 1.1 before 09/02/01, the code was not included."
105 PRINT "therefore you must download and beat v1.1 again."
106 print:print
107 open "ugcode.txt" for input as 1
108 input#1,c$
109 close
111 IF C$="ud1-1-0084mrp10228" then 114
112 goto 1438
114 PRINT "code entered successfully. press any key to continue"
115 I$=INKEY$:if i$="" then 115
119 cls:beep:print "you wake up one morning to find yourself in a cold, musty smelling room."
120 PRINT "it is dark, but there is just enough light for you to see where"
121 PRINT "you are going."
122 PRINT "you get up and start walking to a square opening in the wall."
123 print:print
124 PRINT "you step up to the opening, but do you go in?"
125 print:print
126 PRINT "press y to go in, or n to stand there and wait for..."
127 I$=INKEY$:if i$="" then 127
128 IF I$="y" then cls:goto 133
129 IF I$="n" then cls:goto 178
131 cls:goto 119
133 SOUND 160,9.1
134 PRINT "you crouch down and go into the square opening."
135 print:print
136 PRINT "you get up and hear water dripping from your right."
137 PRINT "you start walking toward it, and trip over something."
138 PRINT "you find that it is a knife, and you pocket it."
139 PRINT "then , you think to yourself: it might come in handy, and you resume your journey toward the water."
140 print:print
141 PRINT "you spot a door to your left, do you want to go in? press y to go in, n to":print "continue toward the water."
142 I$=INKEY$:if i$="" then 142
143 IF I$="y" then cls:goto 148
144 IF I$="n" then cls:goto 185
146 cls:goto 134
148 PRINT "you walk into the doorway, and a powerful gust of wind blows out at you."
150 PRINT "a sour smell stings your nose."
152 PRINT "then , with the little light you have, you see a monster staring at you."
153 PRINT "you get up quick, and pull out your knife."
154 PRINT "the monster growls from deep inside its throat."
156 PRINT "you make an attempt to stab it, but it jumps to the side, and..."
157 PRINT "it attacks you very bad."
159 PRINT "you are now on the ground, bleeding."
160 PRINT "the monster is standing over you."
161 PRINT "luckily, it didn't damage your upper body, therefore you can still use your":print "sword."
163 PRINT "you swing your sword at it, and stab it!"
165 PRINT "then , another one comes out at you."
166 PRINT "it rips the sword from your hand, and stabs you. once..."
168 PRINT "twice..."
170 PRINT "3 times..."
172 PRINT "you are now bleeding very bad."
174 PRINT "you are now unconscious."
175 PRINT "your dead."
176 goto 1441
178 PRINT "you stand there and wait."
179 print:print
180 PRINT "then a powerful force pulls you in, toward the opening."
181 PRINT "you get sucked in."
182 BEEP:PRINT
183 goto 136
185 print:print
186 PRINT "you continue to walk toward the water."
187 PRINT "the air gets more humid, and the floor gets slippery."
188 PRINT "then you step on a hollow platform."
189 PRINT "you see a handle on the rectangular platform."
190 PRINT "press y to pull up the handle, n to keep walking toward the noise."
191 I$=INKEY$:if i$="" then 191
192 IF I$="y" then cls:goto 197
193 IF I$="n" then cls:goto 429
195 cls:goto 186
197 PRINT "you bend down and pull up the handle."
198 SOUND 100,18.2
199 print:print
200 PRINT "you step off the platform, and pull it up."
201 print:print
202 BEEP:PRINT
203 PRINT "you look down in the opening, and see that there is light down there."
204 PRINT "so, that can mean only one thing."
205 print:print
206 PRINT "you lower yourself down into the hole, and are surprised to find"
207 PRINT "that the ground is a sponge material."
208 PRINT "then , you see someone familiar standing in front of you, staring at you."
209 PRINT "you suddenly recognize her."
210 PRINT "it's britney spears!"
211 PRINT "here you have 2 choices:"
212 PRINT "you can either take out your knife and stab her to death, because this is what":print "you always"
213 PRINT "wanted to do."
214 PRINT "or if your her fan, you can spare her!"
215 PRINT "press y to spare her, n to stab her to death."
216 I$=INKEY$:if i$="" then 216
217 IF I$="y" then cls:goto 1444
218 IF I$="n" then cls:goto 222
220 cls:goto 197
222 PRINT "you walk up to her, and say hi, like your not going to do anything."
223 PRINT "she asks: what are ya doing here?"
224 PRINT "you hesitate, then , with a big grin, you say,"
225 PRINT "i was sent here to kill you!"
226 PRINT "you pull out your knife, and britney gasps in horror!"
227 PRINT "you don't give her a chance to beg for mercy. you stab her in the chest,"
228 PRINT "then the mid-section!"
229 PRINT "britney's screaming for help. she's now on the ground."
230 print:print
231 PRINT "you then finish her off by..."
232 print:print
233 BEEP:PRINT
234 BEEP:PRINT
235 PRINT "you now continue to walk down the straight path."
236 PRINT "you find another door to your left. y to open, n to continue down the path."
237 I$=INKEY$:if i$="" then 237
238 IF I$="y" then cls:goto 243
239 IF I$="n" then cls:goto 257
241 cls:goto 235
243 SOUND 144,3.64:SOUND 154,9.1:BEEP
244 print:print
245 PRINT "you open the door. you notice that the passage is colder than the one you were just in."
246 PRINT "you start walking down the passage."
247 print:print
248 PRINT "you then see a wall in front of you."
249 PRINT "the passage has terminated."
250 print:print
251 PRINT "however you spot an outline of a door in the wall."
252 PRINT "you try to open the door, but it is locked."
253 PRINT "you then go out the door you came in, and keep walking down the original path."
254 print:print
257 PRINT "you keep walking down the path."
258 PRINT "then , you come to a two way path."
259 PRINT "there is a path going two ways, and the passage continues."
260 PRINT "the left way is dusty and dark. the right way is barely lit. you can also continue"
261 PRINT "straight ahead."
262 PRINT "press l for left, n for right, or s for straight ahead."
263 I$=INKEY$:if i$="" then 263
264 IF I$="l" then cls:goto 282
265 IF I$="n" then cls:goto 270
266 IF I$="s" then cls:goto 276
268 cls:goto 257
270 print:print
271 PRINT "you turn right."
272 PRINT "you start walking down the path, and get the feeling someone's watching you."
273 PRINT "and, it jumps out, grabs you, and eats you..."
274 goto 1441
276 PRINT "you continue down the path."
277 PRINT "and..."
278 PRINT "its tackles and eats you."
279 PRINT "sorry you loose."
280 goto 1441
282 BEEP:PRINT
283 print:print
284 PRINT "you turn left."
285 PRINT "the path is to dusty, you start coughing."
286 PRINT "you keep walking, not knowing what is ahead of you."
287 print:print
288 PRINT "then , you bump into a wall."
289 print:print
290 PRINT "you feel around on the wall, and notice that"
291 PRINT "the passage turns to the left."
292 PRINT "you also find a door on the wall."
293 PRINT "press y to turn left and keep going, or n to open the door and go in."
294 I$=INKEY$:if i$="" then 294
295 IF I$="y" then cls:goto 300
296 IF I$="n" then cls:goto 1453
298 cls:goto 284
300 BEEP:PRINT
301 PRINT "you turn left and continue to follow the passage."
302 PRINT "the passage is to dark for you to see, so you start doing circle steps to prevent"
303 PRINT "falling into any trapdoors, holes, etc."
304 PRINT "you keep heading straight, and the passage ends."
305 PRINT "on the wall in front of you, you find a door."
306 PRINT "the passage also makes a sharp turn to the left."
307 PRINT "do you want to follow the passage to the left, or open the door and go in."
308 PRINT "press y to follow it to the left, or n to open the door and go in."
309 I$=INKEY$:if i$="" then 309
310 IF I$="y" then cls:goto 315
311 IF I$="n" then cls:goto 1458
313 cls:goto 301
315 beep:print "you continue to follow the passage further to the left."
316 PRINT "the passage gets even more dusty, and now you start coughing."
317 PRINT "you continue to follow the path."
318 print:print
319 PRINT "after 20 minutes of walking, the passage curves sharply to the left."
320 PRINT "your hands hit the wall and you notice an outline with a handle."
321 PRINT "its a door."
322 PRINT "again, you have the choice of opening the door, or following the passage."
323 PRINT "press y to follow the path, or n to open the door."
324 I$=INKEY$:if i$="" then 324
325 IF I$="y" then cls:goto 330
326 IF I$="n" then cls:goto 1464
328 cls:goto 316
330 BEEP:PRINT
331 PRINT "you again continue to follow the path."
332 print:print
333 PRINT "after an hour's worth of walking, the passage"
334 PRINT "turns sharply to the right."
335 PRINT "another door is on the wall."
336 PRINT "press y to turn right, or n to open the door."
337 I$=INKEY$:if i$="" then 337
338 IF I$="y" then cls:goto 343
339 IF I$="n" then cls:goto 1468
341 cls:goto 331
343 PRINT "you turn to the right and continue to follow the path."
344 print:print
345 PRINT "you continue to walk, and the passage"
346 PRINT "terminates abruptly."
347 print:print
348 PRINT "however you see a door on the wall."
349 PRINT "open it? y or n."
350 I$=INKEY$:if i$="" then 350
351 IF I$="y" then cls:goto 367
352 IF I$="n" then cls:goto 356
354 cls:goto 343
356 PRINT "you stand there and wait."
357 print:print
358 PRINT "then , you get attacked from behind."
359 PRINT "the killer stabs you in your heart."
360 print:print
361 PRINT "you fall on your back on the ground, and you are bleeding."
362 print:print
363 PRINT "you are now getting cold..."
364 print:print
365 goto 1441
367 SOUND 154,3.64:SOUND 164,3.64:print:print
368 PRINT "you open the door."
369 PRINT "a musty smell greets you, causing you to choke."
370 PRINT "you walk in, and you find out that the room is brightly lit."
371 PRINT "the walls are pure white, and the room is"
372 PRINT "about 30x30 ft."
373 print:print
374 PRINT "you start walking, and you find a square opening in the wall."
375 PRINT "do you go in? (y/n)"
376 I$=INKEY$:if i$="" then 376
377 IF I$="y" then cls:goto 382
378 IF I$="n" then cls:goto 1471
380 cls:goto 368
382 beep:print "you get down on all fours, and crawl into the square opening."
383 PRINT "however the passage stays low, resembling a tunnel."
384 print:print
385 PRINT "you continue to crawl down the passage."
386 PRINT "then you notice that the passage ceiling gets higher, and therefore"
387 PRINT "allowing you to stand up."
388 PRINT "so you stand up."
389 print:print
390 PRINT "you continue to walk down the expanding tunnel."
391 PRINT "the passage then tilts up-hill."
392 PRINT "you continue to follow the passage."
393 print:print
394 PRINT "you then find another door to your left."
395 PRINT "go in: y or n."
396 I$=INKEY$:if i$="" then 396
397 IF I$="y" then cls:goto 402
398 IF I$="n" then cls:goto 1474
400 cls:goto 383
402 SOUND 154,3.64:SOUND 164,3.64:print:print
403 PRINT "you open and walk into the door."
404 PRINT "it's totally dark, therefore preventing you from seeing"
405 PRINT "clearly. from what you can see, you see that the passage"
406 PRINT "terminates, but you can turn right and"
407 PRINT "keep going."
408 print:print
409 PRINT "you also find a door on the wall. what do you want to do?"
410 PRINT "press y to open the door and go in, n to"
411 PRINT "turn right, and follow the path."
412 I$=INKEY$:if i$="" then 412
413 IF I$="y" then cls:goto 418
414 IF I$="n" then cls:goto 1485
415 if i$="r" then cls:goto 403
416 cls:goto 405
418 SOUND 64,3.64:SOUND 84,3.64:print:print
419 PRINT "you go in the door, and..."
420 print:print
421 PRINT "you hear a voice calling..."
422 print:print
423 PRINT "congratulations..."
424 PRINT "you have just made your way through the many dangers of this maze."
425 PRINT "you have opened the door to the outerworld, and have gotten out of the maze you ended up in."
426 print:print
427 cls:goto 1418
429 PRINT "you continue to walk toward the noise of the dripping"
430 PRINT "water."
431 print:print
432 PRINT "the dripping water gets closer."
433 print:print
434 PRINT "you continue to walk straight ahead."
435 PRINT "the floor gets very slippery."
436 PRINT "you then step into a puddle of burning hot"
437 PRINT "water!"
438 print:print
439 print:print
440 PRINT "you pull your foot back and scream with shock"
441 PRINT "and pain!"
442 print:print
443 PRINT "you are now kneeling on the ground, your left foot still burning."
444 PRINT "the pain is now reaching up your thigh."
445 print:print
446 PRINT "you look into the water, which is crystal clear."
447 PRINT "you see some kind of insect or worm"
448 PRINT "in there, they are swimming around."
449 PRINT "you look at the insects a little while longer, then ..."
450 print:print
451 PRINT "you spot a door under the puddle"
452 PRINT "do you want to take out your knife and cut the insects, then go in?"
453 PRINT "or do you want to stand and wait..."
454 PRINT "press y to open the door, n to stand and wait"
455 I$=INKEY$:if i$="" then 455
456 IF I$="y" then cls:goto 461
457 IF I$="n" then cls:goto 1489
459 cls:goto 429
461 PRINT "you take out your knife, and you kneel down and start"
462 PRINT "chopping the insects or worm like creatures."
464 PRINT "then you find that there are no more insects in the puddle of water."
466 PRINT "you pull up the trapdoor."
467 SOUND 166,18.2
468 PRINT "the water drains into the doorway, and you look down into it."
469 PRINT "you see what looks like an endless drop"
470 PRINT "down to the ground."
471 PRINT "then something catches your eye."
473 PRINT "it is an object, black with what looks like a switch"
474 PRINT "on the top of it."
475 PRINT "then you decide to climb down and take a better look at the"
476 PRINT "object."
478 PRINT "you look for a way to get down into the area, and you spot"
479 PRINT "some foot-holes that you can get down from."
481 PRINT "you step up to the doorway in the ground, and lower one foot"
482 PRINT "down on to the foot-hole."
484 PRINT "then the other."
486 PRINT "you then slowly lower yourself"
487 PRINT "down to the next foot-hole, the next,"
488 PRINT "the one after, etc."
491 PRINT "finally, you land on solid ground."
492 PRINT "you turn toward the passage, and start walking."
494 PRINT "you continue to follow the passage."
495 PRINT "you find a door to your left. go in? y/n"
496 I$=INKEY$:if i$="" then 496
497 IF I$="y" then cls:goto 502
498 IF I$="n" then cls:goto 1494
499 IF I$="r" then cls:goto 468
500 cls:goto 495
502 PRINT "you go into the door, and you can't see anything."
503 PRINT "it's all dark, however you find that the room or passage is dimly lit."
504 print:print
505 PRINT "you start walking, and a musty smell greets you."
506 PRINT "then you hear a roar from somewhere up ahead."
509 print:print
510 PRINT "you see a creature come out from the endless passage up ahead."
511 PRINT "it roars again."
512 print:print
513 PRINT "the creature steps up to you"
514 print:print
535 print:print
536 PRINT "you stand there in shock."
537 print:print
538 PRINT "then it speaks to you."
541 PRINT "what the fuck are you doing here you basterd?"
544 PRINT "who are you?"
547 PRINT "i am kitaro. i was born here in the outerworld, where you don't belong!"
550 PRINT "what the fuck do ya mean?"
553 PRINT "you will not remain here! you will be killed at once!"
556 PRINT "you ass whole, don't you dare hurt me!"
557 PRINT "i have a knife, and i know how to use it."
558 PRINT "get away from me you basterd!"
561 PRINT "hold your breath and anger for later you fucking bitch!!!!!!"
564 print:print
565 PRINT "don't you dare curse at me you basterd!!"
567 PRINT "it pounces at you... you fight back."
568 print:print
569 H=50
570 A=30
571 PRINT "r=round house kick: damage=5"
572 print:print
573 PRINT "b=body slam: damage=20"
574 print:print
575 PRINT "select your move."
576 I$=INKEY$:if i$="" then 576
577 cls:IF I$="r" then 581
578 IF I$="b" then 585
579 goto 571
581 A=A-5
582 IF A<1 then 639
583 goto 645
585 A=A-20
586 IF A<1 then 639
587 goto 650
588 D=INT(RND(1)*5)+1
589 if d=1 then 595
590 if d=2 then 604
591 if d=3 then 613
592 if d=4 then 622
593 if d=5 then 630
595 H=H-10
596 if h<1 then 655
599 SOUND 134,18.20:PRINT "kitaro has used flip."
600 PRINT "your hp is now"
601 PRINT H
602 goto 575
604 H=H-5
605 if h<1 then 655
608 PRINT "kitaro has used punch."
609 PRINT "your hp is now"
610 PRINT H
611 goto 575
613 H=H-30
614 if h<1 then 655
617 beep:print "kitaro has used stomp."
618 PRINT "your hp is now at"
619 PRINT H
620 goto 575
622 H=H-10
623 if h<1 then 655
626 beep:print "kitaro has used fire ball."
627 PRINT "your hp is now at"
628 PRINT H:goto 575
630 H=H-5
631 if h<1 then 655
634 beep:print "kitaro has used kick."
635 PRINT "your hp is now at"
636 PRINT H
637 goto 575
639 SOUND 300,18.2:PRINT "congratulations! you have beat your first"
640 PRINT "challenger!"
641 PRINT "your health currently is"
642 PRINT H
643 PRINT "out of 50.":goto 662
645 PRINT "you selected round house kick."
646 PRINT "kitaro's hp is now at"
647 PRINT A
648 print:print:goto 588
650 PRINT "you selected body slam."
651 PRINT "kitaro's hp is now at"
652 PRINT A
653 print:print:goto 588
655 BEEP:SOUND 154,5.46:SOUND 140,18.2
656 PRINT "your energy has reached zero."
657 PRINT "sorry, you lose. improve your"
658 PRINT "fighting skills before fighting again."
659 print:print
660 END
662 print:print
663 PRINT "the new move/power is called flying kick!"
664 PRINT "damage=30."
665 PRINT "you also earn 30 hp."
666 H=H+30
667 PRINT "your health is now"
668 PRINT H
669 PRINT "hp."
670 print:print
671 PRINT "you find a dark passage in front of you."
672 PRINT "you start walking straight."
673 PRINT "you see a door to your left."
674 PRINT "go in? y or n."
675 I$=INKEY$:if i$="" then 675
676 IF I$="y" then cls:goto 680
677 IF I$="n" then cls:goto 792
678 cls:goto 671
680 BEEP:BEEP:beep:print "congratulations!"
681 PRINT "you have found a health pack!"
682 PRINT "your hp is now at"
683 H=H+60
684 PRINT H
685 print:print
686 PRINT "you hear a growl somewhere in front of you."
687 SOUND 134,3.640:PRINT "looks like another fight."
688 print:print
689 PRINT "what the heck?"
690 PRINT "who the hell are you?"
691 print:print
693 PRINT "what the fuck are you doing here you basterd?"
694 PRINT "do you want to fight?"
696 PRINT "yeah, fine! i'll fight you."
697 print:print
698 PRINT "fight!!!!!!!!!!"
699 A=60
700 PRINT "the creature's energy is currently at"
701 PRINT A:print:print
702 PRINT "r=round house kick: damage=5."
703 PRINT "b=body slam: damage=20"
704 PRINT "f=flying kick: damage=30."
705 print:print
706 PRINT "select your move"
707 I$=INKEY$:if i$="" then 707
708 cls:IF I$="r" then 713
709 IF I$="b" then 719
710 IF I$="f" then 725
711 cls:goto 702
713 A=A-5
714 if a<1 then 780
716 beep:print "you selected round house kick."
717 PRINT "the creature's energy is now at"
718 PRINT A:goto 733
719 A=A-20
720 if a<1 then 780
722 beep:print "you selected body slam. the creature's hp is now at"
723 PRINT A:goto 733
725 A=A-30
726 if a<1 then 780
728 BEEP:BEEP:beep:print "you selected"
729 PRINT "flying kick."
730 PRINT "the creature's hp is now at"
731 PRINT A:goto 733
733 D=INT(RND(1)*5)+1
734 if d=1 then 740
735 if d=2 then 747
736 if d=3 then 755
737 if d=4 then 762
738 if d=5 then 769
740 H=H-30
741 if h<1 then 775
743 BEEP:beep:print "the creature has used toss."
744 PRINT "your hp is now at"
745 PRINT H:goto 706
747 H=H-40
748 if h<1 then 775
750 BEEP:SOUND 154,3.640:BEEP
751 PRINT "the creature has used collide."
752 PRINT "your hp is now at"
753 PRINT H:goto 706
755 H=H-20
756 if h<1 then 775
758 beep:print "the creature has used grab punch."
759 PRINT "your hp is now at"
760 PRINT H:goto 706
762 H=H-5
763 if h<1 then 775
765 beep:print "the creature has used kick."
766 PRINT "your hp is now at"
767 PRINT H:goto 706
769 H=H-10
770 if h<1 then 775
772 beep:print "the creature has used slam."
773 PRINT "your hp is now at"
774 PRINT H:goto 706
775 SOUND 145,3.64:SOUND 154,4:PRINT
777 PRINT "ha ha ha! you weak, pethetic fool."
778 PRINT "you really thought you could beat me?"
779 END
780 SOUND 151,18.2:PRINT "there you go you weak piece of shit!"
781 print:print
782 PRINT "you win! congratulations!"
783 print:print
784 PRINT "you have earned 70 hp."
785 H=H+70
786 PRINT "your hp is now at"
787 PRINT H
788 print:print
789 PRINT "you see a wall in front of you."
790 PRINT "seeing that there is nothing inside the room, you turn around and rezoom your journey down the"
791 PRINT "straight path."
792 print:print
793 PRINT "you continue to walk for approximately half an hour."
794 PRINT "then , you find a door to your left."
795 PRINT "open it? y or n"
796 I$=INKEY$:if i$="" then 796
797 IF I$="y" then cls:goto 800
798 IF I$="n" then cls:goto 928
799 cls:goto 789
800 SOUND 154,4:SOUND 180,9.1:print:print
801 PRINT "you open the door, and..."
802 print:print
803 PRINT "raaaaaaaaaaaaaror!!"
804 BEEP:BEEP:BEEP:BEEP
806 PRINT "you have just lost 10 hp."
807 H=H-10
808 PRINT "hp"
809 PRINT H
810 print:print
811 PRINT "looks like he wants to fight.":A=500
812 print:print
813 PRINT "r=round house kick: damage=5"
814 print:print
815 PRINT "b=body slam: damage=30"
816 print:print
817 PRINT "f=flying kick: damage=50"
818 print:print
819 PRINT "w=whirl wind: damage=150"
820 print:print
821 PRINT "moltaro's hp is currently at"
822 PRINT A
823 print:print
825 PRINT "fight!"
826 print:print
827 PRINT "select your move"
828 I$=INKEY$:if i$="" then 828
829 cls:IF I$="r" then 834
830 IF I$="b" then 841
831 IF I$="f" then 848
832 IF I$="w" then 856
833 cls:goto 827
834 A=A-5
835 if a<1 then 920
837 beep:print "you selected round house kick."
838 PRINT "moltaro's energy is now at"
839 PRINT A:goto 864
841 A=A-30
842 if a<1 then 920
844 SOUND 154,9.1:BEEP:beep:print "you selected body slam."
845 PRINT "moltaro's hp is now at"
846 PRINT A:goto 864
848 A=A-50
849 if a<1 then 920
851 BEEP:SOUND 154,9.1:BEEP
852 PRINT "you selected flying kick."
853 PRINT "moltaro's hp is now at"
854 PRINT A:goto 864
856 A=A-150
857 if a<1 then 920
859 SOUND 160,18.2:BEEP:SOUND 180,9.1:BEEP
860 PRINT "you selected whirl wind."
861 PRINT "moltaro's hp is now at"
862 PRINT A
864 D=INT(RND(1)*6)+1
865 if d=1 then 873
866 if d=2 then 879
867 if d=3 then 886
868 if d=4 then 893
869 if d=5 then 902
870 if d=6 then 908
872 PRINT "invalid move argument.":END
873 H=H-20
874 if h<1 then 915
876 beep:print "moltaro has used punch."
877 PRINT "your energy is now at"
878 PRINT H:goto 827
879 H=H-20
880 if h<1 then 915
882 beep:print "moltaro has used kick."
883 PRINT "your hp is now at"
884 PRINT H:goto 827
886 H=H-30
887 if h<1 then 915
889 SOUND 154,5.46:beep:print "moltaro has used collide."
890 PRINT "your energy is now at"
891 PRINT H:goto 827
893 H=H-40
894 if h<1 then 915
896 BEEP:BEEP:SOUND 154,8
897 PRINT "moltaro has used grab punch."
898 PRINT "your energy is now at"
899 PRINT H:goto 827
902 if h<1 then 915
904 SOUND 155,3.64:PRINT "moltaro has used iron punch."
905 PRINT "your energy is now at"
906 PRINT H:goto 827
908 H=H-30
909 if h<1 then 915
911 SOUND 154,3.64:PRINT "moltaro has used flip."
912 PRINT "your energy is now at"
913 PRINT H:goto 827
915 SOUND 134,18.2:
916 PRINT "you weak pethetic fool."
918 PRINT "you lose.":END
920 SOUND 160,18.2
921 PRINT "you fuckin bitch, that's what you get."
922 print:print
923 PRINT "congratulations, you win."
924 print:print
925 PRINT "you again find nothing on the wall in front of you."
926 PRINT "you turn around and continue down the path,"
927 PRINT "looking for some other doors or passages."
928 print:print
929 PRINT "after walking for about 20 minutes,"
930 PRINT "you find a door to your left."
931 PRINT "do you want to go in the door? y or n"
932 I$=INKEY$:if i$="" then 932
933 IF I$="y" then cls:goto 937
934 IF I$="n" then cls:goto 1121
935 cls:goto 931
937 SOUND 154,6:SOUND 145,5.46:BEEP
938 PRINT "you open the door, and walk in."
939 print:print
940 PRINT "you hear a crowd of people somewhere up ahead."
941 PRINT "then someone comes to greet you."
942 print:print
944 PRINT "welcome child. are you here to participate in the tournament?"
946 PRINT "um... i guess so. i just ended up here some how."
948 PRINT "ok dear, why don't you just follow me. i am going to get you prepared for the imortal master."
950 PRINT "ok, i will. but what exactly are you going to do to me?"
952 PRINT "oh no no dear, it's not me whose going to train you. we have some professionals who will train you."
954 PRINT "fine, let's go."
955 print:print
956 PRINT "you follow the woman. you step into a brightly lit room."
957 PRINT "the walls of the large room are made purely of marble."
958 PRINT "the room is as big as 10 football fields."
959 PRINT "the room is cluttered with strange mastines that look like weight lifting material."
960 print:print
961 PRINT "in the dead center of the room, however, there is a round, ball shaped object."
962 PRINT "the woman leads you up to the object, and you look inside."
963 print:print
964 PRINT "you see a type of dice object in the ball shaped object."
965 PRINT "the woman tells you to just hold on a sec while she calls for assistants."
966 print:print
967 PRINT "she leaves you over by the round object, and hurries over to a door at the back of the room."
988 print:print
989 PRINT "after about five minutes, two uniformed men"
990 PRINT "come jogging up to you."
991 print:print
992 PRINT "both men are very muscular and tall."
993 print:print
994 PRINT "one of the men motions for you to put your hand on the ball."
995 PRINT "good job, now just wait."
996 print:print
997 PRINT "me explain this to you."
998 print:print
999 PRINT "the square you see inside this object is a die."
1000 PRINT "you will turn or roll this object which you have your hand on."
1001 PRINT "the die inside will start to roll as well."
1002 PRINT "then the number, 1 through 6, will"
1003 PRINT "determine your course."
1004 PRINT "however there is one course that will suck your energy to zero."
1005 PRINT "the rest of the courses will raise your hp, so you are fit to fight the imortal master."
1006 print:print
1007 PRINT "at the end of your course, you will be given a key card that will allow"
1008 PRINT "you to fight the imortal master."
1009 print:print
1010 PRINT "ok, now that you know how this works, are you ready?"
1011 PRINT "um, yes, i guess so."
1012 PRINT "very well, go ahead spin the object."
1013 print:print
1014 SOUND 154,9.1:SOUND 154,4:SOUND 154,3
1015 SOUND 154,3.64:SOUND 154,1
1016 print:print
1017 PRINT "you spin the object."
1018 print:print
1019 S=INT(RND(1)*6)+1
1020 IF S=1 then cls:goto 1027
1021 IF S=2 then cls:goto 1040
1022 IF S=3 then cls:goto 1052
1023 IF S=4 then cls:goto 1073
1024 IF S=5 then cls:goto 1089
1025 IF S=6 then cls:goto 1105
1027 PRINT "you have rolled a one."
1028 PRINT "i'm sorry, but all energy will be sucked out of you."
1030 PRINT "aaaaaaaaaaarrrrrrrrghhhh!!"
1031 print:print
1032 H=0
1033 PRINT "you were dragged to the woman who escorted you in the room,"
1034 PRINT "and she handed you a card."
1036 PRINT "this card will act as a pass to you fight the imortal master."
1037 PRINT "sorry, but you have not earned any moves. the moves you had are now gone."
1038 K=1:cls:goto 1122
1040 PRINT "you have rolled a two."
1041 PRINT "you have earned 3000 hp."
1042 PRINT "you have also earned the following moves:"
1043 PRINT "flying kick: damage=100."
1044 print:print
1045 PRINT "whirl wind: damage=200."
1046 H=H+3000:K=2
1047 PRINT "you received a level 2 card."
1049 PRINT "current hp"
1050 PRINT H:cls:goto 1122
1052 PRINT "you rolled a three."
1053 print:print
1054 PRINT "you have earned the following moves:"
1055 print:print
1056 PRINT "flying kick: damage=200."
1057 print:print
1058 PRINT "whirl wind: damage=200."
1059 print:print
1060 PRINT "collide: damage=350."
1061 print:print
1062 PRINT "grab punch: damage=350."
1063 print:print
1064 PRINT "you have also gained 4000 hp."
1065 H=H+4000
1066 PRINT "current hp"
1067 PRINT H
1069 print:print
1070 PRINT "you received a level 3 card, which will allow you to fight the imortal master."
1071 K=3:cls:goto 1122
1073 PRINT "you have rolled a 4."
1074 PRINT "you have earned the following moves:"
1075 print:print
1076 PRINT "collide: damage=450."
1077 print:print
1078 PRINT "whirl wind: damage=3360."
1079 print:print
1080 PRINT "upper cut: damage=400."
1081 print:print
1082 PRINT "you have earned a level 4 card which will allow you to fight the imortal master."
1083 print:print
1084 PRINT "you also gained 5000 hp."
1085 K=4:H=H+5000
1086 PRINT "current hp"
1087 PRINT H:cls:goto 1122
1089 PRINT "you have rolled a 5."
1090 PRINT "you have earned the following moves:"
1091 print:print
1092 PRINT "upper cut: damage=500."
1093 print:print
1094 PRINT "round house fart: damage=600."
1095 print:print
1096 PRINT "skull bash: damage=200."
1097 print:print
1098 PRINT "you have earned a level 5 card which will allow you to fight the imortal master."
1099 print:print
1100 PRINT "you have also earned 6000 hp."
1101 K=5:H=H+6000
1102 PRINT "current hp"
1103 PRINT H:cls:goto 1122
1105 PRINT "you have rolled a 6."
1106 PRINT "you have earned the following moves."
1107 print:print
1108 PRINT "round house fart: damage=600."
1109 print:print
1110 PRINT "knee drop: damage=400."
1111 print:print
1112 print:print
1113 PRINT "komaomaymoay: damage=2000."
1114 print:print
1115 PRINT "you have also earned 9000 hp."
1116 print:print
1117 PRINT "you have also earned a level 6 card which will allow you to fight the imortal master."
1118 H=H+9000:K=6:
1119 PRINT "current hp"
1120 PRINT H:cls:goto 1122
1121 K=7
1122 print:print
1123 PRINT "you continue down the endless path."
1124 print:print
1125 PRINT "finally, you come to the end of the path!"
1126 PRINT "you see a door in front of you, and nothing else."
1127 print:print:print "you step up to the door."
1128 SOUND 180,30:beep:print "the door opens automatically."
1129 print:print
1130 PRINT "you step into the doorway, and a computerized voice says"
1131 PRINT "please move forward and someone will be here to greet you shortly."
1132 print:print
1133 PRINT "then some people in black motion for you to step forward."
1134 print:print
1135 PRINT "there is one person ahead of you, please be patient."
1137 PRINT "you then here the announcer giving commentary on the prior fight."
1138 print:print
1139 PRINT "oh my god, look at this folks."
1140 PRINT "the imortal master has used brutality, and the challenger is down and out!!!"
1141 print:print:print "next challenger, please come forward."
1142 PRINT "go ahead sir, that's you."
1143 print:print:
1144 PRINT "you step into the battle dome, and you see the imortal master standing in front of you."
1145 print:print
1146 IF K=1 then cls:goto 1160
1147 IF K=2 then cls:goto 1160
1148 IF K=3 then cls:goto 1160
1149 IF K=4 then cls:goto 1160
1150 IF K=5 then cls:goto 1160
1151 IF K=6 then cls:goto 1160
1152 cls:goto 1154
1154 SOUND 154,3.64:SOUND 154,3.64:PRINT "you don't have a valid card"
1155 PRINT "from the training center."
1156 PRINT "you are not qualified to fight."
1157 print:print
1158 PRINT "sorry.":END
1160 PRINT "you have a valid pass from the training center."
1161 PRINT "you are qualified to fight."
1162 A=10000
1163 PRINT "the imortal master's energy is currently at":PRINT A
1164 IF K=1 then cls:goto 1171
1165 IF K=2 then cls:goto 1175
1166 IF K=3 then cls:goto 1194
1167 IF K=4 then cls:goto 1233
1168 IF K=5 then cls:goto 1263
1169 IF K=6 then cls:goto 1295
1171 print:print
1172 PRINT "no moves."
1173 PRINT "ffffffffffight!":goto 1329
1175 PRINT "flying kick: damage 100."
1176 PRINT "whirl wind: damage 200."
1177 PRINT "ffffffight!"
1178 PRINT "select your move."
1179 I$=INKEY$:if i$="" then 1179
1180 IF I$="f" then 1183
1181 IF I$="w" then 1190
1182 cls:goto 1178
1183 A=A-100
1184 if a<1 then 1418
1186 beep:print "you selected flying kick."
1187 PRINT "the imortal master's energy it now at"
1188 PRINT A:goto 1329
1190 A=A-200:if a<1 then 1418:BEEP:SOUND 154,3.64:PRINT "you selected whirl wind."
1191 PRINT "the imortal master's energy is now at"
1192 PRINT A:goto 1329
1194 PRINT "w=whirl wind: damage=200":print:print
1195 PRINT "f=flying kick: damage=200":print:print
1196 PRINT "c=collide: damage=350":print:print
1197 PRINT "g=grab punch: damage 350":print:print
1198 PRINT "fffffffffight":PRINT "select your move"
1199 I$=INKEY$:if i$="" then 1199
1200 cls:IF I$="f" then 1205
1201 IF I$="w" then 1212
1202 IF I$="c" then 1219
1203 IF I$="g" then 1226
1204 cls:goto 1199
1205 A=A-200
1206 if a<1 then 1418
1208 beep:print "you selected flying kick."
1209 PRINT "the imortal master's energy is now at"
1210 PRINT A:goto 1329
1212 A=A-200
1213 if a<1 then 1418
1215 SOUND 154,5.46:PRINT "you selected whirl wind."
1216 PRINT "the imortal master's energy is now at"
1217 PRINT A:goto 1329
1219 A=A-350
1220 if a<1 then 1418
1222 BEEP:SOUND 154,18.2:beep:print "you selected collide."
1223 PRINT "the imortal master's energy is now at"
1224 PRINT A:goto 1329
1226 A=A-350
1227 if a<1 then 1418
1229 SOUND 140,9.1:BEEP:SOUND 160,9.1:beep:print "you selected grab punch."
1230 PRINT "the imortal master's energy is now at"
1231 PRINT A:goto 1329
1233 PRINT "c=collide: damage=450":print:print
1234 PRINT "w=whirl wind: damage=360":print:print
1235 PRINT "u=upper cut: damage=400":print:print
1236 PRINT "ffffffffight"
1237 PRINT "select your move"
1238 I$=INKEY$:if i$="" then 1238
1239 cls:IF I$="c" then 1243
1240 IF I$="w" then 1250
1241 IF I$="u" then 1257
1242 cls:goto 1237
1243 A=A-450
1244 if a<1 then 1418
1246 SOUND 154,9.1:beep:print "you selected collide."
1247 PRINT "the imortal master's energy is now at"
1248 PRINT A:goto 1329
1250 A=A-360
1251 if a<1 then 1418
1253 SOUND 154,9.1:PRINT "you selected whirl wind."
1254 PRINT "the imortal master's energy is now at"
1255 PRINT A:goto 1329
1257 A=A-400
1258 if a<1 then 1418
1260 beep:print "you selected upper cut."
1261 PRINT "the imortal master's energy is now at"
1262 PRINT A:goto 1329
1263 PRINT "u=upper cut: damage=500":print:print
1264 PRINT "r=round house fart: damage=600":print:print
1265 PRINT "s=skull bash: damage=200":print:print
1266 PRINT "ffffffight"
1267 PRINT "select your move"
1268 I$=INKEY$:if i$="" then 1268
1269 cls:IF I$="u" then 1273
1270 IF I$="r" then 1280
1271 IF I$="s" then 1288
1272 goto 1267
1273 A=A-500
1274 if a<1 then 1418
1276 SOUND 160,3.64:beep:print "you selected upper cut."
1277 PRINT "the imortal master's energy is now at"
1278 PRINT A:goto 1329
1280 A=A-600
1281 if a<1 then 1418
1284 PRINT "you selected round house fart."
1285 PRINT "the imortal master's energy is now at"
1286 PRINT A:goto 1329
1288 A=A-200
1289 if a<1 then 1418
1291 SOUND 154,1:beep:print "you selected skull bash."
1292 PRINT "the imortal master's energy is now at"
1293 PRINT A:goto 1329
1295 PRINT "r=round house fart: damage=600":print:print
1296 PRINT "k=knee drop: damage=400":print:print
1297 PRINT "h=komayhomay: damage=2000":print:print
1298 PRINT "fffffffffffight"
1299 PRINT "select your move"
1300 I$=INKEY$:if i$="" then 1300
1301 IF I$="r" then 1305
1302 IF I$="k" then 1313
1303 IF I$="h" then 1321
1304 cls:goto 1299
1305 A=A-600
1306 if a<1 then 1418
1309 PRINT "you selected round house fart."
1310 PRINT "the imortal master's energy is now at"
1311 PRINT A:goto 1329
1313 A=A-400
1314 if a<1 then 1418
1316 BEEP:SOUND 154,3.64:SOUND 0,9.1:BEEP
1317 PRINT "you selected knee drop."
1318 PRINT "the imortal master's energy is now at"
1319 PRINT A:goto 1329
1321 A=A-2000
1322 if a<1 then 1418
1324 SOUND 165,9.1:SOUND 0,18.2:SOUND 180,9.1
1325 SOUND 0,3.640:SOUND 300,30:PRINT "you selected komayhomay!!!"
1326 PRINT "the imortal master's energy is now at"
1327 PRINT A:goto 1329
1329 D=INT(RND(1)*6)+1
1330 if d=1 then 1337
1331 if d=2 then 1350
1332 if d=3 then 1363
1333 if d=4 then 1376
1334 if d=5 then 1388
1335 if d=6 then 1399
1337 H=H-1000
1338 if h<1 then 1414
1340 SOUND 154,3.64:SOUND 0,3.64:beep:print "the imortal master has used skull bash."
1341 PRINT "your energy is now at"
1342 PRINT H
1343 IF K=1 then 1172
1344 IF K=2 then 1178
1345 IF K=3 then 1199
1346 IF K=4 then 1237
1347 IF K=5 then 1267
1348 IF K=6 then 1299
1350 H=H-1500
1351 if h<1 then 1414
1353 BEEP:SOUND 145,6:PRINT "the imortal master has used collide."
1354 PRINT "your energy is now at"
1355 PRINT H
1356 IF K=1 then 1172
1357 IF K=2 then 1178
1358 IF K=3 then 1199
1359 IF K=4 then 1237
1360 IF K=5 then 1267
1361 IF K=6 then 1299
1363 H=H-1500
1364 if h<1 then 1414
1366 beep:print "the imortal master has used stomp."
1367 PRINT "your energy is now at"
1368 PRINT H
1369 IF K=1 then 1172
1370 IF K=2 then 1178
1371 IF K=3 then 1199
1372 IF K=4 then 1237
1373 IF K=5 then 1267
1374 IF K=6 then 1299
1376 H=H-1500
1377 if h<1 then 1414
1379 BEEP:SOUND 0,9.1:beep:print "the imortal master has used body slam."
1380 PRINT "your energy is now at"
1381 PRINT H
1382 IF K=1 then 1172
1383 IF K=2 then 1178
1384 IF K=3 then 1199
1385 IF K=5 then 1267
1386 IF K=6 then 1299
1388 H=H-3000:if h<1 then 1414
1389 SOUND 0,18.2:beep:print "the imortal master has used choke."
1390 PRINT "your energy is now at"
1391 PRINT H
1392 IF K=1 then 1172
1393 IF K=2 then 1178
1394 IF K=3 then 1199
1395 IF K=4 then 1237
1396 IF K=5 then 1267
1397 IF K=6 then 1299
1399 H=H-10000
1400 if h<1 then 1414
1402 SOUND 154,3.64:SOUND 0,3.64:SOUND 154,18.2
1403 SOUND 380,3.640
1404 PRINT "the imortal master has used stunner."
1405 PRINT "your energy is now at"
1406 PRINT H
1407 IF K=1 then 1172
1408 IF K=2 then 1178
1409 IF K=3 then 1199
1410 IF K=4 then 1237
1411 IF K=5 then 1267
1412 IF K=6 then 1299
1414 PRINT "looks like our challenger looses again!!"
1415 PRINT "they need to practice more!"
1416 print:print:print "next!":END
1418 BEEP:SOUND 900,30:PRINT "congratulations!"
1419 PRINT "you have beaten unending adventure 1.2!"
1420 PRINT "note that there are 2 ways to beat this adventure."
1421 PRINT "now that you have found one, go ahead and find the next one."
1422 print:print
1423 PRINT "a code will now be written to CODE.TXT which will allow you to access the next unending adventure."
1424 PRINT "REMEMBER NOT TO DELETE IT, OR YOU WON'T BE ABLE TO ACCESS THE NEXT VERSION OF UNENDING ADVENTURE!"
1427 OPEN "CODE.TXT" FOR OUTPUT AS 1
1428 PRINT "copying..."
1429 PRINT#1,"ud1-2-30088-mpr1111-udg":close
1430 PRINT "done."
1431 print:print:print "again congratulations."
1432 print:print
1433 PRINT "to recei ve updates on my games, subscribe to my mailing list."
1434 PRINT "send an empty message to:"
1435 PRINT "SiteNewsUpdates-subscribe@yahoogrumps.com"
1436 CLOSE 1:print:print:print:print:END
1438 PRINT "invalid sequence"
1439 PRINT "FILE WITH CODE IN IT doesn't exist, OR IS CORRUPTED.":END
1441 PRINT "website: www.geocities.com/mun00092."
1442 print:print:END
1444 PRINT "you start talking to her, and you ask her what she's doing here."
1445 PRINT "she says that she was brought here to find this"
1446 PRINT "one person. you start walking with her and step"
1447 PRINT "through this one doorway."
1448 PRINT "then , you find yourself bleeding, on the ground."
1449 print:print:print "you then feel britney put her arms around you."
1450 PRINT "she's crying."
1451 PRINT "then you see darkness.":goto 1441
1453 PRINT "you open the door, and..."
1454 print:print:print "it attacks you and rips out your brain, as well as your heart."
1455 PRINT "your last word is":print:print "shit"
1456 goto 1441
1458 SOUND 154,9.1:SOUND 145,9.1:PRINT "you open the door and go in."
1459 PRINT "and you get attacked."
1460 print:print:print "it knocks you to the ground and"
1461 PRINT "pulls out your heart, brain, lungs, and liver."
1462 PRINT "you are now in a puddle of red.":goto 1441
1464 SOUND 154,9.1:SOUND 145,9.1:PRINT "you open the door and get attacked."
1465 print:print:print "he pulls out your heart, rips off your legs and hands"
1466 PRINT "and crushes you to the bone!!!":goto 1441
1468 SOUND 154,18.2:SOUND 0,9.1:BEEP
1469 PRINT "you open the door."
1470 PRINT "two creatures suck out your blood.":goto 1441
1471 PRINT "you continue walking, and you slip and bust your head on the"
1472 PRINT "floor!!!":goto 1441
1474 PRINT "you continue to follow the passage."
1475 PRINT "another door is spotted. go in? y or n."
1476 I$=INKEY$:if i$="" then 1476
1477 IF I$="y" then cls:goto 1480
1478 IF I$="n" then cls:goto 1483
1479 cls:goto 1475
1480 PRINT "you open the door, and"
1481 PRINT "you get attacked. it decapitates you completely.":goto 1441
1483 PRINT "you continue down the passage.":cls:goto 1481
1485 PRINT "you continue to follow the path."
1486 PRINT "and you don't make it half way cross before"
1487 PRINT "your on the ground cursing and bleeding.":goto 1441
1489 PRINT "you stand and wait."
1490 PRINT "suddenly, you notice that you"
1491 PRINT "are covered in worms!!!":print:print
1492 PRINT "they suck out all your organs.":goto 1441
1494 PRINT "you ignore the door and keep following the passage."
1495 PRINT "the passage terminates, but there is a door in front of you.":print:print
1496 PRINT "it's open so you walk in.":print:print
1497 PRINT "you hear something in the corner of the room.":print:print
1498 PRINT "then something bites you, and before you can utter help, you fall and bust"
1499 PRINT "your head on the floor."
