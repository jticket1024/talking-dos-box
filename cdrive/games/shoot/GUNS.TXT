A SHOOTING PRIMER 
 Daily practice with a given gun over a period of several weeks will make you
better than average with that particular gun. If you shoot daily or every
second day, you'll develop some degree of skill which can't be matched by a
shooter who fires only occasionally. This skill will come to you even if you
don't grasp what goes on within a firearm when you shoot it. However, if you'll
combine consistent practice with some understanding of ballistics-the science
of projectiles- you'll become truly expert 

 CALIBER 
 Caliber is the term applied to the interior diameter of a rifle or pistol
barrel or to the diameter of a bullet. In English-speaking countries this is
indicated in hundredths or thousandths of an inch (.30 caliber) or (.308
caliber). For example .50 caliber is exactly one half inch. The date of
original issue is sometimes included in caliber designation as is the case with
the .30 / 0 6 Springfield. The .30, indicates bore diameter. The 06 Is an
abbreviation of the year (1906) during which the load was issued to the U. S.
Army, and Springfield is the name of the rifle, taken from the armory where the
guns were manufactured.  Commercial developers of cartridges have given their
names to calibers so that       
thay have become household words among shooters. These are the 6 mm Remington,
.270 Winchester, .300 Savage and .375 Holland. Calibers originating in
countries where the metric system of measurement prevails are indicated in
millimeters. The 8/57 R mm, for example, has an 8-millimeter-diameter bullet.
The 57, is the length of the cartridge case or shell given in millimeters while
the R is applied to a shell whose base is rimmed. Since     1 inch equals 25.4
millimeters 8 millimeters equals about one third of an inch. 
 Foreign calibers are also associated with their originators by adding the
developer's name, as is the case with the 8 mm Mauser. 
PISTOLS
The caliber description carries into pistol naming. The .38 Special fires a
bullet .357 inches in diameter so that, the .38 is a .35. the .357 Magnum fires
a bullet .357 inches in diameter. However, a .38 Special load may be fired in a
.357 handgun since the bullets are identical in diameter, but mechanical design
and pressures make it unsafe to fire a .357 Magnum cartridge in a .38 Special
revolver. 

BALLISTICS 
The report of a gun and the smell of gunpowder have much more appeal, so why
bother with the technical aspects of shooting? Ballistics is a fascinating
field which, when learned is easy to grasp. Take the trouble to learn about
your gun while you're learning to shoot and it will pay handsome dividends. The
first step is to know the parts of a cartridge and their functions. Take a
rifle cartridge, for example, made up of a brass shell or case, a primer, a
powder charge and a bullet. Military shooters refer to a cartridge as a round.
Hunters are usually more likely to say, I've got one shot left. TV heros call a
cartridge a bullet, but they are inaccurate, since a bullet is only one part of
the cartridge. 

WHEN YOU PULL THE TRIGGER 
When a gun is cocked the firing pin is drawn back and held under spring
tension. Pulling the trigger frees the pin, which is driven into the cartridge
primer. The primer explodes, sending a tiny flame through the flash hole in the
cartridge base into the powder compartment. The powder does not explode. It
burns building up gas pressure. When this pressure reaches sufficient
intensity, the bullet is loosened from the case and is driven out through the
barrel. The walls of a case have some degree of flexibility so that, as the gas
pressure mounts, the case expands to fit snugly into the gun's firing chamber.
This prevents gas from blowing back past the case wall and injuring the
shooter. After the bullet has left the barrel and the pressure has dissipated,
the case contracts to almost its original size so that it can be extracted
easily. Gun barrels are rifled. Spiraling grooves are cut into their inner
surfaces. As the bullet is pushed toward the muzzle or open end of the barrel,
it is gripped by these grooves causing it to spin at great speed. This gives
the bullet lateral stability so that it doesn't tumble, or turn end-over-end. A
bullet's accuracy stems from the stability in flight created by this spinning.
The longer a bullet in relation to its diameter, the faster it must spin for
ultimate stability. The rate of spin is governed by the degree of pitch of the
grooves. Some rifles are bored with a greater number of grooves than others.
Shotgun barrels are not rifled, since they are designed to shoot a number of
small pellets which would be deformed by rifling and then tend to scatter
erratically. However, because many deer hunters prefer to fire a solid slug in
shotguns, these slugs are manufactured with spiraling grooves along their
sides. Slugs have a hollow base so that the nose is heavier than the tail
giving them the necessary lateral stability for their surprising accuracy. 

RECOIL
When the bullet and the gas pushing it leave the barrel, a strong backward
thrust is set up so that the gun is shoved sharply against the shooter's
shoulder, or hand. Since this thrust is directed against the shooter and is
somewhat absorbed, it causes the muzzle to jump upward. This is known as recoil
or kick. Since the full effect of this recoil is not apparent until after the
bullet has left the barrel, it has little or no effect on the bullet's path of
flight. You can't blame a miss on recoil, unless the kick you expect causes you
to flinch. 

 THE BULLET IN FLIGHT 
 Once the bullet has left the barrel, you're dealing with exterior ballistics.
So far as shooting skill is concerned, this is a more important phase of
ballistics because the bullet's behavior in flight will make the difference
between a hit and a miss. A bullet, even though it flies so fast you can't see
it, is affected by the pull of gravity. In order to offset this pull of gravity
the barrel of a gun must be elevated at the muzzle if you're going to hit a
distant target. The muzzle will then actually point above the target so that
the bullet's path of flight becomes an arc from the muzzle to the point of
impact. This is called trajectory. You look through the gun's sights in a
direct line to the target, known as the line of sight. Most guns have rear
sights which can be adjusted. Raising the rear sight has the effect of lowering
the rear end of the barrel below the muzzle.               
 When you fire, the bullet will continue upward until gravity overcomes its
momentum, and starts to descend, still traveling in an arc, until the bullet
meets exactly on target. The longer the range or distance to the target, the
more allowance you'll have to make. This rear-sight level, is termed elevation.
Gravity, however, isn't the only natural force affecting the flight of a
bullet. 

WINDAGE
Wind, even a slight breeze, will drive a bullet off its aimed path, and the
longer the range, the farther off its course the bullet will deviate. For this
reason, better sights are adjustable laterally to compensate for this wind
drift of a bullet. Shooters refer to this adjustment as windage. For near
shooting or for woods hunting this is rarely necessary but for competitive
target shooting and long-range hunting shots, such windage adjustments are a
must. A 300-yard shot, for example, may well require a lateral compensation of
as much as 12 to 14 inches. 

SPOTTING
Learn to think of your target as the face of a clock with the bulls eye located
at the center and the target's outer rim as the circular row of numbers. Since
you can't see the bullet holes in the target when shooting at long ranges,
someone should spot your shots through a spotting scope. If your spotter were
to tell you that your shot was high and left you would have only an obscure
idea of the location of your hit. On the other hand, if the spotter tells you
that your shot struck at ten o'clock in the seven ring, you can see in your
mind's eye exactly where the bullet struck in relation to the bulls eye. When
the wind is blowing from your right as you face your target, it is blowing from
three o'clock and your bullet will tend to drift to the left or toward nine
o'clock. To offset this, move the rear sight to the right, which in effect
moves the rear of the gun barrel to the left of your line of sight. The muzzle
will now point slightly up winded or toward three o'clock. The bullet will
leave the barrel headed to the right of the target but the pressure of the wind
will carry it back toward the target's center. As in the case of elevation, the
bullet's path and the line of sight will coincide on the bulls eye. Bullet
velocity, or rate of speed, has a sharp bearing upon the degree of elevation
and windage adjustment required to send a bullet accurately to its target. The
faster a bullet travels, the farther it will get before gravity or wind
pressure force it off its intended course, and this is why high-velocity
ammunition is known to have a flat trajectory. Gravity pulls just as hard at a
high-velocity bullet as it does at a slower velocity but it has less time in
which to pull the bullet downward from its path. Hence, with high velocity
ammunition, fewer and smaller elevation and windage adjustments are required. 

FEET PER SECOND
Feet per second is a term you will encounter in dealing with ammunition. This
is the number of feet a bullet will travel in one second. A bullet said to have
a velocity of 2700 feet per second will travel that distance in one second. As
a bullet travels toward its target it loses momentum continually until it finds
its target or falls to the ground. Therefore, we have the term muzzle velocity,
the speed at which a bullet leaves the gun, as compared to the progressive loss
of velocity at 100, 200 and 300 yards. Ballistics charts indicate trajectory by
the number of inches below or above the line of sight at regular intervals,
usually 100, 200 and 300 yards. The velocity of a bullet is governed by the
type and weight of the powder load, weight and type or shape of the bullet, and
by barrel length. Too little powder in a cartridge will decrease velocity.
Certain loads such as used in target shooting, are loaded lightly for lower
velocities, less recoil and greater accuracy. An overloaded cartridge not only
results in possibly dangerous pressures but may cause erratic bullet flight. 

BULLET WEIGHT
The weight and shape of bullets will determine their susceptibility to wind and
air resistance affecting velocity. The weight of a bullet is denoted in grains,
ranging from as little as 15 grains in the shooting gallery .22 Short rim-fire
to 500 grains or more in elephant-gun ammunition. There are 480 grains to one
ounce. 

 THE BULLET'S IMPACT 
 The third phase, terminal ballistics, deals with the impact effects of a
bullet striking its target, usually of greater concern to the hunter than to
the target shooter or plinker. Terminal ballistics are more commonly associated
with military projectiles, but also concern the hunter who wants to kill as
quickly and as humanely as possible with a minimum destruction of edible meat.
The energy of a bullet is measured in foot pounds. As in the case of velocity,
energy diminishes as the bullet travels from the muzzle. Energy upon impact is
the result of a combination of bullet weight, type, shape and velocity.
Bullets, made for killing game, are designed to expand upon impact and have
hollow points or a soft nose with a partial jacket of harder metal so that they
mushroom upon striking the target, frequently more than doubling their original
diameter. The military bullet is completely jacketed and designed to kill or
wound without unnecessary damage to the target and expand very little upon
striking flesh. The development of Magnum or heavy high-speed loads has
introduced a third group into the argument- those who shoot moderate to heavy
bullets at high velocities. The bullet most likely to succeed in hunting is the
one which develops enough energy upon impact to down the quarry being hunted. 
     
There still exist misconceptions about the .22. Used carelessly, the little
bullet is highly dangerous. Few realize its great range.  For example,
Hold a.22 rifle at about 30 degrees from the horizontal and 
a bullet will travel over a mile! Plinking is the favorite sport with .22
shooters. This is simply wooden blocks, mothballs, balloons tin cans, or other
inexpensive or valueless targets.  Bottles should not
be used as targets.  Their hard surfaces may cause ricochets and they leave
dangerous debris.  
Single-shot bolt-action rifles are excellent first guns. 
These are not precision guns but they are reliable, safe and sturdy firearms.
Safety features of the single-shot are obvious in that some must be cocked
manually so that a shooter is made much more conscious that the gun is loaded
and ready to fire, a definite asset during training.  
A lever-action rifle is among the fastest, out-gunned only by the pump or slide
action. Most of the new lever-action rifles are of the
short throw type. The lever can be worked without removing the
right hand's grip from the gun.  Flipping the lever downward and back up again
to eject and reload is a natural motion for either right- or left-handed
shooters and doing this creates a minimum disturbance of the sight alignment.  
PUMP, SLIDE OR TROMBONE ACTION RIFLES
The names derive from the sliding forearm which, when pulled
back, ejects the fired shell and cocks the rifle.  Pushing the forearm forward,
back into firing position, feeds another shell into the chamber.  
It's a toss-up as to whether the slide action or the "short throw" lever action
is the fastest of the manually operated rifles.  Like flipping the
lever action, sliding the forearm back and forth is a natural motion for both
right- and left-handed shooters and can be effected with speed
that would lead an observer to think that the shooter is firing an automatic. 
It's possible to keep the sights on target while working the action, thereby
increasing the gun's speed of effective fire.

AUTOMATIC RIFLES
This is not a true automatic rifle, but rather a self-loading or semi-automatic
gun which fires one shot with each pull of the trigger, with the action
ejecting the empty shell and loading a fresh one into the firing chamber and,
at the same time, cocking the rifle-all without any effort on the part of the
shooter except pulling the trigger. 
A fully automatic gun is a military or police machine gun that continues to
fire as long as the trigger is held back and the magazine contains ammunition.

SKEET SHOOTING 
 In the early 1920s William H. Foster, assistant editor of National Sportsman
magazine, designed a new shooting game in Andover, Massachusetts called
shooting around the clock. The two men who had worked out the rules for the
game with Foster were C. E. Davies, and his son Henry. These three shooters
hunted birds during the open season, and practiced on clay targets the rest of
the year. A clay bird is about five inches across and about an inch thick. They
decided to draw a circle and shoot from various stations around it. The circle
had a twenty-five-yard diameter and took on the form of a large clock. They
marked shooting stations at each of twelve points on the edge of the circle.
Next they installed a trap to throw the clay targets from the twelve-o'clock
position so they would rise out over the circle and cross the six-o'clock
point. By shooting two shells from each station they got a whole series of
angles and a wide variety of practice shots. The game also guaranteed each
shooter an equal chance in competition. The box of twenty-five shells had one
left over, so they decided to shoot the extra shell from the center of the
circle. This proved to be such a challenge that it remained in the game,
Eventually this extra shot was to be the "optional" shot, and the shooter takes
it at his first miss in a round. The boy who pulled the trap release used a
long cord so as not to be hit by the shot. The farmer next door appealed to the
shotgunners to adjust their procedure. In the interest of safety and
neighborliness, Foster and the two Davies solution was simple: cut the circle
in half. This meant that their guns would be pointing away from the farmer's
hens. To provide themselves the same variety of shooting angles, they installed
a second trap across the half-circle from the first one which became the high
house. This provided birds following a flatter path instead of having all the
shots at rising targets. In February 1926, Foster ran a contest to find a
better name for the game. The prize winner of one hundred dollars was Mrs.
Gertrude Hurlbutt of Dayton, Montana, who suggested "skeet," from the
Scandinavian root of "shoot." A round of skeet still consists of twenty-five
shots. Two targets are shot from each of the eight stations. In singles, one
bird is sent, and in Doubles two birds are launched at the same time. One
target comes from the low house at an elevation of three and one half feet, and
the other from the high house ten feet above ground level. Doubles are shot
from stations 1, 2, 6, and 7. The twenty-fifth or "optional" shot provides
another chance at the first target missed. When breaking all twenty-four
targets the shooter can call for the optional shot from any position. 

SKEET GUNS 
The shooter is free to use any shotgun from a 12-, 20-, or 28-gauge, or the
.410. Skeet guns must be able to fire two shots relatively fast, to take care
of those doubles. There is no time for loading a fresh shell into a single-shot
to get this second bird. You can use double-barreled guns, side-by-side, or
over-and-under as well as pump guns and autoloaders. Autoloaders are popular
because the shooter can fire as fast as possible by releasing and pulling the
trigger. Skeet shells are designed to give a thirty-inch pattern at twenty-five
yards, the maximum distance most birds are hit. The shooter calls "pull," and
according to the rules the bird must come from the trap house within one second
after the call. It is launched at about 40 miles an hour, with The wind varying
the height of the bird. The clay birds follow the same prescribed paths from
each trap house, and their paths cross at a point eighteen yards in front of
station 8, which is in the middle of the shooting range. 
