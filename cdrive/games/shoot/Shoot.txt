SHOOTING RANGE
PRODUCED BY
PCS GAMES
WRITTEN BY
CARL MICKLA
SOUNDS BY
PHIL VLASAK
VERSION 2.0
COPYRIGHT 1997

The tension mounts. You need to make a hit on this last shot to
break the range record. Hear the gun go off and the clay bird break
when your shot smashes into it. Sometimes you might even get a
whiff of cordite come wafting out of your floppy drive. 
Shooting Range may be played by one to four players.

INTRODUCTION:
This shooting game was written for the blind community.  The game
follows the format of real shooting ranges.
It was written with speech in mind to enable blind people to know
exactly what is going on in the game at all times.  This game also
has multi media sounds.  If you have a sound card in your computer,
you will now be able to enjoy the game even more.  If you do not
have a sound card, the game is designed to play the sounds through
the P C speaker.  So give this game a shot and see how you like it!

            Getting Started:
This game was written for the DOS included with Windows 95 and 98.
This is a DOS game and works best if you are in DOS using a DOS screen reader and hardware synthesizer.
To install from Windows:
1. From the CD which contains the game hit enter on 
INSTALL Shooting range.bat
This will install your game in the "C:\Shooting range" folder
2. Exit to the MS-DOS prompt
3. type:
CD "c:\Shooting range" 
Then hit the enter key.
4. Type shoot 
Then hit the enter key.
This will start the game.

To install from DOS:
1. From the CD which contains the game hit enter on 
INSTALL Shooting range.bat 
This will install your game in the "C:\Shooting range" folder
2. type: CD "c:\Shooting range" 
then hit the enter key.
3. Type shoot
then hit the enter key.
This will start the game.


The game will begin with the title screen.
The first thing the computer will display at the bottom of the
title screen is what mode the sound driver is in.  
Then the computer will ask if you would like to change the sound
driver setting.  

See the sound setting section later.
We have tried to make this version as easy to use as possible with
the menus working in the same way.
Up and down arrow keys moves you through the choices of a menu.
Hitting the enter key selects a choice.
Where possible, hitting the escape key gets you out of a menu.
In all other cases use the appropriate responses.
You can also hit f1 for a review of the keys and what they do.


Then the game will ask for the number of players.  The game can be
played by one to four people.  If only one person is playing you
can still play against the record set in the ranges that save the
leading score.
Then you will be prompted for each players name.  The name must
contain only one word.  If you wish to type more than one word, you
must connect the words with a dash, or underline punctuation mark
as in Mary-Ann. 

PICK THE RANGE:
You can shoot guns in four different Shooting Ranges.
In the skeet range you will be shooting at clay birds.
In the rifle range you will be shooting at a card board target.
And will be trying to hit the bullseye.
In the pistol range you will be shooting at a steel human
silhouette target
In the junk yard you will be shooting at several objects using a
rifle, a shotgun, or an automatic weapon.
you next pick your gun.
when a letter is used before a gun menu choice, you may type it.
or you can go through the gun menu to try each one out.
Use the up and down arrow keys to review the list, and hit enter to
hear it.
Next you pick the number of targets you would like to shoot at,
or how many rounds you would like to have at the target.
Finally you get a recap of your choices before you go on.

ELECTRONIC AIMING:
The game calculates the point where you should hit the target and
converts that point to a musical tone which is called the Target
tone.
The game then plays that target tone within a range of lower
frequency tones so
you can later identify it.
The game then plays the series of low frequency tones, with the
target tone within it.  You can shoot the gun at any time by
hitting the enter key or space bar. In order to hit the target you
must anticipate when the target tone will
play and shoot your gun before you hear the sound.

DIFFERENT TONE PATTERNS
Each of the four Shooting Ranges have a different pattern of tones.
In the skeet range you will be shooting at flying clay birds.
The skeet target is one tone set in a varying range of lower tones.
In the rifle range you will be shooting at a card board target.
And will be trying to hit the bullseye.
The bullseye target is one tone set in a varying range of lower
tones.
In the pistol range you will be shooting at a steel human
silhouette target using a quick draw western style.
The eleven target tones start at the foot to the head set in a
varying range of lower tones.
In the junk yard you will be shooting at several objects.
The target tones vary in number from one to five, to represent the
size of the object set in a varying range of lower tones.
Note in the skeet range the full span of tones take between two
tenths of a second to one and one half seconds to play, while in
the pistol range, the full span of tones take between two and three
seconds to play.

DIFFERENT GUNS
You can choose among several types of guns, and within a type of
gun.
The pistol or hand gun is only used in the pistol range.  It shoots
one bullet at a time.
the rifle is used in all of the other three ranges.
It shoots one bullet at a time.
The shot gun is used in the skeet range and junk yard.
It shoots one shell at a time but the shell contains many shots or
steel pellets that spread out to make it easier to hit a target.
The automatic weapon is used in the skeet range and junk yard.
It shoots several bullets at a time that form a pattern in which
the target is easier to hit but it could  pass between the bullets.

SCORING 
In the skeet range, you can choose a practice game or a tournament
game.  In the practice game, you will hear the target tone pattern
once for aiming, then get three chances to hit the target.
In the tournament game, you will hear the target tone pattern once
for aiming, then get one chance to hit the target.
You get one point for each clay bird hit.
In the rifle range you will hear the target tone pattern once for
aiming, then get three chances to hit the bulls eye in the target.
The scoring for the rifle range is:
bulls eye 10 points. 
1 ring left of the bulls eye = 9 points.
1 ring right of the bulls eye = 8 points.
2 rings left of the bulls eye = 7 points.
2 rings right of the bulls eye = 6 points.
 3 rings left of the bulls eye = 5 points.
3 rings right of the bulls eye = 4 points.
4 rings left of the bulls eye = 3 points.
4 rings right of the bulls eye = 2 points.
5 rings left of the bulls eye = 1 point.
5 rings right of the bulls eye = 1 point.
6 rings and more left or right of the bulls eye = 0 points.

In the pistol range, you will hear the target tone pattern once for
aiming, then get three chances to hit the figure.
There is no scoring in this range but you do get a recap of which
parts you hit as in the list below:
CARL GOT THESE TOTALS IN THE PISTOL RANGE.
0 HEAD.
0 NECK.
0 SHOULDER.
0 ARM.
0 CHEST.
1 HEART.
5 STOMACH.
2 CROTCH.
0 LEG.
0 KNEE.
0 FOOT.

In the junk yard, you will hear the target tone pattern once for
aiming, then get three chances to hit the object.
There is no scoring in this game other than total objects hit.


After all the players have had a turn, you will get a list of the
players in the game and their scores, with the leader first.
You can then hit the escape key if you want to Quit and end the
game early.

RANGE LEADERS:
The game remembers the highest score from the Skeet and Rifle
ranges and the name of the person that set that record.  If you get
a higher score, your name will be recorded at that level.
You can review the list by hitting f5.
Below is an example of a leaders list:
THE RANGE LEADERS.
10 ROUND RIFLE RANGE CARL WITH 80 POINTS.
25 ROUND RIFLE RANGE PHIL WITH 200 POINTS.
50 ROUND RIFLE RANGE JOAN WITH 400 POINTS.
100 ROUND RIFLE RANGE MARY-ANN WITH 900 POINTS.
TOTAL BULLS EYES CARL WITH 16 BULLS EYES.
10 TARGET SKEET RANGE PHIL WITH 10 BIRDS HIT.
25 TARGET SKEET RANGE MARY-ANN WITH 20 BIRDS HIT.
50 TARGET SKEET RANGE CARL WITH 40 BIRDS HIT.
100 TARGET SKEET RANGE JOAN WITH 90 BIRDS HIT.

FACTS ABOUT GUNS:
You can read about guns, how they work, and the different types,
including a section on the game of skeet in the file GUNS.TXT.
You can do this by typing GUNS from the directory where the game is
located.

HELP:
Hit f1 to get the help screen.
You will get the following information.
F2 = CHANGE SOUND SETTING.
F5 = LEADERS LIST.
ESCAPE = QUIT THE GAME.

Sound Settings:
You can pick or change your sound setting when starting the
game.  The Up and down arrow keys move you through the choices.
Hitting the enter key selects a choice.
The choices are:
DEFAULT SOUND DRIVER ON
WINDOWS SOUND DRIVER
PC SOUND ON
SOUND CARD ON
SOUND OFF

The default sound setting is best. If the default setting does not
work and you have a SOUND card try the sound card setting. 
If you have Windows 95 or 98 and a sound card the Windows Driver
will work but it may be slower than the dos sound drivers.
If you do not have a sound card try the PC setting and if you do
not want any sound choose no
sound.  Many of today's computers come with a sound card that plays
the sound in this game the way they were designed to be heard. If
you don't have a sound card, you can still hear the sounds through
the small speaker built into every computer by choosing the PC
setting. 
If you change your sound setting you will get the following:
SOUND SETTING.
USE THE UP AND DOWN ARROW KEYS OR HIT THE ENTER KEY TO ACCEPT THIS
CHOICE.
DEFAULT SOUND DRIVER ON

If you use the arrow keys and pick a different driver by hitting
the enter key, the game will try out the new driver with a test
sound of a man saying 'Take Off Ah!'.
IF YOU DID NOT HEAR ANY THING TRY CHANGING THE SOUND SETTING.
IS YOUR SOUND SETTING CORRECT?
 YES 
The game will allow you to accept the new sound driver or switch it
to another setting.  
A musical theme song will begin to play after leaving the sound
choice.

Help:
If you have problems playing this game or getting sounds to work,
please read the Help.txt file.
You can do this by typing:
Help and hitting the enter key from the DOS prompt or by loading it
into a word processor.

OTHER PCS GAMES:
At the end of this game you will get a menu of our other games.
You can use the up and down arrow keys to move through the titles.
Hit the enter key for more information about a game.
hit f 1 for help.
The escape key quits the menu, and returns you to the dos prompt.
F3 key gets you the p c s technical support number and address.

Game demos from PCS Games on J.J. Meddaugh's site!
Go to:
www.blindcommunity.com 
You will get a page with the following link:
link J-Squared File Center
Visit our download area to get issues of the Audyssey game magazine
and other great files.

Then hit enter on:
link pcs Directory
P C S Game Demos; Try before you buy!
Then arrow down to a specific game
and hit enter.
You will get more information about the game and be able to download a zipped demo version.
In order to play these games, you will need an unzipping program, either PK UNZIP or Win Zip.
You can find a link to PK Unzip on this same site.

PCS Games is always looking for new ideas
for programs, and if you make a suggestion we will give it some
consideration. If we make a program which you suggested you will
receive the first copy of the program free.
The market base for programs to supply the visually impaired and
blind community is small. If programmers are going to write fun and
educational programs then your support is very important.  Giving
or receiving unpaid for programs and using them only makes it very
hard for people to produce products for this small base of people.
For example, a very good leisure game has been on the market for
several years, and in all that time only 250 to 300 games have been
sold. The game is very good and there are many unpaid for games out
there. Programmers are not looking for any hand outs. Just pay them
for what is do them. Thank you for your time and support and if you
would like to give us some feedback on this or any other topic feil
free to contact us.
Carl Mickla and Phil Vlasak

PCS Games
666 Orchard Street
Temperance, MI 48182
Phone: (734) 850-9502
email: phil@pcsgames.net
Web site:
www.pcsgames.net

